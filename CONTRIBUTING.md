Contributing
============

## Deploying manually a Docker image

```bash
$ docker login gitlab.libriciel.fr:4567
$ docker build --pull -t gitlab.libriciel.fr:4567/libriciel/pole-signature/signature/libersign:latest .
$ docker push gitlab.libriciel.fr:4567/libriciel/pole-signature/signature/libersign:latest
```

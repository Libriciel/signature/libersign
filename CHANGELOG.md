# Change Log

Tous les changements notables sur ce projet seront documentés dans ce fichier.

Ce format est basé sur [Keep a Changelog](http://keepachangelog.com/)
et ce projet adhère au [Semantic Versioning](http://semver.org/).

## [1.11.2] - 2021-02-22

### Modifications
- Déclassement de Certinomis G2 easy (non RGS), suppression de G2 standard.
- Ajout Certinomis G2 prime pour fonctionnement 'legacy' (applet IE11, et vieux modes de signature)

## [1.11.1] - 2021-02-12

### Modifications
- Ajout des autorités Certinomis G2: easy, prime, standard.

## [1.11.0] - 2020-11-02

### Modifications

- Changement de méthode de récupération de certificats :
filtrage des certificats révoqués ou non-affiliés à une autorité de certification connue.

## [1.9.13] - 2020-05-27

### Modifications
- Ajout de l'autorité Libriciel personnel g2 (encore)

## [1.9.12] - 2020-05-03

### Modifications
- Ajout de l'autorité de certification Belgium eID (Belgium root CA, Citizen CA)

## [1.9.11] - 2020-03-19

### Modifications
- Ajout de l'autorité de certification Libriciel Personnel G2

## [1.9.10] - 2020-02-06

### Modifications
- Ajout des AC ANTS Porteurs AAE et ACT 3etoiles

### Corrections
- Problème de multiple signature au format xades détaché

## [1.9.9] - 2020-01-20

### Modifications
- Ajout 3SKey CA et SWIFT Root CA

## [1.9.8] - 2020-01-03

### Modifications
- Mise à jour de l'autorité de certification DGFiP

## [1.9.7] - 2019-11-25

### Modifications
- Ajout de l'autorité de certification intermédiaire ChamberSign France CA3 NG RGS
- Ajout de l'autorité de certification intermédiaire ChamberSign France CA3 NG EID

## [1.9.6] - 2019-10-24

### Modifications
- Ajout de l'autorité de certification racine ChamberSign France CA3
- Ajout de l'autorité de certification intermédiaire ChamberSign France CA3 RGS
- Ajout de l'autorité de certification intermédiaire ChamberSign France CA3 EID

## [1.9.5] - 2019-06-18

### Modifications
- Mise à jour de l'autorité de certification Keynectis ICS Qualified CA

## [1.9.4] - 2019-05-27

### Modifications
- Mise à jour de l'autorité de certification Keynectis ICS Advanced Class 3
- Ajout de l'autorité de certification intermédiaire OpenTrust CA for AATL G1
- Ajout de l'autorité de certification racine OpenTrust Root CA G1

## [1.9.3] - 2019-04-15

### Corrections
- La canonicalisation des flux bancaires EBICS n'était pas correcte

## [1.9.2] - 2019-01-29

### Modifications
- Ajout de l'autorité de certification Libriciel G1

## [1.9.1] - 2018-12-10

### Modifications
- Ajout de l'autorité de certification ANTS
- Ajout de l'autorité de certification Autorité de Certification Personnes AAE de l'ANTS

## [1.9.0] - 2018-09-14

### Modifications
- Changement de méthode de signature des flux EBICS-TS

## [1.8.2] - 2018-08-08

### Modifications
- Ajout de l'autorité de certification CertEurope eID

## [1.8.1] - 2018-07-09

### Corrections
- La signature XAdES détachée fonctionne à nouveau

## [1.8.0] - 2018-05-31

### Modifications
- Ajout d'un nouveau format de signature (PESv2 SHA256)

## [1.7.2] - 2018-04-25

### Corrections
- Blocage en cas de presence de certificat issu d'une AC qui n'a pas de champ 'CN'

## [1.7.1] - 2018-02-23

### Modifications
- Ajout de l'autorité de certification Imprimerie Nationale Substantiel Personnel

## [1.7.0] - 2018-02-15

### Modifications
- Nouveau certificat de signature d'applet pour exécution dans IE.

## [1.6.13] - 2017-01-24

### Modifications
- Ajout d'AC manquante Click & Trust EU Sign

--------
Ancien CHANGELOG

v 1.6.12
    - Ajout d'AC manquante de l'Imprimerie Nationale

v 1.6.11
    - Certains messages d'alertes étaient en arrière plan

v 1.6.10
    - Augmentation du timer pour clés Oberthur

v 1.6.9
    - Problème de signature lors de CRL injoignable
    - Message d'erreur non valide lors d'une vérification de CRL

v 1.6.8
    - Ajout d'AC Swift (bancaire)

v 1.6.7
    - Déploiement amélioré
    - Quelques AC `RGS**` manquantes

v 1.6.6
    - Corrige le fonctionnement en mode applet avec les cartes à puce Oberthur !2

v 1.6.5
    - Corrige le fonctionnement avec Java 8u101 !1

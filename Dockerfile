#
# LiberSign
# Copyright (C) 2008-2022 Libriciel SCOP
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#


# Build
FROM ubuntu:latest AS builder

RUN apt-get update && apt-get install -y wget
RUN mkdir -p /var/www/parapheur/libersign
COPY make.sh .
RUN ./make.sh PROD


# Serve
FROM nginx:1.17.8-alpine

## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /var/www/parapheur/libersign/ /usr/share/nginx/html/libersign/
ADD nginx.conf /etc/nginx/
CMD ["nginx", "-g", "daemon off;"]

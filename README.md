# LiberSign

Executable java (compilé en JAR), permettant la signature électronique de documents.

Peut être lancé en **applet** ou directement via **'java -jar'** (pour une utilisation via extension navigateur, voir projets relatifs).

## Compilation

Gitlab se charge du build pour nous.
Le package généré ne contient que le jar **'SplittedSignatureApplet.jar'**

Pour plus d'informations, voir le fichier `.gitlab-ci.yml`.

## Déploiement

Lors d'une compilation réussie, le jar est déposé sur le serveur http://libriciel-test.libriciel.fr/.

Lors de la création d'un tag, le jar est déposé sur le serveur http://libersign.libriciel.fr/.

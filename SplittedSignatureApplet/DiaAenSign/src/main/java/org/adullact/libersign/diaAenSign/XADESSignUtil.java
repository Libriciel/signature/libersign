/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.libersign.diaAenSign;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.security.auth.x500.X500Principal;
import javax.xml.crypto.dom.DOMStructure;
import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLObject;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;
//import org.apache.xml.security.utils.IdResolver;

/**
 * Production de signature format XAdES.
 *
 * @author Augustin Sarr (Netheos), Stephane Vast (Adullact Projet)
 */
public class XADESSignUtil {

    private static String pesID;
    private static String policyID;
    private static String policyDescryption;
    private static String policyDigestValue;
    
    // mise en static, pourquoi pas?
    private static String SPURI;
    private static String city;
    private static String postalCode;
    private static String countryName;
    private static String claimedRole;
    private boolean idPresence;
    /**
     * Namespace for XAdES 1.1.1 scheme
     */
    public static final String xadesNS = "http://uri.etsi.org/01903/v1.1.1#";
    /**
     * Namespace for XAdES 1.2.2 scheme (required for DIA signing schema)
     */
    public static final String xadesNS122 = "http://uri.etsi.org/01903/v1.2.2#";
    /**
     * Namespace for XAdES 1.3.2 scheme (required for ANTS-ECD signing schema)
     */
    public static final String xadesNS132 = "http://uri.etsi.org/01903/v1.3.2#";
    /**
     * Namespace for DIA scoped XML documents (pretty specific)
     */
    public static final String diaNS = "http://xmlschema.ok-demat.com/DIA";

    /**
     * Default constructor
     */
    public XADESSignUtil() {
        pesID = null;
        idPresence = false;
        city = null;
        policyID = null;
        policyDescryption = null;
        policyDigestValue = null;
        claimedRole = null;
        SPURI = null;
    }

    public XADESSignUtil(String apes_id, String apolicyId, String apolicyDescription, String apolicyDigestValue, String aspuri,
            String acity, String apostalCode, String acountryName, String aclaimedRole) {
        if (apes_id.equalsIgnoreCase("null")) {
            idPresence = false;
            pesID = "";
        } else {
            idPresence = true;
            pesID = apes_id;
        }
        policyID = apolicyId;
        policyDescryption = apolicyDescription;
        policyDigestValue = apolicyDigestValue;
        SPURI = aspuri;
        city = acity;
        postalCode = apostalCode;
        countryName = acountryName;
        claimedRole = aclaimedRole;
    }

    public String getSignatureID() {
        /* TODO : préciser la façon dont est calculé l'identifiant de la signature */
        return pesID + "_SIG_1"; //"IDC" + dateFormat.format(date); //ID signature+date
    }

    public static String getPolicyIdentifierID() {
        if (policyID != null) {
            return policyID;
        }
        return "oid:portnawak";
    }

    public static String getPolicyIdentifierDescription() {
        if (policyDescryption != null) {
            return policyDescryption;
        }
        return "Politique de Signature portnawak";
    }

    public static String getPolicyDigest() {
        if (policyDigestValue != null) {
            return policyDigestValue;
        }
        return "portnawak/q5M/dx3/162m+j6MFe8LzEAFDJk=";
    }

    public static String getSPURI() {
        if (SPURI != null) {
            return SPURI;
        }
        return "http://www.portnawak.fr/";
    }

    public static String getCity() {
        if (city != null) {
            return city;
        }
        return "portnawak city";
    }

    public static String getPostalCode() {
        if (postalCode != null) {
            return postalCode;
        }
        return "34000";
    }

    public static String getCountryName() {
        if (countryName != null) {
            return countryName;
        }
        return "France";
    }

    public static String getClaimedRole() {
        if (claimedRole != null) {
            return claimedRole;
        }
        return "ROLE de portnawak";
    }

    public static String getDocumentID() {
        return pesID;
    }

    /**
     * Signature XADES detachee (EPES??)
     *  Format de signature XAdES de l’administration électronique
     * http://www.synergies-publiques.fr/IMG/pdf/Format_de_signature_Xades_V1.0.pdf
     */
    public static byte[] sign(X509Certificate certificate, PrivateKey privateKey, byte[] digest) throws XMLSignatureException {

        try {
            javax.xml.crypto.dsig.XMLSignatureFactory signatureFactory = javax.xml.crypto.dsig.XMLSignatureFactory.getInstance("DOM");
            //            java.security.Provider[] providers = java.security.Security.getProviders();
            //            int nombre = providers.length;
            //            for(int i=0; i<nombre; i++) {
            //                System.out.println("\tSec-provider: " + providers[i].getName());
            //            }

            //references
            java.util.List<javax.xml.crypto.dsig.Reference> referencesList = new java.util.ArrayList<javax.xml.crypto.dsig.Reference>();
            javax.xml.crypto.dsig.Reference contentReference = signatureFactory.newReference(
                    (java.lang.String) null, signatureFactory.newDigestMethod(javax.xml.crypto.dsig.DigestMethod.SHA1, null),
                    (java.util.List) null, (java.lang.String) null, (java.lang.String) null, digest);
            javax.xml.crypto.dsig.Reference keyInfoReference = signatureFactory.newReference("#keyInfoID", signatureFactory.newDigestMethod(javax.xml.crypto.dsig.DigestMethod.SHA1, null));
            javax.xml.crypto.dsig.Reference dateReference = signatureFactory.newReference("#signedPropertiesID", signatureFactory.newDigestMethod(javax.xml.crypto.dsig.DigestMethod.SHA1, null));
            referencesList.add(contentReference);
            referencesList.add(keyInfoReference);
            referencesList.add(dateReference);
            javax.xml.crypto.dsig.SignedInfo signedInfo = signatureFactory.newSignedInfo
                    (signatureFactory.newCanonicalizationMethod(
                    javax.xml.crypto.dsig.CanonicalizationMethod.INCLUSIVE, (javax.xml.crypto.dsig.spec.C14NMethodParameterSpec) null),
                    signatureFactory.newSignatureMethod(SignatureMethod.RSA_SHA1, null),
                    java.util.Collections.unmodifiableList(referencesList));
//            String myCanAlgoString = "http://www.w3.org/TR/2001/REC-xml-c14n-20010315"; //javax.xml.crypto.dsig.CanonicalizationMethod.INCLUSIVE; 
//            javax.xml.crypto.dsig.CanonicalizationMethod cm = signatureFactory.newCanonicalizationMethod(
//                    myCanAlgoString,
//                    (javax.xml.crypto.dsig.spec.C14NMethodParameterSpec) null);
//            javax.xml.crypto.dsig.SignedInfo signedInfo = signatureFactory.newSignedInfo(
//                    cm,
//                    signatureFactory.newSignatureMethod(SignatureMethod.RSA_SHA1, null), // "http://www.w3.org/2000/09/xmldsig#rsa-sha1"
//                    java.util.Collections.unmodifiableList(referencesList));

            // key info
            javax.xml.crypto.dsig.keyinfo.KeyInfoFactory keyInfoFactory = signatureFactory.getKeyInfoFactory();
            javax.xml.crypto.dsig.keyinfo.X509Data x509Data = keyInfoFactory.newX509Data(java.util.Collections.singletonList(certificate));
            javax.xml.crypto.dsig.keyinfo.KeyInfo keyInfo = keyInfoFactory.newKeyInfo(java.util.Collections.singletonList(x509Data), "keyInfoID");

            javax.xml.parsers.DocumentBuilderFactory documentBuilderFactory = javax.xml.parsers.DocumentBuilderFactory.newInstance();
            documentBuilderFactory.setNamespaceAware(true);
            org.w3c.dom.Document signatureDocument = documentBuilderFactory.newDocumentBuilder().newDocument();
            signatureDocument.appendChild(signatureDocument.createElement("DocumentDetachedExternalSignature"));
            org.w3c.dom.Document document = documentBuilderFactory.newDocumentBuilder().newDocument();

            org.w3c.dom.Element qualifyingPropertiesElement = document.createElement("QualifyingProperties");
            org.w3c.dom.Element signedPropertiesElement = document.createElement("SignedProperties");
            signedPropertiesElement.setAttribute("id", "signedPropertiesID");
            org.w3c.dom.Element signedSignaturePropertiesElement = document.createElement("SignedSignatureProperties");
            org.w3c.dom.Element signingTimeElement = document.createElement("SigningTime");
            // http://www.w3.org/TR/XAdES/#Syntax_for_XAdES_The_SigningTime_element
            java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss");
            java.util.Date date = new java.util.Date();
            signingTimeElement.appendChild(document.createTextNode(dateFormat.format(date)));
            signedSignaturePropertiesElement.appendChild(signingTimeElement);
            signedPropertiesElement.appendChild(signedSignaturePropertiesElement);
            qualifyingPropertiesElement.appendChild(signedPropertiesElement);
            qualifyingPropertiesElement.setAttribute("Target", "signatureID");

            javax.xml.crypto.dsig.XMLObject xmlObject = signatureFactory.newXMLObject(java.util.Collections.singletonList(new javax.xml.crypto.dom.DOMStructure(qualifyingPropertiesElement)), "objectID", null, null);
            //
            javax.xml.crypto.dsig.dom.DOMSignContext signContext = new javax.xml.crypto.dsig.dom.DOMSignContext(privateKey, signatureDocument.getDocumentElement());
            javax.xml.crypto.dsig.XMLSignature xmlSignature = signatureFactory.newXMLSignature(signedInfo, keyInfo, java.util.Collections.singletonList(xmlObject), "signedInfoID", "signatureID");
            xmlSignature.sign(signContext);
            //
            javax.xml.transform.TransformerFactory transformerFactory = javax.xml.transform.TransformerFactory.newInstance();
            javax.xml.transform.Transformer transformer = transformerFactory.newTransformer();
            java.io.ByteArrayOutputStream byteArrayOutputStream = new java.io.ByteArrayOutputStream();
            transformer.transform(new javax.xml.transform.dom.DOMSource(signatureDocument), new javax.xml.transform.stream.StreamResult(byteArrayOutputStream));
            return byteArrayOutputStream.toByteArray();
        } catch (javax.xml.transform.TransformerException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (javax.xml.crypto.MarshalException ex) {
            java.util.logging.Logger.getLogger("global").log(java.util.logging.Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (javax.xml.parsers.ParserConfigurationException ex) {
            java.util.logging.Logger.getLogger("global").log(java.util.logging.Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (java.security.NoSuchAlgorithmException ex) {
            java.util.logging.Logger.getLogger("global").log(java.util.logging.Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (java.security.InvalidAlgorithmParameterException ex) {
            java.util.logging.Logger.getLogger("global").log(java.util.logging.Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        }
    }


    /**
     * Signature XAdES 1.3.2 (detached).
     *   http://www.w3.org/TR/XAdES/#Appendix_A_Schema_Definitions
     *
     * @param certificate the X509 Certificate
     * @param privateKey the private Key
     * @param digest  the digest to be signed
     * @return byte array of the signature
     * @throws XMLSignatureException
     */
    public byte[] signXAdES132(X509Certificate certificate, PrivateKey privateKey, byte[] digest) throws XMLSignatureException {
        String signatureID = "S0";
        String signedPropertiesID = signatureID + "-SignedProperties";
        String keyInfoID = signatureID + "_KI";
        String documentID = getDocumentID();
        String docRefID = "#" + documentID;
        try {
//            Logger.getLogger("XAdES").log(Level.INFO, "signing with cert {0}\n\tprivkey is {1}\n\tdigest is {2}",
//                    new Object[]{certificate.getSerialNumber().toString(16), privateKey.toString(), digest.toString()});
            XMLSignatureFactory signatureFactory = XMLSignatureFactory.getInstance("DOM");

            javax.xml.parsers.DocumentBuilderFactory documentBuilderFactory = javax.xml.parsers.DocumentBuilderFactory.newInstance();
            documentBuilderFactory.setNamespaceAware(true);
//            org.w3c.dom.Document signatureDocument = documentBuilderFactory.newDocumentBuilder().newDocument();
//            signatureDocument.appendChild(signatureDocument.createElement("DocumentDetachedExternalSignature"));
            org.w3c.dom.Document document = documentBuilderFactory.newDocumentBuilder().newDocument();

            document.appendChild(document.createElement("DocumentDetachedExternalSignature"));

            /**
             * Qualifying properties
             */
            Element qualifyingPropertiesElement = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "QualifyingProperties");
            Element signedPropertiesElement = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "SignedProperties");
            signedPropertiesElement.setAttributeNS(null, "Id", signedPropertiesID);
            Element signedSignaturePropertiesElement = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "SignedSignatureProperties");

            /* Signing Time */
            Element signingTimeElement = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "SigningTime");
            java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ssz");
            dateFormat.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
            java.util.Date date = new java.util.Date();
            String mydate = dateFormat.format(date);
            signingTimeElement.appendChild(document.createTextNode(mydate.replaceAll("UTC", "Z")));
            signedSignaturePropertiesElement.appendChild(signingTimeElement);

            /* signingCertificate */
            Element signingCertificateElement = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "SigningCertificate");
            Element certElement = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "Cert");
            Element certDigest = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "CertDigest");
            Element digestMethod = XADESSignUtil.createElement(document, XMLSignature.XMLNS, "DigestMethod");
            Element digestValue  = XADESSignUtil.createElement(document, XMLSignature.XMLNS, "DigestValue");
            digestMethod.setAttribute("Algorithm", javax.xml.crypto.dsig.DigestMethod.SHA256); //"http://www.w3.org/2001/04/xmlenc#sha256"

            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(certificate.getEncoded());
            java.io.ByteArrayOutputStream certDigestBOS = new java.io.ByteArrayOutputStream();
            org.bouncycastle.util.encoders.Base64.encode(messageDigest.digest(), certDigestBOS);

            digestValue.appendChild(document.createTextNode(certDigestBOS.toString()));
            certDigest.appendChild(digestMethod);
            certDigest.appendChild(digestValue);

            Element issuerSerial = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "IssuerSerial");
            Element X509IssuerName = XADESSignUtil.createElement(document, XMLSignature.XMLNS, "X509IssuerName");
            X509IssuerName.appendChild(document.createTextNode(certificate.getIssuerX500Principal().getName(X500Principal.RFC1779)));
            Element X509SerialNumber = XADESSignUtil.createElement(document, XMLSignature.XMLNS, "X509SerialNumber");
            X509SerialNumber.appendChild(document.createTextNode(certificate.getSerialNumber().toString()));
            issuerSerial.appendChild(X509IssuerName);
            issuerSerial.appendChild(X509SerialNumber);

            certElement.appendChild(certDigest);
            certElement.appendChild(issuerSerial);
            signingCertificateElement.appendChild(certElement);
            signedSignaturePropertiesElement.appendChild(signingCertificateElement);

            /* signature policy identifier */
            Element signaturePolicyIdentifier = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "SignaturePolicyIdentifier");
            Element signaturePolicyId = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "SignaturePolicyId");

            Element sigPolicyId = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "SigPolicyId");
            Element identifier = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "Identifier");
            // did not see the 'urn' prefix ???
            //   identifier.appendChild(document.createTextNode("urn:" + getPolicyIdentifierID()));
            identifier.appendChild(document.createTextNode(getPolicyIdentifierID()));
            Element description = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "Description");
            description.appendChild(document.createTextNode(getPolicyIdentifierDescription()));
            sigPolicyId.appendChild(identifier);
            sigPolicyId.appendChild(description);

            Element sigPolicyHash = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "SigPolicyHash");
            Element sdigestMethod = XADESSignUtil.createElement(document, XMLSignature.XMLNS, "DigestMethod");
            sdigestMethod.setAttribute("Algorithm", javax.xml.crypto.dsig.DigestMethod.SHA256);
            Element sdigestValue = XADESSignUtil.createElement(document, XMLSignature.XMLNS, "DigestValue");
            sdigestValue.appendChild(document.createTextNode(getPolicyDigest()));
            sigPolicyHash.appendChild(sdigestMethod);
            sigPolicyHash.appendChild(sdigestValue);

            /* SPURI is not used for the moment */
//            Element sigPolicyQualifiers = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "SigPolicyQualifiers");
//            Element sigPolicyQualifier = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "SigPolicyQualifier");
//            Element SPURIelmt = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "SPURI");
//            SPURIelmt.appendChild(document.createTextNode(this.getSPURI()));
//            sigPolicyQualifier.appendChild(SPURIelmt);
//            sigPolicyQualifiers.appendChild(sigPolicyQualifier);
            signaturePolicyId.appendChild(sigPolicyId);
            signaturePolicyId.appendChild(sigPolicyHash);
//            signaturePolicyId.appendChild(sigPolicyQualifiers);
            signaturePolicyIdentifier.appendChild(signaturePolicyId);
            signedSignaturePropertiesElement.appendChild(signaturePolicyIdentifier);

// ?????? SignerRole ??
            Element signatureProductionPlace = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "SignatureProductionPlace");
            Element cityElmnt = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "City");
            cityElmnt.appendChild(document.createTextNode(getCity()));
            Element postalCodeElmnt = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "PostalCode");
            postalCodeElmnt.appendChild(document.createTextNode(getPostalCode()));
            Element countryNameElmnt = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "CountryName");
            countryNameElmnt.appendChild(document.createTextNode(getCountryName()));
            signatureProductionPlace.appendChild(cityElmnt);
            signatureProductionPlace.appendChild(postalCodeElmnt);
            signatureProductionPlace.appendChild(countryNameElmnt);
            signedSignaturePropertiesElement.appendChild(signatureProductionPlace);

            Element signerRole = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "SignerRole");
            Element claimedRoles = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "ClaimedRoles");
            Element claimedRoleElmt = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "ClaimedRole");
            claimedRoleElmt.appendChild(document.createTextNode(getClaimedRole()));
            claimedRoles.appendChild(claimedRoleElmt);
            signerRole.appendChild(claimedRoles);

            signedSignaturePropertiesElement.appendChild(signerRole);

            /**
             * TODO : SignedDataObjectProperties is badly filled with example text
             */
            Element signedDataObjectPropertiesElement = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "SignedDataObjectProperties");
            Element dataObjectFormatElement = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "DataObjectFormat");
            Element dataObjectFormatObjectIdentifierElement = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "ObjectIdentifier");
            Element objectIdentifierIdElt = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "Identifier");
            objectIdentifierIdElt.appendChild(document.createTextNode("signaturePolicy"));
            Element objectIdentifierDescElt = XADESSignUtil.createXades132Element(document, XADESSignUtil.xadesNS132, "Description");
            objectIdentifierDescElt.appendChild(document.createTextNode("Test verification Etat Civil"));
            dataObjectFormatObjectIdentifierElement.appendChild(objectIdentifierIdElt);
            dataObjectFormatObjectIdentifierElement.appendChild(objectIdentifierDescElt);
            dataObjectFormatElement.appendChild(dataObjectFormatObjectIdentifierElement);
            signedDataObjectPropertiesElement.appendChild(dataObjectFormatElement);

            /* populate the SignedProperties element */
            signedPropertiesElement.appendChild(signedSignaturePropertiesElement);
            signedPropertiesElement.appendChild(signedDataObjectPropertiesElement);

            /* Now we need to compute the digest of signedPropertiesElement ! */
            //   Avoid usage of xmlsec-1.4.1 because it does not behave well in navigator/applet context
            //            org.apache.xml.security.c14n.Canonicalizer c14n;
            //            c14n = org.apache.xml.security.c14n.Canonicalizer.getInstance(org.apache.xml.security.c14n.Canonicalizer.ALGO_ID_C14N_OMIT_COMMENTS);
            //            byte canonicalMessage[] = c14n.canonicalizeSubtree(signedPropertiesElement);
            ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
            // ObjectOutputStream outputstream = new ObjectOutputStream(bytestream);
            nu.xom.Element spXomElement = nu.xom.converters.DOMConverter.convert(signedPropertiesElement);
            // nu.xom.canonical.Canonicalizer outputter = new nu.xom.canonical.Canonicalizer(outputstream, nu.xom.canonical.Canonicalizer.CANONICAL_XML);
            nu.xom.canonical.Canonicalizer outputter = new nu.xom.canonical.Canonicalizer(bytestream, nu.xom.canonical.Canonicalizer.CANONICAL_XML);
            outputter.write(spXomElement);
            // outputstream.close();
            byte canonicalMessage[] = bytestream.toByteArray();

            MessageDigest messageDigest256 = java.security.MessageDigest.getInstance("SHA-256");
            messageDigest256.reset();
            messageDigest256.update(canonicalMessage);
            byte[] spdigest = messageDigest256.digest();


            /**
             * References for the ds:SignedInfo tag
             *  - contentReference digest, already computed and given as parameter
             *  - SignedProperties digest, computed above
             */
            List<Reference> references = new ArrayList<Reference>();
            Reference contentReference = signatureFactory.newReference(documentID,
                    signatureFactory.newDigestMethod(javax.xml.crypto.dsig.DigestMethod.SHA256, null),
                    (List) null, (String) null, (String) null, digest);
            Reference sigpropReference = signatureFactory.newReference("#" + signedPropertiesID,
                    signatureFactory.newDigestMethod(javax.xml.crypto.dsig.DigestMethod.SHA256, null),
                    (List) null,
                    (String) xadesNS132 + "SignedProperties", (String) null, spdigest);
            references.add(contentReference);
            references.add(sigpropReference);

            /**
             * ds:SignedInfo
             */
            SignedInfo signedInfo = signatureFactory.newSignedInfo(
                    signatureFactory.newCanonicalizationMethod(CanonicalizationMethod.INCLUSIVE,
                    (javax.xml.crypto.dsig.spec.C14NMethodParameterSpec) null),
                    signatureFactory.newSignatureMethod("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256", null),
                    Collections.unmodifiableList(references));

            /**
             * ds:KeyInfo
             */
            javax.xml.crypto.dsig.keyinfo.KeyInfoFactory keyInfoFactory = signatureFactory.getKeyInfoFactory();
            javax.xml.crypto.dsig.keyinfo.X509Data x509Data = keyInfoFactory.newX509Data(java.util.Collections.singletonList(certificate));
            KeyInfo keyInfo = keyInfoFactory.newKeyInfo(java.util.Collections.singletonList(x509Data), keyInfoID);
            

            // end of QualifyingProperties
            qualifyingPropertiesElement.appendChild(signedPropertiesElement);
            qualifyingPropertiesElement.setAttributeNS(null, "Target", "#"+ signatureID);

            XMLObject xmlObject = signatureFactory.newXMLObject(
                    java.util.Collections.singletonList(new DOMStructure(qualifyingPropertiesElement)),
                    "#" + signatureID + "_OID", null, null);

            /**
             * Now sign the stuff
             */
            DOMSignContext signContext = new DOMSignContext(privateKey, document.getDocumentElement());
            signContext.putNamespacePrefix(XADESSignUtil.xadesNS132, "xad");
            signContext.putNamespacePrefix(javax.xml.crypto.dsig.XMLSignature.XMLNS, "ds");
            XMLSignature xmlSignature = signatureFactory.newXMLSignature(
                    signedInfo, keyInfo,
                    java.util.Collections.singletonList(xmlObject),
                    signatureID,
                    signatureID + "_SV");
            xmlSignature.sign(signContext);
            TransformerFactory transformerFactory = javax.xml.transform.TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            transformer.transform(new DOMSource(document), new StreamResult(byteArrayOutputStream));
            return byteArrayOutputStream.toByteArray();

            /* Exemple
<ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#" Id="S0">
<ds:SignedInfo>
  <ds:CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"></ds:CanonicalizationMethod>
  <ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"></ds:SignatureMethod>
  <ds:Reference URI="modele.99999.reponse_positive.xml.pdf">
    <ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"></ds:DigestMethod>
    <ds:DigestValue>6OUARQzqBRWzmfGmsx03PkzK7ZNoXHSZftKpVSCCU+s=</ds:DigestValue>
  </ds:Reference>
  <ds:Reference Type="http://uri.etsi.org/01903/v1.3.2#SignedProperties" URI="#S0-SignedProperties">
    <ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"></ds:DigestMethod>
    <ds:DigestValue>MG/w7HVON5HzCbnMDSiZcYsjT0nJjWHhro/L+HURw08=</ds:DigestValue>
  </ds:Reference>
</ds:SignedInfo>

<ds:SignatureValue Id="S0">jenQF2IevIMpUmoN9O6ALidfD7CsDxUJrU6YcksXktItAgr6M1bklno3XTOVjqUVErEb
+TnmtwuHx270kDYmCl3v5hJsXZ0I3gO2Qnxdx2GFMo98Vi9HYnMClXHGf4dzhVbdGvsOvLroa0Lc
duS8AechwcK5uzKWQjPa3wcXTS8id58lmoXxG3WvEQQpmGbd03+7D9b9F8phmXUHKtTXDWDX/Akc
leiWmC/0SJYzvqzNKj3rMWPHlek34dUW6VVoxQi5yMcI4xfAN4TgXvdL28mILaf3pnshNDmAx/k+
6fdYj2oaXwQ2iwrvL0llTdUzgWoxPqylbmsl+Pgv7wl04g==</ds:SignatureValue>

<ds:KeyInfo>
 <ds:X509Data>
  <ds:X509Certificate>MIIFIjCCAwqgAwIBAgIIXy1RZuynlPwwDQYJKoZIhvcNAQELBQAwYTELMAkGA1UEBhMCRlIxGT
.....
JYoNQQ/oq0HN3sut9ZBC+N4N/pD/sRCiylpvH5r2ARMVLeDmOXDID5rbpWeUnY+3LqPRA5PDgkcw
yW9jkWL/xFtA==</ds:X509Certificate>
 </ds:X509Data>
</ds:KeyInfo>

<ds:Object>
<XadesSignatureInjector:QualifyingProperties xmlns:XadesSignatureInjector="http://uri.etsi.org/01903/v1.3.2#" Target="#S0">
  <XadesSignatureInjector:SignedProperties Id="S0-SignedProperties">
      <XadesSignatureInjector:SignedSignatureProperties>
        <XadesSignatureInjector:SigningTime>2010-10-19T23:55:00Z</XadesSignatureInjector:SigningTime>
        <XadesSignatureInjector:SigningCertificate>
          <XadesSignatureInjector:Cert>
            <XadesSignatureInjector:CertDigest>
              <ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"></ds:DigestMethod>
              <ds:DigestValue>K0MQNoHEsF3xWc2lV2RF6MGxoAQ2I/aECxdX04A/LJg=</ds:DigestValue>
            </XadesSignatureInjector:CertDigest>
            <XadesSignatureInjector:IssuerSerial>
              <ds:X509IssuerName>C=F/O=J/OU=0/CN=A</ds:X509IssuerName>
              <ds:X509SerialNumber>6858227310054053116</ds:X509SerialNumber>
            </XadesSignatureInjector:IssuerSerial>
          </XadesSignatureInjector:Cert>
        </XadesSignatureInjector:SigningCertificate>
        <XadesSignatureInjector:SignaturePolicyIdentifier>
          <XadesSignatureInjector:SignaturePolicyId>
            <XadesSignatureInjector:SigPolicyId>
                <XadesSignatureInjector:Identifier>1.2.250.1.200.100.1.1.1</XadesSignatureInjector:Identifier>
                <XadesSignatureInjector:Description>Test vérification Etat Civil</XadesSignatureInjector:Description>
            </XadesSignatureInjector:SigPolicyId>
            <XadesSignatureInjector:SigPolicyHash>
                <ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"></ds:DigestMethod>
                <ds:DigestValue>Ql9DQpb3+DpcyTXZP3IwauDVwjN+A5X1wp8EknmV67SPBGaPEy4dvE/Rg7viUTjohHJ36y4F3kZKyT
                    MEi5gT9RkfA9wmsLwC/JkTdpTg7gWDMMhWPGjRWibNsK7FAV4FRK7+GUHdwu1tbBr+5/98DgIOXx
                    IqrtlLwTVqH5FXLdHr1bCuPhKAaaPocW1nApF421ahHILLeg7Dnhxg4zf96zEtyMGAII1Re4I7lh
                    umPbJ6W8z9gtKnr76yZnUvp9Ictv6nDcqGw/dQleMbWLFzGfG10/HA2RD7st/okC0pF2XswgpE5G
                    OdWlkJD/kqj6HfWfj7bmmBHDuSC2WeI8eWyw==</ds:DigestValue>
            </XadesSignatureInjector:SigPolicyHash>
          </XadesSignatureInjector:SignaturePolicyId>
        </XadesSignatureInjector:SignaturePolicyIdentifier>
        <XadesSignatureInjector:SignerRole>
        </XadesSignatureInjector:SignerRole>
      </XadesSignatureInjector:SignedSignatureProperties>

      <XadesSignatureInjector:SignedDataObjectProperties>
        <XadesSignatureInjector:DataObjectFormat>
          <XadesSignatureInjector:ObjectIdentifier>
            <XadesSignatureInjector:Identifier>signaturePolicy</XadesSignatureInjector:Identifier>
            <XadesSignatureInjector:Description>Test vérification Etat Civil</XadesSignatureInjector:Description>
          </XadesSignatureInjector:ObjectIdentifier>
        </XadesSignatureInjector:DataObjectFormat>
      </XadesSignatureInjector:SignedDataObjectProperties>
  </XadesSignatureInjector:SignedProperties>
</XadesSignatureInjector:QualifyingProperties>
</ds:Object>
</ds:Signature>
             *
             */
        } catch (IOException ex) {
            Logger.getLogger(XADESSignUtil.class.getName()).log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (javax.xml.crypto.MarshalException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
//        } catch (InvalidCanonicalizerException ex) {
//            Logger.getLogger(XADESSignUtil.class.getName()).log(Level.SEVERE, null, ex);
//            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
//        } catch (CanonicalizationException ex) {
//            Logger.getLogger(XADESSignUtil.class.getName()).log(Level.SEVERE, null, ex);
//            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (javax.xml.transform.TransformerException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (CertificateEncodingException ex) {
            Logger.getLogger(XADESSignUtil.class.getName()).log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (javax.xml.parsers.ParserConfigurationException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (java.security.NoSuchAlgorithmException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (java.security.InvalidAlgorithmParameterException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        }
    }

    /**
     * Signature enveloppée XAdES
     *
     * @param certificate
     * @param privateKey
     * @param data
     * @return
     * @throws javax.xml.crypto.dsig.XMLSignatureException
     */
    public byte[] envsign(java.security.cert.X509Certificate certificate, java.security.PrivateKey privateKey, InputStream data) throws javax.xml.crypto.dsig.XMLSignatureException {
        String signatureID = getSignatureID();
        String signedPropertiesID = signatureID + "_SP";
        String keyInfoID = signatureID + "_KI";
        String documentID = getDocumentID();
        String docRefID = "";
        if (this.idPresence) {
            docRefID = "#" + documentID;
        }
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA");
            messageDigest.update(certificate.getEncoded());
            java.io.ByteArrayOutputStream certDigestBOS = new java.io.ByteArrayOutputStream();
            org.bouncycastle.util.encoders.Base64.encode(messageDigest.digest(), certDigestBOS);

            javax.xml.crypto.dsig.XMLSignatureFactory signatureFactory = javax.xml.crypto.dsig.XMLSignatureFactory.getInstance("DOM");
            java.util.List<javax.xml.crypto.dsig.Reference> referencesList = new java.util.ArrayList<javax.xml.crypto.dsig.Reference>();

            java.util.ArrayList<javax.xml.crypto.dsig.Transform> transformList = new java.util.ArrayList<javax.xml.crypto.dsig.Transform>();
            transformList.add(signatureFactory.newTransform(Transform.ENVELOPED, (TransformParameterSpec) null));
            transformList.add(signatureFactory.newTransform(nu.xom.canonical.Canonicalizer.EXCLUSIVE_XML_CANONICALIZATION, (TransformParameterSpec) null));


            /**  ****/
            javax.xml.crypto.dsig.Reference contentReference = signatureFactory.newReference(docRefID, // "#" + documentID,
                    signatureFactory.newDigestMethod(DigestMethod.SHA1, null),
                    transformList, null, null);
            javax.xml.crypto.dsig.Reference keyInfoReference = signatureFactory.newReference("#" + keyInfoID, signatureFactory.newDigestMethod(javax.xml.crypto.dsig.DigestMethod.SHA1, null),
                    transformList, (java.lang.String) null, (java.lang.String) null);

            referencesList.add(contentReference);
            referencesList.add(keyInfoReference);

            javax.xml.crypto.dsig.SignedInfo signedInfo = signatureFactory.newSignedInfo(
                    signatureFactory.newCanonicalizationMethod(javax.xml.crypto.dsig.CanonicalizationMethod.EXCLUSIVE, (javax.xml.crypto.dsig.spec.C14NMethodParameterSpec) null),
                    signatureFactory.newSignatureMethod(SignatureMethod.RSA_SHA1, null), java.util.Collections.unmodifiableList(referencesList));
            javax.xml.crypto.dsig.keyinfo.KeyInfoFactory keyInfoFactory = signatureFactory.getKeyInfoFactory();

            javax.xml.crypto.dsig.keyinfo.X509Data x509Data = keyInfoFactory.newX509Data(java.util.Collections.singletonList(certificate));
            javax.xml.crypto.dsig.keyinfo.KeyInfo keyInfo = keyInfoFactory.newKeyInfo(java.util.Collections.singletonList(x509Data), keyInfoID);
            javax.xml.parsers.DocumentBuilderFactory documentBuilderFactory = javax.xml.parsers.DocumentBuilderFactory.newInstance();
            documentBuilderFactory.setNamespaceAware(true);
            /** ****/
            org.w3c.dom.Document document = documentBuilderFactory.newDocumentBuilder().parse(data);

            /* creation qualifying properties */
            org.w3c.dom.Element qualifyingPropertiesElement = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "QualifyingProperties");

            org.w3c.dom.Element signedPropertiesElement = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "SignedProperties");

            signedPropertiesElement.setAttributeNS(null, "Id", signedPropertiesID);

            org.w3c.dom.Element signedSignaturePropertiesElement = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "SignedSignatureProperties");

            /* signing time */
            org.w3c.dom.Element signingTimeElement = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "SigningTime");
            java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss");
            java.util.Date date = new java.util.Date();
            signingTimeElement.appendChild(document.createTextNode(dateFormat.format(date)));
            /* */

            signedSignaturePropertiesElement.appendChild(signingTimeElement);  //

            /* signingCertificate */
            org.w3c.dom.Element signingCertificateElement = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "SigningCertificate");

            org.w3c.dom.Element certElement = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "Cert");

            org.w3c.dom.Element certDigest = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "CertDigest");
            org.w3c.dom.Element digestMethod = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "DigestMethod");
            org.w3c.dom.Element digestValue = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "DigestValue");
            digestMethod.setAttribute("Algorithm", "http://www.w3.org/2000/09/xmldsig#sha1");
            digestValue.appendChild(document.createTextNode(certDigestBOS.toString()));
            certDigest.appendChild(digestMethod);
            certDigest.appendChild(digestValue);

            org.w3c.dom.Element issuerSerial = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "IssuerSerial");
            org.w3c.dom.Element X509IssuerName = XADESSignUtil.createElement(document, javax.xml.crypto.dsig.XMLSignature.XMLNS, "X509IssuerName");
            X509IssuerName.appendChild(document.createTextNode(certificate.getIssuerX500Principal().getName()));
            org.w3c.dom.Element X509SerialNumber = XADESSignUtil.createElement(document, javax.xml.crypto.dsig.XMLSignature.XMLNS, "X509SerialNumber");
            X509SerialNumber.appendChild(document.createTextNode(certificate.getSerialNumber().toString()));
            issuerSerial.appendChild(X509IssuerName);
            issuerSerial.appendChild(X509SerialNumber);

            certElement.appendChild(certDigest);
            certElement.appendChild(issuerSerial);
            signingCertificateElement.appendChild(certElement);
            /* */

            signedSignaturePropertiesElement.appendChild(signingCertificateElement); //

            /* signature policy identifier */
            org.w3c.dom.Element signaturePolicyIdentifier = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "SignaturePolicyIdentifier");
            org.w3c.dom.Element signaturePolicyId = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "SignaturePolicyId");

            org.w3c.dom.Element sigPolicyId = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "SigPolicyId");
            org.w3c.dom.Element identifier = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "Identifier");
            identifier.appendChild(document.createTextNode("urn" + getPolicyIdentifierID()));
            org.w3c.dom.Element description = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "Description");
            description.appendChild(document.createTextNode(getPolicyIdentifierDescription()));
            sigPolicyId.appendChild(identifier);
            sigPolicyId.appendChild(description);


            org.w3c.dom.Element sigPolicyHash = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "SigPolicyHash");
            org.w3c.dom.Element sdigestMethod = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "DigestMethod");
            org.w3c.dom.Element sdigestValue = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "DigestValue");
            sdigestMethod.setAttribute("Algorithm", "http://www.w3.org/2000/09/xmldsig#sha1");
            sdigestValue.appendChild(document.createTextNode(getPolicyDigest()));
            sigPolicyHash.appendChild(sdigestMethod);
            sigPolicyHash.appendChild(sdigestValue);

            org.w3c.dom.Element sigPolicyQualifiers = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "SigPolicyQualifiers");
            org.w3c.dom.Element sigPolicyQualifier = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "SigPolicyQualifier");
            org.w3c.dom.Element SPURIelmt = XADESSignUtil.createElement(document, XADESSignUtil.xadesNS, "SPURI");
            SPURIelmt.appendChild(document.createTextNode(getSPURI()));
            sigPolicyQualifier.appendChild(SPURIelmt);
            sigPolicyQualifiers.appendChild(sigPolicyQualifier);

            signaturePolicyId.appendChild(sigPolicyId);
            signaturePolicyId.appendChild(sigPolicyHash);
            signaturePolicyId.appendChild(sigPolicyQualifiers);
            signaturePolicyIdentifier.appendChild(signaturePolicyId);
            /* */

            signedSignaturePropertiesElement.appendChild(signaturePolicyIdentifier); //

            /* signature production place */
            org.w3c.dom.Element signatureProductionPlace = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "SignatureProductionPlace");
            org.w3c.dom.Element cityElmnt = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "City");
            cityElmnt.appendChild(document.createTextNode(getCity()));
            org.w3c.dom.Element postalCodeElmnt = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "PostalCode");
            postalCodeElmnt.appendChild(document.createTextNode(getPostalCode()));
            org.w3c.dom.Element countryNameElmnt = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "CountryName");
            countryNameElmnt.appendChild(document.createTextNode(getCountryName()));

            signatureProductionPlace.appendChild(cityElmnt);
            signatureProductionPlace.appendChild(postalCodeElmnt);
            signatureProductionPlace.appendChild(countryNameElmnt);
            /* */

            signedSignaturePropertiesElement.appendChild(signatureProductionPlace); //

            /* signer role */
            org.w3c.dom.Element signerRole = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "SignerRole");
            org.w3c.dom.Element claimedRoles = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "ClaimedRoles");
            org.w3c.dom.Element claimedRoleElmt = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "ClaimedRole");
            claimedRoleElmt.appendChild(document.createTextNode(getClaimedRole()));
            claimedRoles.appendChild(claimedRoleElmt);
            signerRole.appendChild(claimedRoles);
            /* */
            signedSignaturePropertiesElement.appendChild(signerRole);

            signedPropertiesElement.appendChild(signedSignaturePropertiesElement);
            qualifyingPropertiesElement.appendChild(signedPropertiesElement);
            qualifyingPropertiesElement.setAttribute("Target", signatureID);

            javax.xml.crypto.dsig.XMLObject xmlObject = signatureFactory.newXMLObject(
                    java.util.Collections.singletonList(new javax.xml.crypto.dom.DOMStructure(qualifyingPropertiesElement)),
                    "objectID", null, null);
            //
            javax.xml.crypto.dsig.dom.DOMSignContext signContext = new javax.xml.crypto.dsig.dom.DOMSignContext(privateKey, document.getDocumentElement());
            signContext.putNamespacePrefix(XADESSignUtil.xadesNS, "xad");
            signContext.putNamespacePrefix(javax.xml.crypto.dsig.XMLSignature.XMLNS, "ds");


            javax.xml.crypto.dsig.XMLSignature xmlSignature =
                    signatureFactory.newXMLSignature(signedInfo, keyInfo,
                    java.util.Collections.singletonList(xmlObject), signatureID, signatureID + "_SV");

            xmlSignature.sign(signContext);
            //
            javax.xml.transform.TransformerFactory transformerFactory = javax.xml.transform.TransformerFactory.newInstance();
            javax.xml.transform.Transformer transformer = transformerFactory.newTransformer();
            java.io.ByteArrayOutputStream byteArrayOutputStream = new java.io.ByteArrayOutputStream();
            transformer.transform(new javax.xml.transform.dom.DOMSource(document), new javax.xml.transform.stream.StreamResult(byteArrayOutputStream));
            return byteArrayOutputStream.toByteArray();
        } catch (SAXException ex) {
            Logger.getLogger(XADESSignUtil.class.getName()).log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (IOException ex) {
            Logger.getLogger(XADESSignUtil.class.getName()).log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (CertificateEncodingException ex) {
            Logger.getLogger(XADESSignUtil.class.getName()).log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (javax.xml.transform.TransformerException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (javax.xml.crypto.MarshalException ex) {
            java.util.logging.Logger.getLogger("global").log(java.util.logging.Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (javax.xml.parsers.ParserConfigurationException ex) {
            java.util.logging.Logger.getLogger("global").log(java.util.logging.Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (java.security.NoSuchAlgorithmException ex) {
            java.util.logging.Logger.getLogger("global").log(java.util.logging.Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (java.security.InvalidAlgorithmParameterException ex) {
            java.util.logging.Logger.getLogger("global").log(java.util.logging.Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        }
    }

    static org.w3c.dom.Element createElement(org.w3c.dom.Document document, String namespaceURI, String qualifiedName) {
        org.w3c.dom.Element ret = document.createElementNS(namespaceURI, "ds:" + qualifiedName);
        return ret;
    }

    static  org.w3c.dom.Element createXadesElement(org.w3c.dom.Document document, String namespaceURI, String qualifiedName) {
        org.w3c.dom.Element ret = document.createElementNS(namespaceURI, "xad:" + qualifiedName);
        ret.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns", xadesNS);
        return ret;
    }

    static  org.w3c.dom.Element createXades122Element(org.w3c.dom.Document document, String namespaceURI, String qualifiedName) {
        org.w3c.dom.Element ret = document.createElementNS(namespaceURI, "xad:" + qualifiedName);
        ret.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns", xadesNS122);
        return ret;
    }

    static  org.w3c.dom.Element createXades132Element(org.w3c.dom.Document document, String namespaceURI, String qualifiedName) {
        org.w3c.dom.Element ret = document.createElementNS(namespaceURI, "xad:" + qualifiedName);
        ret.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns", xadesNS132);
        return ret;
    }
}

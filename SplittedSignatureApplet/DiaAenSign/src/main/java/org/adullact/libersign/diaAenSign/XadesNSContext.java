/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.libersign.diaAenSign;

import java.util.Iterator;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.namespace.NamespaceContext;

/**
 * @author Stephane Vast - Adullact Projet
 */
public class XadesNSContext implements NamespaceContext {
    /**
     * Namespace for XAdES 1.1.1 scheme
     */
    public static final String xadesNS = "http://uri.etsi.org/01903/v1.1.1#";
    /**
     * Namespace for XAdES 1.2.2 scheme (required for DIA signing schema)
     */
    public static final String xadesNS122 = "http://uri.etsi.org/01903/v1.2.2#";
    /**
     * Namespace for XAdES 1.3.2 scheme (required for ANTS-ECD signing schema)
     */
    public static final String xadesNS132 = "http://uri.etsi.org/01903/v1.3.2#";
    /**
     * Namespace for DIA scoped XML documents (pretty specific)
     */
    public static final String diaNS = "http://xmlschema.ok-demat.com/DIA";
    public static final String diacoNS = "http://xmlschema.ok-demat.com/DIA-CO";
    public static final String xsiNS = "http://www.w3.org/2001/XMLSchema-instance";

    private String xadesSchema;


    /**
     * Constructor.
     *
     * @param xadesVersion la version de XADES qu'on veut 1.1.1, 1.2.2, etc.
     */
    public XadesNSContext(String xadesVersion) throws UnsupportedOperationException {
        if (xadesNS.equals(xadesVersion) ||
                xadesNS122.equals(xadesVersion) ||
                xadesNS132.equals(xadesVersion)) {
            xadesSchema = xadesVersion;
        } else {
            throw new UnsupportedOperationException("Schema XAdES non supporté: " + xadesVersion);
        }
    }

    @Override
    public String getNamespaceURI(String prefix) {
        if ("xad".equals(prefix))
            return xadesSchema;
        if ("xsi".equals(prefix))
            return xsiNS;
        if ("ds".equals(prefix))
            return XMLSignature.XMLNS;
        if ("dia".equals(prefix))
            return diaNS;
        if ("diaco".equals(prefix))
            return diacoNS;
        return null;
    }

    @Override
    public Iterator getPrefixes(String val) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getPrefix(String uri) {
        if (xadesSchema.equals(uri))
            return "xad";
        if (xsiNS.equals(uri))
            return "xsi";
        //if ("ds".equals(uri))
        if (XMLSignature.XMLNS.equals(uri))
            return "ds";
        if (diaNS.equals(uri))
            return "dia";
        if (diacoNS.equals(uri))
            return "diaco";
        return null;
    }

}

/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.libersign.diaAenSign;

import java.io.File;
import java.io.FileInputStream;

/**
 * Facilitateur d'interaction avec TSA
 *
 * @author Stephane Vast - Adullact Projet
 */
public class ProxyTSA {

    public ProxyTSA(String endpoint, String username, String password, String trustStorePath, String trustStorePass, String keyStorePath, String keyStorePass) {
        //Initialisation du contexte et du service d'acces
        System.setProperty("javax.net.ssl.trustStore", trustStorePath);
        System.setProperty("javax.net.ssl.trustStorePassword", trustStorePass);
        System.setProperty("javax.net.ssl.keyStore", keyStorePath);
        System.setProperty("javax.net.ssl.keyStorePassword", keyStorePass);

/*        InterfaceParapheurService serviceLocator = new InterfaceParapheurService();
        service = serviceLocator.getInterfaceParapheurSoap11();
        Map<String, Object> requestContext = ((BindingProvider) service).getRequestContext();

        requestContext.put(BindingProvider.SESSION_MAINTAIN_PROPERTY, Boolean.TRUE);
        requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpoint);
        requestContext.put(BindingProvider.USERNAME_PROPERTY, username);
        requestContext.put(BindingProvider.PASSWORD_PROPERTY, password);*/
    }




    public static byte[] getBytesOfFile(String path){
        byte[] buffer = null;
    	try {
    	    File fileIn = new File(path);
    	    FileInputStream fis = new FileInputStream(fileIn);
    	    buffer = new byte[fis.available()];
    	    fis.read(buffer);
    	    fis.close();
    	} catch(Exception e) {
    	    e.printStackTrace();
    	}
    	return buffer;
    }
}

/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.libersign.diaAenSign;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dom.DOMStructure;
import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLObject;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.bouncycastle.util.encoders.Base64;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.w3c.dom.Element;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * Classe dediee pour la signature XAdES 1.2.2 de flux XML, tres tres orientee
 * pour les flux DIA (notaires)
 * 
 * @author Stephane Vast (Adullact Projet)
 */
public class XADES122SignUtil
{
    private static String pesID = "";
    private static String policyID = "";
    private static String policyDescryption = "";
    private static String policyDigestValue = "";
    private static String SPURI = "";
    private static String city = "";
    private static String postalCode = "";
    private static String countryName = "France";
    private static String claimedRole = "";

    // namespace
    static final String DIA_NS = "http://xmlschema.ok-demat.com/DIA";
    
    // static final String DIA_ROOT = "/dia:renonciationAnticipee";
    static final String DIA_ROOT = "/dia:accuseNPR";
    static final String DIA_AEN_ROOT = "/dia:accuseAEN";

    // identification node xpath
    // static final String ID_PATH = DIA_ROOT + "/dia:identificationDIA";
    static final String ID_PATH = DIA_ROOT + "/dia:identification";
    static final String ID_AEN_PATH = DIA_AEN_ROOT + "/dia:identification";

    // departement node xpath
    static final String DP_PATH = DIA_ROOT + "/dia:titulairesDroitPreemption/dia:departement";

    // conservatoire node xpath
    static final String CL_PATH = DIA_ROOT + "/dia:titulairesDroitPreemption/dia:conservatoireLittoral";

    // commune node xpath
    static final String CM_PATH = DIA_ROOT + "/dia:titulairesDroitPreemption/dia:commune";

    /**
     * Signature IDs as described in the dictionary for DIA
     */
    static final String CM_SIG = "sigOrgaCom";
    static final String CL_SIG = "sigOrgaConsLit";
    static final String DP_SIG = "sigOrgaDep";

    /**
     * The ID_SEP shall not be "|", as this is not a valid character in the naming conventions for IDs
     * Better use common separators such as "-" or "_"
     */
    static final String ID_SEP = "_";

    static final Logger logger = Logger.getLogger(XADES122SignUtil.class.getName());


    /**
     * Department signature function
     * @param certificate the department (x509)  certificate
     * @param privateKey the certificate's corresponding  private key 
     * @param dataIs data inputstream (i.e., the sml data stream to be signed)
     * @param comment an optional text describing the motive of the signer
     * @return the signed data (data and signature) as an xml byte array
     * @throws XMLSignatureException
     */
    public static byte[] departmentSign(X509Certificate certificate, PrivateKey privateKey, InputStream dataIs, String comment) throws XMLSignatureException
    {
        return organismSign(certificate, privateKey, XADES122SignUtil.DP_PATH, XADES122SignUtil.DP_SIG, dataIs, comment);
    }

    /**
     * The CS signature function
     * @param certificate the department (x509)  certificate
     * @param privateKey the certificate's corresponding  private key
     * @param dataIs data inputstream (i.e., the sml data stream to be signed)
     * @param comment an optional text describing the motive of the signer
     * @return the signed data (data and signature) as an xml byte array
     * @throws XMLSignatureException
     */
    public static byte[] conservatoireLittoralSign(X509Certificate certificate, PrivateKey privateKey, InputStream dataIs, String comment) throws XMLSignatureException
    {
        return organismSign(certificate, privateKey, XADES122SignUtil.CL_PATH, XADES122SignUtil.CL_SIG, dataIs, comment);
    }

    /**
     * The commune signature function
     * @param certificate the department (x509)  certificate
     * @param privateKey the certificate's corresponding  private key
     * @param dataIs data inputstream (i.e., the sml data stream to be signed)
     * @param comment an optional text describing the motive of the signer
     * @return the signed data (data and signature) as an xml byte array
     * @throws XMLSignatureException
     */
    public static byte[] communeSign(X509Certificate certificate, PrivateKey privateKey, InputStream dataIs, String comment) throws XMLSignatureException
    {
        return organismSign(certificate, privateKey, XADES122SignUtil.CM_PATH, XADES122SignUtil.CM_SIG, dataIs, comment);
    }

    /**
     * Generic signature function for DIA NPR
     *
     * @param certificate the department (x509)  certificate
     * @param privateKey the certificate's corresponding  private key
     * @param orgPath the organism xpath (see the static fields above).
     * @param SignatureIdString the organism signature ID (see the static fields above).
     * @param dataIs data inputstream (i.e., the sml data stream to be signed)
     * @param comment an optional text describing the motive of the signer
     * @return the signed data (data and signature) as an xml byte array
     * @throws XMLSignatureException
     */
    static byte[] organismSign(X509Certificate certificate, PrivateKey privateKey, String orgPath, String SignatureIdString, InputStream dataIs, String comment) throws XMLSignatureException
    {
        try
        {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            documentBuilderFactory.setNamespaceAware(true);
            DocumentBuilder builder = documentBuilderFactory.newDocumentBuilder();
            Document document = builder.parse(dataIs);

            NamespaceContext nsContext = new XadesNSContext(XadesNSContext.xadesNS122);

            XPathFactory xpathFact = XPathFactory.newInstance();
            XPath xpath = xpathFact.newXPath();
            xpath.setNamespaceContext(nsContext);

            Element idElmt = (Element) xpath.evaluate(XADES122SignUtil.ID_PATH, document, XPathConstants.NODE);
            Element dpElmt = (Element) xpath.evaluate(orgPath, document, XPathConstants.NODE);

            // Add tag <dia:observation>"comment"</dia:observation>
            Element observation = document.createElementNS(XADES122SignUtil.DIA_NS, "dia:observation");
            observation.appendChild(document.createTextNode(comment));
            dpElmt.appendChild(observation);
            

            Document signedDocument = XADES122SignUtil.cosign(document, idElmt, dpElmt, SignatureIdString, certificate, privateKey);
            TransformerFactory transformerFactory = javax.xml.transform.TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            transformer.transform(new DOMSource(signedDocument), new StreamResult(byteArrayOutputStream));
            return byteArrayOutputStream.toByteArray();

        } catch (TransformerException ex) {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (XPathExpressionException ex) {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (SAXException ex) {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (ParserConfigurationException ex) {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        }
    }

    /**
     * Generic Xades cosignature method for DIA ARE stuff
     *
     * @param document the document to be signed
     * @param identification the identification node to be signed
     * @param organism the signing organism element
     * @param SignatureIdString the organism signature ID (see the static fields above).
     * @param certificate the signer's x509 certificate
     * @param privateKey the signer's private key
     * @return A signed document
     * @throws XMLSignatureException
     */
    static Document cosign(Document document, Element identification, Element organism, String SignatureIdString, X509Certificate certificate, PrivateKey privateKey) throws XMLSignatureException
    {
        try
        {
            /**
             * The signature attribute ID was concatenation of 'DIA id' and 'organism id'
             * String signatureAttributeId = identification.getAttribute("Id") + ID_SEP + organism.getAttribute("Id");
             */
            String signatureAttributeId = SignatureIdString;
            XMLSignatureFactory signatureFactory = XMLSignatureFactory.getInstance("DOM");

            ArrayList<Transform> transformList = new ArrayList<Transform>();
            transformList.add(signatureFactory.newTransform(Transform.ENVELOPED, (TransformParameterSpec) null));
            //transformList.add(signatureFactory.newTransform(CanonicalizationMethod.INCLUSIVE, (TransformParameterSpec) null));

            List<Reference> references = new ArrayList<Reference>();
            Reference idRef = signatureFactory.newReference("#" + identification.getAttribute("Id"), signatureFactory.newDigestMethod(DigestMethod.SHA1, null));
            references.add(idRef);
            if (organism != null) {
                Reference orgRef = signatureFactory.newReference("#" + organism.getAttribute("Id"), signatureFactory.newDigestMethod(DigestMethod.SHA1, null),
                        Collections.singletonList(signatureFactory.newTransform(Transform.ENVELOPED, (TransformParameterSpec) null)), null, null);
                references.add(orgRef);
            }

            javax.xml.crypto.dsig.SignedInfo signedInfo = signatureFactory.newSignedInfo(
                    signatureFactory.newCanonicalizationMethod(CanonicalizationMethod.INCLUSIVE, (javax.xml.crypto.dsig.spec.C14NMethodParameterSpec) null),
                    signatureFactory.newSignatureMethod(SignatureMethod.RSA_SHA1, null), Collections.unmodifiableList(references));

            KeyInfoFactory keyInfoFactory = signatureFactory.getKeyInfoFactory();
            X509Data x509Data = keyInfoFactory.newX509Data(java.util.Collections.singletonList(certificate));

            KeyInfo keyInfo = keyInfoFactory.newKeyInfo(java.util.Collections.singletonList(x509Data), signatureAttributeId + "_KI");

            Element qualifyingPropertiesElement = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "QualifyingProperties");
            Element signedPropertiesElement = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "SignedProperties");

            signedPropertiesElement.setAttributeNS(null, "Id", signatureAttributeId + "_SP");
            Element signedSignaturePropertiesElement = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "SignedSignatureProperties");


            Element signingTimeElement = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "SigningTime");
            java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss");
            java.util.Date date = new java.util.Date();
            signingTimeElement.appendChild(document.createTextNode(dateFormat.format(date)));

            signedSignaturePropertiesElement.appendChild(signingTimeElement);

            /* signingCertificate */
            Element signingCertificateElement = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "SigningCertificate");
            Element certElement = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "Cert");

            Element certDigest = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "CertDigest");
            // à changer
            // Element digestMethod = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "DigestMethod");
            // Element digestValue = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "DigestValue");
            Element digestMethod = XADESSignUtil.createElement(document, XMLSignature.XMLNS, "DigestMethod");
            Element digestValue  = XADESSignUtil.createElement(document, XMLSignature.XMLNS, "DigestValue");
            digestMethod.setAttribute("Algorithm", "http://www.w3.org/2000/09/xmldsig#sha1");
            // DigestMethod digMethod = signatureFactory.newDigestMethod(DigestMethod.SHA1, null);

            MessageDigest messageDigest = MessageDigest.getInstance("SHA");
            messageDigest.update(certificate.getEncoded());
            java.io.ByteArrayOutputStream certDigestBOS = new java.io.ByteArrayOutputStream();
            Base64.encode(messageDigest.digest(), certDigestBOS);

            digestValue.appendChild(document.createTextNode(certDigestBOS.toString()));
            certDigest.appendChild(digestMethod);
            certDigest.appendChild(digestValue);

            Element issuerSerial = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "IssuerSerial");
            Element X509IssuerName = XADESSignUtil.createElement(document, XMLSignature.XMLNS, "X509IssuerName");
            X509IssuerName.appendChild(document.createTextNode(certificate.getIssuerX500Principal().getName()));
            Element X509SerialNumber = XADESSignUtil.createElement(document, XMLSignature.XMLNS, "X509SerialNumber");
            X509SerialNumber.appendChild(document.createTextNode(certificate.getSerialNumber().toString()));
            issuerSerial.appendChild(X509IssuerName);
            issuerSerial.appendChild(X509SerialNumber);

            certElement.appendChild(certDigest);
            certElement.appendChild(issuerSerial);
            signingCertificateElement.appendChild(certElement);
            /* */

            signedSignaturePropertiesElement.appendChild(signingCertificateElement); //

            /* signature policy identifier */
            Element signaturePolicyIdentifier = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "SignaturePolicyIdentifier");
            Element signaturePolicyId = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "SignaturePolicyId");

            Element sigPolicyId = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "SigPolicyId");
            Element identifier = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "Identifier");
            identifier.appendChild(document.createTextNode("urn:" + XADES122SignUtil.getPolicyIdentifierID()));
            Element description = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "Description");
            description.appendChild(document.createTextNode(XADES122SignUtil.getPolicyIdentifierDescription()));
            sigPolicyId.appendChild(identifier);
            sigPolicyId.appendChild(description);

            Element sigPolicyHash = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "SigPolicyHash");
            // Element sdigestMethod = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "DigestMethod");
            // Element sdigestValue = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "DigestValue");
            Element sdigestMethod = XADESSignUtil.createElement(document, XMLSignature.XMLNS, "DigestMethod");
            Element sdigestValue = XADESSignUtil.createElement(document, XMLSignature.XMLNS, "DigestValue");
            sdigestMethod.setAttribute("Algorithm", "http://www.w3.org/2000/09/xmldsig#sha1");
            sdigestValue.appendChild(document.createTextNode(XADES122SignUtil.getPolicyDigest()));
            sigPolicyHash.appendChild(sdigestMethod);
            sigPolicyHash.appendChild(sdigestValue);

            Element sigPolicyQualifiers = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "SigPolicyQualifiers");
            Element sigPolicyQualifier = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "SigPolicyQualifier");
            Element SPURIelmt = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "SPURI");
            SPURIelmt.appendChild(document.createTextNode(XADES122SignUtil.getSPURI()));
            sigPolicyQualifier.appendChild(SPURIelmt);
            sigPolicyQualifiers.appendChild(sigPolicyQualifier);

            signaturePolicyId.appendChild(sigPolicyId);
            signaturePolicyId.appendChild(sigPolicyHash);
            signaturePolicyId.appendChild(sigPolicyQualifiers);
            signaturePolicyIdentifier.appendChild(signaturePolicyId);

            signedSignaturePropertiesElement.appendChild(signaturePolicyIdentifier); //

            Element signatureProductionPlace = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "SignatureProductionPlace");
            Element cityElmnt = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "City");
            cityElmnt.appendChild(document.createTextNode(XADES122SignUtil.getCity()));
            Element postalCodeElmnt = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "PostalCode");
            postalCodeElmnt.appendChild(document.createTextNode(XADES122SignUtil.getPostalCode()));
            Element countryNameElmnt = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "CountryName");
            countryNameElmnt.appendChild(document.createTextNode(XADES122SignUtil.getCountryName()));

            signatureProductionPlace.appendChild(cityElmnt);
            signatureProductionPlace.appendChild(postalCodeElmnt);
            signatureProductionPlace.appendChild(countryNameElmnt);

            signedSignaturePropertiesElement.appendChild(signatureProductionPlace); //
            
            Element signerRole = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "SignerRole");
            Element claimedRoles = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "ClaimedRoles");
            Element claimedRoleElmt = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "ClaimedRole");
            claimedRoleElmt.appendChild(document.createTextNode(XADES122SignUtil.getClaimedRole()));
            claimedRoles.appendChild(claimedRoleElmt);
            signerRole.appendChild(claimedRoles);            

            signedSignaturePropertiesElement.appendChild(signerRole);

            signedPropertiesElement.appendChild(signedSignaturePropertiesElement);
            qualifyingPropertiesElement.appendChild(signedPropertiesElement);
            qualifyingPropertiesElement.setAttribute("Target", signatureAttributeId);

            XMLObject xmlObject = signatureFactory.newXMLObject(
                    java.util.Collections.singletonList(new DOMStructure(qualifyingPropertiesElement)),
                    signatureAttributeId + "_OID", null, null);

            DOMSignContext signContext = new DOMSignContext(privateKey, organism);

            signContext.putNamespacePrefix(XADESSignUtil.xadesNS122, "xad");
            signContext.putNamespacePrefix(javax.xml.crypto.dsig.XMLSignature.XMLNS, "ds");
            signContext.putNamespacePrefix(XADES122SignUtil.DIA_NS, "dia");
            XMLSignature xmlSignature = signatureFactory.newXMLSignature(
                    signedInfo, keyInfo,
                    java.util.Collections.singletonList(xmlObject), 
                    signatureAttributeId,
                    signatureAttributeId + "_SV");
            xmlSignature.sign(signContext);

            return document;

        } catch (MarshalException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (IOException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (CertificateEncodingException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (NoSuchAlgorithmException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (InvalidAlgorithmParameterException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        }
    }


    /**
     * Signing method for DIA_AEN
     *
     * Sample:
     * < xml version="1.0" encoding="UTF-8"?>
     <dia:accuseAEN 	Id="ID000001"
     xmlns:xad="http://uri.etsi.org/01903/v1.2.2#"
     xmlns:n1="http://www.altova.com/samplexml/other-namespace"
     xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xmlns:dia="http://xmlschema.ok-demat.com/DIA"
     xmlns:diaco="http://xmlschema.ok-demat.com/DIA-CO" >
     <dia:identification>
     <diaco:emetteur emailContact="laurence.baillet.groupeadsn@notaires.fr" contact="Laurence BAILLET" codeCRPCEN="013002"/>
     <diaco:destinataire complementCode="003" codeInseeSimplifie="34003" emailContact="roger.martin@Marie-agde.fr" contact="Roger MARTIN"/>
     <diaco:dateDeCreation>2013-03-15</diaco:dateDeCreation>
     <diaco:numerotationQuotidienne>00001</diaco:numerotationQuotidienne>
     <diaco:intituleDossier>Test CG34 - Numero 1</diaco:intituleDossier>
     </dia:identification></dia:accuseAEN>

     */
    static byte[] diaAenSign(X509Certificate certificate, PrivateKey privateKey, String xPath, String SignatureIdString, InputStream dataIs) throws XMLSignatureException {
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            documentBuilderFactory.setNamespaceAware(true);
            DocumentBuilder builder = documentBuilderFactory.newDocumentBuilder();
            Document document = builder.parse(dataIs);

            NamespaceContext nsContext = new XadesNSContext(XadesNSContext.xadesNS122);

            XPathFactory xpathFact = XPathFactory.newInstance();
            XPath xpath = xpathFact.newXPath();
            xpath.setNamespaceContext(nsContext);

            //Element idElmt = (Element) xpath.evaluate(XADES122SignUtil.ID_AEN_PATH, document, XPathConstants.NODE);
            Element idElmt = (Element) xpath.evaluate(XADES122SignUtil.ID_AEN_PATH, document, XPathConstants.NODE);
            //Element dpElmt = (Element) xpath.evaluate(orgPath, document, XPathConstants.NODE);
            //Element dpElmt = (Element) xpath.evaluate(XADES122SignUtil.ID_AEN_PATH, document, XPathConstants.NODE);
            Element dpElmt = (Element) xpath.evaluate(XADES122SignUtil.DIA_AEN_ROOT, document, XPathConstants.NODE);

            Document signedDocument = XADES122SignUtil.cosignSha256(document, idElmt, dpElmt, SignatureIdString, certificate, privateKey);
            TransformerFactory transformerFactory = javax.xml.transform.TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            transformer.transform(new DOMSource(signedDocument), new StreamResult(byteArrayOutputStream));
            return byteArrayOutputStream.toByteArray();
        } catch (TransformerException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (XPathExpressionException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (SAXException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (IOException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (ParserConfigurationException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        }
    }

    /**
     * Generic Xades cosignature method for DIA AEN stuff: mostly based on RSA-SHA256
     *
     * @param document the document to be signed
     * @param identification the identification node to be signed
     * @param organism the signing organism element
     * @param SignatureIdString the organism signature ID (see the static fields above).
     * @param certificate the signer's x509 certificate
     * @param privateKey the signer's private key
     * @return A signed document
     * @throws XMLSignatureException
     */
    static Document cosignSha256(Document document, Element identification, Element organism, String SignatureIdString, X509Certificate certificate, PrivateKey privateKey) throws XMLSignatureException
    {
        try
        {
            /**
             * The signature attribute ID was concatenation of 'DIA id' and 'organism id'
             * String signatureAttributeId = identification.getAttribute("Id") + ID_SEP + organism.getAttribute("Id");
             */
            String signatureAttributeId = SignatureIdString;
            XMLSignatureFactory signatureFactory = XMLSignatureFactory.getInstance("DOM");

            /**
             * Transforms tag:
             */
            ArrayList<Transform> transformList = new ArrayList<Transform>();
            transformList.add(signatureFactory.newTransform(Transform.ENVELOPED, (TransformParameterSpec) null));
            //transformList.add(signatureFactory.newTransform(CanonicalizationMethod.INCLUSIVE, (TransformParameterSpec) null));

            /**
             * Reference tag
             */
            List<Reference> references = new ArrayList<Reference>();
            Reference idRef = signatureFactory.newReference("#" + identification.getAttribute("Id"), signatureFactory.newDigestMethod(DigestMethod.SHA1, null));
            references.add(idRef);

            Reference orgRef = signatureFactory.newReference("#" + organism.getAttribute("Id"), signatureFactory.newDigestMethod(DigestMethod.SHA256, null),
                    Collections.singletonList(signatureFactory.newTransform(Transform.ENVELOPED, (TransformParameterSpec) null)), null, null);
            references.add(orgRef);

            /**
             * ds:signedInfo:
             *   canonicalization : http://www.w3.org/2001/10/xml-exc-c14n#
             *   sign. algorithm  : http://www.w3.org/2001/04/xmldsig-more#rsa-sha256
             */
            javax.xml.crypto.dsig.SignedInfo signedInfo = signatureFactory.newSignedInfo(
                    signatureFactory.newCanonicalizationMethod(CanonicalizationMethod.EXCLUSIVE, (javax.xml.crypto.dsig.spec.C14NMethodParameterSpec) null),
                    signatureFactory.newSignatureMethod("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256", null),
                    Collections.unmodifiableList(references),
                    signatureAttributeId + "-SignedInfo");


            /**
             * ds:keyInfo
             */
            KeyInfoFactory keyInfoFactory = signatureFactory.getKeyInfoFactory();
            X509Data x509Data = keyInfoFactory.newX509Data(java.util.Collections.singletonList(certificate));
            KeyInfo keyInfo = keyInfoFactory.newKeyInfo(java.util.Collections.singletonList(x509Data), signatureAttributeId + "-KeyInfo");

            /**
             * xad:QualifyingProperties
             */
            Element qualifyingPropertiesElement = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "QualifyingProperties");
            Element signedPropertiesElement = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "SignedProperties");

            signedPropertiesElement.setAttributeNS(null, "Id", signatureAttributeId + "-SignedProperties");
            Element signedSignaturePropertiesElement = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "SignedSignatureProperties");

            /**
             * xad:SigningTime , format "2012-09-24T16:50:12.618+02:00"
             */
            Element signingTimeElement = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "SigningTime");
            DateTime date = DateTime.now();
            DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd\'T\'HH:mm:ss.SSSZZ");
            String dateStr = date.toString(fmt);
            //java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss.SSSZ");
            //java.util.Date date = new java.util.Date(); String dateStr = dateFormat.format(date);
            signingTimeElement.appendChild(document.createTextNode(dateStr));

            signedSignaturePropertiesElement.appendChild(signingTimeElement);

            /**
             * xad:signingCertificate
             */
            Element signingCertificateElement = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "SigningCertificate");
            Element certElement = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "Cert");

            Element certDigest = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "CertDigest");
            Element digestMethod = XADESSignUtil.createElement(document, XMLSignature.XMLNS, "DigestMethod");
            Element digestValue  = XADESSignUtil.createElement(document, XMLSignature.XMLNS, "DigestValue");
            digestMethod.setAttribute("Algorithm", DigestMethod.SHA256);

            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(certificate.getEncoded());
            java.io.ByteArrayOutputStream certDigestBOS = new java.io.ByteArrayOutputStream();
            Base64.encode(messageDigest.digest(), certDigestBOS);

            digestValue.appendChild(document.createTextNode(certDigestBOS.toString()));
            certDigest.appendChild(digestMethod);
            certDigest.appendChild(digestValue);

            Element issuerSerial = XADESSignUtil.createXades122Element(document, XADESSignUtil.xadesNS122, "IssuerSerial");
            Element X509IssuerName = XADESSignUtil.createElement(document, XMLSignature.XMLNS, "X509IssuerName");
            X509IssuerName.appendChild(document.createTextNode(certificate.getIssuerX500Principal().getName()));
            Element X509SerialNumber = XADESSignUtil.createElement(document, XMLSignature.XMLNS, "X509SerialNumber");
            X509SerialNumber.appendChild(document.createTextNode(certificate.getSerialNumber().toString()));
            issuerSerial.appendChild(X509IssuerName);
            issuerSerial.appendChild(X509SerialNumber);

            certElement.appendChild(certDigest);
            certElement.appendChild(issuerSerial);
            signingCertificateElement.appendChild(certElement);
            /* */

            signedSignaturePropertiesElement.appendChild(signingCertificateElement); //

            signedPropertiesElement.appendChild(signedSignaturePropertiesElement);
            qualifyingPropertiesElement.appendChild(signedPropertiesElement);
            qualifyingPropertiesElement.setAttribute("Target", signatureAttributeId + "-QualifyingProperties");

            XMLObject xmlObject = signatureFactory.newXMLObject(
                    java.util.Collections.singletonList(new DOMStructure(qualifyingPropertiesElement)),
                    signatureAttributeId + "_OID", null, null);

            DOMSignContext signContext = new DOMSignContext(privateKey, organism);
            /**
             * namespaces:  xmlns:xd="http://www.w3.org/2000/09/xmldsig#" ?? identique à dessous???
             *              xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
             */
            signContext.putNamespacePrefix(XADESSignUtil.xadesNS122, "xad");
            signContext.putNamespacePrefix(javax.xml.crypto.dsig.XMLSignature.XMLNS, "ds");
            signContext.putNamespacePrefix(XADES122SignUtil.DIA_NS, "dia");
            XMLSignature xmlSignature = signatureFactory.newXMLSignature(
                    signedInfo, keyInfo,
                    java.util.Collections.singletonList(xmlObject),
                    signatureAttributeId,
                    signatureAttributeId + "-Value");
            xmlSignature.sign(signContext);

            return document;

        } catch (MarshalException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (IOException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (CertificateEncodingException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (NoSuchAlgorithmException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (InvalidAlgorithmParameterException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        }
    }






    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Init the variables for use in the signing process
     * return nothing
     */
    public static void setSignParameters(String apes_id, String apolicyId, String apolicyDescription, String apolicyDigestValue, String aspuri,
            String acity, String apostalCode, String acountryName, String aclaimedRole) {
        if (apes_id.equalsIgnoreCase("null")) {
            pesID = "";
        } else {
            pesID = apes_id;
        }
        policyID = apolicyId;
        policyDescryption = apolicyDescription;
        policyDigestValue = apolicyDigestValue;
        SPURI = aspuri;
        city = acity;
        postalCode = apostalCode;
        countryName = acountryName;
        claimedRole = aclaimedRole;
    }

    // TODO : fulfill the following methods
    // Note that this cannot be performed without a further knowledge
    // of the intended usages.
    // A good way to correctly fulfill the following functions is probably
    // a configuration based implementation 
    private static String getPolicyIdentifierID()
    {
        return (policyID==null) ? "" : policyID;
    }

    private static String getPolicyIdentifierDescription()
    {
        return (policyDescryption==null) ? "" : policyDescryption;
    }

    private static String getPolicyDigest()
    {
        return (policyDigestValue==null) ? "" : policyDigestValue;
    }

    private static String getSPURI()
    {
        return (SPURI==null) ? "" : SPURI;
    }

    private static String getCity()
    {
        return (city==null) ? "" : city;
    }

    private static String getClaimedRole()
    {
        return (claimedRole==null) ? "" : claimedRole;
    }

    private static String getPostalCode()
    {
        return (postalCode==null) ? "" : postalCode;
    }

    private static String getCountryName()
    {
        return (countryName==null) ? "" : countryName;
    }

}

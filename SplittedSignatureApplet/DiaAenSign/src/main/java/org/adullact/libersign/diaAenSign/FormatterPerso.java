/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.libersign.diaAenSign;

import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 * Created with IntelliJ IDEA.
 * User: svast
 * Date: 20/10/13
 * Time: 17:17
 *
 */
public class FormatterPerso extends Formatter {

    @Override
    public String format(LogRecord record) {
        StringBuilder s = new StringBuilder(1000);
        s.append(formatMessage(record)).append("\n");
        return s.toString();
    }
}

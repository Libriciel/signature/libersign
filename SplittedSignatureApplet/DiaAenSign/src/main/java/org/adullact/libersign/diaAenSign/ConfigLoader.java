/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.libersign.diaAenSign;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.String;
import java.util.Properties;

/**
 * 
 * @author Nicolas LE GRAND
 * @author Stephane Vast
 */
public class ConfigLoader {

    Properties cf;
    private static String endPoint, user, pass, trustStorePass, trustStorePath,keyStorePath, keyStorePass,
            typeTechnique, sousType, emailEmetteur, visibilite, datelimite, outputPath;
    private static String dossierID;

    /**
     * Constructeur par defaut
     *
     * @param path chemin d'acces au fichier de configuration (conf.cf)
     */
    public ConfigLoader(String path) {
        // Chargement de l'ensemble de la config
        // TO DO .. Tester la bonne config & remonter erreur

        // Ouverture du fichier de configuration
    	cf = new Properties();
        FileInputStream in;
        try {
            in = new FileInputStream(path);
            cf.load(in);
            in.close();
        } catch (FileNotFoundException e) {
            System.out.println("Impossible de trouver le fichier " + System.getProperty("user.dir") + "/" + path);
            System.exit(0);
        } catch (IOException e) {
            System.out.println("Probleme lors de l'ouverture du fichier " + System.getProperty("user.dir") + "/" + path);
            System.exit(0);
        }

        // Initialisation des paramètres de connexion au serveur i-Parapheur
        endPoint = cf.getProperty("endPoint");
        user = cf.getProperty("user");
        pass = cf.getProperty("pass");
        trustStorePass = cf.getProperty("trustStorePass");
        trustStorePath = cf.getProperty("trustStorePath");
        keyStorePass = cf.getProperty("keyStorePass");
        keyStorePath = cf.getProperty("keyStorePath");
        
        // Verification presence des fichiers
        verifPresenceFichiersObligatoires();

    }
    private void verifPresenceFichiersObligatoires() {
        if (!(isFileExists("iparapheur.wsdl")
                && isFileExists(keyStorePath)
                && isFileExists(trustStorePath))) {
            System.exit(0);
        }
    }

    public boolean isFileExists(String filePath) {
        File f;
        f = new File(filePath);
        if (!f.exists()) {
            System.out.println("Impossible de trouver le fichier obligatoire " + filePath);
            return false;
        }
        return true;
    }

    public String getType() {
        return typeTechnique;
    }

    public String getOutputPath() {
        return outputPath;
    }

    public String getEndPoint() {
        return endPoint;
    }

    public String getUser() {
        return user;
    }

    public String getPass() {
        return pass;
    }

    public String getTrustStorePass() {
        return trustStorePass;
    }

    public String getTrustStorePath() {
        return trustStorePath;
    }

    public String getKeyStorePath() {
        return keyStorePath;
    }

    public String getKeyStorePass() {
        return keyStorePass;
    }

	public String getDatelimite() {
		return datelimite;
	}

	public String getDossierID() {
		return dossierID;
	}

	public String getSousType() {
		return sousType;
	}

	public String getEmailEmetteur() {
		return emailEmetteur;
	}

	public String getVisibilite() {
		return visibilite;
	}

}

/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.libersign.diaAenSign;

// import org.adullact.libersign.util.signature.XadesDigestComputer;
// import org.adullact.libersign.util.signature.XadesStreamDigest;
import nu.xom.ParsingException;
import nu.xom.ValidityException;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import sun.tools.jar.Main;
import xades4j.XAdES4jException;
import xades4j.providers.CertificateValidationProvider;
import xades4j.providers.impl.PKIXCertificateValidationProvider;
import xades4j.verification.*;
import xades4j.utils.*;
import javax.xml.crypto.dsig.XMLSignatureException;
import java.io.*;
import java.security.*;
import java.security.cert.*;
import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

/**
 * Outil de signature XAdES en ligne de commande.
 * Objectif: creer de la signature "server-side" pour les DIA_AEN
 *
 * @author Stephane Vast - Adullact Projet
 */
public class DiaAenSigner {

    static String logFilePath = "DiaAenSigner.log";

    /**
     * xPathPourSignature: chemin xPATH du bloc à signer.
     */
    static String xPathPourSignature = ".";

    // static ProxyTSA proxy;
    // static ConfigLoader configLoader;

    public static void main( String[] args ){

        MesOptions options = new MesOptions();
        CmdLineParser parser = new CmdLineParser(options);

        /**
         * Analyse des parametres de la ligne de commande
         */
        try {
            parser.parseArgument(args);
            if (options.isHelp()) {
                parser.setUsageWidth(80);
                parser.printUsage(System.out);
                return;
            }
        }
        catch (CmdLineException e) {
            System.out.println("Probleme sur les parametres, cause: " + e.getLocalizedMessage());
     	    parser.setUsageWidth(80);
            parser.printUsage(System.out);
            return;
        }

        // ##### Creation du log
        Logger logger;
        logger = Logger.getLogger("LogTout");
        try {
            FileHandler fh = new FileHandler(logFilePath);
            logger.addHandler(fh);
            logger.setUseParentHandlers(false);
            fh.setFormatter(new FormatterPerso());
        } catch (SecurityException e) {
            e.printStackTrace();
            return;
        } catch (IOException e1) {
            e1.printStackTrace();
            return;
        }

        /**
         * ##### Chargement du certificat
         */
        KeyStore keyStore;
        X509Certificate cert = null;
        PrivateKey theKey = null;
        // String certAlias;
        try {
            keyStore = KeyStore.getInstance("PKCS12");
            InputStream is = new FileInputStream(options.getCertFile());
            keyStore.load(is, options.getCertPassword().toCharArray());
            Enumeration<String> certAliases = keyStore.aliases();
            while (certAliases.hasMoreElements()) {
                String alias = certAliases.nextElement();
                Certificate tempCert = keyStore.getCertificate(alias);
                if (tempCert instanceof java.security.cert.X509Certificate) {
                    X509Certificate xcert = (X509Certificate) tempCert;
                    xcert.checkValidity();
                    if (xcert.getBasicConstraints() != -1) {
                        logger.warning(xcert.getSubjectX500Principal().getName() + " est un certificat d'autorite de certification... ignoré.");
                    } else {
                        cert = (X509Certificate) tempCert;
                        // certAlias = alias;
                        if (keyStore.getKey(alias, options.getCertPassword().toCharArray()) != null) {
                            theKey = (PrivateKey) keyStore.getKey(alias, options.getCertPassword().toCharArray());
                        }
                    }
                }
            }
        } catch (IOException e) {
            logger.severe("Impossible de lire le certificat cité en paramètre.");
            return;
        } catch (NoSuchAlgorithmException e) {
            logger.severe("Impossible d'initialiser le module de Cryptographie");
            return;
        } catch (CertificateException e) {
            logger.severe("Probleme avec le chargement du certificat, probablement invalide.");
            return;
        } catch (KeyStoreException e) {
            logger.severe("Impossible de charger le certificat.");
            return;
        } catch (UnrecoverableKeyException e) {
            logger.severe("Le mot de passe du certificat est erroné.");
            return;
        }

        /**
         * ##### Chargement de la configuration
         */
        // configLoader = new ConfigLoader(options.getConfFile().getAbsolutePath());

        /**
         * ##### Initialisation du proxy pour l'horodatage. POSTPONED !
         */
/*        proxy = new ProxyTSA(configLoader.getEndPoint(),
                configLoader.getUser(), configLoader.getPass(),
                configLoader.getTrustStorePath(), configLoader.getTrustStorePass(),
                configLoader.getKeyStorePath(), configLoader.getKeyStorePass());*/
        // ##### SANITY CHECK to be done: are we able to connect to the TSA ?

        /**
         * 1- calcul du hash
         */
        InputStream in;
        try {
            in = new FileInputStream(options.getXmlInput());
        } catch (FileNotFoundException e) {
            System.out.println("Fichier impossible a lire: " + options.getXmlInput().getAbsolutePath());
            logger.severe("Impossible de lire le fichier " + options.getXmlInput().getAbsolutePath());
            return;
        }
        // XadesStreamDigest leDigest = XadesDigestComputer.computeDigest(in, XadesDigestComputer.ALG_SHA256, xPathPourSignature);

        /**
         * 2- signature
         */
        if (cert == null || theKey == null) {
            logger.severe("Impossible de signer, pas de certificat trouvé.");
            return;
        }
        byte[] xmlSigne;
        try {
            xmlSigne = XADES122SignUtil.diaAenSign(cert, theKey, xPathPourSignature, "S0", in);
        } catch (XMLSignatureException e) {
            logger.severe("Le XML n'a pas pu etre signé, cause: " + e.getLocalizedMessage());
            return;
        }

        /**
         * 3- demande de jeton d'horodatage, modif de la signature. POSTPONED !
         */
/*
        String retour = ProxyTSA.appelCreerDossier(options.getType(), options.getsType(), options.getEmailEmetteur(),
                options.getIdDossier(), options.getDocumentPrincipal().getAbsolutePath(),
                options.getNomDocPrincipal(),
                options.getDocumentVisu().getAbsolutePath(), options.getVisibilite());
*/

        /**
         * 3,5-  verification de la signature
         */                                                                                       /*
        try {
            FileSystemDirectoryCertStore certStore = null;
            certStore = new FileSystemDirectoryCertStore(".");
            CertStore certs = CertStore.getInstance("Collection", new CollectionCertStoreParameters(new ArrayList()), "SUN");

            InputStream kst_pwd_is = Main.class.getResourceAsStream("/ac-truststore.password");
            ByteArrayOutputStream ksPasswordWriter = new ByteArrayOutputStream();
            int readed;
            byte[] data = new byte[256];
            while ((readed = kst_pwd_is.read(data)) != -1) {
                ksPasswordWriter.write(data, 0, readed);
            }
            String ksPassword = new String(ksPasswordWriter.toByteArray());

            KeyStore keystore = KeyStore.getInstance("JKS");

            InputStream kst_is = Main.class.getResourceAsStream("/ac-truststore.jks");
            keystore.load(kst_is, ksPassword.toCharArray());

            // KeyStore trustAnchors = ...;
            // CertificateValidationProvider certValidator = new PKIXCertificateValidationProvider(trustAnchors, false, certStore.getStore());
            PKIXCertificateValidationProvider cvp = new PKIXCertificateValidationProvider(keystore, false, certs);

            XadesVerificationProfile p = new XadesVerificationProfile(cvp);
            XadesVerifier verifier = p.newVerifier();

            // java.io.ByteArrayOutputStream baStream = new java.io.ByteArrayOutputStream();
            nu.xom.Builder builder = new nu.xom.Builder();
            // nu.xom.canonical.Canonicalizer canonicalizer = new nu.xom.canonical.Canonicalizer(baStream, nu.xom.canonical.Canonicalizer.EXCLUSIVE_XML_CANONICALIZATION);
            nu.xom.Document document = builder.build(new ByteArrayInputStream(xmlSigne));
            nu.xom.Element  root     = document.getRootElement();
            //  Element sigElem = ...;
            XAdESVerificationResult r = verifier.verify((org.w3c.dom.Element)root, null);

            System.out.println(r.getSignatureForm());
            System.out.println(r.getSignatureAlgorithmUri());
            System.out.println(r.getSignedDataObjects().size());
            System.out.println(r.getQualifyingProperties().all().size());
        } catch (CertificateException e) {
            logger.severe("probleme de chargement de certStore");
        } catch (CRLException e) {
            logger.severe("probleme de CRL: " + e.getLocalizedMessage());
        } catch (NoSuchAlgorithmException e) {
            logger.severe("probleme de validation de certificat" + e.getLocalizedMessage());
        } catch (NoSuchProviderException e) {
            logger.severe("probleme de provider de crypto" + e.getLocalizedMessage());
        } catch (XadesProfileResolutionException e) {
            logger.severe("probleme de profilage XADES" + e.getLocalizedMessage());
        } catch (XAdES4jException e) {
            logger.severe("probleme de verification de signature XAdES: " + e.getLocalizedMessage());
        } catch (ValidityException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (ParsingException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (KeyStoreException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

   */
        /**
         * 4- Ecriture du fichier enfin signé...
         */
        OutputStream out;
        try {
            out = new FileOutputStream(options.getXmlOutput());
            out.write(xmlSigne);
            out.flush();
            out.close();
        } catch (FileNotFoundException e) {
            System.out.println("Fichier impossible a atteindre: " + options.getXmlInput().getAbsolutePath());
            return;
        } catch (IOException e) {
            logger.severe("Impossible d'ecrire le XML signé");
            return;
        }
        logger.info("Fichier signé avec succès.");
    }

}

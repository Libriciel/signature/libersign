/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.libersign.diaAenSign;

import java.io.File;
import java.util.List;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.Option;

public class MesOptions {

	@Option(name = "-h", aliases = {"--help"}, usage = "Affiche l'aide")
    private boolean help;

    @Option(name = "-c", aliases = {"--config"}, metaVar = "File", usage = "fichier de configuration. Par defaut conf.cf")
    private File confFile;

    @Option(name = "-i", aliases = {"--inputFile"}, metaVar = "path", usage = "fichier XML a signer", required = true)
    private File xmlInput;

    @Option(name = "-o", aliases = {"--outputFile"}, metaVar = "FILE", usage = "fichier genere, contient la signature", required = true)
    private File xmlOutput;
  
    @Option(name = "-k", aliases = {"--pkcs12certFile"}, metaVar = "FILE", usage = "certificat format PKCS12", required = true)
    private File certFile;
    
    @Option(name = "-p", aliases = {"--pkcs12password"}, metaVar = "String", usage = "mot de passe du certificat", required = true)
    private String certPassword;
    


	@Argument
    private List<String> arguments;
    
	public boolean isHelp() {
		return help;
	}

	public File getConfFile() {
		return confFile;
	}

	public File getXmlInput() {
		return xmlInput;
	}

	public File getXmlOutput() {
		return xmlOutput;
	}

    public File getCertFile() {
        return certFile;
    }

	public String getCertPassword() {
		return certPassword;
	}

	public List<String> getArguments() {
		return arguments;
	}
}

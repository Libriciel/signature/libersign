/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.adullact.libersignclient;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class InjecteSignatureXML {
    public static void main( String[] args ) throws IOException
    {
        String xmlpath = args[0];
        if (args[0]==null){
            System.out.println("Merci de preciser le chemin vers le fichier xml a signer");
            System.exit(0);
        }

        String xmlsignpath = args[1];
        if (args[1]==null){
            System.out.println("Merci de preciser le chemin vers le fichier xml de la signature");
            System.exit(0);
        }
        
        //Variables
//        String xmlpath = "/home/yann/Work/asalae/ATR_CGINIT_101.xml";
        File xmlfile = new File(xmlpath);
//        String xmlsignpath = "/home/yann/Work/asalae/signature.xml";
        File xmlsignfile = new File(xmlsignpath);
        String xmloutput = xmlpath.concat(".signed");
        String xpath = ".";

        //Appel WS
        InterfaceLibersignService service = new InterfaceLibersignService();
        InterfaceLibersign port = service.getInterfaceLibersignSoap11();

        //Renvoie l'encodage de xmlpath
        InputStreamReader isr = new InputStreamReader (new FileInputStream(xmlpath)) {
        @Override
            public int read() throws IOException {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };
        String encoding = isr.getEncoding();
        if (encoding.equals("UTF8")) {
            encoding="UTF-8";
        }

        //Encodage fichier XML en base64
        BASE64Encoder enc = new BASE64Encoder();

        byte[] byt = getArrayByteFromFile(xmlfile);
        String encoded = enc.encode(byt);
        
        //Encodage signature en base64
        byte[] signbyt = getArrayByteFromFile(xmlsignfile);
        String signencoded = enc.encode(signbyt);

        //Conversion en TypeXML
        TypeXML fluxSource = new TypeXML();
        fluxSource.setValue(encoded.getBytes());
        TypeXML signature = new TypeXML();
        signature.setValue(signencoded.getBytes());

        //
        //Requete
        //
        InjecteSignatureXmlRequest req = new InjecteSignatureXmlRequest();

        req.setFluxSource(fluxSource);
        req.setSignature(signature);
        req.setEncoding(encoding);
        req.setXPathPourSignature(xpath);

        System.out.println("Requete:");
        System.out.println("bytes = "+req.fluxSource.value);
        System.out.println("signature = "+req.signature.value);
        System.out.println("encoding = "+req.encoding);
        System.out.println("xpath = "+req.xPathPourSignature);

        //
        //Reponse
        //
        InjecteSignatureXmlResponse resp = port.injecteSignatureXml(req);

        byte[] fluxSigneEncoded = resp.getFluxSigne().value;

        //Decodage
        BASE64Decoder dec = new BASE64Decoder();
        InputStream in = new ByteArrayInputStream(resp.getFluxSigne().value);
        byte[] fluxSigneDecoded = dec.decodeBuffer(in);
        writeFile(fluxSigneDecoded, xmloutput);

        System.out.println("-------------------------------------------------");
        System.out.println("Reponse:");
        System.out.println("fluxSigne:");
        System.out.println(new String(fluxSigneDecoded));
        System.out.println("Code retour: " + resp.getMessageRetour().getCodeRetour() + " / severite: " + resp.getMessageRetour().getSeverite());
        System.out.println("messageRetour: " + resp.getMessageRetour().getMessage());
        }

    public static byte[] getArrayByteFromFile(File xmloutput) throws IOException {

            final long length = xmloutput.length();
            if (length > Integer.MAX_VALUE) { // + de 2 Go
                    throw new IOException("File too big");
            }

            byte[] data = new byte[(int) length];

            final FileInputStream in = new FileInputStream(xmloutput);
            try {

                    int off = 0;	// Position de lecture
                    int len = data.length;	// Nombre de bytes restant à lire
                    int read;		// Nb de byte lu

                    do {
                            read = in.read(data, off, len);
                            if (read > 0) {
                                    off += read;
                                    len -= read;
                            }
                    } while (read >= 0 && len > 0);

            } finally {
                    in.close();
            }
            return data;
    }

    private static void writeFile(byte[] fluxSigne, String xmloutput) throws IOException{
        OutputStream out = new FileOutputStream(xmloutput);
        out.write(fluxSigne);
        out.close();
    }
}

/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.adullact.libersignclient;

import sun.misc.BASE64Encoder;
import sun.misc.BASE64Decoder;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

public class GetShaXml
{
    public static void main( String[] args ) throws IOException
    {
        String xmlpath = args[0];
//        for (String s: args) {
//            xmlpath = s;
//            if (s==null){
//                System.out.println("Merci de preciser le chemin vers le fichier xml");
//                System.exit(0);
//            }
//        }
        
        //Variables
//        String xmlpath = "/home/yann/Work/asalae/ATR_CGINIT_101.xml";
        File xmlfile = new File(xmlpath);
        String xpath = ".";

        //Appel WS
        InterfaceLibersignService service = new InterfaceLibersignService();
        InterfaceLibersign port = service.getInterfaceLibersignSoap11();

        //Encodage en base64
        byte[] byt = getArrayByteFromFile(xmlfile);
        BASE64Encoder enc = new BASE64Encoder();
        String encoded = enc.encode(byt);

        //Conversion en TypeXML
        TypeXML fluxSource = new TypeXML();
        fluxSource.setValue(encoded.getBytes());

        //
        //Requete
        //
        GetShaXmlRequest req = new GetShaXmlRequest();

        req.setFluxSource(fluxSource);
        req.setXPathPourSignature(xpath);

        System.out.println("Requete:");
        System.out.println("bytes = "+req.fluxSource.value);
        System.out.println("xpath = "+req.xPathPourSignature);

        //
        //Reponse
        //
        GetShaXmlResponse resp = port.getShaXml(req);

        //Decodage
        BASE64Decoder dec = new BASE64Decoder();
        InputStream in = new ByteArrayInputStream(resp.getCondensat().value);
        byte[] condensatdecoded = dec.decodeBuffer(in);

        System.out.println("-------------------------------------------------");
        System.out.println("Reponse:");
        System.out.println("sha1: " + new String(condensatdecoded));
//        System.out.println("sha1_2: " + new String(condensatdecoded));
        System.out.println("fluxId: " + resp.getFluxID());
        System.out.println("Code retour " + resp.getMessageRetour().getCodeRetour() + " / severite: " + resp.getMessageRetour().getSeverite());
        System.out.println("messageRetour: " + resp.getMessageRetour().getMessage());

    }

    public static byte[] getArrayByteFromFile(File xmloutput) throws IOException {

            final long length = xmloutput.length();
            if (length > Integer.MAX_VALUE) { // + de 2 Go
                    throw new IOException("File too big");
            }

            byte[] data = new byte[(int) length];

            final FileInputStream in = new FileInputStream(xmloutput);
            try {

                    int off = 0;	// Position de lecture
                    int len = data.length;	// Nombre de bytes restant à lire
                    int read;		// Nb de byte lu

                    do {
                            read = in.read(data, off, len);
                            if (read > 0) {
                                    off += read;
                                    len -= read;
                            }
                    } while (read >= 0 && len > 0);

            } finally {
                    in.close();
            }

            return data;
    }

    private static String readFileAsString(String filePath)
    throws java.io.IOException{
        StringBuffer fileData = new StringBuffer(1000);
        BufferedReader reader = new BufferedReader(
                new FileReader(filePath));
        char[] buf = new char[1024];
        int numRead=0;
        while((numRead=reader.read(buf)) != -1){
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
            buf = new char[1024];
        }
        reader.close();
        return fileData.toString();
    }

}

/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.adullact.libersign.server;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringBufferInputStream;
import java.security.NoSuchAlgorithmException;
import javax.jws.WebService;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.adullact.libersign.util.signature.DigestComputer;
import org.adullact.libersign.util.signature.PesDigest;
import org.adullact.libersign.util.signature.xades;
import org.adullact.spring_ws.libersign._1.GetShaXmlResponse;
import org.adullact.spring_ws.libersign._1.InjecteSignatureXmlResponse;
import org.adullact.spring_ws.libersign._1.MessageRetour;
import org.adullact.spring_ws.libersign._1.TypeCondensat;
import org.adullact.spring_ws.libersign._1.TypeXML;
import org.xml.sax.InputSource;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 *
 * @author yann
 */

@WebService(serviceName = "InterfaceLibersignService", portName = "InterfaceLibersignSoap11", endpointInterface = "org.adullact.spring_ws.libersign._1.InterfaceLibersign", targetNamespace = "http://www.adullact.org/spring-ws/libersign/1.0", wsdlLocation = "WEB-INF/wsdl/SplittedSignature-service.wsdl")
public class InterfaceLibersignImpl {

    public org.adullact.spring_ws.libersign._1.GetShaXmlResponse getShaXml(org.adullact.spring_ws.libersign._1.GetShaXmlRequest getShaXmlRequest) throws NoSuchAlgorithmException, IOException {
        //
        //Reponse
        //
        GetShaXmlResponse resp = new GetShaXmlResponse();
        TypeCondensat condensat = new TypeCondensat();
        MessageRetour messageRetour = new MessageRetour();

        //
        //Recuperation des arguments de la requete
        //
        TypeXML fluxSource = getShaXmlRequest.getFluxSource();
        String xPathPourSignature = getShaXmlRequest.getXPathPourSignature();

        //
        //Phase de controle des inputs
        //
        if (fluxSource==null){
            String msgRetour="Le fluxSource est null";
            resp.setMessageRetour(properExit(msgRetour));
            resp.setFluxID("null");
            condensat.setValue("".getBytes());
            resp.setCondensat(condensat);
            return resp;
        }
        if (xPathPourSignature==null){
            String msgRetour="xPath est null, la valeur par défaut est \".\"";
            resp.setMessageRetour(properExit(msgRetour));
            resp.setFluxID("null");
            condensat.setValue("".getBytes());
            resp.setCondensat(condensat);
            return resp;
        }

        //Condensat
        BASE64Decoder dec = new BASE64Decoder();
        String fluxSourceEncoded = new String(fluxSource.getValue());
        byte[] fluxSourceDecoded = dec.decodeBuffer(fluxSourceEncoded);
        String fluxSourceDecodedString = new String(fluxSourceDecoded);

        //Teste l'encodage de fluxSource
        System.out.println("Tests sur le fluxSource");
        InputStreamReader isr = new InputStreamReader (new StringBufferInputStream (fluxSourceDecodedString));
        String encodingDetected=isr.getEncoding();
        if (encodingDetected.equals("UTF8")) {
            encodingDetected="UTF-8";
            System.out.println("Encodage detecte " + encodingDetected);
        }
        else if (encodingDetected.equals("UTF-8")) {
            System.out.println("Encodage detecte " + encodingDetected);
        }
        else {
            String msgRetour="Le fluxSource doit être encodé en UTF-8 (detecte " + encodingDetected + ")";
            resp.setMessageRetour(properExit(msgRetour));
            condensat.setValue("".getBytes());
            resp.setCondensat(condensat);
            return resp ;
        }

        //Teste si le fluxSource est un XML bien forme
        File tmpXMLFile = new File("tmpXML");
        FileWriter out = new FileWriter(tmpXMLFile);
        out.write(fluxSourceDecodedString);
        out.close();

        try {
          // Create a new factory to create parsers
          DocumentBuilderFactory dBF = DocumentBuilderFactory.newInstance();
          // Use the factory to create a parser (builder) and use
          // it to parse the document.
          DocumentBuilder builder = dBF.newDocumentBuilder();
          // builder.setErrorHandler(new MyErrorHandler());
          InputSource is = new InputSource("tmpXML");
          org.w3c.dom.Document doc = builder.parse(is);
          System.out.println("Le fluxSource est un document XML bien forme");
          tmpXMLFile.delete();
        }
        catch (Exception e) {
          tmpXMLFile.delete();
          String msgRetour="Le fluxSource n'est pas un document XML bien forme";
          resp.setMessageRetour(properExit(msgRetour));
          condensat.setValue("".getBytes());
          resp.setCondensat(condensat);
          return resp ;
        }

        InputStream in = new ByteArrayInputStream(fluxSourceDecoded);
        PesDigest pes = DigestComputer.computeDigest(in, xPathPourSignature);
        in.close();

        BASE64Encoder enc = new BASE64Encoder();
        System.out.println("Calcul et encodage du condensat");
        byte[] condensatDecoded = hashToString(pes.getDigest()).getBytes();
        byte[] condensatEncodedBytes = enc.encode(condensatDecoded).getBytes();
        condensat.setValue(condensatEncodedBytes);

        //FluxID
        System.out.println("Recuperation du fluxId");
        String fluxID = pes.getId();

        //Message retour
        messageRetour.setCodeRetour("OK");
        messageRetour.setMessage(new String(condensatDecoded));
        messageRetour.setSeverite("INFO");

        resp.setCondensat(condensat);
        resp.setFluxID(fluxID);
        resp.setMessageRetour(messageRetour);
        return resp;
    }

    public org.adullact.spring_ws.libersign._1.InjecteSignatureXmlResponse injecteSignatureXml(org.adullact.spring_ws.libersign._1.InjecteSignatureXmlRequest injecteSignatureXmlRequest) throws IOException {

        //
        //Reponse
        //
        InjecteSignatureXmlResponse resp = new InjecteSignatureXmlResponse();
        TypeXML fluxSigne = new TypeXML();
        MessageRetour messageRetour = new MessageRetour();

        //
        //Recuperation des arguments de la requete
        //
        TypeXML fluxSource = injecteSignatureXmlRequest.getFluxSource();
        TypeXML signature = injecteSignatureXmlRequest.getSignature();
        String xPathPourSignature = injecteSignatureXmlRequest.getXPathPourSignature();
        String encoding = injecteSignatureXmlRequest.getEncoding();

        //
        //Phase de controle des inputs
        //
        if (fluxSource==null){
            String msgRetour="Le fluxSource est null";
            resp.setMessageRetour(properExit(msgRetour));
            fluxSigne.setValue("".getBytes());
            resp.setFluxSigne(fluxSigne);
            return resp;
        }
        if (signature==null){
            String msgRetour="La signature est null";
            resp.setMessageRetour(properExit(msgRetour));
            fluxSigne.setValue("".getBytes());
            resp.setFluxSigne(fluxSigne);
            return resp;
        }
        if (xPathPourSignature==null){
            String msgRetour="xPath est null, la valeur par défaut est \".\"";
            resp.setMessageRetour(properExit(msgRetour));
            fluxSigne.setValue("".getBytes());
            resp.setFluxSigne(fluxSigne);
            return resp;
        }
         if (encoding==null){
            String msgRetour="encoding est null";
            resp.setMessageRetour(properExit(msgRetour));
            fluxSigne.setValue("".getBytes());
            resp.setFluxSigne(fluxSigne);
            return resp;
        }

        //Decodage XML base64
        BASE64Decoder dec = new BASE64Decoder();

        String fluxSourceEncoded = new String(fluxSource.getValue());
        byte[] fluxSourceDecoded = dec.decodeBuffer(fluxSourceEncoded);
        String fluxSourceDecodedString = new String(fluxSourceDecoded);

        //Teste l'encodage de fluxSource
        System.out.println("Tests sur le fluxSource");
        InputStreamReader isr = new InputStreamReader (new StringBufferInputStream (fluxSourceDecodedString));
        String encodingDetected=isr.getEncoding();
        if (encodingDetected.equals("UTF8")) {
            encoding="UTF-8";
            System.out.println("Encodage detecte " + encoding);
        }
        else if (encodingDetected.equals("UTF-8")) {
            System.out.println("Encodage detecte " + encoding);
        }
        else {
            String msgRetour="Le fluxSource doit être encodé en UTF-8 (detecte " + encodingDetected + ")";
            resp.setMessageRetour(properExit(msgRetour));
            fluxSigne.setValue("".getBytes());
            resp.setFluxSigne(fluxSigne);
            return resp ;
        }

        //Teste si le fluxSource est un XML bien forme
        File tmpXMLFile = new File("tmpXML");
        FileWriter out = new FileWriter(tmpXMLFile);
        out.write(fluxSourceDecodedString);
        out.close();

        try {
          // Create a new factory to create parsers
          DocumentBuilderFactory dBF = DocumentBuilderFactory.newInstance();
          // Use the factory to create a parser (builder) and use
          // it to parse the document.
          DocumentBuilder builder = dBF.newDocumentBuilder();
          // builder.setErrorHandler(new MyErrorHandler());
          InputSource is = new InputSource("tmpXML");
          org.w3c.dom.Document doc = builder.parse(is);
          System.out.println("Le fluxSource est un document XML bien forme");
          tmpXMLFile.delete();
        }
        catch (Exception e) {
          tmpXMLFile.delete();
          String msgRetour="Le fluxSource n'est pas un document XML bien forme";
          resp.setMessageRetour(properExit(msgRetour));
          fluxSigne.setValue("".getBytes());
          resp.setFluxSigne(fluxSigne);
          return resp ;
        }

        //signature
        String signatureEncoded = new String(signature.getValue());
        byte[] signatureDecoded = dec.decodeBuffer(signatureEncoded);
        String signatureDecodedString = new String(signatureDecoded);

        //Renvoie l'encodage de la signature
        System.out.println("Tests sur la signature");
        InputStreamReader isr2 = new InputStreamReader (new StringBufferInputStream (signatureDecodedString));
        String encodingDetectedSig=isr2.getEncoding();
        if (encodingDetectedSig.equals("UTF8")) {
            encoding="UTF-8";
            System.out.println("Encodage detecte " + encoding);
        }
        else if (encodingDetectedSig.equals("UTF-8")) {
            System.out.println("Encodage detecte " + encoding);
        }
        else {
            String msgRetour="La signature doit être encodée en UTF-8 (detecte " + encodingDetectedSig + ")";
            resp.setMessageRetour(properExit(msgRetour));
            fluxSigne.setValue("".getBytes());
            resp.setFluxSigne(fluxSigne);
            return resp ;
        }

        //Teste si la signature est un document XML bien forme
        File tmpXMLSig = new File("tmpXMLSig");
        FileWriter out2 = new FileWriter(tmpXMLSig);
        out2.write(signatureDecodedString);
        out2.close();

        try {
          // Create a new factory to create parsers
          DocumentBuilderFactory dBF2 = DocumentBuilderFactory.newInstance();
          // Use the factory to create a parser (builder) and use
          // it to parse the document.
          DocumentBuilder builder2 = dBF2.newDocumentBuilder();
          // builder.setErrorHandler(new MyErrorHandler());
          InputSource is2 = new InputSource("tmpXMLSig");
          org.w3c.dom.Document doc2 = builder2.parse(is2);
          System.out.println("La signature est un document XML bien forme");
          tmpXMLSig.delete();
        }
        catch (Exception e) {
          tmpXMLSig.delete();
          String msgRetour="La signature n'est pas un document XML bien forme";
          resp.setMessageRetour(properExit(msgRetour));
          fluxSigne.setValue("".getBytes());
          resp.setFluxSigne(fluxSigne);
          return resp ;
        }

        System.out.println(fluxSourceDecodedString);
        System.out.println(signatureDecodedString);
        System.out.println(xPathPourSignature);
        System.out.println(encoding);

        //Injecte Signature
        System.out.println("Debut de l'injection");
        String fluxSigneString = xades.injectXadesSigIntoXml(fluxSourceDecodedString, signatureDecodedString, xPathPourSignature, encoding);

        //conversion puis encodage fluxSigne
        byte[] fluxSigneByte = fluxSigneString.getBytes();
        BASE64Encoder enc = new BASE64Encoder();

        byte[] fluxSigneEncoded = enc.encode(fluxSigneByte).getBytes();

        //fluxSigne
        fluxSigne.setValue(fluxSigneEncoded);

        //Message retour
        messageRetour.setCodeRetour("OK");
        messageRetour.setMessage("Tout s'est bien passe");
        messageRetour.setSeverite("INFO");

        resp.setFluxSigne(fluxSigne);
        resp.setMessageRetour(messageRetour);
        return resp;
    }

    public static String hashToString(byte[] hash) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hash.length; i++) {
            int v = hash[i] & 0xFF;
            if(v < 16) {
                sb.append("0");
            }
        sb.append(Integer.toString(v, 16));
        }
        return sb.toString();
    }

    private MessageRetour properExit(String msgRetour) {        
            System.out.println(msgRetour);
            MessageRetour messageRetour = new MessageRetour();            
            messageRetour.setCodeRetour("KO");
            messageRetour.setMessage(msgRetour);
            messageRetour.setSeverite("SEVERE");
            return messageRetour;
    }
}

/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.libersign.util.signature;

import java.util.logging.Level;

import org.apache.log4j.Logger;
import org.apache.xerces.parsers.DOMParser;
import org.apache.xml.security.c14n.Canonicalizer;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.apache.xpath.XPathAPI;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.StringReader;
import java.io.StringWriter;

import org.xml.sax.InputSource;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;

//import com.sun.org.apache.xml.internal.security.c14n.Canonicalizer;
//import com.sun.org.apache.xerces.internal.parsers.DOMParser;
//import com.sun.org.apache.xml.internal.serialize.OutputFormat;
//import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
//import com.sun.org.apache.xpath.internal.XPathAPI;

/**
 * @author svast
 * Signature XAdES. 
 * C'est compliqué: signature, co-signature, contre-signature.
 * Il faut un chemin XPath pour savoir où poser la signature dans le doc XML
 *
 */
public class xades
{
    private static Logger logger = Logger.getLogger(xades.class);
    
    public xades()
    {
	// constructeur vide
    }
    
    public static String injectXadesSigIntoXml(String strXml, String strSig, String strXpath, String pXmlencoding)
    {
        System.out.println("injectXadesSigIntoXml: BEGIN");
        System.out.println(strXml);
	String l_str = null;
	DOMParser domParser = new DOMParser();
        Document l_xmlDocument;
        Document l_sigDocument;
        logger.debug("injectXadesSigIntoXml: DEBUT");

//        //Modifs
//        org.apache.xml.security.Init.init();
//        try {
//            domParser.setFeature("http://xml.org/sax/features/namespaces", true);
//        } catch (SAXNotRecognizedException ex) {
//            java.util.logging.Logger.getLogger(xades.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (SAXNotSupportedException ex) {
//            java.util.logging.Logger.getLogger(xades.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        //
        
        try
        {
            InputSource docSource = new InputSource(new StringReader(strXml));
            domParser.parse(docSource);
            l_xmlDocument = domParser.getDocument();
            InputSource sigSource = new InputSource(new StringReader(strSig));
            domParser.parse(sigSource);
            l_sigDocument = domParser.getDocument();
            // On travaille sur le contenu de l'attribut "Id"
            String ID = l_xmlDocument.getDocumentElement().getAttribute("Id");
            String tmpID = null;
            tmpID = ID + "_SIG_";
            String xpath = null;
            String xpath1 = null;
            // Si "Id" est absent, on essaie de gerer quand meme...
            if (ID == null || ID.isEmpty()) {
                xpath1 = "//*/" + strXpath;
            } else {
                xpath1 = "//*[@Id='" + ID + "']/" + strXpath;
            }
            xpath = xpath1 + "/ds:Signature[starts-with(@Id,'" + tmpID + "')]";
            NodeList nTmp = null;
            logger.debug("injectXadesSigIntoXml: xpath1="+ xpath1+", xpath="+xpath);
            Element nsctx = addNameSpace(l_xmlDocument);
            if (nsctx==null) {
                logger.error("Allo HOUSTON ?");
            } else {
                logger.debug("NameSpaceContext nsctx=["+nsctx.getAttributes().toString()+"]");
            }
            nTmp = XPathAPI.selectNodeList(l_xmlDocument, xpath, nsctx);

            logger.debug("[nTmp = "+nTmp.toString()+"]");
            logger.debug("[nTmp value = "+nTmp.getLength()+"]");

            int SigNumber = 0;
            if(nTmp != null)
                SigNumber = nTmp.getLength();
            SigNumber++;
            tmpID = tmpID + Integer.toString(SigNumber);
            logger.debug("injectXadesSigIntoXml: "+ nTmp.getLength()+" node(s). tmpID=" + tmpID);
            NodeList sigNode = XPathAPI.selectNodeList(l_xmlDocument, xpath1, nsctx);
            if(sigNode.getLength() == 0)
            { // strXpath est pourri
                logger.warn("injectXadesSigIntoXml:  strXpath est pourri");
        	return null;
            }
            // Attraper la signature dans l_sigDocument (qui est une signature détachée)
            Node sigElt = l_sigDocument.getDocumentElement().getFirstChild();

            org.apache.xml.security.Init.init();
//            org.apache.xml.internal.security.Init.init();
            Node l_sideNode = l_xmlDocument.importNode(sigElt, true);
            l_xmlDocument.getDocumentElement().setAttribute("xmlns:ds", "http://www.w3.org/2000/09/xmldsig#");
            l_sideNode.setPrefix("ds");
            l_sideNode.getAttributes().getNamedItem("Id").setNodeValue(tmpID);
            sigNode.item(0).appendChild(l_sideNode);  // terrain glissant
            
            StringBuffer tmpSignedDocument = new StringBuffer();
            String signedData = null;
            
            StringWriter strWriter = null;
            XMLSerializer probeMsgSerializer = null;
            OutputFormat outFormat = null;
            tmpSignedDocument.delete(0, tmpSignedDocument.length());
            probeMsgSerializer = new XMLSerializer();
            strWriter = new StringWriter();
            outFormat = new OutputFormat();
            outFormat.setEncoding(pXmlencoding);
            outFormat.setIndenting(false);
            outFormat.setPreserveSpace(true);
            probeMsgSerializer.setOutputCharStream(strWriter);
            probeMsgSerializer.setOutputFormat(outFormat);
            probeMsgSerializer.serialize(l_xmlDocument);
            String tmp = strWriter.toString();
            tmpSignedDocument.append(tmp);
            strWriter.close();

            /*nu.xom.Builder builder = new nu.xom.Builder();
            nu.xom.canonical.Canonicalizer canonicalizer = new nu.xom.canonical.Canonicalizer(baStream, nu.xom.canonical.Canonicalizer.EXCLUSIVE_XML_CANONICALIZATION);
            nu.xom.Document document = builder.build(inputStream); */
            // Canonicalizer c14n = Canonicalizer.getInstance("http://www.w3.org/TR/2001/REC-xml-c14n-20010315#WithComments");
            Canonicalizer c14n = Canonicalizer.getInstance(Canonicalizer.ALGO_ID_C14N_EXCL_OMIT_COMMENTS);
            byte canonicalMessage[] = c14n.canonicalizeSubtree(l_xmlDocument);
            l_xmlDocument = null;  // Liberez la memoire!!
            logger.debug("injectXadesSigIntoXml: xmlencoding="+pXmlencoding);
            signedData = new String(canonicalMessage, Canonicalizer.ENCODING);
            canonicalMessage = null;  // Liberez la memoire!!
            l_str = "<?xml version=\"1.0\" encoding=\"" + pXmlencoding + "\"?>\n" + signedData;
        }
        catch (Exception e)
        {
            logger.debug("injectXadesSigIntoXml: Exception\n" + e.getMessage());
            e.printStackTrace();
        }
        logger.debug("injectXadesSigIntoXml: SORTIE");
        System.out.println("injectXadesSigIntoXml: SORTIE");
        System.out.println(l_str);
	return l_str;
    }
    
    public static NodeList getNodeFromIdAndXpath(Document document, String strId, String strXpath)
    {
        String l_xpath = "//*[@Id='"+strId+"']/"+strXpath;
        Element l_nsctx;
        NodeList l_elt = null;
        try
        {
            l_nsctx = addNameSpace(document);
            l_elt = XPathAPI.selectNodeList(document, l_xpath, l_nsctx);
        }
        catch (Exception e)
        {
            logger.debug("getNodeFromIdAndXpath: Exception\n" + e.getMessage());
            e.printStackTrace();
        }
        return l_elt;
    }

    public static NodeList getExistingSig(Document document, String strId)
    {
	// String l_xpath = "*[starts-with(@Id, '" + strId + "_SIG_')]";
	String l_xpath = "//*[starts-with(@Id, '" + strId + "_SIG_')]";
        Element l_nsctx;
        NodeList l_elt = null;
        try
        {
            l_nsctx = addNameSpace(document);
            l_elt = XPathAPI.selectNodeList(document, l_xpath, l_nsctx);
        }
        catch (Exception e)
        {
            logger.debug("getExistingSig: Exception\n" + e.getMessage());
            e.printStackTrace();
        }
        return l_elt;
    }
    
    private static Element addNameSpace(Document document)
    {
	if (document == null) {
            logger.error("document is null, exiting.");
            return null;
        }
        Element l_nsctx = document.createElement("nsctx");
        if (l_nsctx == null) {
            logger.error("Unable to create Element 'nsctx'.");
        } else {
            l_nsctx.setAttribute("xmlns:ds", "http://www.w3.org/2000/09/xmldsig#");
            l_nsctx.setAttribute("xmlns:xad", "http://uri.etsi.org/01903/v1.1.1#");
            l_nsctx.setAttribute("xmlns:xenc", "http://www.w3.org/2001/04/xmlenc#");
        }
	return l_nsctx;
    }
}

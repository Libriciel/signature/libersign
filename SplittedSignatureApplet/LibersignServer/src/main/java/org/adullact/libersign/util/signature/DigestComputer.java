/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.libersign.util.signature;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import nu.xom.ParsingException;


/**
 * Classe de calcul de Hash sur du flux XML
 * Utilisé dans le cadre de signature XAdES, en conjonction avec Libersign
 *
 * @author a.sarr (Netheos), Stephane Vast
 */
public class DigestComputer {

    public DigestComputer() {
    }
    
    public static PesDigest computeDigest(java.io.InputStream inputStream) {
        try {
            java.io.ByteArrayOutputStream baStream = new java.io.ByteArrayOutputStream();
            nu.xom.Builder builder = new nu.xom.Builder();
            nu.xom.canonical.Canonicalizer canonicalizer = new nu.xom.canonical.Canonicalizer(baStream, nu.xom.canonical.Canonicalizer.EXCLUSIVE_XML_CANONICALIZATION);
            nu.xom.Document document = builder.build(inputStream);
            // document.query("//")
            PesDigest res = new PesDigest();
            res.setId( document.getRootElement().getAttributeValue("Id"));
            canonicalizer.write(document);
            byte[] canonicalData = baStream.toByteArray();
            java.security.MessageDigest messageDigest = java.security.MessageDigest.getInstance("sha1");
            messageDigest.update(canonicalData);

            res.setDigest( messageDigest.digest());
            return res;

        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(DigestComputer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParsingException ex) {
            Logger.getLogger(DigestComputer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DigestComputer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static PesDigest computeDigest(java.io.InputStream inputStream, String XPath) {
        try {
            java.io.ByteArrayOutputStream baStream = new java.io.ByteArrayOutputStream();
            nu.xom.Builder builder = new nu.xom.Builder();
            nu.xom.canonical.Canonicalizer canonicalizer = new nu.xom.canonical.Canonicalizer(baStream, nu.xom.canonical.Canonicalizer.EXCLUSIVE_XML_CANONICALIZATION);
            nu.xom.Document document = builder.build(inputStream);
            nu.xom.Element  root     = document.getRootElement();
            // Aller au bon endroit d'arborescence
            // document.query("//")
            // Ejecter les blocs signatures
            if (root == null)
                return null;
            PesDigest res = new PesDigest();
            res.setId( document.getRootElement().getAttributeValue("Id"));
            canonicalizer.write(document);
            byte[] canonicalData = baStream.toByteArray();
            java.security.MessageDigest messageDigest = java.security.MessageDigest.getInstance("sha1");
            messageDigest.update(canonicalData);

            res.setDigest( messageDigest.digest());
            return res;

        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(DigestComputer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParsingException ex) {
            Logger.getLogger(DigestComputer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DigestComputer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}

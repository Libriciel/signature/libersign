#
# LiberSign
# Copyright (C) 2008-2022 Libriciel-SCOP
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

pushd src/certificates

keystores=(rgs adullact libriciel eidas etat sante)

for i in "${keystores[@]}"; do
  pushd "$i"
  for file in *.crt; do
    keytool -import -alias "${file%%.*}" -file "${file}" -keystore "../$i.jks" -storepass "certificate-$i" -noprompt
    keytool -import -alias "${file%%.*}" -file "${file}" -keystore "../../ac-truststore.jks" -storepass "S_UaowJ8" -noprompt
  done
  popd
done

popd

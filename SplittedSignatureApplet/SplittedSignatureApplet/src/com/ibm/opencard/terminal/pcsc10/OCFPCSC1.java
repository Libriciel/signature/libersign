/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.ibm.opencard.terminal.pcsc10;

//import it.plio.ext.oxsit.ooo.GlobConstant;

//ROB: Commented out to eliminate dependencies from opencard.core.util.Tracer
//import opencard.core.util.Tracer;

/** <tt>OCFPCSC1</tt> for PCSC card terminals.
  * Original class from OCF framework thats maps native methods.
  * Modified to eliminate dependencies from opencard.core.util.Tracer
  * 
  * @author  Roberto Resoli
  * @version $Id: OCFPCSC1.java,v 1.2 2008/04/18 08:17:27 resoli Exp $
  */

public class OCFPCSC1 {

    public static final String m_sPCSC_WRAPPER_NATIVE = "OCFPCSC1";
    
//ROB: Commented out to eliminate dependencies from opencard.core.util.Tracer
//  private Tracer iTracer = new Tracer(this, OCFPCSC1.class);

  /** Constructor with initialization of the OCF tracing mechanism.
   *  @exception com.ibm.opencard.terminal.pcsc10.PcscException
   *		 thrown when error occured in PC/SC-Interface
   */
	
	static {
		System.out.println("Started...");
	}
	
  public OCFPCSC1() throws PcscException {
      //ROB: Commented out to eliminate dependencies from:
      // opencard.core.util.Tracer
     //initTrace();
  }

  /* load the Wrapper-DLL */
  static private void loadLibZZ() {
	  boolean ret = false;
    try {
    	System.out.println("loading library...");
      //netscape.security.PrivilegeManager.enablePrivilege("UniversalLinkAccess");
      
      //ROB: Decommented (used instead of:
      //opencard.core.util.SystemAccess.getSystemAccess().loadLibrary()
		System.loadLibrary("OCFPCSC1");
		ret = true;
      //ROB: commented to avoid dependencies from core ocf packages
      //opencard.core.util.SystemAccess.getSystemAccess().loadLibrary("OCFPCSC1");

    } catch (SecurityException  e) {
          e.printStackTrace();
    } catch (UnsatisfiedLinkError  e) {
          e.printStackTrace();
    } catch (NullPointerException e) {
          e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  // 	return ret;
  }
  
  static public void loadLib() {
	  	OCFPCSC1.loadLib(null);
  }

  /* load the Wrapper-DLL */
  static public boolean loadLib(String aPath) {
//	  String aPath = PCSCHelper.m_sLibPath;
	  boolean ret = false;
    try {
      //netscape.security.PrivilegeManager.enablePrivilege("UniversalLinkAccess");
      
      //ROB: Decommented (used instead of:
      //opencard.core.util.SystemAccess.getSystemAccess().loadLibrary()
    	if(aPath == null || aPath.length() == 0) {
    		//System.loadLibrary(GlobConstant.m_sPCSC_WRAPPER_NATIVE);
    		System.loadLibrary(m_sPCSC_WRAPPER_NATIVE);
    		ret = true;
    	}
    	else {
    		System.load(aPath);
    		ret = true;
    	}

      //ROB: commented to avoid dependencies from core ocf packages
      //opencard.core.util.SystemAccess.getSystemAccess().loadLibrary("OCFPCSC1");

    } catch (SecurityException  e) {
          e.printStackTrace();
    } catch (UnsatisfiedLinkError  e) {
          e.printStackTrace();
    } catch (NullPointerException e) {
          e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return ret;
  }

  /**************************************************************/
  /*								*/
  /* native Methods						*/
  /*								*/
  /**************************************************************/

  /* initialize the native tracing mechanism */
  public native void initTrace();

  /* returns a list of terminals found in the PCSC resource manager */
  public native synchronized String[] SCardListReaders(String groups)
				  throws PcscException;

  /* returns the context */
  public native synchronized int  SCardEstablishContext(int scope)
				  throws PcscException;

  public native synchronized void SCardReleaseContext(int context)
				  throws PcscException;

  /* returns the SCARDHANDLE */
  public native synchronized int  SCardConnect(int context, String reader,
				  int shareMode, int preferredProtocol, Integer activeProtocol)
				  throws PcscException;

  public native synchronized void SCardReconnect(int card, int shareMode,
				  int preferredProtocoll,  int initialization, Integer activeProtocol)
				  throws PcscException;

  public native synchronized void SCardDisconnect(int card, int disposition)
				  throws PcscException;

  public native synchronized void SCardGetStatusChange(int context, int timeout, PcscReaderState[] readerState)
				  throws PcscException;

  /* returns the AttributeBuffer */
  public native synchronized byte[] SCardGetAttrib(int card, int attrId)
				  throws PcscException;

  /* returns the count of received bytes in OutBuffer */
  public native synchronized byte[] SCardControl(int card, int controlCode, byte[] inBuffer)
				  throws PcscException;

  /* returns the receiveBuffer */
  /* the DLL has to manage the special behaviour of the T0/T1 protocol */
  public native synchronized byte[] SCardTransmit(int card, byte[] sendBuffer)
				  throws PcscException;

//ROB: msg callback method commented out to
//  eliminate dependencies from opencard.core.util.Tracer

/* is called by the native methods to trace via OCF trace mechanism */
/*
  protected void msg(int level, String methodName, String aLine) {
    iTracer.error("OCFPCSC1." + methodName, aLine);
  }
*/

}

// $Log: OCFPCSC1.java,v $
// Revision 1.2  2008/04/18 08:17:27  resoli
// Changed source file encoding to utf-8
//
// Revision 1.1  2004/12/27 11:14:32  resoli
// First release
//
// Revision 1.1  2004/12/23 15:34:05  resolicvs
// First release on remote CVS
//
// Revision 1.1  2004/12/23 17:58:48  resolicvs
// First release
//
// Revision 1.1  2004/12/23 13:52:13  resolicvs
// First release
//
// light version for use in j4sign 2004/12/20 - resoli
// eliminated dependencies from other ocf packages 
// see comments marked with ROB in source code.


// Revision 1.6  1999/04/07 15:20:31  breid
// load library fixed for native browser support
//
// Revision 1.5  1999/04/01 13:11:27  pbendel
// native browser support RFC-0005 (pbendel)
//
// Revision 1.4  1998/04/22 20:08:29  breid
// support for T0 implemented
//
// Revision 1.3  1998/04/14 16:16:46  breid
// htmldoc exception modified
//
// Revision 1.2  1998/04/09 13:40:53  breid
// *** empty log message ***
//
// Revision 1.1  1998/04/07 13:56:45  breid
// initial version
//

/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.ibm.opencard.terminal.pcsc10;

/**
 * <tt>PcscException</tt> is thrown when any type of error occurs in the
 * PCSC-Interface.
 *
 * The PCSC return code is provided as exception data.
 *
 * @authod  Stephan Breideneich (sbreiden@de.ibm.com)
 * @version $Id: PcscException.java,v 1.2 2008/04/18 08:17:27 resoli Exp $
 */

public class PcscException extends Exception {

  /**
   * Constructor accepting a message text
   *
   * @param   msg - exception message
   */
  public PcscException(String s) {
    super(s); rc=0;
  }

  /**
   * Constructor accepting a message text and the PCSC return code.
   *
   * @param   msg - exception message
   * @param   retCode - the PCSC return code.
   * @see     PcscConstants
   */
  public PcscException(String msg, int retCode) {
    super(msg);
    rc = retCode;
  }

  /**
   * provides application with PCSC return code.
   *
   * @return  PCSC return code.
   */
  public int returnCode() {
    return rc;
  }

  private int rc;
}

// $Log: PcscException.java,v $
// Revision 1.2  2008/04/18 08:17:27  resoli
// Changed source file encoding to utf-8
//
// Revision 1.1  2004/12/27 11:14:32  resoli
// First release
//
// Revision 1.1  2004/12/23 15:34:05  resolicvs
// First release on remote CVS
//
// Revision 1.1  2004/12/23 17:58:48  resolicvs
// First release
//
// Revision 1.1  2004/12/23 13:52:13  resolicvs
// First release
//
// Revision 1.2  1998/04/07 12:40:48  breid
// *** empty log message ***
//
// Revision 1.1  1998/04/07 11:32:25  breid
// initial version
//
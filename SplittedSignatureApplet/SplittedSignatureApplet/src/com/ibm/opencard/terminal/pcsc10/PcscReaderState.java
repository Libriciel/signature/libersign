/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.ibm.opencard.terminal.pcsc10;

/** Defines a data structure getting status information from PC/SC.
 *
 * @author  Stephan Breideneich (sbreiden@de.ibm.com)
 * @version $Id: PcscReaderState.java,v 1.2 2008/04/18 08:17:26 resoli Exp $
 */

public class PcscReaderState {

   /**
    * <tt>Reader</tt> is the friendly reader name.
    */
   public String     Reader;

   /**
    * <tt>UserData</tt> is arbitrary application-supplied data for the
    * card reader. Its use will depend on the reader capabilities.
    */

   public byte[]     UserData;

   /**
    * <tt>CurrentState</tt> is set by the application to the current
    * reader state. This variable can take on values that are
    * defined by the <tt>SCARD_</tt> constants.
    */

   public int	     CurrentState;

   /**
    * <tt>EventState</tt> is set by the resource manager to the current
    * reader state. This variable can take on values defined by
    * the <tt>SCARD_</tt> constants.
    */

   public int	     EventState;

   /**
    * <tt>ATR</tt> is set by the resource manager to the current ATR-String, 
    * if one card is inserted.
    */
   public byte[]     ATR;
}

// $Log: PcscReaderState.java,v $
// Revision 1.2  2008/04/18 08:17:26  resoli
// Changed source file encoding to utf-8
//
// Revision 1.1  2004/12/27 11:14:32  resoli
// First release
//
// Revision 1.1  2004/12/23 15:34:05  resolicvs
// First release on remote CVS
//
// Revision 1.1  2004/12/23 17:58:48  resolicvs
// First release
//
// Revision 1.1  2004/12/23 13:52:13  resolicvs
// First release
//
// Revision 1.3  1998/06/09 14:21:43  breid
// ATR-field added
//
// Revision 1.2  1998/04/07 12:40:48  breid
// *** empty log message ***
//
// Revision 1.1  1998/04/07 11:30:59  breid
// initial version
//
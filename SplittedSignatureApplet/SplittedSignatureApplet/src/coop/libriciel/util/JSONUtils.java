/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.util;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class JSONUtils {

    private JSONUtils() {}

    public static String mapToJSONString(Map<String, Object> map) {
        StringBuilder result = new StringBuilder("{");
        boolean isFirst = true;
        for(Map.Entry<String, Object> entry: map.entrySet()) {
            if(isFirst) {
                isFirst = false;
            } else {
                result.append(",");
            }
            result.append("\"").append(entry.getKey()).append("\":");
            result.append(valueToJSON(entry.getValue()));
        }
        result.append("}");
        return result.toString();
    }

    public static String valueToJSON(Object obj) {
        StringBuilder result = new StringBuilder("");
        if(obj instanceof String) {
            result.append("\"").append(obj).append("\"");
        } else if(obj instanceof List) {
            result.append("[");
            boolean isFirst = true;
            for(Object loopObj: (List<Object>) obj) {
                if(isFirst) {
                    isFirst = false;
                } else {
                    result.append(",");
                }
                result.append(valueToJSON(loopObj));
            }
            result.append("]");
        } else if(obj instanceof Long) {
            result.append(obj);
        } else if(obj instanceof Map) {
            result.append(mapToJSONString((Map<String, Object>) obj));
        }
        return result.toString();
    }

    public static List<String> jsonStringToList(String json) {
        return Arrays.asList(json.replace("[", "").replace("]", "").split(","));
    }
}

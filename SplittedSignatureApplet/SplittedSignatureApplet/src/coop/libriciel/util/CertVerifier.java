/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.util;

import org.adullact.parapheur.applets.splittedsign.CRLNotFoundException;
import org.adullact.parapheur.applets.splittedsign.CertificateRevokedException;
import org.adullact.parapheur.applets.splittedsign.CertificateVerificationException;
import org.adullact.parapheur.applets.splittedsign.CertificateVerifier;
import org.adullact.parapheur.applets.splittedsign.Main;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.AccessController;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.PrivilegedAction;
import java.security.cert.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CertVerifier {
    static KeyStore RGS_ROOTS;
    static KeyStore LIBRICIEL_ROOTS;
    static KeyStore EIDAS_ROOTS;
    static KeyStore ADULLACT_ROOTS;
    static KeyStore ETAT_ROOTS;
    static KeyStore SANTE_ROOTS;

    static String RGS_ROOTS_NAME = "RGS";
    static String LIBRICIEL_ROOTS_NAME = "LIBRICIEL";
    static String ADULLACT_ROOTS_NAME = "ADULLACT";
    static String ETAT_ROOTS_NAME = "ETAT";
    static String EIDAS_ROOTS_NAME = "EIDAS";
    static String SANTE_ROOTS_NAME = "SANTE";

    static Map<String, KeyStore> rootKeystores;

    static byte[] crlListContent;


    private static boolean isLoaded = false;

    public static void loadAuthorizedCertificated() {
        if (!isLoaded) {
            try {
                rootKeystores = new HashMap<>();

                InputStream keystoreIs = CertVerifier.class.getResourceAsStream("/certificates/rgs.jks");
                RGS_ROOTS = KeyStore.getInstance("JKS");
                RGS_ROOTS.load(keystoreIs, "certificate-rgs".toCharArray());
                rootKeystores.put(RGS_ROOTS_NAME, RGS_ROOTS);

                keystoreIs = CertVerifier.class.getResourceAsStream("/certificates/libriciel.jks");
                LIBRICIEL_ROOTS = KeyStore.getInstance("JKS");
                LIBRICIEL_ROOTS.load(keystoreIs, "certificate-libriciel".toCharArray());
                rootKeystores.put(LIBRICIEL_ROOTS_NAME, LIBRICIEL_ROOTS);

                keystoreIs = CertVerifier.class.getResourceAsStream("/certificates/eidas.jks");
                EIDAS_ROOTS = KeyStore.getInstance("JKS");
                EIDAS_ROOTS.load(keystoreIs, "certificate-eidas".toCharArray());
                rootKeystores.put(EIDAS_ROOTS_NAME, EIDAS_ROOTS);

                keystoreIs = CertVerifier.class.getResourceAsStream("/certificates/adullact.jks");
                ADULLACT_ROOTS = KeyStore.getInstance("JKS");
                ADULLACT_ROOTS.load(keystoreIs, "certificate-adullact".toCharArray());
                rootKeystores.put(ADULLACT_ROOTS_NAME, ADULLACT_ROOTS);

                keystoreIs = CertVerifier.class.getResourceAsStream("/certificates/etat.jks");
                ETAT_ROOTS = KeyStore.getInstance("JKS");
                ETAT_ROOTS.load(keystoreIs, "certificate-etat".toCharArray());
                rootKeystores.put(ETAT_ROOTS_NAME, ETAT_ROOTS);

                keystoreIs = CertVerifier.class.getResourceAsStream("/certificates/sante.jks");
                SANTE_ROOTS = KeyStore.getInstance("JKS");
                SANTE_ROOTS.load(keystoreIs, "certificate-sante".toCharArray());
                rootKeystores.put(SANTE_ROOTS_NAME, SANTE_ROOTS);

                InputStream crlConfIs = Main.class.getResourceAsStream("/crl-list.conf");
                crlListContent = toByteArray(crlConfIs);
            } catch (Exception e) {
                e.printStackTrace();
            }

            isLoaded = true;
        }
    }

    public static List<String> getVerifiedWith(X509Certificate cert) {
        loadAuthorizedCertificated();
        List<String> result = new ArrayList<String>();

        if(verifyWith(cert, EIDAS_ROOTS)) {
            result.add(EIDAS_ROOTS_NAME);
        }
        if (verifyWith(cert, RGS_ROOTS)) {
            result.add(RGS_ROOTS_NAME);
        }
        if (verifyWith(cert, ETAT_ROOTS)) {
            result.add(ETAT_ROOTS_NAME);
        }
        if (verifyWith(cert, LIBRICIEL_ROOTS)) {
            result.add(LIBRICIEL_ROOTS_NAME);
        }
        if (verifyWith(cert, ADULLACT_ROOTS)) {
            result.add(ADULLACT_ROOTS_NAME);
        }
        if (verifyWith(cert, SANTE_ROOTS)) {
            result.add(SANTE_ROOTS_NAME);
        }

        return result;
    }

    /**
     * Verify that a certificate is endorsed by at least one CA contained in a specific keystore.
     * Does *not* check for revocation status - on purpose - this should be done in a second time
     *
     * @param cert     the certificate to be verified
     * @param keystore the keystore containing the CAs public parts
     * @return true if cert belongs to one of the CAs stored in keystore
     */
    public static boolean verifyWith(X509Certificate cert, KeyStore keystore) {
        try {
            ArrayList<X509Certificate> certAndCrls = new ArrayList<X509Certificate>();
            certAndCrls.add(cert);

            CertStore certStore = CertStore.getInstance("Collection", new CollectionCertStoreParameters(certAndCrls), "SUN");

            java.security.cert.X509CertSelector certSelector = new java.security.cert.X509CertSelector();
            certSelector.setCertificate(cert);

            PKIXBuilderParameters builderParams = new PKIXBuilderParameters(keystore, certSelector);
            // certpath building parameters (CAs, cert and CRLs)
            builderParams.addCertStore(certStore);
            builderParams.setRevocationEnabled(false);

            CertPathBuilder builder = CertPathBuilder.getInstance("PKIX", "SUN");
            // building a valid certpath
            PKIXCertPathBuilderResult result = (PKIXCertPathBuilderResult) builder.build(builderParams);
            //if the certpath is needed, if not delete the following line
            result.getCertPath();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


    /**
     * Check if a certificate has been revoked, running as privileged user.
     *
     * Return the result of {@link #isCRLCheckOk(X509Certificate, String, List<String>) isCRLCheckOk},
     * but wrapped in an AccessController.doPrivileged.
     *
     * @param certificate The certificate to check for revocation
     * @param keystoreName the name of the keystore containing the CA to which the certificate belongs
     * @param invalidsCRL a list of url of CRL that will *not* be taken into account
     * @return true
     * @throws RuntimeException, possibly wrapping a CertificateException
     * when the certificate tested has been found to be revoked
     */
    public static boolean isCRLCheckOkWithPriveleged(final X509Certificate certificate, final String keystoreName, final List<String> invalidsCRL) {
        System.out.println("isCRLCheckOkWithPriveleged");
        return AccessController.doPrivileged(new PrivilegedAction<Boolean>() {

            @Override
            public Boolean run() {
                try {
                    return CertVerifier.isCRLCheckOk(certificate, keystoreName, invalidsCRL);
                } catch (CertificateException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }


    /**
     * Check if a certificate has been revoked.
     *
     * Retrieve the appropriate keystore from the keystore name passed, then return the result of
     * {@link #checkCrlWithKeystore(X509Certificate, KeyStore, List<String>) checkCrlWithKeystore}
     *
     * @param certificate The certificate to check for revocation
     * @param keystoreName the name of the keystore containing the CA to which the certificate belongs
     * @param invalidsCRL a list of url of CRL that will *not* be taken into account
     * @return true
     * @throws RuntimeException, possibly wrapping a CertificateException
     *  when the certificate tested has been found to be revoked
     */
    public static boolean isCRLCheckOk(X509Certificate certificate, String keystoreName, List<String> invalidsCRL) throws CertificateException {
        System.out.println("isCRLCheckOk");
        if (!rootKeystores.containsKey(keystoreName)) {
            System.err.println("Invalid root keystore name : " + keystoreName);
            return false;
        }

        return CertVerifier.checkCrlWithKeystore(certificate, rootKeystores.get(keystoreName), invalidsCRL);
    }


    /**
     * Check if a certificate has been revoked.
     * <p>
     * First verify the "local" list of CRLs, then check the ones on the CAs and the certificate itself.
     *
     * If some referenced CRLs are unreachable, malformed etc. the rsult is considered positive
     * (i.e the method can still returns true)
     *
     * @param certificate The certificate to check for revocation
     * @param keystore    the keystore containing the CA to which the certificate belongs
     * @param invalidsCRL a list of url of CRL that will *not* be taken into account
     * @return true
     * @throws CertificateException when a certificate is found  to be revoked.
     * Can be one of [java.security.cert.CertificateRevokedException, org.adullact.parapheur.applets.splittedsign.CertificateRevokedException]
     */
    static boolean checkCrlWithKeystore(final X509Certificate certificate, final KeyStore keystore, final List<String> invalidsCRL) throws CertificateException {
        System.out.println("checkCrlWithKeystore");

        /* Input stream containing the  CRLs' URL (one per line)*/
        InputStream crlConfIs = new ByteArrayInputStream(crlListContent);

        try {
            ArrayList<X509Extension> certAndCrls = CertificateVerifier.loadCRLsFromStreamAndCheckCert(certificate, crlConfIs, invalidsCRL);
        } catch (CRLNotFoundException | CRLException e) {
            e.printStackTrace();
        }

        // Here we assume all AC related stuff have been performed before,
        // so we just dump a stack trace if something goes wrong on this side
        try {
            CertificateVerifier.validateCertAndACsAgainstProvidedCRL(certificate, keystore, invalidsCRL);
        } catch (KeyStoreException | CertPathBuilderException | CertificateVerificationException | CRLNotFoundException e) {
            e.printStackTrace();
        }

        return true;
    }

    /**
     * Copy the full content of an inputStream into a byte array (allocated within the method).
     *
     * @param in the inputStream
     * @return the content of the stream as byte array
     * @throws IOException
     */
    private static byte[] toByteArray(InputStream in) throws IOException {

        ByteArrayOutputStream os = new ByteArrayOutputStream();

        byte[] buffer = new byte[1024];
        int len;

        // read bytes from the input stream and store them in buffer
        while ((len = in.read(buffer)) != -1) {
            // write bytes from the buffer into output stream
            os.write(buffer, 0, len);
        }

        return os.toByteArray();
    }


}

/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel;

import coop.libriciel.action.ListCertsAction;
import coop.libriciel.action.SignAction;
import coop.libriciel.util.JSONUtils;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.cert.CertificateEncodingException;

public class NewSignatureMethodHandler {

    static final ListCertsAction listCertsAction = new ListCertsAction();
    static final SignAction signAction = new SignAction();

    private NewSignatureMethodHandler() {
    }

    public static String listCerts() throws NoSuchAlgorithmException, CertificateEncodingException {
        return listCertsAction.getCertificates();
    }


    public static String listCerts(boolean listAllCerts) throws NoSuchAlgorithmException, CertificateEncodingException {
        return listCertsAction.getCertificates(listAllCerts);
    }


    public static String sign(String certificateId, String jsonDataToSign) throws InvalidKeyException, SignatureException, NoSuchAlgorithmException, NoSuchProviderException, IOException {
        return signAction.sign(listCertsAction.findForCertificate(certificateId), JSONUtils.jsonStringToList(jsonDataToSign));
    }

}

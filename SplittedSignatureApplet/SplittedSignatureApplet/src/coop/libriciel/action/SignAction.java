/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.action;

import coop.libriciel.model.SignCertificate;
import coop.libriciel.util.JSONUtils;
import coop.libriciel.util.StringUtils;
import org.adullact.parapheur.applets.splittedsign.Base64;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.reflect.MethodUtils;

import java.io.IOException;
import java.lang.reflect.Method;
import java.security.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Used to perform the signature, using the passed certificate and key.
 * This precise class is only used from IE (with SplittedSignatureApplet.jar loaded as an applet), and iParapheur 4.7+
 */
public class SignAction {
    public String sign(final SignCertificate signingObj, final List<String> jsonDataToSign) {
        return AccessController.doPrivileged(new PrivilegedAction<String>() {
            @Override
            public String run() {
                List<String> signs = new ArrayList<String>();
                for (int i = 0; i < jsonDataToSign.size(); i++) {
                    String toSign = jsonDataToSign.get(i);
                    List<String> finalToSign = new ArrayList<String>();
                    for(String tmpToSign: toSign.split(",")) {
                        try {
                            if(tmpToSign.startsWith("pkcs1:")) {
                                finalToSign.add(doSignPKCS1(Base64.decode(tmpToSign.substring(6)), signingObj.getPrivateKey()));
                            } else {
                                finalToSign.add(doSign(Base64.decode(tmpToSign), signingObj.getPrivateKey()));
                            }
                        } catch(Exception e) {
                            e.printStackTrace();
                            throw new RuntimeException("Cannot sign document, see error log", e);
                        }
                    }
                    signs.add(StringUtils.join(",", finalToSign));
                }
                return JSONUtils.valueToJSON(signs);
            }
        });


    }

    public static String doSign(byte[] bytesToSign, PrivateKey privateKey) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        Signature sig;
        if (privateKey instanceof java.security.interfaces.RSAPrivateKey) {
            sig = Signature.getInstance("SHA256WithRSA", "BC");
        } else if (System.getProperty("os.name").startsWith("Windows")) {
            sig = Signature.getInstance("SHA256WithRSA", "SunMSCAPI");
        } else {
            sig = Signature.getInstance("SHA256WithRSA", "SunRsaSign");
        }
        sig.initSign(privateKey);
        sig.update(bytesToSign);

        byte[] signature = sig.sign();
        return Base64.encodeBytes(signature);
    }

    public static String doSignPKCS1(byte[] bytesToSign, PrivateKey privateKey) throws Exception {
        System.out.println(System.getProperty("os.name"));
        if (System.getProperty("os.name").startsWith("Windows")) {
            try {
                // Obtain the handles
                long hCryptKey = (Long) MethodUtils.invokeMethod(privateKey, "getHCryptKey", null);
                long hCryptProvider = (Long)MethodUtils.invokeMethod(privateKey, "getHCryptProvider", null);
                // Call the internal native method
                Class<?> internalClass = Class.forName("sun.security.mscapi.RSASignature");
                Method internalSignHashMethod = internalClass.getDeclaredMethod("signHash", boolean.class, byte[].class, int.class, String.class, long.class, long.class);
                internalSignHashMethod.setAccessible(true);
                byte[] res = (byte[])internalSignHashMethod.invoke(internalClass, false, bytesToSign, bytesToSign.length, "SHA-256", hCryptProvider, hCryptKey);
                ArrayUtils.reverse(res); // Make it big endian
                return Base64.encodeBytes(res);
            } catch(Exception e) {
                e.printStackTrace();
                throw e;
            }
        } else {
            Signature sig;
            if (privateKey instanceof java.security.interfaces.RSAPrivateKey) {
                sig = Signature.getInstance("NONEWithRSA", "BC");
            } else {
                sig = Signature.getInstance("NONEwithRSA", "SunRsaSign");
            }
            sig.initSign(privateKey);
            sig.update(bytesToSign);

            byte[] signature = sig.sign();
            return Base64.encodeBytes(signature);
        }
    }
}

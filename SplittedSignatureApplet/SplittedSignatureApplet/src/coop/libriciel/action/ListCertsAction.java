/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.action;

import coop.libriciel.model.SignCertificate;
import coop.libriciel.util.CertUtils;
import coop.libriciel.util.CertVerifier;
import coop.libriciel.util.JSONUtils;
import org.adullact.parapheur.applets.splittedsign.Base64;
import org.adullact.parapheur.applets.splittedsign.CertListUtil;
import org.adullact.parapheur.applets.splittedsign.CertificateInfosExtractor;

import java.security.AccessController;
import java.security.NoSuchAlgorithmException;
import java.security.PrivilegedAction;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;


/**
 * Used to list existing certificates installed in the system.
 * This precise class is only used from IE (with SplittedSignatureApplet.jar loaded as an applet), and iParapheur 4.7+
 */
public class ListCertsAction {


    public SignCertificate findForCertificate(final String id) {
        return AccessController.doPrivileged(new PrivilegedAction<SignCertificate>() {
            @Override
            public SignCertificate run() {
                CertListUtil certListUtil = new CertListUtil();

                SignCertificate result = new SignCertificate();

                List<Certificate> certs = certListUtil.getAvailableCertificates();
                for (Certificate cert : certs) {
                    try {
                        String thumbprint = CertUtils.getThumbPrint((X509Certificate) cert);

                        if (thumbprint.equalsIgnoreCase(id)) {
                            // We found IT !
                            result.setSigningCertificate((X509Certificate) cert);
                            // Si on est sur windows, on prépare le onTop pour la demande de code PIN
                            result.setPrivateKey(certListUtil.getKey(result.getSigningCertificate()));
                            break;
                        }
                    } catch (CertificateEncodingException e) {
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    }
                }
                return result;
            }
        });
    }

    public String getCertificates() throws CertificateEncodingException, NoSuchAlgorithmException {
        return getCertificates(false);
    }

    public String getCertificates(boolean alsoListRootlessCerts) throws CertificateEncodingException, NoSuchAlgorithmException {
        List<Certificate> certs = AccessController.doPrivileged(new PrivilegedAction<List<Certificate>>() {
            @Override
            public List<Certificate> run() {
                return new CertListUtil().getAvailableCertificates();
            }
        });

        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("nonce", "applet");
        obj.put("result", "ok");

        List<Object> arrayCerts = new ArrayList<Object>();
        for (Certificate cert : certs) {
            X509Certificate cer = (X509Certificate) cert;
            Map<String, Object> certDetail = new HashMap<String, Object>();

            HashMap<String, String> subjectInfos = CertificateInfosExtractor.makeSubjectInfos(cer);
            HashMap<String, String> issuerInfos = CertificateInfosExtractor.makeIssuerInfos(cer);

            certDetail.put("CN", subjectInfos.get("CN"));
            certDetail.put("ID", CertUtils.getThumbPrint(cer));

            if (subjectInfos.get("EMAILADDRESS") != null) {
                String emailString = subjectInfos.get("EMAILADDRESS");
                if (!emailString.trim().isEmpty()) {
                    certDetail.put("EMAILADDRESS", emailString.trim());
                }
            }
            if (subjectInfos.get("T") != null) {
                String titleString = subjectInfos.get("T");
                if (!titleString.trim().isEmpty()) {
                    certDetail.put("T", titleString.trim());
                    if (subjectInfos.get("O") != null) {
                        String orgString = subjectInfos.get("O");
                        if (!orgString.trim().isEmpty()) {
                            certDetail.put("O", orgString.trim());
                        }
                    }
                }
            }
            Calendar cal = Calendar.getInstance();
            cal.setTime(cer.getNotAfter());
            certDetail.put("NOTAFTER", cal.getTimeInMillis());
            certDetail.put("ISSUERDN", issuerInfos.get("CN"));

            certDetail.put("PUBKEY", Base64.encodeBytes(cert.getEncoded()));

            boolean certIsValid = true;
            List<String> certifyingRootList = CertVerifier.getVerifiedWith(cer);
            for (String rootKeystoreName : certifyingRootList) {
                try {
                    CertVerifier.isCRLCheckOkWithPriveleged(cer, rootKeystoreName, Collections.EMPTY_LIST);
                } catch (RuntimeException re) {

                    if (re.getCause() != null && re.getCause() instanceof CertificateException) {
                        certIsValid = false;
                        System.out.println("Certificate has been revoked!");
                        System.out.println("Cause : " + re.getCause());
                        break;
                    } else {
                        re.printStackTrace();
                    }
                }
            }

            if (certIsValid) {
                if (certifyingRootList.size() > 0 || alsoListRootlessCerts) {
                    certDetail.put("verifiedWith", certifyingRootList);
                    arrayCerts.add(certDetail);
                }
            }
        }

        obj.put("certs", arrayCerts);
        return JSONUtils.mapToJSONString(obj);
    }
}

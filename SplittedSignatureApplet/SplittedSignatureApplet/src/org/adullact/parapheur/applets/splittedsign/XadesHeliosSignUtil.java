/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.adullact.parapheur.applets.splittedsign;

import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import javax.xml.crypto.dsig.XMLSignatureException;

/**
 * Classe abstraite mais specialisee (pour le moment) dans la signature XAdES HELIOS
 *   (refactoring partiel de XADESSignUtil)
 *
 * @author Stephane Vast - Libriciel SCOP
 */
public abstract class XadesHeliosSignUtil {

    public abstract void setDocumentID(String id);

    public abstract byte[] signPES(javax.swing.JApplet applet, X509Certificate certificate, PrivateKey privateKey, byte[] digest) throws XMLSignatureException ;
}
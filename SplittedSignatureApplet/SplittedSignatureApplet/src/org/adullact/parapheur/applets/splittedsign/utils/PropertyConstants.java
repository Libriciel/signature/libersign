/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.adullact.parapheur.applets.splittedsign.utils;

/**
 *
 * @author svast
 */
public enum PropertyConstants {
    DigestAlgName("DigestAlgName"),
    EnvelopeSignType("EnvelopeSignType"),
    EnvelopeEncode("EnvelopeEncode"),
    EncryptionAlgName("EncryptionAlgName"),
    TSAURL("TSAURL"),
    TSAUser("TSAUser"),    
    TSAPassword("TSAPassword"), 
    SecurityProvider("SecurityProvider"), 
    VerifyCRL("VerifyCRL"),
    VerifyCertificate("VerifyCertificate"),    
    FileKeyStoreTrustedRootCerts("FileKeyStoreTrustedRootCerts"),
    PassKeyStoreTrustedRootCerts("PassKeyStoreTrustedRootCerts");  
    
    private String literal;
    private PropertyConstants (String literal) {
        this.literal = literal;
    }

    public String getLiteral() {
        return literal;
    } 
}

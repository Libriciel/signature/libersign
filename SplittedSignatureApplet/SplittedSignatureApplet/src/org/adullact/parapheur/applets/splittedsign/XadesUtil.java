/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.parapheur.applets.splittedsign;

/**
 * Classe de base pour manipuler du XAdES: namespaces, etc.
 *
 * @author Stephane VAST
 */
public class XadesUtil {
    
    /**
     * Namespace du schema XAdES 1.1.1
     */
    public static final String xadesNS = "http://uri.etsi.org/01903/v1.1.1#";
    /**
     * Namespace pour schema DIA, et nouvelle politique HELIOS: XAdES 1.2.2
     */
    public static final String xadesNS122 = "http://uri.etsi.org/01903/v1.2.2#";
    /**
     * Namespace pour schema ANTS-ECD: XAdES 1.3.2
     */
    public static final String xadesNS132 = "http://uri.etsi.org/01903/v1.3.2#";

}

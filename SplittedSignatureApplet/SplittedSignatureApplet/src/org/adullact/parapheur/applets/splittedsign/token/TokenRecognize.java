/** 
 * This file is part of LIBERSIGN.
 * 
 * Copyright (c) 2008-2014, ADULLACT-Projet
 *  Initiated by ADULLACT-Projet S.A.
 *  Developped by ADULLACT-Projet S.A.
 *
 *  contact@adullact-projet.coop
 * 
 * LIBERSIGN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LIBERSIGN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with LIBERSIGN.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.adullact.parapheur.applets.splittedsign.token;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * From project j4ops (code.google.com) GPLv3
 * 
 * @author fzanutto 
 * @author svast
 */
public class TokenRecognize {
    private static final Logger logger = Logger.getLogger("TokenRecognize");

    private static class Token {

        private String atrMask;
        private String driver;
        private String description;

        public String getAtrMask() {
            return atrMask;
        }

        public void setAtrMask(String atrMask) {
            this.atrMask = atrMask;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDriver() {
            return driver;
        }

        public void setDriver(String driver) {
            this.driver = driver;
        }

        public void parse(Element element) {
            NodeList nodeLst = element.getChildNodes();
            for (int index = 0; index < nodeLst.getLength(); index++) {
                Node n = nodeLst.item(index);
                if (n.getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }
                Element e = (Element) n;

                // verifico di che tag si tratta
                if (e.getTagName().equalsIgnoreCase("atr_mask")) {
                    atrMask = e.getFirstChild().getNodeValue();
                } else if (e.getTagName().equalsIgnoreCase("driver")) {
                    driver = e.getFirstChild().getNodeValue();
                } else if (e.getTagName().equalsIgnoreCase("description")) {
                    description = e.getFirstChild().getNodeValue();
                }
            }
        }

        public void build(Document doc, Element parent) {
            Element element = doc.createElement("token");
            parent.appendChild(element);

            Element eAtr = doc.createElement("atr_mask");
            eAtr.appendChild(doc.createTextNode(atrMask == null ? "" : atrMask));

            Element eDriver = doc.createElement("driver");
            eDriver.appendChild(doc.createTextNode(driver == null ? "" : driver));

            Element eDescription = doc.createElement("description");
            eDescription.appendChild(doc.createTextNode(description == null ? "" : description));

            element.appendChild(eAtr);
            element.appendChild(eDriver);
            element.appendChild(eDescription);
        }
    }

    public static TokenInformation recognize(String file, TokenInformation cardInfo) throws Exception {
        ArrayList<Token> lstTokens = new ArrayList<Token>();
        InputStream is = null;
        try {
            is = TokenRecognize.class.getClassLoader().getResourceAsStream(file);
            if (is == null) {
                is = new FileInputStream(file);
            }

            System.out.println("    ZZZ before parsing");
            // parsing files
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder parser = factory.newDocumentBuilder();
            factory.setValidating(false);
            factory.setIgnoringComments(true);
            parser.setEntityResolver(new EntityResolver() {
                @Override
                public InputSource resolveEntity(String publicId, String systemId)
                        throws SAXException, IOException {
                    return new InputSource(new StringReader(""));
                }
            });
            Document doc = parser.parse(is);
            Element rootElement = doc.getDocumentElement();
            NodeList nodeLst = rootElement.getChildNodes();
            for (int index = 0; index < nodeLst.getLength(); index++) {
                Node n = nodeLst.item(index);
                if (n.getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }
                Element e = (Element) n;
                if (e.getTagName().equalsIgnoreCase("token")) {
                    Token token = new Token();
                    token.parse(e);
                    lstTokens.add(token);
                }
            }
        } finally {
            try {
                if (is != null) {
                    is.close();
                    is = null;
                }
            } catch (Exception ex) {
            }
        }

        System.out.println("    ZZZ before for");
        for (Token token : lstTokens) {
            logger.log(Level.FINE, "recognizing token match atr with:{0}", token.getAtrMask());
            if (cardInfo.getAtr().matches(token.getAtrMask().toUpperCase())) {
                logger.log(Level.FINE, "recognized token use driver:{0}", token.getDriver());
                cardInfo.setDriver(token.getDriver());
                cardInfo.setDriverDescription(token.getDescription());
                return cardInfo;
            }
        }
        System.out.println("    ZZZ returning null");
        return null;
    }

    public static void buildDefault() throws Exception {
        ArrayList<Token> lstTokens = new ArrayList<Token>();

        Token token = new Token();
        //GEMPLUS        "3B7F9600000031B86440F3851073940180829000"
        token.setAtrMask("3b7f9600000031b86440f3851073940180829000");
        token.setDriver("/usr/lib/pkcs11/libgclib.dylib");
        token.setDescription("Gemplus GemPC Key");
        lstTokens.add(token);
        
        //certeurop bleu  3B7F9600000031B8644070141073940180829000  different de GEMPLUS Gemalto
        token.setAtrMask("3b7f9600000031b8644070141073940180829000");
        token.setDriver("/usr/lib/pkcs11/libgclib.dylib");
        token.setDescription("Gemplus GemPC Key 00 00");
        lstTokens.add(token);
        
        token = new Token();
        token.setAtrMask("3bfc9800ffc11031fe55c803496e666f63616d65726528");
        token.setDriver("SI_PKCS11");
        token.setDescription("Siemens 140/150");
        lstTokens.add(token);

        token = new Token();
        token.setAtrMask("3bff1100ff81318055006802001010494e43525950544f001a");
        token.setDriver("ipmpki32");
        token.setDescription("ST INCARD INCRYPTO V1 32K");
        lstTokens.add(token);

        token = new Token();
        token.setAtrMask("3bf49800ffc11031fe554d346376b4");
        token.setDriver("ipmpki32");
        token.setDescription("CryptoVision");
        lstTokens.add(token);

        token = new Token();
        token.setAtrMask("3bff1800ff8131fe55006b0209020001..01434e53..3180..");
        token.setDriver("bit4ipki");
        token.setDescription("incrypto34v2");
        lstTokens.add(token);

        token = new Token();
        token.setAtrMask("3bf4180002c10a31fe5856346376c5");
        token.setDriver("cmp11");
        token.setDescription("Token EUTRON CARDOS M4.3B - Charismatics");
        lstTokens.add(token);

        token = new Token();
        token.setAtrMask("3bff1800ff8131fe55006b0209030301..01434e53..3180..");
        token.setDriver("bit4ipki");
        token.setDescription("Touch Sign con chiavi DS a 2048 bits");
        lstTokens.add(token);

        //CNS        
        token = new Token();
        token.setAtrMask("3bff1800008131fe45006b0405010001..01434e53..3180..");
        token.setDriver("bit4opki");
        token.setDescription("CNS Oberthur");
        lstTokens.add(token);

        token = new Token();
        token.setAtrMask("3bff1800ffc10a31fe55006b0508c80501..01434e53..3180..");
        token.setDriver("cnsPKCS11");
        token.setDescription("CNS Siemens");
        lstTokens.add(token);

        token = new Token();
        token.setAtrMask("3bff1800ffc10a31fe55006b0508c80501..02485043..3180..");
        token.setDriver("sissP11");
        token.setDescription("SISS");
        lstTokens.add(token);

        // creo il document
        DocumentBuilder builder = DocumentBuilderFactory
                .newInstance().newDocumentBuilder();
        Document doc = builder.newDocument();
        Element rootElement = doc.createElement("tokens");
        doc.appendChild(rootElement);

        for (Token t : lstTokens) {
            t.build(doc, rootElement);
        }

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream("tokens.xml");
            TransformerFactory transfac = TransformerFactory.newInstance();
            Transformer trans = transfac.newTransformer();

            StreamResult result = new StreamResult(fos);
            DOMSource source = new DOMSource(doc);
            trans.transform(source, result);
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                    fos = null;
                }
            } catch (Exception ex) {
            }
        }
    }

    public static void main(String[] args) throws Exception {
        buildDefault();
    }

}

/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.parapheur.applets.splittedsign;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.*;
import java.security.cert.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class for building a certification chain for given certificate and verifying
 * it. Relies on a set of root CA certificates and intermediate certificates
 * that will be used for building the certification chain. The verification
 * process assumes that all self-signed certificates in the set are trusted
 * root CA certificates and all other certificates in the set are intermediate
 * certificates.
 *
 * @author Svetlin Nakov
 * @author Stephane Vast
 */
public class CertificateVerifier {

    /**
     * Attempts to build a certification chain for given certificate and to verify
     * it. Relies on a set of root CA certificates and intermediate certificates
     * that will be used for building the certification chain. The verification
     * process assumes that all self-signed certificates in the set are trusted
     * root CA certificates and all other certificates in the set are intermediate
     * certificates.
     *
     * @param cert - certificate for validation
     * @param additionalCerts - set of trusted root CA certificates that will be
     * 		used as "trust anchors" and intermediate CA certificates that will be
     * 		used as part of the certification chain. All self-signed certificates
     * 		are considered to be trusted root CA certificates. All the rest are
     * 		considered to be intermediate CA certificates.
     * @param invalidsCRL
     * @return the certification chain (if verification is successful)
     * @throws CertificateVerificationException - if the certification is not
     * 		successful (e.g. certification path cannot be built or some
     * 		certificate in the chain is expired or CRL checks are failed)
     * @throws CertificateRevokedException
     * @throws CertPathBuilderException
     * @throws CRLNotFoundException
     */
    public static PKIXCertPathBuilderResult verifyCertificate(X509Certificate cert, Set<X509Certificate> additionalCerts, List<String> invalidsCRL)
            throws CertificateVerificationException, CertificateRevokedException, CertPathBuilderException, CRLNotFoundException {
        try {
            // Check for self-signed certificate
            if (isSelfSigned(cert)) {
                throw new CertificateVerificationException("The certificate is self-signed.");
            }

            // Prepare a set of trusted root CA certificates
            // and a set of intermediate certificates
            Set<X509Certificate> trustedRootCerts = new HashSet<X509Certificate>();
            Set<X509Certificate> intermediateCerts = new HashSet<X509Certificate>();
            for (X509Certificate additionalCert : additionalCerts) {
                if (isSelfSigned(additionalCert)) {
                    trustedRootCerts.add(additionalCert);
                } else {
                    intermediateCerts.add(additionalCert);
                }
            }

            // Attempt to build the certification chain and verify it
            PKIXCertPathBuilderResult verifiedCertChain =
                    verifyCertificate(cert, trustedRootCerts, intermediateCerts);

            for (java.security.cert.Certificate vcert : verifiedCertChain.getCertPath().getCertificates()) {
                CertificateFactory myfactory = CertificateFactory.getInstance("X.509");
                X509Certificate mycert;
                if (vcert instanceof X509Certificate) {
                    mycert = (X509Certificate) vcert;
                } else {
                    InputStream certificateInputStream = new java.io.ByteArrayInputStream(vcert.getEncoded());
                    mycert = (X509Certificate) myfactory.generateCertificate(certificateInputStream);
                }
                System.out.println("\t Found in chain : " + mycert.getSubjectX500Principal().getName());
                
                // TODO probably?? : move here the CRL verification method CRLVerifier.verifyCertificateCRLs(cert, invalidsCRL);
                
            }

            // Check whether the certificate is revoked by the CRL
            // given in its CRL distribution point extension
            CRLVerifier.verifyCertificateCRLs(cert, invalidsCRL);
            
            // The chain is built and verified. Return it as a result
            return verifiedCertChain;
        } catch (CertificateRevokedException certRevEx) {
            throw certRevEx;
        } catch (CRLNotFoundException notFoundException) {
            throw notFoundException;
        } catch (CertPathBuilderException certPathEx) {
            // throw new CertificateVerificationException("Error building certification path: "
            //        + cert.getSubjectX500Principal(), certPathEx);
            throw certPathEx;
        } catch (CertificateVerificationException cvex) {
            throw cvex;
        } catch (GeneralSecurityException ex) {
            throw new CertificateVerificationException("Error verifying the certificate: "
                    + cert.getSubjectX500Principal(), ex);
        }
    }

    /**
     * Checks whether given X.509 certificate is self-signed.
     * @param cert
     * @return 
     * @throws CertificateException 
     * @throws NoSuchAlgorithmException 
     * @throws NoSuchProviderException 
     */
    public static boolean isSelfSigned(X509Certificate cert)
            throws CertificateException, NoSuchAlgorithmException,
            NoSuchProviderException {
        try {
            // Try to verify certificate signature with its own public key
            PublicKey key = cert.getPublicKey();
            cert.verify(key);
            return true;
        } catch (SignatureException sigEx) {
            // Invalid signature --> not self-signed
            return false;
        } catch (InvalidKeyException keyEx) {
            // Invalid key --> not self-signed
            return false;
        }
    }

    /**
     * Attempts to build a certification chain for given certificate and to verify
     * it. Relies on a set of root CA certificates (trust anchors) and a set of
     * intermediate certificates (to be used as part of the chain).
     * @param cert - certificate for validation
     * @param trustedRootCerts - set of trusted root CA certificates
     * @param intermediateCerts - set of intermediate certificates
     * @return the certification chain (if verification is successful)
     * @throws GeneralSecurityException - if the verification is not successful
     * 		(e.g. certification path cannot be built or some certificate in the
     * 		chain is expired)
     */
    private static PKIXCertPathBuilderResult verifyCertificate(X509Certificate cert,
            Set<X509Certificate> trustedRootCerts, Set<X509Certificate> intermediateCerts)
            throws GeneralSecurityException {
//java.security.Provider.add(new BouncyCastleProvider());
        // Create the selector that specifies the starting certificate
        X509CertSelector selector = new X509CertSelector();
        selector.setCertificate(cert);

        // Create the trust anchors (set of root CA certificates)
        Set<TrustAnchor> trustAnchors = new HashSet<TrustAnchor>();
        for (X509Certificate trustedRootCert : trustedRootCerts) {
            trustAnchors.add(new TrustAnchor(trustedRootCert, null));
        }

        // Configure the PKIX certificate builder algorithm parameters
        PKIXBuilderParameters pkixParams =
                new PKIXBuilderParameters(trustAnchors, selector);

        // Disable CRL checks (this is done manually as additional step)
        pkixParams.setRevocationEnabled(false);

        // Specify a list of intermediate certificates
        CertStore intermediateCertStore = CertStore.getInstance("Collection", new CollectionCertStoreParameters(intermediateCerts), "SUN");
        // CertStore intermediateCertStore = CertStore.getInstance("Collection", new CollectionCertStoreParameters(intermediateCerts), "BC");
        pkixParams.addCertStore(intermediateCertStore);

        // Build and verify the certification chain
        CertPathBuilder builder = CertPathBuilder.getInstance("PKIX", "SUN");
        // CertPathBuilder builder = CertPathBuilder.getInstance("PKIX", "BC");
        PKIXCertPathBuilderResult result = (PKIXCertPathBuilderResult) builder.build(pkixParams);
        return result;
    }

    public static void validateCertAndACsAgainstProvidedCRL(X509Certificate certificate,
                                                            KeyStore keystore,
                                                            List<String> invalidsCRL)
            throws CertificateException,
                   CRLNotFoundException,
                   KeyStoreException,
                   CertificateVerificationException,
                   CertPathBuilderException {

        // check this: http://www.nakov.com/blog/2009/12/01/x509-certificate-validation-in-java-build-and-verify-chain-and-verify-clr-with-bouncy-castle/
        // List the aliases
        java.util.Enumeration enumeration = keystore.aliases();
        Set<X509Certificate> aC = new HashSet<>();

        if (enumeration != null) {
            for ( ; enumeration.hasMoreElements() ; ) {
                String alias = (String) enumeration.nextElement();

                // Does alias refer to a private key?
                // boolean b = keystore.isKeyEntry(alias);

                // Does alias refer to a trusted certificate?
                if (keystore.isCertificateEntry(alias)) {
                    aC.add((X509Certificate) keystore.getCertificate(alias));
                }
            }
        }

//        this.progress("Vérification validité certificat: chaine de certification et non-révocation en cours");
        PKIXCertPathBuilderResult ver = CertificateVerifier.verifyCertificate(certificate, aC, invalidsCRL);

    }

    public static ArrayList<X509Extension> loadCRLsFromStreamAndCheckCert(X509Certificate certificate, InputStream crlConfIs, List<String> invalidsCRL)
            throws CertificateRevokedException, CRLException, CRLNotFoundException {
        String nLine; // a line in the configuration file
        URL url;
        InputStream crlIstream = null;
        X509CRL crl;
        ArrayList<X509Extension> certAndCrls = new ArrayList<>();
        CertificateFactory cf = null;
        try {
            cf = CertificateFactory.getInstance("X.509");
        } catch (CertificateException e) {
            System.err.println("Fatal : Error trying to load internal x509 factory");
            throw new RuntimeException("Error trying to load internal x509 factory", e);
        }

        try {
            BufferedReader bfReader = new BufferedReader(new java.io.InputStreamReader(crlConfIs));
            while ((nLine = bfReader.readLine()) != null) {
                nLine = nLine.trim();
                if (!nLine.isEmpty() && !nLine.startsWith("#")) {
                    try {
                        if (nLine.startsWith("http")) {
                            url = new URL(nLine);
                            crlIstream = url.openStream();
                        } else if (nLine.startsWith("file://")) {
                            System.out.println("Info: loading local CRL: " + nLine.substring("file://".length()));
                            crlIstream = Main.class.getResourceAsStream(nLine.substring("file://".length()));
                        } else {
                            continue;
                        }
                        crl = (X509CRL) cf.generateCRL(crlIstream);
                        certAndCrls.add(crl);

                        /**
                         * Check the certificate against local CRL
                         */
                        if (crl.isRevoked(certificate)) {
                            throw new CertificateRevokedException("Certificate is revoked");
                        }
                        if (crlIstream != null) {
                            crlIstream.close();
                        }
                    } catch (IOException e) {
                        if (!invalidsCRL.contains(nLine)) {
                            throw new CRLNotFoundException(e.getMessage(), nLine);
                        }
                    }
                }
            }
            bfReader.close();
        } catch (IOException e) {
            System.err.println("Error reading embed CRL list");
            throw new RuntimeException("Error reading embed CRL list", e);
        }
        return certAndCrls;
    }

}

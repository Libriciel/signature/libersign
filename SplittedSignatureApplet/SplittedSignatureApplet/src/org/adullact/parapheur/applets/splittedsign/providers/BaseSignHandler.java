/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.adullact.parapheur.applets.splittedsign.providers;

import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.adullact.parapheur.applets.splittedsign.Configurator;
import org.adullact.parapheur.applets.splittedsign.token.TokenInformation;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.util.Store;

/**
 *
 * @author svast
 */
public class BaseSignHandler implements SignHandler {
    private static final Logger logger = Logger.getLogger("BaseSignHandler");

    @Override
    public KeyIDAndX509Cert selectKeyIDAndX509Cert(List<KeyIDAndX509Cert> lstKeyAndX509Cert) throws Exception {
        logger.log(Level.INFO, "lstKeyAndX509Cert.size: {0} certificates on slot.", lstKeyAndX509Cert.size());

        for (KeyIDAndX509Cert keyIDAndX509Cert : lstKeyAndX509Cert) {
            X509Certificate x509Cert = keyIDAndX509Cert.getX509Cert();
            boolean[] keyUsage = x509Cert.getKeyUsage();
            // 1 nonRepudiation
            if (keyUsage != null && keyUsage[1]) {
                try {
                    x509Cert.checkValidity();
                    logger.log(Level.INFO, "select certificate sn:{0}", x509Cert.getSerialNumber().toString(16));
                    return keyIDAndX509Cert;
                } catch (CertificateExpiredException ex) {
                    logger.log(Level.INFO, "padbol expired certificate sn{0}", x509Cert.getSerialNumber().toString(16));
                } catch (CertificateNotYetValidException ex) {
                    logger.log(Level.INFO, "padbol not yet valid certificate sn{0}", x509Cert.getSerialNumber().toString(16));
                }
            }
        }

        logger.info("no certificate selected");

        return null;
    }

    @Override
    public TokenInformation selectToken(List<TokenInformation> lstTokens) throws Exception {
        if (lstTokens.size() > 0) {
            return lstTokens.get(0);
        }
        return null;
    }

    @Override
    public SignerInformation selectSigner(Store certs, SignerInformationStore signers) throws Exception {
        Iterator iterSigns = signers.getSigners().iterator();
        SignerInformation selectSigner = null;
        if (iterSigns.hasNext()) {
            selectSigner = (SignerInformation) iterSigns.next();
            if (selectSigner.getCounterSignatures() != null && selectSigner.getCounterSignatures().size() > 0) {
                selectSigner = (SignerInformation) selectSigner.getCounterSignatures().getSigners().iterator().next();
            }
        }

        return selectSigner;
    }
}

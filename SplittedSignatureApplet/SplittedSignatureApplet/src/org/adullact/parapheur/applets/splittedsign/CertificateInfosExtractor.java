/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.parapheur.applets.splittedsign;


import java.security.cert.X509Certificate;
import java.util.HashMap;

/**
 * Cette classe extrait les informations textuelles et personnelles
 * contenues dans des champs de certificat X509 en en fournissant un 
 * accès interessant pour la programmation (HashMap par exemple).
 * 
 * @author Xavier FACELINA <x.facelina@nethos.net>
 */
public class CertificateInfosExtractor {

    /** Creates a new instance of CertificateInfosExtractor */
    public CertificateInfosExtractor() {
    }

    /**
     * Fournit une représentation Clé, Valeur d'une ligne
     * SUBJECT ou ISSUER d'un certificat.
     * @param in la chaine concernée
     * @return la hashmap K,V
     */
    private static HashMap<String, String> dnTokenizer(String in) {
        HashMap<String, String> retVal = new HashMap<String, String>();
        int idx = 0;

        String buffer = "";
        String[] currentPair = new String[2];
        boolean ignoreComma = false;

        while (idx < in.length()) {
            if (in.charAt(idx) == '=') {
                currentPair[0] = buffer;
                buffer = "";
                idx++;
                continue;
            }
            if (in.charAt(idx) == '"') {
                ignoreComma = !ignoreComma;
                idx++;
                continue;
            }
            /*
             * Gestion de la seconde manière d'échapper les virgules
             */
            if (idx + 1 < in.length() && in.charAt(idx) == '\\' && in.charAt(idx + 1) == ',') {
                idx += 2;
                buffer = buffer + ",";
                continue;
            }
            if (!ignoreComma && in.charAt(idx) == ',') {

                currentPair[1] = buffer;
                retVal.put(currentPair[0], currentPair[1]);

                currentPair = new String[2];
                buffer = "";
                idx++;

                // On ignore les espace en debut de clef
                while (in.charAt(idx) == ' ') {
                    idx++;
                }
                continue;
            }

            buffer = buffer + in.charAt(idx);
            idx++;
        }

        /* Si le buffer n'est pas vide c'est qu'il nous reste la derniere valeur
         * à récuperer.
         */

        if (buffer.length() > 0) {
            currentPair[1] = buffer;
            retVal.put(currentPair[0], currentPair[1]);
        }

        return retVal;
    }
    
    /**
     * fournit une représentation clé,valeur de la ligne SUBJECT obtenue
     * par cert.getSubjectX500Principal().toString();
     * @param cert le certificate concerné
     * @return le hash de valeurs
     */
    public static HashMap<String, String> makeSubjectInfos(X509Certificate cert) {
        String subject = cert.getSubjectX500Principal().toString();
        //String[] subjectArray = subject.split(", ");
        HashMap subjectHashMap = dnTokenizer(subject);
                /*new HashMap();
        for (String kvPair : subjectArray) {
            String[] kv = kvPair.split("=");
            subjectHashMap.put(kv[0], kv[1]);
        }*/
        return subjectHashMap;
    }

    /**
     * fournit une représentation clé,valeur de la ligne ISSUER obtenue
     * par cert.getIssuerX500Principal().toString()
     * @param cert le certificat concerné
     * @return le hashmap de valeurs
     */
    public static HashMap<String, String> makeIssuerInfos(X509Certificate cert) {

        String issuer = cert.getIssuerX500Principal().toString();
        //String[] issuerArray = issuer.split(", ");
        HashMap issuerHashMap = dnTokenizer(issuer);
                /*new HashMap();
        for (String kvPair : issuerArray) {
            String[] kv = kvPair.split("=");
            issuerHashMap.put(kv[0], kv[1]);
        }*/
        return issuerHashMap;
    }

}

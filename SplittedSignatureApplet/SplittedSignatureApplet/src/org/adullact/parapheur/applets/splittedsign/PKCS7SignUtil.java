/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.parapheur.applets.splittedsign;

//import iaik.asn1.structures.AlgorithmID;
//import iaik.pkcs.pkcs11.Module;
//import iaik.pkcs.pkcs11.Slot;
//import iaik.pkcs.pkcs11.Token;
//import iaik.pkcs.pkcs11.TokenException;
//import iaik.pkcs.pkcs11.TokenInfo;
//import iaik.pkcs.pkcs11.objects.RSAPrivateKey;
//import iaik.pkcs.pkcs11.wrapper.PKCS11Constants;
//import iaik.pkcs.pkcs7.IssuerAndSerialNumber;
//import iaik.pkcs.pkcs7.SignedData;
//import iaik.pkcs.pkcs7.SignedDataStream;
//import iaik.utils.KeyAndCertificate;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
//import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
//import org.adullact.parapheur.applets.splittedsign.pcsc.CardInfo;
//import org.adullact.parapheur.applets.splittedsign.pcsc.PCSCHelper;
//import org.adullact.parapheur.applets.splittedsign.pkcs11.PKCS11Signer;
//import org.adullact.parapheur.applets.splittedsign.providers.IaikPkcs11;
//import org.adullact.parapheur.applets.splittedsign.providers.KeyIDAndX509Cert;
//import org.adullact.parapheur.applets.splittedsign.utils.IaikUtil;
import org.bouncycastle.cms.CMSException;
import sun.security.pkcs.ContentInfo;
import sun.security.pkcs.PKCS7;
import sun.security.pkcs.PKCS9Attribute;
import sun.security.pkcs.PKCS9Attributes;
import sun.security.pkcs.ParsingException;
import sun.security.pkcs.SignerInfo;
import sun.security.x509.AlgorithmId;
import sun.security.x509.X500Name;
import sun.security.x509.X509CertInfo;

/**
 * Class for CMS/PKCS#7 signature production and formatting.
 * 
 * @author NETHEOS
 * @author Stephane Vast - ADULLACT Projet
 */
public class PKCS7SignUtil {

    static final String OS_NAME = System.getProperty("os.name");

    static final String DIGEST_ALG = "SHA1";

    static final Logger logger = Logger.getLogger(PKCS7SignUtil.class.getName());

    public PKCS7SignUtil() {
    }

    public static byte[] signAndCreateP7(X509Certificate[] certChain, PrivateKey privateKey, byte[] digestValue) throws NoSuchAlgorithmException, InvalidKeyException, IOException, SignatureException, CertificateException, CMSException {
        Principal issuerName = certChain[0].getIssuerDN();
        if (!(issuerName instanceof X500Name)) {
            X509CertInfo tbsCert = new X509CertInfo(certChain[0].getTBSCertificate());
            issuerName = (Principal) tbsCert.get("issuer.dname");
            System.out.println("Certificat fourni par: " + issuerName);
        }
        try {
            /**
             * Set up the basic stuff
             * LEGACY PROCESS
             */
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

            AlgorithmId digestAlg = AlgorithmId.get(DIGEST_ALG);

            /**
             * Set the Behavior for signature
             */
            // signature alg
//                Signature sig =  (privateKey instanceof java.security.interfaces.RSAPrivateKey)
//                        ? Signature.getInstance("SHA1WithRSA", "BC")
//                        : (OS_NAME.startsWith("Windows") ? Signature.getInstance("SHA1WithRSA", "SunMSCAPI")
//                        : Signature.getInstance("SHA1WithRSA", "SunRsaSign"));
            Signature sig;
            if (privateKey instanceof iaik.pkcs.pkcs11.provider.keys.IAIKPKCS11RsaPrivateKey) {
                sig = Signature.getInstance("ExternalSHA1WithRSA");
            } else if (privateKey instanceof java.security.interfaces.RSAPrivateKey) {
                sig = Signature.getInstance("SHA1WithRSA", "BC");
            } else if (OS_NAME.startsWith("Windows")) {
                sig = Signature.getInstance("SHA1WithRSA", "SunMSCAPI");
            } else {
                sig = Signature.getInstance("SHA1WithRSA", "SunRsaSign");
            }

            // p9 attributes
            PKCS9Attribute[] authenticatedAttributeList = {
                new PKCS9Attribute(PKCS9Attribute.SIGNING_TIME_OID, new Date()),
                new PKCS9Attribute(PKCS9Attribute.CONTENT_TYPE_OID, ContentInfo.DATA_OID),
                new PKCS9Attribute(PKCS9Attribute.MESSAGE_DIGEST_OID, digestValue)
            };
            PKCS9Attributes authenticatedAttributes = new PKCS9Attributes(authenticatedAttributeList);

            // signed attributes computation
            sig.initSign(privateKey);
            sig.update(authenticatedAttributes.getDerEncoding());
            byte[] signedAttributes = sig.sign();

            // detached signature
            ContentInfo contentInfo = new ContentInfo(ContentInfo.DATA_OID, null);

            // signer serial
            java.math.BigInteger serial = certChain[0].getSerialNumber();
            SignerInfo signerInfo = new SignerInfo(
                    new X500Name(certChain[0].getIssuerDN().getName()),
                    serial, digestAlg, authenticatedAttributes,
                    new AlgorithmId(AlgorithmId.RSAEncryption_oid), signedAttributes, null);
            AlgorithmId[] algs = {digestAlg};
            SignerInfo[] infos = {signerInfo};
            PKCS7 p7Signature = new PKCS7(algs, contentInfo, certChain, infos);
            ByteArrayOutputStream nbaos = new ByteArrayOutputStream();
            p7Signature.encodeSignedData(nbaos);
            return nbaos.toByteArray();
            
        } catch (NoSuchProviderException ex) {
            Logger.getLogger(PKCS7SignUtil.class.getName()).log(Level.SEVERE, null, ex);
            throw new CMSException(ex.getMessage(), ex);
        }
    }

    /*
     * Genere la signature pkcs#7 à partir du haché du document
     * @param cert certificat du signataire
     * @param privKey clef privée de signature
     * @param digestValue valeur du haché du document (algorithme de hachage par defaut sha1)
     * @return signature au format pkcs#7
     */
    public static byte[] signAndCreateP7(X509Certificate cert, PrivateKey privKey, byte[] digestValue) throws CMSException, InvalidKeyException {
        try {
            java.security.cert.X509Certificate[] certChain = {cert};
            return signAndCreateP7(certChain, privKey, digestValue);
        } catch (NoSuchAlgorithmException ex) {
            logger.log(Level.SEVERE, null, ex);
            throw new CMSException(ex.getMessage(), ex);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
            throw new CMSException(ex.getMessage(), ex);
        } catch (SignatureException ex) {
            logger.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
            throw new CMSException(ex.getMessage(), ex);
        } catch (CertificateException ex) {
            logger.log(Level.SEVERE, null, ex);
            throw new CMSException(ex.getMessage(), ex);
        }
    }

    /**
     * Generates a  new pkcs7  signature and update the p7 file
     * @param cert signing certificate
     * @param privKey signer's private key
     * @param digestValue data digest value
     * @param signed already existing p7 signature
     * @return updated parallel signature
     * @throws org.bouncycastle.cms.CMSException
     * @throws java.security.InvalidKeyException
     */
    public static byte[] updateP7Signature(X509Certificate cert, PrivateKey privKey, byte[] digestValue, byte[] signed) throws CMSException, InvalidKeyException
    {
        if (signed == null)
            return signAndCreateP7(cert, privKey, digestValue);
        logger.log(Level.INFO, "signed length {0}", signed.length);
        try
        {
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
            // read the signature
            PKCS7 p7Sig = new PKCS7(new java.io.ByteArrayInputStream(signed));

            // attributes
            PKCS9Attribute[] authenticatedAttributes =
            {
                new PKCS9Attribute(PKCS9Attribute.SIGNING_TIME_OID, new Date()),
                new PKCS9Attribute(PKCS9Attribute.CONTENT_TYPE_OID, ContentInfo.DATA_OID),
                new PKCS9Attribute(PKCS9Attribute.MESSAGE_DIGEST_OID, digestValue)
            };
            PKCS9Attributes p9Attributes = new PKCS9Attributes(authenticatedAttributes);

            // Instantiate the right signature algorithm
            Signature signature = (privKey instanceof java.security.interfaces.RSAPrivateKey)
                    ? Signature.getInstance("SHA1WithRSA", "BC")
                    : (OS_NAME.startsWith("Windows") ? Signature.getInstance("SHA1WithRSA", "SunMSCAPI")
                    : Signature.getInstance("SHA1WithRSA", "SunRsaSign"));

            signature.initSign(privKey);
            signature.update(p9Attributes.getDerEncoding());
            byte[] signedP9Attributes = signature.sign();

            ContentInfo contentInfo = p7Sig.getContentInfo();

            AlgorithmId newAlgId = AlgorithmId.get(DIGEST_ALG);
            AlgorithmId[] algIds = p7Sig.getDigestAlgorithmIds();
            algIds = Arrays.copyOf(algIds, algIds.length + 1);
            algIds[algIds.length - 1] = newAlgId;

            SignerInfo signerInfo = new SignerInfo(new X500Name(cert.getIssuerDN().getName()), cert.getSerialNumber(),
                    newAlgId, p9Attributes, new AlgorithmId(AlgorithmId.RSAEncryption_oid),
                    signedP9Attributes, null); // à la place de NULL, on trouve les "unsigned attributes" (y mettre le token horodatage par exemple)
            SignerInfo[] signerInfos = p7Sig.getSignerInfos();
            signerInfos = Arrays.copyOf(signerInfos, signerInfos.length + 1);
            signerInfos[signerInfos.length - 1] = signerInfo;

            X509Certificate[] certs = p7Sig.getCertificates();
            certs = Arrays.copyOf(certs, certs.length + 1);
            certs[certs.length - 1] = cert;

            PKCS7 np7sig = new PKCS7(algIds, contentInfo, certs, signerInfos);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            np7sig.encodeSignedData(baos);
            return baos.toByteArray();
        } catch (SignatureException ex) {
            logger.log(Level.SEVERE, null, ex);
            throw new CMSException(ex.getMessage(), ex);
        } catch (NoSuchAlgorithmException ex) {
            logger.log(Level.SEVERE, null, ex);
            throw new CMSException(ex.getMessage(), ex);
        } catch (NoSuchProviderException ex) {
            logger.log(Level.SEVERE, null, ex);
            throw new CMSException(ex.getMessage(), ex);
        } catch (ParsingException ex) {
            logger.log(Level.SEVERE, null, ex);
            throw new CMSException(ex.getMessage(), ex);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
            throw new CMSException(ex.getMessage(), ex);
        }
    }

}
/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.parapheur.applets.splittedsign;

import coop.libriciel.NewSignatureMethodHandler;
import org.adullact.parapheur.applets.splittedsign.http.ClientHttpRequest;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.*;
import java.security.cert.*;
import java.util.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main class for display and controlling the signing process
 *
 * @author Xavier Facelina (Netheos)
 * @author Stephane Vast (Adullact Projet)
 * @version {TAG_ID}
 */
public class Main extends javax.swing.JApplet implements Runnable {

    /**
     * Name of the product
     */
    public static String productString = "LiberSign";
    /**
     * Version number for Libersign, as string
     */
    public static String versionString = "{TAG_ID}";
    /**
     * Date of build
     */
    public static String buildString   = "build {BUILD_ID}";
    /**
     * Legal statements
     */
    public static String copyrightMsg = "&copy; Libriciel 2008-2018, tous droits r&eacute;serv&eacute;s.";
    public static String copyrightTxt = "Copyright Libriciel 2008-2018, tous droits reserves";
    public static String licenceString = "LiberSign est un logiciel libre sous licences AFFERO GPLv3, et GPLv3.<br/>"
                                         +
                                         "Toute copie de LiberSign doit laisser cette notice apparente &agrave; l'utilisateur, ainsi que publier les modifications des sources.<br/>"
                                         + "Site du projet: <a href=\"http://adullact.net/projects/libersign\">Forge&nbsp;ADULLACT</a>.";
    Thread signingThread = null;
    int delay;
    /**
     * Signing-related variables stuff
     */
    X509Certificate signingCertificate;
    PrivateKey privKey;
    HashMap<String, Digest> digests;
    HashMap<String, String> signedHashMap = new HashMap<String, String>();
    String observation = "";
    /**
     * Cancel mode: javascript trigger "cancelSignature()", or "history.go(-1)"
     */
    boolean isCancelModeJavaScript;
    String aboutMessage = "";
    /**
     * The list containing the available signing certificates
     */
    private UserCertificatesListModel certListModel = null;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel algorithmLabel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JButton cancelCertSignButton;
    private javax.swing.JPanel certViewerPanel;
    private javax.swing.JList certificateJList;
    private javax.swing.JLabel email;
    private javax.swing.JLabel infoLabel;
    private javax.swing.JLabel issuerName;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel name;
    private javax.swing.JPanel returnPanel;
    private javax.swing.JButton signButton;
    private javax.swing.JPanel signaturePanelAutoSelected;
    private javax.swing.JPanel signaturePanelUserSelected;
    private javax.swing.JLabel titleIssuer;
    private javax.swing.JLabel titreFingerprint;
    private javax.swing.JButton useThisCertButton;
    private javax.swing.JLabel validityLabel;
    // Just for grafx effects
    private InfiniteProgressPanel infiniteProgress;

    public static String systemeInfoString() {
        String bits;
        String info = "";
        try {
            info = "  Java: " + System.getProperty("java.vendor")
                   + ", version: " + System.getProperty("java.version");
            bits = System.getProperty("sun.arch.data.model");
            if (bits != null) {
                info += ", " + bits + " bit";
            }
            info += "\n  Machine: " + System.getProperty("os.name")
                    + ", " + System.getProperty("os.version")
                    + ", " + System.getProperty("os.arch");
        } catch (SecurityException ex) {
            info += "\n" + ex.getMessage();
        }
        return info;
    }

    @Override
    public void stop() {
        System.exit(0);
    }

    /** Initializes the applet Main */
    @Override
    public void init() {
        String str = getParameter("fps");
        int fps = (str != null) ? Integer.parseInt(str) : 10;
        delay = (fps > 0) ? (1000 / fps) : 100;
        /**
         * Look at the host OS and chose the appropriate cryptographic
         * sub-system and look and feel.
         */
        // ??  KeyStore ks = Configurator.getInstance().getKeyStore();
        Configurator.getInstance().applyLookAndFeel();
        try {

            java.awt.EventQueue.invokeAndWait(new Runnable() {

                @Override
                public void run() {
                    try {
                        /**
                         * Show the version number.
                         */
                        aboutMessage = productString + " \t v" + versionString
                                       + ", " + buildString + "\n    " + copyrightTxt
                                       + "\n\n" + systemeInfoString();
                        System.out.println(aboutMessage);

                        launchJavascriptAction("javascript:appletIsLoaded();");

                        /**
                         * Loads the documents hashes in a processable structure to be signed.
                         * Print an error message if the is no document to sign.
                         */
                        digests = getDigests();
                        certListModel = UserCertificatesListModel.getInstance();
                        initComponents();

                        launchJavascriptAction("javascript:giveCommentToApplet();");

                        /**
                         * two operating modes:
                         *  - let the user choose the signing certificate,
                         *  - OR give the certificate information as parameter.
                         */
                        if (getParameter("certificat_cn") == null) {
                            /**
                             * List available certificates, and let the user
                             * choose the one to be used for signing.
                             */
                            add(signaturePanelUserSelected);
                            /**
                             * Auto-select if the list has only one element
                             * to be chosen.
                             */
                            if (certListModel.size() == 1) {
                                certificateJList.setSelectedIndex(0);
                            } else {
                                useThisCertButton.setText("Sélectionnez un certificat");
                                useThisCertButton.setEnabled(false);
                                System.out.println(certListModel.size() + " valid certificates to be selected.");
                            }
                            /**
                             * The "cancel" button is hidden by default.
                             * Let's display it only if display_cancel parameter
                             * is set to 'true'.
                             */
                            if (getParameter("display_cancel") != null && getParameter("display_cancel").equalsIgnoreCase("true")) {
                                cancelCertSignButton.setVisible(true);
                            } else {
                                System.out.println("\tregular use, only 'Sign' button");
                                cancelCertSignButton.setVisible(false);
                            }
                            /**
                             * The cancel mode.
                             */
                            isCancelModeJavaScript =
                                    getParameter("cancel_mode") != null && getParameter("cancel_mode").equalsIgnoreCase("javascript");
                        } else {
                            /**
                             * Prompt the user for signing showing him the
                             * number of documents ready to be signed and
                             * the selected certificate.
                             *
                             * Choose the appropriate certificate for signing
                             * with the data provided as applet parameters.
                             * Print an error message if the certificate pointed by the applet
                             * params is not reachable by the applet running environnement.
                             */
                            signingCertificate = CertChooser.getVerifiedCertificate(getParameter("certificat_issuer_cnl"),
                                                                                    getParameter("certificat_serial"),
                                                                                    getParameter("certificat_cn"));
                            privKey = CertChooser.getVerifiedPrivateKey(getParameter("certificat_issuer_cnl"),
                                                                        getParameter("certificat_serial"),
                                                                        getParameter("certificat_cn"));
                            setCertificatePanelInformations(signingCertificate);
                            int numberOfDigest = Integer.parseInt(getParameter("hash_count"));
                            infoLabel.setText("Vous vous apprêtez à signer " + getParameter("hash_count") + " document" +
                                              (numberOfDigest > 1 ? "s" : "") + " avec le certificat suivant : ");
                            certViewerPanel.updateUI();
                            add(signaturePanelAutoSelected);
                        }
                    } catch (KeyStoreException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (NoSuchAlgorithmException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (UnrecoverableKeyException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (CertificateException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    launchJavascriptAction("javascript:appletIsLoaded();");
                }
            });
        } catch (InterruptedException ex) {
            System.out.println(ex.getLocalizedMessage());
        } catch (InvocationTargetException ex) {
            System.out.println(ex.getLocalizedMessage());
        }
    }

    public String getCertificates() throws CertificateEncodingException, NoSuchAlgorithmException {
        return NewSignatureMethodHandler.listCerts();
    }

    public String getCertificates(boolean listAllCerts) throws CertificateEncodingException, NoSuchAlgorithmException {
        return NewSignatureMethodHandler.listCerts(listAllCerts);
    }

    public String sign(String certificateID, String dataToSign) throws NoSuchAlgorithmException, NoSuchProviderException, SignatureException, InvalidKeyException, IOException {
        return NewSignatureMethodHandler.sign(certificateID, dataToSign);
    }

    /**
     * The Digests are given and then stored in the map as Hexa strings
     *
     * @author Stephane Vast
     */
    private HashMap<String, Digest> getDigests() {

        HashMap<String, Digest> digestsMap = new HashMap<String, Digest>();

        int numberOfDigest = Integer.parseInt(getParameter("hash_count"));
        String lKey;
        System.out.println("Number of documents to be signed: " + numberOfDigest);
        for (int i = 1; i <= numberOfDigest; i++) {
            if (getParameter("iddoc_1") != null) {
                lKey = getParameter("iddoc_" + i);
            } else {
                lKey = "hash_" + i;
            }
            Digest newDigest = new Digest(
                    getParameter("hash_" + i),
                    getParameter("format_" + i),
                    getParameter("p7s_" + i),
                    getParameter("pesid_" + i),
                    getParameter("pespolicyid_" + i),
                    getParameter("pespolicydesc_" + i),
                    getParameter("pespolicyhash_" + i),
                    getParameter("pesspuri_" + i),
                    getParameter("pescity_" + i),
                    getParameter("pespostalcode_" + i),
                    getParameter("pescountryname_" + i),
                    getParameter("pesclaimedrole_" + i),
                    getParameter("pesencoding_" + i));
            System.out.println("   Document #" + i + ": " + lKey);
            digestsMap.put(lKey, newDigest);
        }

        return digestsMap;
    }

    /**
     * Used by navigator (javascript within web page) to get the signatures.
     *
     * NB: This is to be used just after the sign method.
     * Please note that is designed specifically for the "form" return mode
     *
     * @author Stephane Vast
     *
     * @param id the Id of the hash primarily processed
     * @return the signature corresponding to the Id.
     */
    public String returnSignature(String id) {
        if (signedHashMap.containsKey(id)) {
            System.out.println("\n SIGNATURE BASE64 of [" + id + "]: \n" + signedHashMap.get(id) + "\n\n");
            return signedHashMap.get(id);
        } else {
            System.out.println("\n Id [" + id + "] souhaite, mais inconnu. Pas de bras, pas de chocolat...\n");
            return id;
        }
    }

    /**
     * Order given by navigator (javascript within web page) to set a comment
     *   used in the signature process at run-time.
     * That comment will be included in the XML fields to be signed
     *
     * @author Stephane Vast
     *
     * @param commentString
     * @return
     */
    public String injectSignature(String commentString) {
        observation = commentString.trim();
        System.out.println("observation: \n" + observation);
        return "";
    }

    /**
     * Order given by navigator (Javascript) to kill the applet
     *
     * @param byeBye the final message
     * @return nothing, just die
     * @author Stephane Vast
     */
    public String killApplet(String byeBye) {
        System.out.println("Navigator m'a tuer: " + byeBye.trim());
        System.exit(0);
        return "";
    }

    public void killApplet() {
        System.out.println("Navigator m'a tuer, adieu monde cruel!");
        System.exit(0);
    }

    public void setCertificatePanelInformations(X509Certificate certificate) {
        HashMap<String, String> subjectInfos = CertificateInfosExtractor.makeSubjectInfos(certificate);
        HashMap<String, String> issuerInfos = CertificateInfosExtractor.makeIssuerInfos(certificate);
        name.setText(subjectInfos.get("CN"));
        email.setText(subjectInfos.get("EMAILADDRESS"));
        issuerName.setText(issuerInfos.get("CN"));
        validityLabel.setText(certificate.getNotAfter().toString());
        //        Logger.getLogger("org.adullact.parapheur.applets.splittedsignatureapplet").severe("CN=" +
        //                subjectInfos.get("CN") + ", Email=" + subjectInfos.get("EMAILADDRESS") + ", IssuerCN=" + issuerInfos.get("CN") + ", validity=" + certificate.getNotAfter().toString());
    }

    /**
     * Central point to start the sign process.
     * Do all the regular controls, then crypto-ize !
     */
    private void sign() {
        final Component main = this;
        SignUtil signUtil = new SignUtil(digests, false);
        SignHandler signHandler = new SignHandler() {
            @Override
            public void print(String text) {
                JOptionPane.showMessageDialog(main, text);
            }

            @Override
            public void progress(String text) {
                infiniteProgress.setText(text);
                validate();
            }

            @Override
            public void validateCertificate(X509Certificate certificate, List<String> invalidsCRL)
                    throws CRLException, CRLNotFoundException, IOException, KeyStoreException, NoSuchAlgorithmException,
                    CertificateException,
                    CertificateVerificationException, CertPathBuilderException {

                InputStream kst_pwd_is = Main.class.getResourceAsStream("/ac-truststore.password");
                ByteArrayOutputStream ksPasswordWriter = new ByteArrayOutputStream();
                int readed;
                byte[] data = new byte[256];
                while ((readed = kst_pwd_is.read(data)) != -1) {
                    ksPasswordWriter.write(data, 0, readed);
                }
                String ksPassword = new String(ksPasswordWriter.toByteArray());

                /**
                 * inputstrean containing the jks of trusted CAs
                 */
                this.progress("Vérification validité certificat: CRL locale.");
                InputStream kst_is = Main.class.getResourceAsStream("/ac-truststore.jks");
                KeyStore keystore = KeyStore.getInstance("JKS");
                // TODO : set the ksPassword to the right value
                keystore.load(kst_is, ksPassword.toCharArray());
                /* Input stream containing the  CRLs' URL (one per line)*/
                InputStream crlConfIs = Main.class.getResourceAsStream("/crl-list.conf");

                ArrayList<X509Extension> certAndCrls = CertificateVerifier.loadCRLsFromStreamAndCheckCert(certificate, crlConfIs, invalidsCRL);

                //  this seems like useless code
                certAndCrls.add(certificate);
                this.progress("Vérification validité certificat: CRL locale... OK!");

                CertificateVerifier.validateCertAndACsAgainstProvidedCRL(certificate, keystore, invalidsCRL);

                this.progress("Vérification validité certificat... OK!");
                // Should be nice to deal with certAndCrls is useful, no?
                System.out.println("CertAndCrls.size = " + certAndCrls.size());
            }

            @Override
            public void success(HashMap<String, String> result) {
                signedHashMap = result;
                try {
                    if (getParameter("certificat_cn") == null) {
                        remove(signaturePanelUserSelected);
                    } else {
                        remove(signaturePanelAutoSelected);
                    }
                    add(returnPanel);
                    validate();
                    Thread.sleep(1500);
                    jLabel1.setFont(new java.awt.Font("Verdana", 0, 15));
                    jLabel1.setText("L'opération de signature s'est correctement déroulée....");
                    validate();

                    if ((getParameter("return_mode") != null) && getParameter("return_mode").equalsIgnoreCase("form")) {
                        // In Form mode the host Web page has to implement a
                        // javascript function injectSignature() which in return
                        // would call applet.returnSignature("hash_i") to get the
                        // signatures.
                        launchJavascriptAction("javascript:injectSignature(\"" + "toto" + "\");");

                    } else {
                        String urlSendContent = getParameter("url_send_content");
                        String protocol = urlSendContent.substring(0, urlSendContent.indexOf(":"));
                        if (protocol.toLowerCase().equals("https")) {
                        /*
                         * Do the upload in https mode.
                         * The problem is with the management of client certificate
                         *
                         * How to automate that?
                         *
                         * For the moment just do as in http below
                         */
                            ClientHttpRequest clientHttpRequest = new ClientHttpRequest(urlSendContent);
                            clientHttpRequest.setParameters(signedHashMap);
                            if (getParameter("id_user") != null) {
                                clientHttpRequest.setParameters(getParameter("id_user").split("="));
                            }
                            clientHttpRequest.post();
                        } else {
                            ClientHttpRequest clientHttpRequest = new ClientHttpRequest(urlSendContent);
                            clientHttpRequest.setParameters(signedHashMap);
                        /*for (int i = 0; i < signedHashList.size(); i++) {
                        clientHttpRequest.setParameter("signature_" + (i+1), signedHashList.get(i));  }
                        String userId[] = getParameter("id_user").split("="); clientHttpRequest.setParameter(userId[0], userId[1]); */
                            if (getParameter("id_user") != null) {
                                clientHttpRequest.setParameters(getParameter("id_user").split("="));
                            }
                            clientHttpRequest.post();
                        }

                        // In HTTP mode the host Web page may implement a
                        // javascript function injectSignature() which in return
                        // may call applet.returnSignature("hash_i") to get the
                        // signatures in another way.
                        // BUT the true benefit of this is by having the javascript
                        // function redirect the user to another web page !
                        launchJavascriptAction("javascript:injectSignature(\"" + "toto" + "\");");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        signUtil.sign(signingCertificate, privKey, signHandler);
    }

    /** This method is called from within the init() method to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings(value = "unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        signaturePanelAutoSelected = new javax.swing.JPanel();
        infoLabel = new javax.swing.JLabel();
        certViewerPanel = new javax.swing.JPanel();
        name = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        email = new javax.swing.JLabel();
        titreFingerprint = new javax.swing.JLabel();
        titleIssuer = new javax.swing.JLabel();
        issuerName = new javax.swing.JLabel();
        algorithmLabel = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        validityLabel = new javax.swing.JLabel();
        signButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        returnPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        signaturePanelUserSelected = new javax.swing.JPanel();
        useThisCertButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        certificateJList = new javax.swing.JList();
        jLabel4 = new javax.swing.JLabel();
        cancelCertSignButton = new javax.swing.JButton();

        signaturePanelAutoSelected.setBackground(new java.awt.Color(255, 255, 255));
        signaturePanelAutoSelected.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
                                                                                          "Signature de documents",
                                                                                          javax.swing.border.TitledBorder.LEFT,
                                                                                          javax.swing.border.TitledBorder.DEFAULT_POSITION,
                                                                                          new java.awt.Font("Verdana", 0, 11),
                                                                                          new java.awt.Color(102, 102, 255))); // NOI18N
        signaturePanelAutoSelected.setMaximumSize(null);

        infoLabel.setForeground(new java.awt.Color(102, 102, 255));
        infoLabel.setText("Vous vous apprêtez à signer X documents avec le certificat suivant : ");

        certViewerPanel.setBackground(new java.awt.Color(255, 255, 255));

        name.setText("MARTIN Joel");

        jLabel2.setText("Certificat x.509");

        email.setText("jmartin@wanadoo.fr");

        titreFingerprint.setForeground(new java.awt.Color(102, 102, 255));
        titreFingerprint.setText("Algorithme : ");

        titleIssuer.setForeground(new java.awt.Color(102, 102, 255));
        titleIssuer.setText("Autorité de Certification :");

        issuerName.setText("AC");

        algorithmLabel.setText("SHA1WithRSA");

        jLabel3.setForeground(new java.awt.Color(102, 102, 255));
        jLabel3.setText("Valide jusqu' à :");

        validityLabel.setText("2008");

        javax.swing.GroupLayout certViewerPanelLayout = new javax.swing.GroupLayout(certViewerPanel);
        certViewerPanel.setLayout(certViewerPanelLayout);
        certViewerPanelLayout.setHorizontalGroup(
                certViewerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(certViewerPanelLayout.createSequentialGroup()
                                          .addContainerGap()
                                          .addGroup(certViewerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addComponent(jLabel2,
                                                                          javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                          389,
                                                                          Short.MAX_VALUE)
                                                            .addGroup(certViewerPanelLayout.createSequentialGroup()
                                                                              .addGroup(certViewerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING,
                                                                                                                                  false)
                                                                                                .addComponent(titleIssuer,
                                                                                                              javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                              javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                              Short.MAX_VALUE)
                                                                                                .addComponent(titreFingerprint)
                                                                                                .addComponent(jLabel3)
                                                                                                .addComponent(name,
                                                                                                              javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                              javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                              Short.MAX_VALUE))
                                                                              .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                              .addGroup(certViewerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                                                .addComponent(issuerName,
                                                                                                              javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                              206,
                                                                                                              Short.MAX_VALUE)
                                                                                                .addComponent(algorithmLabel,
                                                                                                              javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                              206,
                                                                                                              Short.MAX_VALUE)
                                                                                                .addComponent(validityLabel,
                                                                                                              javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                              206,
                                                                                                              Short.MAX_VALUE)
                                                                                                .addComponent(email,
                                                                                                              javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                              206,
                                                                                                              Short.MAX_VALUE))))
                                          .addContainerGap())
        );
        certViewerPanelLayout.setVerticalGroup(
                certViewerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(certViewerPanelLayout.createSequentialGroup()
                                          .addContainerGap()
                                          .addComponent(jLabel2,
                                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                                        Short.MAX_VALUE)
                                          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                          .addGroup(certViewerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                            .addComponent(name,
                                                                          javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                          javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                          Short.MAX_VALUE)
                                                            .addComponent(email,
                                                                          javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                          javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                          Short.MAX_VALUE))
                                          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                          .addGroup(certViewerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                            .addComponent(titleIssuer)
                                                            .addComponent(issuerName))
                                          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                          .addGroup(certViewerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                            .addComponent(titreFingerprint)
                                                            .addComponent(algorithmLabel))
                                          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                          .addGroup(certViewerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                            .addComponent(jLabel3)
                                                            .addComponent(validityLabel,
                                                                          javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                          14,
                                                                          javax.swing.GroupLayout.PREFERRED_SIZE))
                                          .addContainerGap())
        );

        signButton.setText("Signer");
        signButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                signButtonActionPerformed(evt);
            }
        });

        cancelButton.setText("Annuler");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout signaturePanelAutoSelectedLayout = new javax.swing.GroupLayout(signaturePanelAutoSelected);
        signaturePanelAutoSelected.setLayout(signaturePanelAutoSelectedLayout);
        signaturePanelAutoSelectedLayout.setHorizontalGroup(
                signaturePanelAutoSelectedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(signaturePanelAutoSelectedLayout.createSequentialGroup()
                                          .addContainerGap()
                                          .addGroup(signaturePanelAutoSelectedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addComponent(certViewerPanel,
                                                                          javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                          javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                          javax.swing.GroupLayout.PREFERRED_SIZE)
                                                            .addComponent(infoLabel,
                                                                          javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                          448,
                                                                          Short.MAX_VALUE)
                                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
                                                                      signaturePanelAutoSelectedLayout.createSequentialGroup()
                                                                              .addComponent(signButton,
                                                                                            javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                            80,
                                                                                            javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                              .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                              .addComponent(cancelButton,
                                                                                            javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                            78,
                                                                                            javax.swing.GroupLayout.PREFERRED_SIZE)))
                                          .addContainerGap())
        );
        signaturePanelAutoSelectedLayout.setVerticalGroup(
                signaturePanelAutoSelectedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(signaturePanelAutoSelectedLayout.createSequentialGroup()
                                          .addContainerGap()
                                          .addComponent(infoLabel)
                                          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                          .addComponent(certViewerPanel,
                                                        javax.swing.GroupLayout.PREFERRED_SIZE,
                                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                          .addGroup(signaturePanelAutoSelectedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                            .addComponent(cancelButton)
                                                            .addComponent(signButton))
                                          .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        returnPanel.setBackground(new java.awt.Color(76, 76, 76));
        returnPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
                                                                           "Informations",
                                                                           javax.swing.border.TitledBorder.LEFT,
                                                                           javax.swing.border.TitledBorder.DEFAULT_POSITION,
                                                                           new java.awt.Font("Verdana", 0, 11),
                                                                           new java.awt.Color(102, 102, 255))); // NOI18N
        returnPanel.setForeground(new java.awt.Color(242, 241, 241));

        jLabel1.setBackground(new java.awt.Color(76, 76, 76));
        jLabel1.setFont(new java.awt.Font("Verdana", Font.BOLD, 15)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(242, 241, 240));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("L'opération de signature s'est correctement déroulée.");

        javax.swing.GroupLayout returnPanelLayout = new javax.swing.GroupLayout(returnPanel);
        returnPanel.setLayout(returnPanelLayout);
        returnPanelLayout.setHorizontalGroup(
                returnPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(returnPanelLayout.createSequentialGroup()
                                          .addContainerGap()
                                          .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 448, Short.MAX_VALUE)
                                          .addContainerGap())
        );
        returnPanelLayout.setVerticalGroup(
                returnPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(returnPanelLayout.createSequentialGroup()
                                          .addContainerGap()
                                          .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE)
                                          .addContainerGap())
        );

        signaturePanelUserSelected.setBackground(new java.awt.Color(255, 255, 255));
        signaturePanelUserSelected.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
                                                                                          " Choix du certificat de signature ",
                                                                                          javax.swing.border.TitledBorder.LEFT,
                                                                                          javax.swing.border.TitledBorder.DEFAULT_POSITION,
                                                                                          new java.awt.Font("Verdana", 0, 11),
                                                                                          new java.awt.Color(102, 102, 255))); // NOI18N

        useThisCertButton.setText("Signer avec ce certificat");
        useThisCertButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                useThisCertButtonActionPerformed(evt);
            }
        });

        jScrollPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 255)));

        certificateJList.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        certificateJList.setForeground(new java.awt.Color(0, 0, 204));
        certificateJList.setModel(certListModel);
        certificateJList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        certificateJList.setCellRenderer(CertificateListCellRender.getInstance());
        certificateJList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                certificateJListValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(certificateJList);

        jLabel4.setFont(new java.awt.Font("Verdana", 0, 10)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(102, 102, 255));
        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource(
                "/org/adullact/parapheur/applets/splittedsign/toolbar_get_info-16px.png"))); // NOI18N
        jLabel4.setText("Notice légale");
        jLabel4.setToolTipText("Notice légale, à propos de l'outil de signature");
        jLabel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel4MouseClicked(evt);
            }
        });

        cancelCertSignButton.setText("Annuler");
        cancelCertSignButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelCertSignButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout signaturePanelUserSelectedLayout = new javax.swing.GroupLayout(signaturePanelUserSelected);
        signaturePanelUserSelected.setLayout(signaturePanelUserSelectedLayout);
        signaturePanelUserSelectedLayout.setHorizontalGroup(
                signaturePanelUserSelectedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(signaturePanelUserSelectedLayout.createSequentialGroup()
                                          .addContainerGap()
                                          .addGroup(signaturePanelUserSelectedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addComponent(jScrollPane1,
                                                                          javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                          448,
                                                                          Short.MAX_VALUE)
                                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
                                                                      signaturePanelUserSelectedLayout.createSequentialGroup()
                                                                              .addComponent(jLabel4)
                                                                              .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                                                                                               81,
                                                                                               Short.MAX_VALUE)
                                                                              .addComponent(cancelCertSignButton)
                                                                              .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                              .addComponent(useThisCertButton)))
                                          .addContainerGap())
        );
        signaturePanelUserSelectedLayout.setVerticalGroup(
                signaturePanelUserSelectedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, signaturePanelUserSelectedLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 157, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(signaturePanelUserSelectedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                  .addComponent(useThisCertButton)
                                                  .addComponent(jLabel4)
                                                  .addComponent(cancelCertSignButton))
                                .addContainerGap())
        );

        signaturePanelUserSelected.getAccessibleContext().setAccessibleDescription("");

        getContentPane().setLayout(new java.awt.GridLayout(1, 0));
    }// </editor-fold>//GEN-END:initComponents

    private void signButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_signButtonActionPerformed
        sign();
    }//GEN-LAST:event_signButtonActionPerformed

    private void useThisCertButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_useThisCertButtonActionPerformed
        infiniteProgress = new InfiniteProgressPanel();
        this.setGlassPane(infiniteProgress);
        infiniteProgress.setSize(getWidth(), getHeight());
        infiniteProgress.start();
        validate();

        signingCertificate = (X509Certificate) certificateJList.getSelectedValue();

        privKey = certListModel.getKey(signingCertificate);

        Configurator.isMacOsX();
        // quelques traces
        System.out.println("\t######################################################");
        System.out.println("\tsigningCertificate: " + signingCertificate.getSubjectX500Principal());
        System.out.println("\tprivKey: " + privKey);
        System.out.println("\t######################################################");

        infiniteProgress.setText("Prêt à signer...");
        validate();
        repaint();

        // STV 2013-12-24 Xmas hack : put the signing in a separate thread
        signingThread = new Thread(this);
        signingThread.start();
        // sign();

        // infiniteProgress.stop();
    }//GEN-LAST:event_useThisCertButtonActionPerformed

    @Override
    public void run() {
        // exclusive to the thread "signThread"
        sign();
        infiniteProgress.stop();
    }


    private void launchJavascriptAction(final String action) {
        /*
         * Protection du contexte d'usage, pour éviter une fenetre modale
         * http://docs.oracle.com/javase/7/docs/api/java/security/AccessController.html
         */
        AccessController.doPrivileged(new PrivilegedAction<Void>() {
            @Override
            public Void run() {
                try {
                    getAppletContext().showDocument(new URL(action));
                } catch (MalformedURLException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null; // nothing to return
            }
        });
    }

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        if (isCancelModeJavaScript) {
            launchJavascriptAction("javascript:cancelSignature();");
        } else {
            launchJavascriptAction("javascript:history.go(-1);");
        }
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void jLabel4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseClicked
        new AboutDialog(this).display();
        // JOptionPane.showMessageDialog(this, aboutMessage, "A propos de LiberSign", JOptionPane.INFORMATION_MESSAGE, null);
    }//GEN-LAST:event_jLabel4MouseClicked

    private void cancelCertSignButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelCertSignButtonActionPerformed
        if (isCancelModeJavaScript) {
            launchJavascriptAction("javascript:cancelSignature();");
        } else {
            launchJavascriptAction("javascript:history.go(-1);");
        }
    }//GEN-LAST:event_cancelCertSignButtonActionPerformed
    // End of variables declaration//GEN-END:variables

    private void certificateJListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_certificateJListValueChanged
        System.out.print("certificateJListValueChanged : ");
        if (certificateJList.getSelectedIndex() == -1) {
            // no selection
            useThisCertButton.setText("Sélectionnez un certificat");
            useThisCertButton.setEnabled(false);
        } else {
            System.out.println(" Certificat sélectionné.");
            useThisCertButton.setText("Signer avec ce certificat");
            useThisCertButton.setEnabled(true);
        }
    }//GEN-LAST:event_certificateJListValueChanged
}

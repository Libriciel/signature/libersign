/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.parapheur.applets.splittedsign;


import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CertChooser {

    public static X509Certificate getCertificate(String ac, String id, String cn) throws CertificateException, KeyStoreException {

        KeyStore ks = Configurator.getInstance().getKeyStore();

        for (Enumeration<String> enumAlias = ks.aliases(); enumAlias.hasMoreElements();) {
            String actualAlias = enumAlias.nextElement();
            if (ks.entryInstanceOf(actualAlias, KeyStore.PrivateKeyEntry.class)) {
                X509Certificate actualCert = (X509Certificate) ks.getCertificate(actualAlias);
                HashMap<String, String> certInfos = CertificateInfosExtractor.makeSubjectInfos(actualCert);
                if (certInfos.get("CN").equalsIgnoreCase(ac) && certInfos.get("ID").equalsIgnoreCase(id) && certInfos.get("CN").equalsIgnoreCase(cn)) {
                    if (isValidCertificate(actualCert)) {
                        return actualCert;
                    } else {
                        throw new CertificateException("Invalid Certificate");
                    }
                }
            }
        }

        throw new CertificateException("No matching certificate in user KeyStore");

    }

    public static String getMatchingEntryAlias(String ac, String id /*BigInteger serialNumber*/, String cn) throws KeyStoreException, CertificateException, IOException, NoSuchAlgorithmException {

        KeyStore ks = Configurator.getInstance().getKeyStore();
        if (null == ks) {
            Logger.getLogger("org.adullact.parapheur.applets.splittedsignatureapplet").severe("KeyStore NULL !!");
            throw new KeyStoreException("Unable to access/open the keystore");
        }
        String actualAlias;
        for (Enumeration<String> enumAlias = ks.aliases(); enumAlias.hasMoreElements();) {
            actualAlias = enumAlias.nextElement();
            if (ks.entryInstanceOf(actualAlias, KeyStore.PrivateKeyEntry.class)) {
                X509Certificate actualCert = (X509Certificate) ks.getCertificate(actualAlias);
                HashMap<String, String> subjectCertInfos = CertificateInfosExtractor.makeSubjectInfos(actualCert);
                HashMap<String, String> issuerInfos = CertificateInfosExtractor.makeIssuerInfos(actualCert);//  makeSubjectInfos(actualCert);
                if (actualCert==null) {
                    throw new CertificateException("Could not get the certificate from keyStore for alias ["+ actualAlias +"]!!!");
                }
                if (subjectCertInfos==null) {
                    throw new CertificateException("Could not have subjectCertInfos (is null) from certificate alias ["+ actualAlias +"]!!!");
                }
                if (issuerInfos==null) {
                    throw new CertificateException("Could not have issuerInfos (is null) from certificate alias ["+ actualAlias +"]!!!");
                }
                if (issuerInfos.get("CN")!=null && issuerInfos.get("CN").equalsIgnoreCase(ac) && 
                        /*actualCert.getSerialNumber()==serialNumber && */ 
                        subjectCertInfos.get("CN")!=null && subjectCertInfos.get("CN").equalsIgnoreCase(cn)) {
                    if (isValidCertificate(actualCert)) {
                        return actualAlias;
                    } else {
                        throw new CertificateException("Invalid Certificate");
                    }
                }
                if (subjectCertInfos.get("CN")==null) {
                    Logger.getLogger("org.adullact.parapheur.applets.splittedsignatureapplet").log(Level.SEVERE, "subjectCertInfos has no CN field for certificate alias [{0}]???", actualAlias);
                } else  if (issuerInfos.get("CN")==null) {
                    Logger.getLogger("org.adullact.parapheur.applets.splittedsignatureapplet").log(Level.SEVERE, "issuerInfos has no CN field for certificate alias [{0}]???", actualAlias);
                } else {
                    Logger.getLogger("org.adullact.parapheur.applets.splittedsignatureapplet").log(Level.SEVERE, "Alias=[{0}], issuerInfos.getCN={1}//{2}, subjectCertInfos.getCN={3}//{4}",
                            new Object[]{actualAlias, issuerInfos.get("CN"), ac, subjectCertInfos.get("CN"), cn});
                }
            }
        }
        throw new CertificateException("No matching certificate in user KeyStore");

    }

    public static X509Certificate getVerifiedCertificate(String ac, String id, String cn) throws KeyStoreException, CertificateException, IOException, NoSuchAlgorithmException {
        String alias = getMatchingEntryAlias(ac, id, cn);
        KeyStore ks = Configurator.getInstance().getKeyStore();
        if (ks == null) {
            Logger.getLogger("org.adullact.parapheur.applets.splittedsignatureapplet").severe("keystore is NULL ?!");
            throw new KeyStoreException("impossible d'accéder au certificat electronique");
        }
        return (X509Certificate) ks.getCertificate(alias);
    }

    public static PrivateKey getVerifiedPrivateKey(String ac, String id, String cn) throws KeyStoreException, NoSuchAlgorithmException, UnrecoverableKeyException, CertificateException, IOException {
        String alias = getMatchingEntryAlias(ac, id, cn);
        KeyStore ks = Configurator.getInstance().getKeyStore();
        return (PrivateKey) ks.getKey(alias, Configurator.getInstance().getPassword(false));
    }

    /**
     * Dans le cadre du produit actuel, la validité du certificat a été
     * vérifiée en amont. Nous laissons cependant cette méthode pour
     * l'extensibilité de l'applet
     * @param cert
     * @return
     */
    private static Boolean isValidCertificate(X509Certificate cert) {
        return true;
    }

}
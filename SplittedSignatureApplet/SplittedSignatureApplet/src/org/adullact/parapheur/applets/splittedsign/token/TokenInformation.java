/** 
 * This file is part of LIBERSIGN.
 * 
 * Copyright (c) 2008-2014, ADULLACT-Projet
 *  Initiated by ADULLACT-Projet S.A.
 *  Developped by ADULLACT-Projet S.A.
 *
 *  contact@adullact-projet.coop
 * 
 * LIBERSIGN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LIBERSIGN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with LIBERSIGN.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.adullact.parapheur.applets.splittedsign.token;

/**
 * From project j4ops (code.google.com) GPLv3
 * 
 * @author fzanutto , svast
 */
public class TokenInformation {
    private String atr;
    private int slotID;
    private String terminalName;
    private String driver;
    private String driverDescription;
    private boolean tokenPresent;
   
    public String getAtr() {
        return atr;
    }

    public void setAtr(String atr) {
        this.atr = atr;
    }

    public int getSlotID() {
        return slotID;
    }

    public void setSlotID(int slotID) {
        this.slotID = slotID;
    }

    public String getTerminalName() {
        return terminalName;
    }

    public void setTerminalName(String terminalName) {
        this.terminalName = terminalName;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getDriverDescription() {
        return driverDescription;
    }

    public void setDriverDescription(String driverDescription) {
        this.driverDescription = driverDescription;
    }

    public boolean isTokenPresent() {
        return tokenPresent;
    }

    public void setTokenPresent(boolean tokenPresent) {
        this.tokenPresent = tokenPresent;
    }

    @Override
    public String toString() {
        return terminalName;
    }
    
}

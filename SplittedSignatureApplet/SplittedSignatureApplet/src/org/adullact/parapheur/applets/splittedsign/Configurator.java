/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.parapheur.applets.splittedsign;

import iaik.pkcs.pkcs11.Module;
import iaik.pkcs.pkcs11.provider.IAIKPkcs11;
import iaik.security.provider.IAIK;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.cert.CertificateException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.smartcardio.CardException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;
import org.adullact.parapheur.applets.splittedsign.pcsc.CardInfo;
import org.adullact.parapheur.applets.splittedsign.pcsc.PCSCHelper;
import org.adullact.parapheur.applets.splittedsign.providers.BaseSignHandler;
import org.adullact.parapheur.applets.splittedsign.providers.IaikPkcs11;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class Configurator {
    
//    static {
//        if (System.getProperty("os.name", "Windows").startsWith("Mac")){
//            try {
//                System.out.println("lib path: " + System.getProperty("java.library.path"));
//                pkcs11Driver = Module.getInstance("libpkcs11wrapper.jnilib");
//            } catch (IOException ex) {
//                Logger.getLogger(Configurator.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }
    /**
     * on est des fous: si Mac, alors gros HACK pourri.
     */
    static {
        if (System.getProperty("os.name", "Windows").startsWith("Mac")) {
            System.out.println("###### LIB path: " + System.getProperty("java.library.path"));
            System.loadLibrary("pkcs11wrapper");
            System.out.println("###### J'ai charge pkcs11wrapper passque j'suis sur '"+ System.getProperty("os.name", "Windows") +"'.");
        }
    }

    private static String hostOperatingSystem;
    private char[] password;
    private IaikPkcs11 signProvider;

    private Configurator() {
        hostOperatingSystem = System.getProperty("os.name", "Windows");
    }

    public static Configurator getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new Configurator();
        }
        return uniqueInstance;
    }

    public static void  reset() {
        ks = null;
        keyStoreLoaded = false;
        uniqueInstance = null;
        
        // macintosheries
        macTokenks = null;
        macTokenkeyStoreLoaded = false;
    }

    public KeyStore getKeyStore() throws KeyStoreException {
        try {
            if (isWindows()) {
                System.out.println("OS: " + hostOperatingSystem + ", is Windows...");
                if (!keyStoreLoaded) {
                    ks = KeyStore.getInstance("Windows-MY");
                    ks.load(null, null);
                    keyStoreLoaded = true;
                }
//                java.util.Set<java.util.Map.Entry<Object, Object>> s = ks.getProvider().entrySet();
//                int i=0;
//		for (java.util.Map.Entry<Object, Object> e : s) {
//			System.out.println(i++ + " * " + e.getKey() + ":\n\t" +e.getValue());
//		}

                return ks;
            } else if (isMacOsX()) {
                System.out.println("OS: " + hostOperatingSystem + ", is OSX...");
                if (!keyStoreLoaded) {
                    ks = KeyStore.getInstance("KeychainStore");// , "Apple");
                    ks.load(null, null);
                    keyStoreLoaded = true;
                }
                // code snippet from  http://lists.apple.com/archives/Java-dev/2007/Aug/msg00132.html
                //   KeyStore keyStore = KeyStore.getInstance("KeychainStore", "Apple");
                //   keyStore.load(null, null);
                /*
                java.util.Enumeration<String> aliases = ks.aliases();
                while (aliases.hasMoreElements()) {
                    String alias = aliases.nextElement();
                    System.out.print("Alias [" + alias + "]=");
                    if (ks.isKeyEntry(alias)) {
                        Key k = ks.getKey(alias, "-".toCharArray());
                        System.out.println(" Key found: " + (k==null ? "<null> (cannot retrieve key)" : k));
                    } else if (ks.isCertificateEntry(alias)) {
                        java.security.cert.Certificate certificate = ks.getCertificate(alias);
                        if (certificate instanceof java.security.cert.X509Certificate) {
                            java.security.cert.X509Certificate x509certificate = (java.security.cert.X509Certificate) certificate;
                            System.out.println(" Certificate: " + x509certificate.getSubjectX500Principal().getName());
                        } else {
                            System.out.println(" Unknown format certificate: " + alias);
                        }
                    } else {
                        System.out.println(" The alias is unknown: " + alias);
                    }
                }  */
                
                // Taken from http://lists.apple.com/archives/apple-cdsa/2008/Feb/msg00016.html
                // At the moment, suppose the CN fragment of my Subject DN is
                //  'firstname lastname', the alias name for my private key is 
                // going to be 'firstname lastname', and the one for the 
                // corresponding cert is going to be 'firstname lastname 1'. 
                // If there's no key, the alias name seems to be only the CN 
                // fragment (so a number is added only if there already is a 
                // key). In practice, this makes it very difficult to have a 
                // consistent configuration mechanism from a Java application, 
                // since it always depends on what other certificates may be 
                // available in the keychains.
                
                return ks;
            } else {
                System.out.println("OS: " + hostOperatingSystem + ", is Default... default KS=" + System.getProperty("javax.net.ssl.keyStore"));
                System.out.println("  java.home=" + System.getProperty("java.home"));
                System.out.println("  java.ext.dirs=" + System.getProperty("java.ext.dirs"));
                if (!keyStoreLoaded) {
                    /*
                     * If token: keystore SunPKCS#11 with lib-provider ??
                     * needs config file: periph-ID + path to .so library, 
                     * through OpenSC ... Pas simple du tout!
                     * If soft: P12 through keystore JKS??
                     */
                    //ks = KeyStore.getInstance("JKS");
                    //ks = KeyStore.getInstance("JKS", "SUN");
                    //ks = KeyStore.getInstance(KeyStore.getDefaultType());

                    //ks.load(null, null);
                    //  autre méthode
                    //String filename = System.getProperty("java.home")  + "/lib/security/cacerts".replace('/', File.separatorChar);
                    //FileInputStream is = new FileInputStream(filename);
                    //String kspassword = "";
                    //ks.load(is, kspassword.toCharArray());

                    if (ks != null) {
                        keyStoreLoaded = true;
                        System.out.println("keystore pas null");
                        return ks;
                    } else {
                        ks = getPkcs12KeyStore();
                        if (ks != null) {
                            keyStoreLoaded = true;
                            System.out.println("keystore PKCS12 pas null");
                            return ks;
                        } else {
                            System.out.println("keystore tout null");
                            throw new UnsupportedOperationException("Linux support still under development; Not yet implemented");
                        }
                    }
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(Configurator.class.getName()).log(Level.SEVERE, null, ex);
            throw new KeyStoreException(ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Configurator.class.getName()).log(Level.SEVERE, null, ex);
            throw new KeyStoreException(ex);
        } catch (CertificateException ex) {
            Logger.getLogger(Configurator.class.getName()).log(Level.SEVERE, null, ex);
            throw new KeyStoreException(ex);
//        } catch (java.security.NoSuchProviderException ex) {
//            System.out.println("Loader keystore (probably Mac OSX) : NoSuchProviderException");
//            // Maybe list the registered security providers?
//            // TODO...
//            Logger.getLogger(Configurator.class.getName()).log(Level.SEVERE, null, ex);
//            throw new KeyStoreException(ex);
            
            
            
            
   /*     } catch (java.security.UnrecoverableKeyException ex) {
            Logger.getLogger(Configurator.class.getName()).log(Level.SEVERE, null, ex);
            throw new KeyStoreException(ex);  */
        }
        return ks;
    }

    public java.security.cert.X509Certificate getMacTokenX509Certificate(int index) {
        if (!macTokenkeyStoreLoaded) {
            return null;
        }
        return signProvider.getX509Certificate();
    }
    public org.adullact.parapheur.applets.splittedsign.providers.IaikPkcs11 getMacTokenSigProvider() {
        return signProvider;
    }

    /**
     * pour le xades sur mac (heresie)
     */
    private IAIKPkcs11 pkcs11Provider_;
    public String getPkcs11providerName() {
        if (pkcs11Provider_ == null) {
            pkcs11Provider_ = new IAIKPkcs11();
            Security.addProvider(pkcs11Provider_);
        }
        return pkcs11Provider_.getName();
    }
    public KeyStore getMacTokenKeyStore() throws KeyStoreException {
        System.out.print("Configurator::getMacTokenKeyStore"); //   OS: " + hostOperatingSystem);
        try {
            if (isMacOsX()) {
                System.out.println("  is Apple-OSX family...");
                if (!macTokenkeyStoreLoaded) {
                    System.out.println("  Module init: 1. ");

                    Security.addProvider(new BouncyCastleProvider());
                    Security.addProvider(new IAIK());   // au cas où.... lol

                    System.out.println("  JAVA.LIBRARY.PATH=" + System.getProperty("java.library.path"));
                    
                    //pkcs11Driver = Module.getInstance("pkcs11wrapper");
                    //pkcs11Driver = Module.getInstance("/Library/Java/Extensions/libpkcs11wrapper.jnilib");
            //        pkcs11Driver = Module.getInstance("pkcs11wrapper", "/Library/Java/Extensions/libpkcs11wrapper.jnilib");
            //        if (pkcs11Driver == null) {
            //            System.out.println("HELP !. ");
            //        }
            //        pkcs11Driver.initialize(null);
                    
                    System.out.println("  Module init: 2. ");
                    
                    
                    signProvider = new IaikPkcs11("tokens.xml");
                    System.out.println("  Module init: IaikPkcs11. ");

                    BaseSignHandler signHandler = new BaseSignHandler();
                    System.out.println("  Module init: BaseSignHandler. ");
                                
                    // String digestAlgName, String encryptionAlgName,
                    //        SignProviderHandler handlerProvider, String securityProvider
                    signProvider.init("SHA1", "RSA", signHandler, "BC");
                                
                    // Demande du mot de passe si token present
                    if (password == null || password.length==0) {
                        password = getPassword(true);
                    }

                    
                    
                    macTokenkeyStoreLoaded = true;

                    macTokenks = KeyStore.getInstance("PKCS11KeyStore");
                        
                    
                    System.out.print(", 10.  ");
                    if (macTokenks != null) {
                        System.out.print(" 11.  ");
                        macTokenks.load(null, password);
                        macTokenkeyStoreLoaded = true;
                    } else {
                        System.out.println("### WARN Got no key store for Token. If there is, ensure that the provider is properly configured and installed.");
                    }
                }
                return macTokenks;

            } else {
                System.out.println("  is NOT OSX..., so abort gently  :-)");
            }
        } catch (IOException ex) {
            System.out.println("Configurator I/O ex ..................");
            Logger.getLogger(Configurator.class.getName()).log(Level.SEVERE, null, ex);
            throw new KeyStoreException(ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Configurator.class.getName()).log(Level.SEVERE, null, ex);
            throw new KeyStoreException(ex);
        } catch (CertificateException ex) {
            Logger.getLogger(Configurator.class.getName()).log(Level.SEVERE, null, ex);
            throw new KeyStoreException(ex);
        } catch (CardException ex) {
            Logger.getLogger(Configurator.class.getName()).log(Level.SEVERE, null, ex);
            throw new KeyStoreException(ex);
        } catch (Exception ex) {
            System.out.println(" Exception passee dans les trous de la raquette: " + ex.getLocalizedMessage());
            ex.printStackTrace();
        }
        return macTokenks;
    }
    // private static Module pkcs11Driver;
    static Module pkcs11Module_;
    public void destroySession() throws Exception {
        if (signProvider != null) {
            System.out.println("\tDestroy the already existing signature provider...");
            signProvider.destroy();
        }
    }
    private KeyStore getPkcs12KeyStore() throws KeyStoreException, NoSuchAlgorithmException, CertificateException//GeneralSecurityException
    {
        KeyStore keyStore = null;
        while (keyStore == null) {
            try {
                // On a besoin d'un fichier PFX (InputStream) et de son mot de
                // passe (char[])
                JFileChooser p12chooser = new JFileChooser();
                p12chooser.removeChoosableFileFilter(p12chooser.getAcceptAllFileFilter());
                p12chooser.setDialogTitle("Ouverture du certificat");
                p12chooser.setFileFilter(new FileFilter() {

                    @Override
                    public String getDescription() {
                        return "Échange d'informations personnelles";
                    }

                    @Override
                    public boolean accept(File f) {
                        if (f.isDirectory()) {
                            return true;
                        } else {
                            String filename = f.getName().toUpperCase();
                            return filename.endsWith(".PFX") || filename.endsWith(".P12");
                        }
                    }
                });
                if (p12chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                    keyStore = KeyStore.getInstance("PKCS12");
                    InputStream is = new FileInputStream(p12chooser.getSelectedFile());
                    char[] passwd = getPassword(false);
                    keyStore.load(is, passwd);
                }
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, "Impossible de charger le certificat.\nVérifiez que le fichier choisi est valide, et que le mot de passe est correct.");
                password = null;
                keyStore = null;
            }
        }
        return keyStore;
    }

    /**
     * Get password protecting private key. Meaningfull on Linux, and MacOS-X (token)
     * 
     * @return the password
     */
    public char[] getPassword(boolean requestCodePIN) {
        if (password == null) {
            if (isWindows()) {
                // Password is not required when using MSCAPI
                password = "".toCharArray();
            }
            else if (isMacOsX()) {
                /**
                 * Mac OSX platform specific
                 */
                if (requestCodePIN) {
                    JPasswordField passwdField = new JPasswordField() {
                            @Override
                            public void addNotify() {
                                super.addNotify();
                                requestFocus();
                            }
                        };
                    if (JOptionPane.OK_OPTION == JOptionPane.showOptionDialog(null, // parent component - determines the Frame in which the dialog is displayed; if null, or if the parentComponent has no Frame, a default Frame is used
                            passwdField,        // message - the Object to display 
                            "Saisie du code PIN",      // title - the title string for the dialog
                            JOptionPane.OK_CANCEL_OPTION,   // optionType - an integer designating the options available on the dialog: DEFAULT_OPTION, YES_NO_OPTION, YES_NO_CANCEL_OPTION, or OK_CANCEL_OPTION 
                            JOptionPane.QUESTION_MESSAGE,   // messageType - an integer designating the kind of message this is, primarily used to determine the icon from the pluggable Look and Feel: ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE, QUESTION_MESSAGE, or PLAIN_MESSAGE
                            null,   // icon - the icon to display in the dialog
                            null,   // options - an array of objects indicating the possible choices the user can make; if the objects are components, they are rendered properly; non-String objects are rendered using their toString methods; if this parameter is null, the options are determined by the Look and Feel
                            null    // initialValue - the object that represents the default selection for the dialog; only meaningful if options is used; can be null
                            )) {
                        password = passwdField.getPassword();
                    }
                    
                } else {
                    password = "-".toCharArray();
                }
            }
            else {
                /**
                 * The fallback for any system not Windows nor Apple MacOS
                 * 
                 * GNU/Linux for PKCS12: get user input
                 */ 
                JPasswordField passwdField = new JPasswordField() {
                        @Override
                        public void addNotify() {
                            super.addNotify();
                            requestFocus();
                        }
                    };
                if (JOptionPane.OK_OPTION == JOptionPane.showOptionDialog(null, // parent component - determines the Frame in which the dialog is displayed; if null, or if the parentComponent has no Frame, a default Frame is used
                        passwdField,        // message - the Object to display 
                        "Pass phrase",      // title - the title string for the dialog
                        JOptionPane.OK_CANCEL_OPTION,   // optionType - an integer designating the options available on the dialog: DEFAULT_OPTION, YES_NO_OPTION, YES_NO_CANCEL_OPTION, or OK_CANCEL_OPTION 
                        JOptionPane.QUESTION_MESSAGE,   // messageType - an integer designating the kind of message this is, primarily used to determine the icon from the pluggable Look and Feel: ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE, QUESTION_MESSAGE, or PLAIN_MESSAGE
                        null,   // icon - the icon to display in the dialog
                        null,   // options - an array of objects indicating the possible choices the user can make; if the objects are components, they are rendered properly; non-String objects are rendered using their toString methods; if this parameter is null, the options are determined by the Look and Feel
                        null    // initialValue - the object that represents the default selection for the dialog; only meaningful if options is used; can be null
                        )) {
                    password = passwdField.getPassword();
                }
            }
        }
        return password;
    }

      public void applyLookAndFeel() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ex) {
            Logger.getLogger(Configurator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setSignatureFormat(int format) {
        this.signatureFormat = format;
    }

    public int getSignatureFormat() {
        return this.signatureFormat;
    }

    /**
     * Detection of the Host Operating System, because keystore behavior is 
     * clearly not the same from one OS to another. :-(
     * 
     * @return True of False
     */
    public static boolean isWindows() {
        return hostOperatingSystem.startsWith("Windows");
    }

    public static boolean isMacOsX() {
        return hostOperatingSystem.startsWith("Mac");
    }

    private static boolean isLinux() {
        return hostOperatingSystem.startsWith("Linux");
    }

    /**
     * Following source code taken from Apache commons-lang package
     */
    public static final String JAVA_VERSION = getSystemProperty("java.version");
    
    private static String getSystemProperty(String property) {
        try {
            return System.getProperty(property);
        } catch (SecurityException ex) {
            // we are not allowed to look at this property
            System.err.println("Caught a SecurityException reading the system property '" + property
                    + "'; the SystemUtils property value will default to null.");
            return null;
        }
    }

    private static String[] splitWorker(String str, String separatorChars, int max, boolean preserveAllTokens) {
        // Performance tuned for 2.0 (JDK1.4)
        // Direct code is quicker than StringTokenizer.
        // Also, StringTokenizer uses isSpace() not isWhitespace()

        if (str == null) {
            return null;
        }
        int len = str.length();
        if (len == 0) {
            return new String[0];
        }
        java.util.List list = new java.util.ArrayList();
        int sizePlus1 = 1;
        int i = 0, start = 0;
        boolean match = false;
        boolean lastMatch = false;
        if (separatorChars == null) {
            // Null separator means use whitespace
            while (i < len) {
                if (Character.isWhitespace(str.charAt(i))) {
                    if (match || preserveAllTokens) {
                        lastMatch = true;
                        if (sizePlus1++ == max) {
                            i = len;
                            lastMatch = false;
                        }
                        list.add(str.substring(start, i));
                        match = false;
                    }
                    start = ++i;
                    continue;
                }
                lastMatch = false;
                match = true;
                i++;
            }
        } else if (separatorChars.length() == 1) {
            // Optimise 1 character case
            char sep = separatorChars.charAt(0);
            while (i < len) {
                if (str.charAt(i) == sep) {
                    if (match || preserveAllTokens) {
                        lastMatch = true;
                        if (sizePlus1++ == max) {
                            i = len;
                            lastMatch = false;
                        }
                        list.add(str.substring(start, i));
                        match = false;
                    }
                    start = ++i;
                    continue;
                }
                lastMatch = false;
                match = true;
                i++;
            }
        } else {
            // standard case
            while (i < len) {
                if (separatorChars.indexOf(str.charAt(i)) >= 0) {
                    if (match || preserveAllTokens) {
                        lastMatch = true;
                        if (sizePlus1++ == max) {
                            i = len;
                            lastMatch = false;
                        }
                        list.add(str.substring(start, i));
                        match = false;
                    }
                    start = ++i;
                    continue;
                }
                lastMatch = false;
                match = true;
                i++;
            }
        }
        if (match || (preserveAllTokens && lastMatch)) {
            list.add(str.substring(start, i));
        }
        return (String[]) list.toArray(new String[list.size()]);
    }

    // à utiliser avec JAVA_VERSION
    private boolean isRequiredVersionOK(String version) {
        if (version == null) {
            return false;
        }
        float javaVersion;
        String[] strings = splitWorker(version, "._- ", -1, false);
        int complex = Math.min(3, strings.length);
        int[] ints = new int[complex];
        int j = 0;
        for (int i = 0; i < strings.length && j < 3; i++) {
            String s = strings[i];
            if (s.length() > 0) {
                try {
                    ints[j] = Integer.parseInt(s);
                    j++;
                } catch (NumberFormatException e) {
                }
            }
        }
        if (complex > j) {
            int[] newInts = new int[j];
            System.arraycopy(ints, 0, newInts, 0, j);
            ints = newInts;
        }
        if (ints == null || ints.length == 0) {
            javaVersion = 0f;
        } else if (ints.length == 1) {
            javaVersion = ints[0];
        } else {
            StringBuilder builder = new StringBuilder();
            builder.append(ints[0]);
            builder.append('.');
            for (int i = 1; i < ints.length; i++) {
                builder.append(ints[i]);
            }
            try {
                javaVersion = Float.parseFloat(builder.toString());
            } catch (NumberFormatException ex) {
                javaVersion = 0f;
            }
        }
        return (javaVersion >= 1.6);
    }

    
    
    
    
    
    
        
    /**
     * This triggers the PCSC wrapper stuff; a {@link PCSCHelper} class
     * is used to detect reader and token presence, trying also to provide a
     * candidate PKCS#11 cryptoki for it; detection is bypassed if an
     * applet parameter forcing cryptoki selection is provided.
     * 
     * Used in PKCS7SignUtil for example...
     * 
     * @return true if a token with corresponding candidate cryptoki is detected.
     * @throws IOException
     */
    public boolean detectCardAndCriptoki() throws IOException {
        CardInfo ci = null;
        boolean cardPresent;
        System.out.println("\t========= DETECTING CARD ===========");
        
        System.out.println("\tResetting cryptoki name");
        setCryptokiLib(null);
        
        if (getParameter("cryptokilib") != null){
            System.out.println("\tGetting cryptoki name from Applet parameter 'cryptokilib': "+getParameter("cryptokilib"));
            setCryptokiLib(getParameter("cryptokilib"));
        }
        else {
            System.out.println("\t       Trying to detect card via PCSC ...");
            //            JNIUtils jni = new JNIUtils();
            //            jni.loadLibrary("OCFPCSC1");
            //            jni.loadLibrary("pkcs11wrapper");

            PCSCHelper pcsc = new PCSCHelper(true);
            List cards = pcsc.findCards();
            cardPresent = !cards.isEmpty();
            if (cardPresent) {
                ci = (CardInfo) cards.get(0);
                System.out.println("\n\t       For signing we will use card: '"
                        + ci.getProperty("description") + "' with criptoki '"
                        + ci.getProperty("lib") + "'");
                setCryptokiLib(ci.getProperty("lib"));

            } else
                System.out.println("Sorry, no card detected!");
        }
        System.out.println("\t=================================");
        return ((ci != null) || (getCryptokiLib() != null));
    }

    private java.lang.String cryptokiLib = null;
    /**
     * Gets the native cryptoki library name; this is the library
     * provided by the token manufacturer that implements the PKCS#11 standard API.
     * 
     * @return java.lang.String
     */
    private java.lang.String getCryptokiLib() {
        return cryptokiLib;
    }

    /**
     * Sets the native PKCS#11 implementation to use.
     * 
     * @param newCryptokiLib
     *            java.lang.String name of the native library
     */
    private void setCryptokiLib(java.lang.String newCryptokiLib) {
        cryptokiLib = newCryptokiLib;
        System.out.println("\tUsing cryptoki: " + getCryptokiLib());
    }
    private String getParameter(String toto) {
        if (toto.equals("cryptokilib")) {
            return "/usr/lib/pkcs11/libgclib.dylib";  // hack pour rigoler
        }
        return null;
    }

    
    
    
    
    
    
    
    /**
     * Class members...
     */
    
    public static int XADES_SIGNATURE = 0;
    public static int CMS_SIGNATURE = 1;
    public static int PESV2_SIGNATURE = 2;
    private int signatureFormat = 1;
    private static Configurator uniqueInstance = null;
    private static KeyStore ks = null;
    private static boolean keyStoreLoaded = false;
    // macintosheries
    private static KeyStore macTokenks = null;
    public static boolean macTokenkeyStoreLoaded = false;
}

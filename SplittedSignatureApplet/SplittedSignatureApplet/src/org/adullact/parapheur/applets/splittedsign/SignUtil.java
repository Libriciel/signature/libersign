/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.parapheur.applets.splittedsign;

import org.bouncycastle.cms.CMSException;

import javax.swing.*;
import javax.xml.crypto.dsig.XMLSignatureException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.*;
import java.security.cert.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Utilitaire de signature
 *
 * Created by lhameury on 27/01/16.
 */
public class SignUtil {

    private Map<String, Digest> digests = new HashMap<String, Digest>();
    private boolean acceptSelfSigned = false;

    public SignUtil(Map<String, Digest> digests, boolean acceptSelfSigned) {
        this.digests = digests;
        this.acceptSelfSigned = acceptSelfSigned;
    }

    /**
     * Decode the Hexadecimal char sequence (as string) into Byte Array.
     *
     * @param data The Hex encoded sequence to be decoded.
     * @return Decoded byte array.
     * @throws IllegalArgumentException <var>data</var> when wrong number of chars is given or invalid chars.
     */
    private static byte[] hexDecode(String data) {
        // Vérification des arguments
        int length = data.length();
        if ((length % 2) != 0) {
            throw new IllegalArgumentException("Odd number of characters.");
        }
        // 2 caractères hexadécimaux sont utilisés pour représenter un octet
        try {
            byte[] bytes = new byte[length / 2];
            for (int i = 0, j = 0; i < length; i = i + 2) {
                bytes[j++] = (byte) Integer.parseInt(data.substring(i, i + 2), 16);
            }
            return bytes;
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Illegal hexadecimal character.", e);
        }
    }

    /**
     * Check the validity of the certificate given as parameter.
     * This process can be long , because of realtime check of the CRL !
     *
     * @param certificate Le certif
     * @param invalidsCRL Liste des CRL invalidées
     * @return true if the certificate is valid, false otherwise.
     */
    boolean validateCertificate(final X509Certificate certificate, List<String> invalidsCRL, SignHandler helper)
            throws CRLException, CRLNotFoundException,
            IOException, KeyStoreException, NoSuchAlgorithmException, CertificateException, CertificateVerificationException,
            CertPathBuilderException {

        helper.progress("Vérification validité certificat en cours.");
        certificate.checkValidity();
        helper.progress("Vérification validité certificat... OK!");

        Principal subject = certificate.getSubjectDN();
        Principal issuer = certificate.getIssuerDN();
        if (issuer.equals(subject) && !acceptSelfSigned) {
            helper.progress("Vérification validité certificat... KO !");
            throw new CertificateSelfSignedException("Self signed certificates are refused");
        }

        helper.validateCertificate(certificate, invalidsCRL);

        return true;
    }

    public void sign(X509Certificate signingCertificate, PrivateKey privKey, SignHandler helper) {
        HashMap<String, String> signedHashMap = new HashMap<String, String>();
        List<String> invalidsCRL = new ArrayList<String>();
        while (true) {
            try {
                validateCertificate(signingCertificate, invalidsCRL, helper);
                break;
            } catch (CertificateSelfSignedException e) {
                helper.print("Votre certificat est auto-signé.\nIl est impropre à la signature électronique.");
                return;
                //                int result = JOptionPane.showConfirmDialog(this, "Votre certificat est auto-signé. \nVoulez-vous quand même utiliser ce certificat ?", "Certificat auto-signé", JOptionPane.YES_NO_OPTION);
                //                if (result == JOptionPane.OK_OPTION) {
                //                    acceptSelfSigned = true;
                //                    // continue;
                //                } else {
                //                    return;
                //                }
            } catch (CertificateExpiredException e) {
                helper.print("Votre certificat est expiré depuis le " + signingCertificate.getNotAfter());
                return;
            } catch (CertificateNotYetValidException e) {
                helper.print("Votre certificat ne sera valide qu'à partir du " + signingCertificate.getNotBefore());
                return;
            } catch (CertificateRevokedException e) {
                helper.print("Votre certificat a été révoqué.\nIl est impropre à la signature électronique.");
                return;
            } catch (CertPathBuilderException e) {
                helper.print(
                        "La chaîne de confiance n'a pas pu être construite.\nL'autorité de certification n'est pas valide, ou inconnue.");
                return;
            } catch (CRLNotFoundException e) {
                // TODO - Do something
                final AtomicReference<Integer> atResult = new AtomicReference<Integer>();
                final CRLNotFoundException crlNotFoundException = e;

                try {
                    SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                            JDialog frame = new JDialog();
                            frame.setVisible(false);
                            frame.setAlwaysOnTop(true);
                            frame.toFront();
                            atResult.set(JOptionPane.showConfirmDialog(frame,
                                                                     "ATTENTION: La CRL '" + crlNotFoundException.getCrlLocation() +
                                                                     "' ne peut être contactée. \nSouhaitez-vous quand même continuer ?",
                                                                     "CRL inconnue",
                                                                     JOptionPane.YES_NO_OPTION));
                        }
                    });
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                int result = atResult.get();
                if (result == JOptionPane.OK_OPTION) {
                    invalidsCRL.add(e.getCrlLocation());
                    break;
                } else {
                    return;
                }
            } catch (CertificateVerificationException e) {
                System.err.println(e.getLocalizedMessage() + "\n" + e.toString() + "\n");
                helper.print(
                        "La vérification du certificat sélectionné a échoué. \nConsulter votre administrateur système, message:\n  " +
                        e.getMessage());
                return;
            } catch (CRLException e) {
                System.err.println(e.getLocalizedMessage() + "\n" + e.toString() + "\n");
                helper.print(
                        "Erreur inconnue lors de la vérification de la validité du certificat.\nConsulter votre administrateur.\n  " +
                        e.getMessage());
                return;
            } catch (IOException e) {
                System.err.println(e.getLocalizedMessage() + "\n" + e.toString() + "\n");
                helper.print(
                        "Erreur inconnue lors de la vérification de la validité du certificat.\nConsulter votre administrateur.\n  " +
                        e.getMessage());
                return;
            } catch (KeyStoreException e) {
                System.err.println(e.getLocalizedMessage() + "\n" + e.toString() + "\n");
                helper.print(
                        "Erreur inconnue lors de la vérification de la validité du certificat.\nConsulter votre administrateur.\n  " +
                        e.getMessage());
                return;
            } catch (NoSuchAlgorithmException e) {
                System.err.println(e.getLocalizedMessage() + "\n" + e.toString() + "\n");
                helper.print(
                        "Erreur inconnue lors de la vérification de la validité du certificat.\nConsulter votre administrateur.\n  " +
                        e.getMessage());
                return;
            } catch (CertificateException e) {
                System.err.println(e.getLocalizedMessage() + "\n" + e.toString() + "\n");
                helper.print(
                        "Erreur inconnue lors de la vérification de la validité du certificat.\nConsulter votre administrateur.\n  " +
                        e.getMessage());
                return;
            }
        }

        try {
            Iterator<String> itk = digests.keySet().iterator();
            byte[] signatureBytes;
            String iddoc;
            int nbreHash = digests.size();
            int numeroHash = 0;
            while (itk.hasNext()) {
                numeroHash++;
                iddoc = itk.next();
                String formatSigString = digests.get(iddoc).formatSignature;

                // Message utilisateur:
                helper.progress("Patientez, signature n°" + numeroHash + "/" + nbreHash + " en cours. Format : " + formatSigString);

                if ("CMS".equalsIgnoreCase(formatSigString) || "PADES-basic".equalsIgnoreCase(formatSigString)) {
                    if ("PADES-basic".equalsIgnoreCase(formatSigString)) {
                        /**
                         * Produce a PAdES ISO 32000-1 signature.
                         */
                        System.out.println("Bonjour, je vais signer en PAdES-basique");
                        // tu crois que c'est tordu, mais il suffit de produire du PKCS7
                    } else {
                        /**
                         * Produce PKCS#7 formatted signature.
                         */
                        System.out.println("\tiddoc=\"" + iddoc + "\", digest=\"" + digests.get(iddoc) + "\"");
                    }

                    if (digests.get(iddoc).digests.contains(",")) {
                        // the result will be serialized in this:
                        StringBuilder sigStringBuilder = new StringBuilder();

                        String[] hashesTab = digests.get(iddoc).digests.split(",");
                        boolean first = true;
                        for (String hashesTab1 : hashesTab) {
                            if (first) {
                                first = false;
                            } else {
                                sigStringBuilder.append(",");
                            }
                            signatureBytes = PKCS7SignUtil.signAndCreateP7(signingCertificate, privKey, hexDecode(hashesTab1));
                            // After, gather the signatures!
                            sigStringBuilder.append(new String(Base64Coder.encode(Base64Coder.der2pem(signatureBytes))));
                        }
                        signedHashMap.put(iddoc, sigStringBuilder.toString());
                    } else {
                        signatureBytes = PKCS7SignUtil.signAndCreateP7(signingCertificate, privKey, hexDecode(digests.get(iddoc).digests));
                        signedHashMap.put(iddoc, new String(Base64Coder.encode(Base64Coder.der2pem(signatureBytes))));
                    }
                } else if ("PKCS1_SHA256_RSA".equalsIgnoreCase(formatSigString)) {
                    if(digests.get(iddoc).digests.contains(":")) { // this is not base64, but an URL
                        signatureBytes = PKCS1SignUtil.sign(signingCertificate, privKey, digests.get(iddoc).digests.getBytes(), true);
                    } else {
                        signatureBytes = PKCS1SignUtil.sign(signingCertificate, privKey, Base64.decode(digests.get(iddoc).digests), false);
                    }

                    String sigString = String.valueOf(Base64Coder.encode(signatureBytes)) +
                                       ";" +
                                       String.valueOf(Base64Coder.encode(signingCertificate.getEncoded()));

                    signedHashMap.put(iddoc, sigString);
                } else if ("CMS-Allin1".equalsIgnoreCase(formatSigString)) {
                    /**
                     * if the given p7s parameter is empty (null) then do a
                     * regular CMS signature.
                     */
                    String currentP7s = digests.get(iddoc).p7s;
                    if (currentP7s == null || currentP7s.trim().isEmpty() ||
                        "null".equalsIgnoreCase(currentP7s)) {
                        signatureBytes = PKCS7SignUtil.signAndCreateP7(signingCertificate, privKey, hexDecode(digests.get(iddoc).digests));
                    } else {
                        System.out.println(
                                "param currentP7s=" + currentP7s.substring(0, (currentP7s.length() > 20) ? 20 : currentP7s.length()));
                        byte[] currentP7sDecoded = Base64.decode(currentP7s);
                        String totoString = new String(currentP7sDecoded);
                        System.out.println("param currentP7sDECODED=" +
                                           totoString.substring(0, (totoString.length() > 10) ? 10 : totoString.length()));

                        if (totoString.contains("-----BEGIN")) {
                            // signature format PEM
                            signatureBytes = PKCS7SignUtil.updateP7Signature(signingCertificate, privKey,
                                                                             hexDecode(digests.get(iddoc).digests),
                                                                             PKCS7VerUtil.pem2der(Base64.decode(currentP7s.replaceAll(
                                                                                     "![a-zA-Z0-9+/=]",
                                                                                     "")), "-----BEGIN".getBytes(), "-----END".getBytes()));
                            // PKCS7VerUtil.pem2der(Base64Coder.decode(currentP7s.replaceAll("![a-zA-Z0-9+/=]", "")), "-----BEGIN".getBytes(), "-----END".getBytes()));
                            // PKCS7VerUtil.pem2der(Base64Coder.decode(currentP7s.replaceAll("![a-zA-Z0-9=]", "")), "-----BEGIN".getBytes(), "-----END".getBytes()));
                            // PKCS7VerUtil.pem2der(hexDecode(currentP7s), "-----BEGIN".getBytes(), "-----END".getBytes()));
                            // Base64Coder.pem2der(hexDecode(currentP7s), "-----BEGIN".getBytes(), "-----END".getBytes()));
                        } else {
                            signatureBytes = PKCS7SignUtil.updateP7Signature(signingCertificate, privKey,
                                                                             hexDecode(digests.get(iddoc).digests),
                                                                             currentP7sDecoded);
                        }
                    }
                    signedHashMap.put(iddoc, new String(Base64Coder.encode(Base64Coder.der2pem(signatureBytes))));
                } else if ("XADES".equalsIgnoreCase(formatSigString)) { // Signature détachée XAdES
                    System.out.println("##### iddoc=" + iddoc + ", digest=" + digests.get(iddoc));
                    // signatureBytes = XADESSignUtil.sign(signingCertificate, privKey, hexDecode(digests.get(iddoc)));
                    XADESSignUtil signeur = new XADESSignUtil(digests.get(iddoc).pesIds);

                    if (digests.get(iddoc).digests.contains(",")) {
                        // the result will be serialized in this:
                        StringBuilder sigStringBuilder = new StringBuilder();

                        String[] hashesTab = digests.get(iddoc).digests.split(",");
                        boolean first = true;
                        for (String hashesTab1 : hashesTab) {
                            if (first) {
                                first = false;
                            } else {
                                sigStringBuilder.append(",");
                            }
                            signatureBytes = signeur.signXAdES132(signingCertificate, privKey, hexDecode(hashesTab1));
                            // After, gather the signatures!
                            sigStringBuilder.append(new String(Base64Coder.encode(signatureBytes)));
                        }
                        signedHashMap.put(iddoc, sigStringBuilder.toString());
                    } else {
                        signatureBytes = signeur.signXAdES132(signingCertificate, privKey, hexDecode(digests.get(iddoc).digests));
                        signedHashMap.put(iddoc, new String(Base64Coder.encode(signatureBytes)));
                    }
                } else if ("XADES132".equalsIgnoreCase(formatSigString)) {
                    XADESSignUtil signeur = new XADESSignUtil(digests.get(iddoc).pesIds,
                                                              digests.get(iddoc).pespolicyid,
                                                              digests.get(iddoc).pespolicydesc,
                                                              digests.get(iddoc).pespolicyhash,
                                                              digests.get(iddoc).pesspuri,
                                                              digests.get(iddoc).pescity,
                                                              digests.get(iddoc).pespostalcode,
                                                              digests.get(iddoc).pescountryname,
                                                              digests.get(iddoc).pesclaimedrole);
                    signatureBytes = signeur.signXAdES132(signingCertificate, privKey, hexDecode(digests.get(iddoc).digests));
                    // System.out.println("Signature is : " + new String(signatureBytes, "UTF-8"));
                    signedHashMap.put(iddoc, new String(Base64Coder.encode(signatureBytes)));
                } else if ("PESV2".equalsIgnoreCase(formatSigString) ||
                           "XADES-env".equalsIgnoreCase(formatSigString) ||
                           "xades-env-1.2.2-sha256".equalsIgnoreCase(formatSigString)) {

                    XadesHeliosSignUtil signeur;

                    if ("xades-env-1.2.2-sha256".equalsIgnoreCase(formatSigString)) {
                        // new DGFiP HELIOS signature policy (valid since oct.2017)
                        signeur = new XADES122Sha256SignUtil(
                                digests.get(iddoc).pesIds,
                                digests.get(iddoc).pespolicyid,
                                digests.get(iddoc).pespolicydesc,
                                digests.get(iddoc).pespolicyhash,
                                digests.get(iddoc).pesspuri,
                                digests.get(iddoc).pescity,
                                digests.get(iddoc).pespostalcode,
                                digests.get(iddoc).pescountryname,
                                digests.get(iddoc).pesclaimedrole,
                                digests.get(iddoc).encodingMap);
                    }
                    else { // legacy process (valid until sept.2018)
                        /**
                         * Produce a XAdES EPES v1.1.1 signature .
                         */
                        // XADES111SignUtil signeur = new XADES111SignUtil(
                        signeur = new XADES111SignUtil(
                                digests.get(iddoc).pesIds,
                                digests.get(iddoc).pespolicyid,
                                digests.get(iddoc).pespolicydesc,
                                digests.get(iddoc).pespolicyhash,
                                digests.get(iddoc).pesspuri,
                                digests.get(iddoc).pescity,
                                digests.get(iddoc).pespostalcode,
                                digests.get(iddoc).pescountryname,
                                digests.get(iddoc).pesclaimedrole,
                                digests.get(iddoc).encodingMap);
                    }

                    /**
                     * Detect if this is a multi signing process
                     */
                    System.out.println("\tiddoc=\"" + iddoc + "\", pes_id=\"" + digests.get(iddoc).pesIds + "\"");
                    if (digests.get(iddoc).pesIds.contains(",")) {
                        // the result will be serialized in this:
                        StringBuilder sigStringBuilder = new StringBuilder();
                        // TODO : split 'n' loop
                        String[] iddocTab = digests.get(iddoc).pesIds.split(",");
                        String[] hashTab = digests.get(iddoc).digests.split(",");
                        boolean first = true;
                        for (int i = 0; i < hashTab.length; i++) {
                            if (first) {
                                first = false;
                            } else {
                                sigStringBuilder.append(",");
                            }
                            String localhash = hashTab[i];
                            signeur.setDocumentID(iddocTab[i]);
                            signatureBytes = signeur.signPES(null, signingCertificate, privKey, hexDecode(localhash));
                            // After, gather the signatures!
                            sigStringBuilder.append(new String(Base64Coder.encode(signatureBytes)));
                        }
                        signedHashMap.put(iddoc, sigStringBuilder.toString());

                    } else {
                        // regular use: only one ID and one Hash.
                        signatureBytes = signeur.signPES(null, signingCertificate, privKey, hexDecode(digests.get(iddoc).digests));
                        signedHashMap.put(iddoc, new String(Base64Coder.encode(signatureBytes)));
                    }
                } else if ("XADES-env-xpath".equalsIgnoreCase(formatSigString)) {
                    /**
                     * Signature DIA.
                     * BE CAREFUL: This is NOT a classic CO-sign process
                     *  it is tied to the DIA structure !!
                     */
                    System.out.print("Signature DIA :  ");
                    XADESCosigner.setSignParameters(digests.get(iddoc).pesIds,
                                                    digests.get(iddoc).pespolicyid,
                                                    digests.get(iddoc).pespolicydesc,
                                                    digests.get(iddoc).pespolicyhash,
                                                    digests.get(iddoc).pesspuri,
                                                    digests.get(iddoc).pescity,
                                                    digests.get(iddoc).pespostalcode,
                                                    digests.get(iddoc).pescountryname,
                                                    digests.get(iddoc).pesclaimedrole);
                    String role = digests.get(iddoc).pesclaimedrole.toLowerCase();
                    ByteArrayInputStream bIs = new ByteArrayInputStream(hexDecode(digests.get(iddoc).digests));
                    System.out.println(new String(hexDecode(digests.get(iddoc).digests)));
                    if (role.contains("departement")) {
                        signatureBytes = XADESCosigner.departmentSign(signingCertificate, privKey, bIs, "");
                    } else if (role.contains("conservatoire") && role.contains("littoral")) {
                        signatureBytes = XADESCosigner.conservatoireLittoralSign(signingCertificate, privKey, bIs, "");
                    } else if (role.contains("commune")) {
                        signatureBytes = XADESCosigner.communeSign(signingCertificate, privKey, bIs, "");
                    } else {
                        signatureBytes = XADESCosigner.organismSign(signingCertificate, privKey, iddoc, null, bIs, "");
                    }
                    System.out.println(new String(signatureBytes));
                    signedHashMap.put(iddoc, new String(Base64Coder.encode(signatureBytes)));
                } else {
                    //  , XADES-T-env
                    // throw new XXXException à définir
                    Logger.getLogger("org.adullact.parapheur.applets.splittedsignatureapplet").severe("unsupportedSignatureFormatExeptionCN");
                }
            }
            helper.success(signedHashMap);
        } catch (InvalidKeyException ex) {
            // Logger.getLogger("global").log(Level.SEVERE, null, ex);
            helper.print("Clé privée inaccessible.\nVotre certificat est-il bien connecté?");
        } catch (XMLSignatureException ex) {
            helper.print(ex.getMessage());
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
        } catch (CMSException ex) {
            helper.print(ex.getMessage());
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            helper.print(ex.getMessage());
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            // ex.printStackTrace();
        } catch (CertificateEncodingException ex) {
            helper.print(ex.getMessage());
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.adullact.parapheur.applets.splittedsign.utils;

/**
 *
 * @author svast
 */
public enum SignType {
    Pkcs7("Pkcs7"),
    CAdES_BES("CAdES_BES"),
    CAdES_T("CAdES_T"),
    CAdES_A("CAdES_A"),
    CAdES_C("CAdES_C"),
    CAdES_EPES("CAdES_EPES"),
    CAdES_X_1("CAdES_X_1"),
    CAdES_X_2("CAdES_X_2"),
    CAdES_X_L("CAdES_X_L"),
    
    PDF("PDF"),    
    PAdES_BES("PAdES_BES"),     
    PAdES_T("PAdES_T"),  
    PAdES_A("PAdES_A"), 
    PAdES_C("PAdES_C"),     
    PAdES_EPES("PAdES_EPES"), 
    PAdES_X_1("PAdES_X_1"),     
    PAdES_X_2("PAdES_X_2"),
    PAdES_X_L("PAdES_X_L"),
        
    XMLDSIG("XMLDSIG"),
    XAdES_BES("XAdES_BES"),
    XAdES_T("XAdES_T"),
    XAdES_C("XAdES_C"),
    XAdES_X_1("XAdES_X_1"),
    XAdES_X_2("XAdES_X_2"),
    XAdES_X_L("XAdES_X_L"),
    XAdES_A("XAdES_A");   
    
    
    private final String literal;    
    private SignType(String literal) {
        this.literal = literal;
    }
    
    public String getLiteral() {
        return literal;
    }
}

/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.parapheur.applets.splittedsign;

import iaik.pkcs.pkcs11.TokenException;
import iaik.xml.crypto.XSecProvider;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.crypto.dom.DOMStructure;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLObject;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * Classe specialisee (pour le moment) dans la signature XAdES 1.2.2
 *   (refactoring partiel de XADES111SignUtil pour v1.2.2)
 *
 * @author Stephane Vast - Libriciel SCOP
 */
public class XADES122Sha256SignUtil extends XadesHeliosSignUtil {

    private static String pesID;
    private static String policyID;
    private static String policyDescription;
    private static String policyDigestValue;
    private static String SPURI;
    private static String city;
    private static String postalCode;
    private static String countryName;
    private static String claimedRole;
    private static String encoding;
    private final boolean idPresence;

    private static final String OS_NAME = System.getProperty("os.name");


    public XADES122Sha256SignUtil() {

        pesID = null;
        idPresence = false;
        city = null;
        policyID = null;
        policyDescription = null;
        policyDigestValue = null;
        claimedRole = null;
        SPURI = null;
    }

    public XADES122Sha256SignUtil(String apes_id, String apolicyId, String apolicyDescription, String apolicyDigestValue, String aspuri,
                            String acity, String apostalCode, String acountryName, String aclaimedRole, String aencoding) {
        if (apes_id.equalsIgnoreCase("null")) {
            idPresence = false;
            pesID = "";
        } else {
            idPresence = true;
            pesID = apes_id;
        }
        policyID = apolicyId;
        policyDescription = apolicyDescription;
        policyDigestValue = apolicyDigestValue;
        SPURI = aspuri;
        city = acity;
        postalCode = apostalCode;
        countryName = acountryName;
        claimedRole = aclaimedRole;
        encoding = aencoding;

    }


    private String getSignatureID() {
        /* TODO : préciser la façon dont est calculée l'identifiant de la signature */
        return pesID + "_SIG_1"; //"IDC" + dateFormat.format(date); //ID signature+date
    }

    private static String getPolicyIdentifierID() {
        if (policyID != null) {
            return policyID;
        }
        return "urn:oid:1.2.250.1.131.1.5.18.21.1.7";
    }

    private static String getPolicyIdentifierDescription() {
        if (policyDescription != null) {
            return policyDescription;
        }
        return "Politique de Signature de la DGFiP...";
    }

    private static String getPolicyDigest() {
        if (policyDigestValue != null) {
            return policyDigestValue;
        }
        return "b29w/zLWrx7cjwGX7fDOdTTBjrvXuM/z1rJyekzw1K0=";
    }

    private static String getSPURI() {
        if (SPURI != null) {
            return SPURI;
        }
        return "https://www.collectivites-locales.gouv.fr/files/files/finances_locales/dematerialisation/ps_helios_dgfip.pdf";
    }

    private static String getCity() {
        if (city != null) {
            return city;
        }
        return "MONTPELLIER";
    }

    private static String getPostalCode() {
        if (postalCode != null) {
            return postalCode;
        }
        return "34000";
    }

    private static String getCountryName() {
        if (countryName != null) {
            return countryName;
        }
        return "France";
    }

    private static String getClaimedRole() {
        if (claimedRole != null) {
            return claimedRole;
        }
        return "ROLE de Signataire";
    }

    private static String getDocumentID() {
        return pesID;
    }

    public void setDocumentID(String id) {
        pesID = id;
    }

    /**
     * Signature ETSI TS 101 903 - XAdES-EPES v1.2.2
     *  Format de signature XAdES de l’administration électronique
     *  Source: beaucoup de sueur.
     *
     * @param applet         (deprecated) applet object
     * @param certificate    the X509 certificate
     * @param privateKey     the private key
     * @param digest         the digest of the data to be signed
     * @return               the XML signature
     * @throws javax.xml.crypto.dsig.XMLSignatureException si probleme
     */
    public byte[] signPES(javax.swing.JApplet applet, X509Certificate certificate, PrivateKey privateKey, byte[] digest) throws XMLSignatureException {

        String signatureID = getSignatureID();
        String signedPropertiesID = signatureID + "_SP";
        // String keyInfoID = signatureID + "_KI";          <--- aucun interet, ne sert pas
        String documentID = getDocumentID();
        String docRefID = "";
        if (this.idPresence) {
            docRefID = "#" + documentID;
        }
        try {
            java.io.ByteArrayOutputStream digestBOS = new java.io.ByteArrayOutputStream();
            org.bouncycastle.util.encoders.Base64.encode(digest, digestBOS);            //  <----- à quoi ça sert??

            /*
             * Preparation d'un nouvel arbre DOM pour construire la signature
             */
            javax.xml.parsers.DocumentBuilderFactory documentBuilderFactory = javax.xml.parsers.DocumentBuilderFactory.newInstance();
            documentBuilderFactory.setNamespaceAware(true);
            org.w3c.dom.Document document = documentBuilderFactory.newDocumentBuilder().newDocument();
            document.appendChild(document.createElement("DocumentDetachedExternalSignature"));

            /*
             * Ce truc-là est juste essentiel, sinon pas de chocolat
             */
            XMLSignatureFactory signatureFactory = XMLSignatureFactory.getInstance("DOM");


            /*
             * Creation qualifying properties, contient xad:SignedProperties,
             * qui ne contiennent "que" SignedSignatureProperties.
             */
            org.w3c.dom.Element qualifyingPropertiesElement = XADES122Sha256SignUtil.createXades122Element(document, XadesUtil.xadesNS122, "QualifyingProperties");
            qualifyingPropertiesElement.setAttribute("Target", "#" + signatureID);  // signatureID);

            org.w3c.dom.Element signedPropertiesElement = XADES122Sha256SignUtil.createXades122Element(document, XadesUtil.xadesNS122, "SignedProperties");
            signedPropertiesElement.setAttributeNS(null, "Id", signedPropertiesID);

            /* les SignedSignatureProperties */
            org.w3c.dom.Element signedSignaturePropertiesElement = XADES122Sha256SignUtil.createXades122Element(document, XadesUtil.xadesNS122, "SignedSignatureProperties");

            signedSignaturePropertiesElement.appendChild(XADES122Sha256SignUtil.createSigningTime(document, XadesUtil.xadesNS122));
            /* -- */
            signedSignaturePropertiesElement.appendChild(XADES122Sha256SignUtil.createSigningCertificate(document, XadesUtil.xadesNS122, certificate));
            /* -- */
            signedSignaturePropertiesElement.appendChild(XADES122Sha256SignUtil.createSignaturePolicyIdentifier(document, XadesUtil.xadesNS122,
                    getPolicyIdentifierID(), getPolicyIdentifierDescription(), getPolicyDigest(), getSPURI()));
            /* -- */
            signedSignaturePropertiesElement.appendChild(XADES122Sha256SignUtil.createSignatureProductionPlace(document, XadesUtil.xadesNS122,
                    getCity(), getPostalCode(), getCountryName()));
            /* -- */
            signedSignaturePropertiesElement.appendChild(XADES122Sha256SignUtil.createSignerRole(document, XadesUtil.xadesNS122, getClaimedRole()));

            signedPropertiesElement.appendChild(signedSignaturePropertiesElement);


            /*
             * 1ere Reference pour le signedInfo à venir: contenu du PES
             */
            List<Reference> referencesList = new ArrayList<Reference>();
            //List<Reference> referencesList = new ArrayList<>();
            java.util.ArrayList<javax.xml.crypto.dsig.Transform> transformList = new java.util.ArrayList<javax.xml.crypto.dsig.Transform>();
            //java.util.ArrayList<javax.xml.crypto.dsig.Transform> transformList = new java.util.ArrayList<>();
            transformList.add(signatureFactory.newTransform(Transform.ENVELOPED, (TransformParameterSpec) null));
            transformList.add(signatureFactory.newTransform(nu.xom.canonical.Canonicalizer.EXCLUSIVE_XML_CANONICALIZATION, (TransformParameterSpec) null));
            Reference contentReference = signatureFactory.newReference(docRefID,
                    signatureFactory.newDigestMethod(DigestMethod.SHA256, null),
                    transformList,
                    null, null, digest);
            referencesList.add(contentReference);


            /*
             * 2nde Reference pour le signedInfo à venir: signedPropertiesElement
             *  il faut calculer au prealable un condensat, en 2 phases:
             *     - canonicalisation
             *     - hashage
             */
            /*  1- canonicalisation !  */
            ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
            nu.xom.Element spXomElement = nu.xom.converters.DOMConverter.convert(signedPropertiesElement);

            nu.xom.canonical.Canonicalizer outPutter = new nu.xom.canonical.Canonicalizer(bytestream,
                    nu.xom.canonical.Canonicalizer.EXCLUSIVE_XML_CANONICALIZATION);                 // http://www.w3.org/2001/10/xml-exc-c14n#
            outPutter.write(spXomElement);
            byte canonicalMessage[] = bytestream.toByteArray();

            /*  2- hashage SHA256 !   */
            MessageDigest messageDigestDeSaMere = java.security.MessageDigest.getInstance("SHA-256");
            messageDigestDeSaMere.update(canonicalMessage);
            byte[] spDigest = messageDigestDeSaMere.digest();

            /*
             *   Source: 090925_H1_3_ET_DOSTEC_SystemeEchangesDonneesPES_V2.doc, avec les erreurs de documentation qui vont avec :-X
             *
             * Le deuxième élément ds:Reference identifie les propriétés signées. Il est constitué :
             *         Un attribut URI
             *         Un attribut Type
             *         Un élément ds:Transforms
             *         Un élément ds:DigestMethod
             *         Un élément ds:DigestValue
             * L’attribut obligatoire URI pointe sur l’attribut Id de l’élément xad:SignedProperties (cf. 4.2.2.2.2.1)
             * L’attribut obligatoire Type est fixé à la valeur "http://uri.etsi.org/01903/v1.1.1#SignedProperty"
             * L’attribut obligatoire ds:Transforms comprend un élément ds:Transform indiquant l’algorithme de mise sous forme canonique à appliquer aux données.
             *    ds:Transforms Algorithm = "http://www.w3.org/2001/xml-exc14n#"
             * Une empreinte du résultat de cette transformation est ensuite calculée, en suivant l’algorithme défini en attribut obligatoire de ds:DigestMethod . Cet algorithme est fixé à :
             *    ds:DigestMethodAlgorithm = "http://www.w3.org/2000/09/xmldsig#sha1"
             * L’élément obligatoire ds:DigestValue contient alors la valeur de l’empreinte ainsi créée.
             */
            java.util.ArrayList<javax.xml.crypto.dsig.Transform> sigPropsTransforms = new java.util.ArrayList<javax.xml.crypto.dsig.Transform>();
            //java.util.ArrayList<javax.xml.crypto.dsig.Transform> sigPropsTransforms = new java.util.ArrayList<>();
            javax.xml.crypto.dsig.Transform excC14nTransform = signatureFactory.newTransform(
                    nu.xom.canonical.Canonicalizer.EXCLUSIVE_XML_CANONICALIZATION,                      // http://www.w3.org/2001/10/xml-exc-c14n#
                    (TransformParameterSpec) null);
            sigPropsTransforms.add(excC14nTransform);
            Reference signedPropertiesReference = signatureFactory.newReference("#" + signedPropertiesID,
                    signatureFactory.newDigestMethod(DigestMethod.SHA256, null),
                    sigPropsTransforms,
                    XadesUtil.xadesNS122 + "SignedProperties", null, spDigest);

            referencesList.add(signedPropertiesReference);

            /* signed info */
            SignedInfo signedInfo = signatureFactory.newSignedInfo(
                    signatureFactory.newCanonicalizationMethod(javax.xml.crypto.dsig.CanonicalizationMethod.EXCLUSIVE,  //  http://www.w3.org/2001/10/xml-exc-c14n#
                            (javax.xml.crypto.dsig.spec.C14NMethodParameterSpec) null),
                    signatureFactory.newSignatureMethod("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256", null),
                    java.util.Collections.unmodifiableList(referencesList));

            /*
             * Assemblage des différents éléments : poupees russes du QualifyingProperties
             */
            qualifyingPropertiesElement.appendChild(signedPropertiesElement);

            XMLObject xmlObject = signatureFactory.newXMLObject(
                    java.util.Collections.singletonList(new DOMStructure(qualifyingPropertiesElement)),
                    null, null, null);    //  "objectID", null, null);        Id ne sert pas

            /*
             * keyInfo
             */
            javax.xml.crypto.dsig.keyinfo.KeyInfoFactory keyInfoFactory = signatureFactory.getKeyInfoFactory();
            javax.xml.crypto.dsig.keyinfo.X509Data x509Data = keyInfoFactory.newX509Data(java.util.Collections.singletonList(certificate));
            javax.xml.crypto.dsig.keyinfo.KeyInfo  keyInfo  = keyInfoFactory.newKeyInfo(java.util.Collections.singletonList(x509Data)); //, keyInfoID);

            /*
             * signature actually begins HERE !!!
             */
            /*
             * Set the Behavior for signature
             */
            boolean tokenSpecificBehavior = false;
            if (OS_NAME.toLowerCase().contains("mac os x") && privateKey == null) {
                /*
                 * Probably a token on MAC OSX. let's verify this assumption
                 */
                if (Configurator.macTokenkeyStoreLoaded &&
                        certificate.getSerialNumber().equals(Configurator.getInstance().getMacTokenX509Certificate(0).getSerialNumber())) {
                    System.out.println("###########################################################################");
                    System.out.println("######  Token detected on Mac OSX system... (too bad for the developer) ###");
                    System.out.println("###########################################################################");
                    tokenSpecificBehavior = true;
                    try {
                        Configurator.getInstance().destroySession();
                        Configurator.getInstance().detectCardAndCriptoki();


                        /*
                         * the following is from demo.pkcs.pkcs11.provider.SignandVerifyXmlSig
                         */
                        // configure delegation for RSA signature with SHA-1
                        XSecProvider.setDelegationProvider("Signature.SHA256withRSA", Configurator.getInstance().getPkcs11providerName());
                        // install IAIK XML Security Provider (XSECT) and IAIK JCE Provider
                        XSecProvider.addAsProvider(false, true);

                        /*
                         * ########## SPECIFIC LINES DEDICATED TO MAC OS-X !!! ##
                         */
                        OSXPkcs11Signer    osxSigner;
                        osxSigner   = new OSXPkcs11Signer("usr/lib/pkcs11/libgclib.dylib",
                                Configurator.getInstance().getPassword(true));


                        iaik.pkcs.pkcs11.objects.PrivateKey iaiakPrivateKey = (iaik.pkcs.pkcs11.objects.PrivateKey)osxSigner.selectMatchingSigningPrivateKey(certificate);
                        // java.security.spec.RSAPrivateKeySpec(iaiakPrivateKey.);
                        //privateKey = java.security.KeyFactory.getInstance("RSA", Configurator.getInstance().getPkcs11providerName()).generatePrivate(null);

                        privateKey = new org.adullact.parapheur.applets.splittedsign.pkcs11.TokenPrivateKey(iaiakPrivateKey);
                        if (privateKey == null) {
                            System.out.println("OMG : privateKey is null");
                        } else  {
                            System.out.println("OMG : privateKey is NOT null");
                        }
                    } catch (TokenException ex) {
                        Logger.getLogger(XADES122Sha256SignUtil.class.getName()).log(Level.SEVERE, null, ex);
                        return null;
                    } catch (Exception ex) {
                        Logger.getLogger(XADES122Sha256SignUtil.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } // ------- end of MAC OSX specific behavior -----------

            DOMSignContext signContext = new DOMSignContext(privateKey, document.getDocumentElement());
            signContext.putNamespacePrefix(XadesUtil.xadesNS122, "xad");
            signContext.putNamespacePrefix(javax.xml.crypto.dsig.XMLSignature.XMLNS, "ds");

            /*
             * Objet de signature
             */
            javax.xml.crypto.dsig.XMLSignature xmlSignature =
                    signatureFactory.newXMLSignature(signedInfo, keyInfo,
                            java.util.Collections.singletonList(xmlObject), signatureID, signatureID + "_SV");

            xmlSignature.sign(signContext);



            /*
             * Mise en forme du resultat
             */
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            if (encoding != null) {
                transformer.setOutputProperty(OutputKeys.ENCODING, encoding);
            }
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            transformer.transform(new DOMSource(document), new StreamResult(byteArrayOutputStream));

            return byteArrayOutputStream.toByteArray();
        } catch (IOException ex) {
            Logger.getLogger(XADES122Sha256SignUtil.class.getName()).log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (CertificateEncodingException ex) {
            Logger.getLogger(XADES122Sha256SignUtil.class.getName()).log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (javax.xml.transform.TransformerException ex) {
            Logger.getLogger(XADES122Sha256SignUtil.class.getName()).log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (javax.xml.crypto.MarshalException ex) {
            Logger.getLogger(XADES122Sha256SignUtil.class.getName()).log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (javax.xml.parsers.ParserConfigurationException ex) {
            Logger.getLogger(XADES122Sha256SignUtil.class.getName()).log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (java.security.NoSuchAlgorithmException ex) {
            Logger.getLogger(XADES122Sha256SignUtil.class.getName()).log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (java.security.InvalidAlgorithmParameterException ex) {
            Logger.getLogger(XADES122Sha256SignUtil.class.getName()).log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        }
    }

    /**
     * SigningTime tag...
     * @param document   dom document
     * @param namespaceURI   the URI namespace
     * @return the DOM element
     */
    private static org.w3c.dom.Element createSigningTime(org.w3c.dom.Document document, String namespaceURI) {
        org.w3c.dom.Element signingTimeElement = XADES122Sha256SignUtil.createXades122Element(document, namespaceURI, "SigningTime");
        java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ssz");
        dateFormat.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
        java.util.Date date = new java.util.Date();
        String myStrDate = dateFormat.format(date);
        signingTimeElement.appendChild(document.createTextNode(myStrDate.replaceAll("UTC", "Z")));
        return signingTimeElement;
    }

    private static org.w3c.dom.Element createSigningCertificate(org.w3c.dom.Document document, String namespaceURI, java.security.cert.X509Certificate certificate) throws NoSuchAlgorithmException, CertificateEncodingException, IOException {
        org.w3c.dom.Element signingCertificateElement = XADES122Sha256SignUtil.createXades122Element(document, namespaceURI, "SigningCertificate");

        /*
         * calcul de Hash SHA-1  Certificate Digest
         */
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
        messageDigest.update(certificate.getEncoded());
        java.io.ByteArrayOutputStream certDigestBOS = new java.io.ByteArrayOutputStream();
        org.bouncycastle.util.encoders.Base64.encode(messageDigest.digest(), certDigestBOS);

        org.w3c.dom.Element certElement = XADES122Sha256SignUtil.createXades122Element(document, namespaceURI, "Cert");

        org.w3c.dom.Element certDigest = XADES122Sha256SignUtil.createXades122Element(document, namespaceURI, "CertDigest");
        org.w3c.dom.Element digestMethod = XADES122Sha256SignUtil.createXades122Element(document, namespaceURI, "DigestMethod");
        org.w3c.dom.Element digestValue = XADES122Sha256SignUtil.createXades122Element(document, namespaceURI, "DigestValue");
        digestMethod.setAttribute("Algorithm", DigestMethod.SHA1);
        digestValue.appendChild(document.createTextNode(certDigestBOS.toString()));
        certDigest.appendChild(digestMethod);
        certDigest.appendChild(digestValue);

        org.w3c.dom.Element issuerSerial = XADES122Sha256SignUtil.createXades122Element(document, namespaceURI, "IssuerSerial");
        org.w3c.dom.Element X509IssuerName = XADES122Sha256SignUtil.createXmldsElement(document, javax.xml.crypto.dsig.XMLSignature.XMLNS, "X509IssuerName");
        X509IssuerName.appendChild(document.createTextNode(certificate.getIssuerX500Principal().getName()));
        org.w3c.dom.Element X509SerialNumber = XADES122Sha256SignUtil.createXmldsElement(document, javax.xml.crypto.dsig.XMLSignature.XMLNS, "X509SerialNumber");
        X509SerialNumber.appendChild(document.createTextNode(certificate.getSerialNumber().toString()));
        issuerSerial.appendChild(X509IssuerName);
        issuerSerial.appendChild(X509SerialNumber);

        certElement.appendChild(certDigest);
        certElement.appendChild(issuerSerial);
        signingCertificateElement.appendChild(certElement);
        return signingCertificateElement;
    }

    /**
     * Does what it is supposed to: set up a signaturePolicyIdentifier XAdES bloc
     *
     * @param document         the doc
     * @param namespaceURI     the namespace URI
     * @param policyIdentifierIdString     PES policyID
     * @param policyIdentifierDescriptionString     PES policy desc
     * @param policyDigestString                    PES policy digest
     * @param spURIString                           PES spURI
     * @return the XML DOM element
     */
    private static org.w3c.dom.Element createSignaturePolicyIdentifier(org.w3c.dom.Document document, String namespaceURI,
                                                                       String policyIdentifierIdString, String policyIdentifierDescriptionString, String policyDigestString, String spURIString) {
        org.w3c.dom.Element signaturePolicyIdentifier = XADES122Sha256SignUtil.createXades122Element(document, namespaceURI, "SignaturePolicyIdentifier");
        org.w3c.dom.Element signaturePolicyId = XADES122Sha256SignUtil.createXades122Element(document, namespaceURI, "SignaturePolicyId");

        org.w3c.dom.Element sigPolicyId = XADES122Sha256SignUtil.createXades122Element(document, namespaceURI, "SigPolicyId");
        org.w3c.dom.Element identifier = XADES122Sha256SignUtil.createXades122Element(document, namespaceURI, "Identifier");
        identifier.appendChild(document.createTextNode(policyIdentifierIdString));
        org.w3c.dom.Element description = XADES122Sha256SignUtil.createXades122Element(document, namespaceURI, "Description");
        description.appendChild(document.createTextNode(policyIdentifierDescriptionString));
        sigPolicyId.appendChild(identifier);
        sigPolicyId.appendChild(description);

        org.w3c.dom.Element sigPolicyHash = XADES122Sha256SignUtil.createXades122Element(document, namespaceURI, "SigPolicyHash");
        org.w3c.dom.Element sdigestMethod = XADES122Sha256SignUtil.createXades122Element(document, namespaceURI, "DigestMethod");
        org.w3c.dom.Element sdigestValue = XADES122Sha256SignUtil.createXades122Element(document, namespaceURI, "DigestValue");
        sdigestMethod.setAttribute("Algorithm", DigestMethod.SHA256);
        sdigestValue.appendChild(document.createTextNode(policyDigestString));
        sigPolicyHash.appendChild(sdigestMethod);
        sigPolicyHash.appendChild(sdigestValue);

        org.w3c.dom.Element sigPolicyQualifiers = XADES122Sha256SignUtil.createXades122Element(document, namespaceURI, "SigPolicyQualifiers");
        org.w3c.dom.Element sigPolicyQualifier = XADES122Sha256SignUtil.createXades122Element(document, namespaceURI, "SigPolicyQualifier");
        org.w3c.dom.Element SPURIelmt = XADES122Sha256SignUtil.createXades122Element(document, namespaceURI, "SPURI");
        SPURIelmt.appendChild(document.createTextNode(spURIString));
        sigPolicyQualifier.appendChild(SPURIelmt);
        sigPolicyQualifiers.appendChild(sigPolicyQualifier);

        signaturePolicyId.appendChild(sigPolicyId);
        signaturePolicyId.appendChild(sigPolicyHash);
        signaturePolicyId.appendChild(sigPolicyQualifiers);
        signaturePolicyIdentifier.appendChild(signaturePolicyId);
        return signaturePolicyIdentifier;
    }

    private static org.w3c.dom.Element createSignatureProductionPlace(org.w3c.dom.Document document, String namespaceURI, String cityString, String postalCodeString, String countryNameString) {
        org.w3c.dom.Element signatureProductionPlace = XADES122Sha256SignUtil.createXades122Element(document, namespaceURI, "SignatureProductionPlace");
        org.w3c.dom.Element cityElmnt = XADES122Sha256SignUtil.createXades122Element(document, namespaceURI, "City");
        cityElmnt.appendChild(document.createTextNode(cityString));
        org.w3c.dom.Element postalCodeElmnt = XADES122Sha256SignUtil.createXades122Element(document, namespaceURI, "PostalCode");
        postalCodeElmnt.appendChild(document.createTextNode(postalCodeString));
        org.w3c.dom.Element countryNameElmnt = XADES122Sha256SignUtil.createXades122Element(document, namespaceURI, "CountryName");
        countryNameElmnt.appendChild(document.createTextNode(countryNameString));
        signatureProductionPlace.appendChild(cityElmnt);
        signatureProductionPlace.appendChild(postalCodeElmnt);
        signatureProductionPlace.appendChild(countryNameElmnt);
        return signatureProductionPlace;
    }

    private static org.w3c.dom.Element createSignerRole(org.w3c.dom.Document document, String namespaceURI, String claimedRoleString) {
        org.w3c.dom.Element signerRole = XADES122Sha256SignUtil.createXades122Element(document, namespaceURI, "SignerRole");
        org.w3c.dom.Element claimedRoles = XADES122Sha256SignUtil.createXades122Element(document, namespaceURI, "ClaimedRoles");
        org.w3c.dom.Element claimedRoleE = XADES122Sha256SignUtil.createXades122Element(document, namespaceURI, "ClaimedRole");
        claimedRoleE.appendChild(document.createTextNode(claimedRoleString));
        claimedRoles.appendChild(claimedRoleE);
        signerRole.appendChild(claimedRoles);
        return signerRole;
    }

    static org.w3c.dom.Element createXmldsElement(org.w3c.dom.Document document, String namespaceURI, String qualifiedName) {
        org.w3c.dom.Element ret = document.createElementNS(namespaceURI, "ds:" + qualifiedName);
        return ret;
    }

//    static  org.w3c.dom.Element createXadesElement(org.w3c.dom.Document document, String namespaceURI, String qualifiedName) {
//        org.w3c.dom.Element ret = document.createElementNS(namespaceURI, "xad:" + qualifiedName);
//        ret.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns", XadesUtil.xadesNS);
//        return ret;
//    }

    static  org.w3c.dom.Element createXades122Element(org.w3c.dom.Document document, String namespaceURI, String qualifiedName) {
        org.w3c.dom.Element ret = document.createElementNS(namespaceURI, "xad:" + qualifiedName);
        ret.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns", XadesUtil.xadesNS122);
        return ret;
    }

}


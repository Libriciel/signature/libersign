/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.adullact.parapheur.applets.splittedsign.sign;

import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;
import org.adullact.parapheur.applets.splittedsign.providers.SignHandler;
import org.adullact.parapheur.applets.splittedsign.providers.SignProvider;
import org.adullact.parapheur.applets.splittedsign.sign.cms.ExternalSignerInfoGenerator;
import org.adullact.parapheur.applets.splittedsign.utils.CRLVerifier;
import org.adullact.parapheur.applets.splittedsign.utils.PropertyConstants;
import org.adullact.parapheur.applets.splittedsign.utils.SignType;
import org.adullact.parapheur.applets.splittedsign.utils.TimeStampTokenUtil;
import org.adullact.parapheur.applets.splittedsign.utils.X509Util;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.tsp.TimeStampToken;

/**
 *
 * @author svast
 */
public abstract class BaseSign implements Sign {
    private final Logger logger = Logger.getLogger("BaseSign");
    
    private SignHandler signHandler;    
    private SignProvider signProvider;
    private Properties properties;  
    private List<X509Certificate> certificateChain;
    private boolean initialized = false;
    
    public BaseSign (SignProvider signProvider, SignHandler signHandler, Properties prop) {
        this.signProvider = signProvider;
        this.signHandler = signHandler;
        
        // updating properties
        properties = new Properties(getDefault());
        for (Map.Entry<Object, Object> entry : prop.entrySet()) {
            logger.info(String.format("updating property key:%s value:%s", (String)entry.getKey(), (String)entry.getValue()));
            properties.setProperty((String)entry.getKey(), (String)entry.getValue());
        }        

        // check if add security provider
        if (BouncyCastleProvider.PROVIDER_NAME.equals(properties.getProperty(PropertyConstants.SecurityProvider.getLiteral())) && 
            Security.getProvider(properties.getProperty(PropertyConstants.SecurityProvider.getLiteral())) == null) {
            Security.addProvider(new BouncyCastleProvider());
            logger.info("Add bouncy castle provider");
        }
    }
    
    public static Properties getDefault () {
        Properties defaultProp = new Properties ();
        defaultProp.setProperty(PropertyConstants.SecurityProvider.getLiteral(), BouncyCastleProvider.PROVIDER_NAME);                 
        defaultProp.setProperty(PropertyConstants.DigestAlgName.getLiteral(), "SHA256");
        defaultProp.setProperty(PropertyConstants.EncryptionAlgName.getLiteral(), "RSA");
        defaultProp.setProperty(PropertyConstants.EnvelopeEncode.getLiteral(), "DER");
        defaultProp.setProperty(PropertyConstants.EnvelopeSignType.getLiteral(), SignType.PAdES_BES.getLiteral());             
        defaultProp.setProperty(PropertyConstants.TSAURL.getLiteral(), "http://timestamping.edelweb.fr/service/tsp");
        defaultProp.setProperty(PropertyConstants.TSAUser.getLiteral(), ""); 
        defaultProp.setProperty(PropertyConstants.TSAPassword.getLiteral(), ""); 
        defaultProp.setProperty(PropertyConstants.VerifyCRL.getLiteral(), "true");
        defaultProp.setProperty(PropertyConstants.VerifyCertificate.getLiteral(), "true");  
        defaultProp.setProperty(PropertyConstants.FileKeyStoreTrustedRootCerts.getLiteral(), "certs.ks");   
        defaultProp.setProperty(PropertyConstants.PassKeyStoreTrustedRootCerts.getLiteral(), "j4ops");    
        
        return defaultProp;    
    }

    public String getProperty(String key) {
        return properties.getProperty(key);
    }    
    
    public void setProperty(String key, String value) {
        properties.setProperty(key, value);
    }       
    
    public Properties getProperties() {
        return properties;
    }
    
    public String getDigestAlgOID () {
        return ExternalSignerInfoGenerator.getOIDFromDigestAlgName(getProperty(PropertyConstants.DigestAlgName.getLiteral()));
    }

    public String getEncryptionAlgOID () {
        return ExternalSignerInfoGenerator.getOIDFromEncryptionAlgName(getProperty(PropertyConstants.EncryptionAlgName.getLiteral()));
    }
    
    public SignType getEnvelopeSignType () {
        return SignType.valueOf(getProperty(PropertyConstants.EnvelopeSignType.getLiteral()));
    }    
    
    public SignProvider getSignProvider () {
        return signProvider;
    }

    public SignHandler getSignHandler() {
        return signHandler;
    }
    
    protected List<X509Certificate> buildAndValidateChain (X509Certificate x509Cert) throws Exception {
        logger.info("loading ca trusted certificates from " + getProperty(PropertyConstants.FileKeyStoreTrustedRootCerts.getLiteral()));
        Set<X509Certificate> trustedCerts =  X509Util.loadKeyStore(getProperty(PropertyConstants.FileKeyStoreTrustedRootCerts.getLiteral()),
                                                                   getProperty(PropertyConstants.PassKeyStoreTrustedRootCerts.getLiteral()));
        logger.info(String.format("loaded %d ca trusted certificates", trustedCerts.size())); 
        return X509Util.buildAndValidateChain(x509Cert, trustedCerts, getProperty(PropertyConstants.SecurityProvider.getLiteral()));
    } 
    
    protected void validateTimeStampToken (TimeStampToken timeStampToken) throws Exception {
        logger.info("loading ca trusted certificates from " + getProperty(PropertyConstants.FileKeyStoreTrustedRootCerts.getLiteral()));
        Set<X509Certificate> trustedCerts =  X509Util.loadKeyStore(getProperty(PropertyConstants.FileKeyStoreTrustedRootCerts.getLiteral()),
                                                                   getProperty(PropertyConstants.PassKeyStoreTrustedRootCerts.getLiteral()));
        logger.info(String.format("loaded %d ca trusted certificates", trustedCerts.size()));         
        TimeStampTokenUtil.validateTimeStampToken(timeStampToken, trustedCerts, getProperty(PropertyConstants.SecurityProvider.getLiteral()));
    }     

    @Override
    public X509Certificate init () throws Exception {
        
        // check if alredy initialized
        if (initialized) {
            throw new Exception ("already initialized");
        }

        // sign provider initialization
        switch (getEnvelopeSignType()) {        
            case PDF:   
            case PAdES_BES:     
            case PAdES_T:  
            case PAdES_A: 
            case PAdES_C:     
            case PAdES_EPES: 
            case PAdES_X_1:     
            case PAdES_X_2:
            case PAdES_X_L:
                
                signProvider.init("NONE", getProperty(PropertyConstants.EncryptionAlgName.getLiteral()), 
                                getSignHandler(), getProperty(PropertyConstants.SecurityProvider.getLiteral()));                 
                break;
                
            default:
                signProvider.init(getProperty(PropertyConstants.DigestAlgName.getLiteral()), getProperty(PropertyConstants.EncryptionAlgName.getLiteral()), 
                                getSignHandler(), getProperty(PropertyConstants.SecurityProvider.getLiteral()));                
                break;
        }

        // get certificate selected
        X509Certificate x509Cert = signProvider.getX509Certificate();
        
        // verify certificate
        x509Cert.checkValidity();
        
        // build and validate chain
        certificateChain = buildAndValidateChain (x509Cert);
        
        // check if verify CRL
        if (Boolean.valueOf(getProperty(PropertyConstants.VerifyCRL.getLiteral()))) {
            CRLVerifier.verifyCertificateCRLs(x509Cert);
        }

        // set initialized is made
        initialized = true;
        
        return x509Cert;        
    }   
    
    @Override
    public void destroy () throws Exception {
        signProvider.destroy();
        initialized = false;
    }

    public boolean isInitialized() {
        return initialized;
    }

    public List<X509Certificate> getCertificateChain() {
        return certificateChain;
    }
}

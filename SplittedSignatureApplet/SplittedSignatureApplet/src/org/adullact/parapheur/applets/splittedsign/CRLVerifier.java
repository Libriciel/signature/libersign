/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.parapheur.applets.splittedsign;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.*;
import java.util.ArrayList;
import java.util.List;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.*;
import org.bouncycastle.asn1.*;
import org.bouncycastle.asn1.x509.*;

/**
 * Class that verifies CRLs for given X509 certificate. Extracts the CRL
 * distribution points from the certificate (if available) and checks the
 * certificate revocation status against the CRLs coming from the
 * distribution points. Supports HTTP, HTTPS, FTP (LDAP is planned) based URLs.
 *
 * @author Svetlin Nakov
 * @author Stephane Vast
 */
public class CRLVerifier {

    /**
     * Extracts the CRL distribution points from the certificate (if available)
     * and checks the certificate revocation status against the CRLs coming from
     * the distribution points. Supports HTTP, HTTPS, FTP and LDAP based URLs.
     *
     * @param cert the certificate to be checked for revocation
     * @param invalidsCRL
     * @throws CertificateVerificationException if the certificate is revoked
     * @throws org.adullact.parapheur.applets.splittedsign.CertificateRevokedException
     * @throws org.adullact.parapheur.applets.splittedsign.CRLNotFoundException
     */
    public static void verifyCertificateCRLs(X509Certificate cert, List<String> invalidsCRL)
            throws CertificateVerificationException,
            CertificateRevokedException, CRLNotFoundException {
        try {
            List<String> crlDistPoints = getCrlDistributionPoints(cert);
            if (crlDistPoints.isEmpty()) {
                System.out.println("verifyCertificateCRLs : crlDistPoints.isEmpty");
                crlDistPoints = getNetscapeCaRevocationUrl(cert);
            } else {
                System.out.println("verifyCertificateCRLs : " + ((crlDistPoints.isEmpty()) ? "empty list" : crlDistPoints));
            }
            Exception lastException = null;
            boolean isValid = false;
            for (String crlDP : crlDistPoints) {
                if (invalidsCRL.contains(crlDP)) {
                    System.out.println("\t ignoring invalid CRL: " + crlDP);
                } else {
                    try {
                        System.out.println("\t processing " + crlDP);
                        X509CRL crl = downloadCRL(crlDP);
                        if (crl == null) {
                            System.out.println("verifyCertificateCRLs : " + crlDP + " download not successful.");
                            continue;
                        }
                        else if (crl.isRevoked(cert)) {
                            throw new CertificateRevokedException("The certificate is revoked by CRL: " + crlDP);
                        }
                        isValid = true;
                    } catch(Exception e) {
                        e.printStackTrace();
                        lastException = e;
                    }
                }
            }
            if(!isValid && lastException != null) {
                throw lastException;
            }
        } catch (Exception ex) {
            if (ex instanceof CertificateVerificationException) {
                throw (CertificateVerificationException) ex;
            } else if (ex instanceof CertificateRevokedException) {
                throw (CertificateRevokedException) ex;
            } else if (ex instanceof CRLNotFoundException) {
                System.out.println("verifyCertificateCRLs : CRL download is not possible. " + ex.getLocalizedMessage());
                throw (CRLNotFoundException) ex;
            } else if (ex instanceof IOException
                    || ex instanceof NamingException
                    || ex instanceof CertificateException
                    || ex instanceof CRLException) {
                // throw a CRLNotFoundException for known acceptable exceptions
                throw new CRLNotFoundException(ex.getLocalizedMessage(), cert.getSubjectX500Principal().getName());
            } else {
                System.out.println("verifyCertificateCRLs : il y a une exception inconnue: " + ex.getLocalizedMessage());
                String stack = "";
                for (StackTraceElement stackTrace : ex.getStackTrace()) {
                    stack += stackTrace + "\n";
                }
                throw new CertificateVerificationException("Can not verify CRL for certificate: " + 
                        cert.getSubjectX500Principal() + "\n" + stack);
            }
        }
    }

    /**
     * Downloads CRL from given URL. Supports http, https, ftp and ldap based URLs.
     */
    private static X509CRL downloadCRL(String crlURL) throws IOException,
            CertificateException, CRLException, CRLNotFoundException,
            CertificateVerificationException, NamingException {
        if (crlURL.startsWith("http://") || crlURL.startsWith("https://")
                || crlURL.startsWith("ftp://")) {
            try {
                X509CRL crl = downloadCRLFromWeb(crlURL);
                return crl;
            } catch (SecurityException ex) {
                System.out.println("\t" + ex.getMessage());
                throw new CRLNotFoundException(ex.getMessage(), crlURL);
                //return null;
            }
        } else if (crlURL.startsWith("ldap://")) {
            
            return null;  // FIXME: la methode n'est pas fiable
            
//            X509CRL crl = downloadCRLFromLDAP(crlURL);
//            return crl;
            
        } else {
            throw new CertificateVerificationException("Can not download CRL from certificate distribution point: " + crlURL);
        }
    }

    /**
     * Downloads a CRL from given LDAP url, e.g.
     * ldap://ldap.infonotary.com/dc=identity-ca,dc=infonotary,dc=com
     */
    private static X509CRL downloadCRLFromLDAP(String ldapURL)
            throws CertificateException, NamingException, CRLException,
            CertificateVerificationException {
        java.util.Hashtable<String, String> env = new java.util.Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, ldapURL);

        DirContext ctx = new InitialDirContext(env);
        byte[] val = null;
        System.out.println("\t processing 1");
        SearchControls controls = new SearchControls(); 
        controls.setSearchScope(SearchControls.SUBTREE_SCOPE); 
        System.out.println("\t processing 2");
        NamingEnumeration<?> results = ctx.search("", "(certificateRevocationList;binary=*)", controls); 
        System.out.println("\t processing 3");
        while (results.hasMore()) { 
            SearchResult searchResult = (SearchResult) results.next(); 
            Attributes attributes = searchResult.getAttributes(); 
            Attribute attr = attributes.get("certificateRevocationList;binary");
            val = (byte[]) attr.get(); 
            System.out.println(" CRL = " + val); 
        } 
//        Attributes avals = ctx.getAttributes("");
//        System.out.println("\t processing 3");
//        Attribute aval = avals.get("certificateRevocationList;binary");
//        System.out.println("\t processing 4");
//        byte[] val = (byte[]) aval.get();
        System.out.println("\t processing 5");
        if ((val == null) || (val.length == 0)) {
            throw new CertificateVerificationException("Can not download CRL from: " + ldapURL);
        } else {
            InputStream inStream = new ByteArrayInputStream(val);
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            X509CRL crl = (X509CRL) cf.generateCRL(inStream);
            return crl;
        }
    }

    /**
     * Downloads a CRL from given HTTP/HTTPS/FTP URL, e.g.
     * http://crl.infonotary.com/crl/identity-ca.crl
     */
    private static X509CRL downloadCRLFromWeb(String crlURL)
            throws MalformedURLException, IOException, CertificateException,
            CRLException, CRLNotFoundException, SecurityException {
        URL url = new URL(crlURL);
        InputStream crlStream = null;
        try {
            crlStream = url.openStream();
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            X509CRL crl = (X509CRL) cf.generateCRL(crlStream);
            return crl;
        } catch(IOException e) {
            throw new CRLNotFoundException("", crlURL);
        } catch(java.lang.SecurityException e) {
            throw new SecurityException("'" + crlURL + "'Problème de sécurité: " + e.getLocalizedMessage());
        }finally {
            if (crlStream != null) {
                crlStream.close();
            }
        }
    }

    /**
     * Extracts all CRL distribution point URLs from the "CRL Distribution Point"
     * extension in a X.509 certificate. If CRL distribution point extension is
     * unavailable, returns an empty list.
     */
    public static List<String> getCrlDistributionPoints(X509Certificate cert)
            throws CertificateParsingException, IOException {
        /**
         * "2.5.29.31" : CRLDistributionPoints
         */
        // Deprecated in bcprov 1.46: byte[] crldpExt = cert.getExtensionValue(X509Extensions.CRLDistributionPoints.getId());
        // Deprecated in bcprov 1.50: byte[] crldpExt = cert.getExtensionValue(X509Extension.cRLDistributionPoints.getId());
        byte[] crldpExt = cert.getExtensionValue(org.bouncycastle.asn1.x509.Extension.cRLDistributionPoints.getId());
        if (crldpExt == null) {
            System.out.print("CRLs getCrlDistributionPoints: getExtensionValue() is null !?!");
            List<String> emptyList = new ArrayList<String>();
            return emptyList;
        }
        ASN1InputStream oAsnInStream = new ASN1InputStream(new ByteArrayInputStream(crldpExt));
        // as of BC1.47 onwards DERObject changes to ASN1Primitive
        // DERObject derObjCrlDP = oAsnInStream.readObject();
        ASN1Primitive derObjCrlDP = oAsnInStream.readObject();
        DEROctetString dosCrlDP = (DEROctetString) derObjCrlDP;
        byte[] crldpExtOctets = dosCrlDP.getOctets();
        ASN1InputStream oAsnInStream2 = new ASN1InputStream(new ByteArrayInputStream(crldpExtOctets));
        // DERObject derObj2 = oAsnInStream2.readObject();
        ASN1Primitive derObj2 = oAsnInStream2.readObject();
        CRLDistPoint distPoint = CRLDistPoint.getInstance(derObj2);
        List<String> crlUrls = new ArrayList<String>();
        for (DistributionPoint dp : distPoint.getDistributionPoints()) {
            DistributionPointName dpn = dp.getDistributionPoint();
            // Look for URIs in fullName
            if (dpn != null) {
                if (dpn.getType() == DistributionPointName.FULL_NAME) {
                    GeneralName[] genNames = GeneralNames.getInstance(dpn.getName()).getNames();
                    // Look for an URI
                    for (int j = 0; j < genNames.length; j++) {
                        if (genNames[j].getTagNo() == GeneralName.uniformResourceIdentifier) {
                            String url = DERIA5String.getInstance(genNames[j].getName()).getString();
                            crlUrls.add(url);
                            System.out.println("CRLs getCrlDistributionPoints GOTCHA : " + url);
                        }
                    }
                }
            }
        }
        return crlUrls;
    }

    /**
     * Extracts the URL from the "Netscape CA Revocation Url" extension in a 
     * X.509 certificate. If that extension is unavailable, returns an empty
     * list.
     */
    public static List<String> getNetscapeCaRevocationUrl(X509Certificate cert)
            throws CertificateParsingException, IOException {
//        System.out.println(" getNetscapeCaRevocationUrl with oid:"
//                + org.bouncycastle.asn1.misc.MiscObjectIdentifiers.netscapeCARevocationURL.getId());
//        byte[] revUrl = cert.getExtensionValue(org.bouncycastle.asn1.misc.MiscObjectIdentifiers.netscapeCARevocationURL.getId());
//        if (revUrl == null) {
//            System.out.println("CRLs getNetscapeCaRevocationUrl: getExtensionValue() is null");
//            List<String> emptyList = new ArrayList<String>();
//            return emptyList;
//        }
//
//        // Get and return string
//        ASN1InputStream ais = new ASN1InputStream(new ByteArrayInputStream(revUrl));
//        System.out.print("  CRLs getNetscapeCaRevocationUrl: 1");
//        DERObject derObjRevUrl = ais.readObject();
//        System.out.print(", 2 " + derObjRevUrl.toString());
//        try {
//            String str = DERIA5String.getInstance(derObjRevUrl).getString();
//            
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new CertificateParsingException("verole");
//        }

        String str = getExtensionValueAsString(cert, org.bouncycastle.asn1.misc.MiscObjectIdentifiers.netscapeCARevocationURL.getId());

        List<String> crlUrls = new ArrayList<String>();
        if (str != null) {
            crlUrls.add(str.substring(str.indexOf("http")));
            System.out.println("CRLs getNetscapeCaRevocationUrl GOTCHA : [" + str + "]");
        }
        
        return crlUrls;
    }

    private static String getExtensionValueAsString(X509Certificate cert, String oid)
            throws IOException {
        String decoded = null;
        byte[] extensionValue = cert.getExtensionValue(oid);

        if (extensionValue != null) {
            // DERObject derObject = toDERObject(extensionValue);
            ASN1Primitive derObject = toASN1Primitive(extensionValue);
            if (derObject instanceof DEROctetString) {
                DEROctetString derOctetString = (DEROctetString) derObject;

                derObject = toASN1Primitive(derOctetString.getOctets());
                if (derObject instanceof DERUTF8String) {
                    DERUTF8String s = DERUTF8String.getInstance(derObject);
                    decoded = s.getString();
                } else if (derObject instanceof DERIA5String) {
                    // System.out.print("getExtensionValueAsString::derObject instanceof DERIA5String. ");
                    DERIA5String s = DERIA5String.getInstance(derObject);
                    decoded = s.getString();
                } else {
                    System.out.print("  getExtensionValueAsString::derObject not a known parsable String??  ");
                    System.out.println(derObject.toString());
                }

            } else {
                System.out.println("  getExtensionValueAsString::derObject not a DEROctetString??");
            }
        } else {
            System.out.println("  getExtensionValueAsString::extensionValue is null??");
        }
        return decoded;
    }

    /**
     * Formatter un object en ASN1
     * 
     * NB: as of BC1.47 onwards DERObject changes to ASN1Primitive
     * @param data flux d'octets à convertir
     * @return le resultat en object ASN1
     * @throws IOException 
     */
    private static ASN1Primitive toASN1Primitive(byte[] data) throws IOException {
        ByteArrayInputStream inStream = new ByteArrayInputStream(data);
        ASN1InputStream asnInputStream = new ASN1InputStream(inStream);
        return asnInputStream.readObject();
    }

}

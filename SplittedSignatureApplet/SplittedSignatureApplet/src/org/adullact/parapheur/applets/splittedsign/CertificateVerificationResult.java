/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.parapheur.applets.splittedsign;


import java.security.cert.PKIXCertPathBuilderResult;

/**
 * This class keeps the result from the certificate verification
 * process. If the certificate is verified as valid, the built
 * certification chain is stored in the Result property. If the
 * certificate is invalid, the problem is stored in the Exception
 * property.
 *
 * @author Svetlin Nakov
 */
public class CertificateVerificationResult {
    private boolean valid;
    private PKIXCertPathBuilderResult result;
    private Throwable exception;

    /**
     * Constructs a certificate verification result for valid
     * certificate by given certification path.
     */
    public CertificateVerificationResult(PKIXCertPathBuilderResult result) {
        this.valid = true;
        this.result = result;
    }

    /**
     * Constructs a certificate verification result for invalid
     * certificate by given exception that keeps the problem
     * occurred during the verification process.
     */
    public CertificateVerificationResult(Throwable exception) {
        this.valid = false;
        this.exception = exception;
    }

    public boolean isValid() {
        return valid;
    }

    public PKIXCertPathBuilderResult getResult() {
        return result;
    }

    public Throwable getException() {
        return exception;
    }
}


/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.parapheur.applets.splittedsign;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLConnection;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.InvalidParameterException;
import java.security.MessageDigest;
import java.security.cert.CertStoreException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dom.DOMStructure;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.Text;
import org.bouncycastle.tsp.TimeStampRequest;
import org.bouncycastle.tsp.TimeStampRequestGenerator;
import org.bouncycastle.tsp.TSPAlgorithms;
import org.bouncycastle.tsp.TimeStampResponse;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.asn1.cmp.PKIStatus;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.tsp.TSPValidationException;
import org.bouncycastle.tsp.TimeStampToken;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.DefaultCMSSignatureAlgorithmNameGenerator;
import org.bouncycastle.cms.SignerInformationVerifier;
import org.bouncycastle.cms.bc.BcRSASignerInfoVerifierBuilder;
import org.bouncycastle.operator.DefaultDigestAlgorithmIdentifierFinder;
import org.bouncycastle.operator.DefaultSignatureAlgorithmIdentifierFinder;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.bc.BcDigestCalculatorProvider;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.util.Store;
import org.xml.sax.SAXException;

/**
 * Stamps already signed documents. Notice that the stamping can occur late after
 * the document is signed, so the timestamping code is separadted from the signing code
 * 
 * @author Augustin Sarr (NETHEOS), Stephane Vast (Adullact projet)
 */
public class TimeStamper
{

    static final String DEFAULT_DIGEST = "SHA1";

    static final int DEFAULT_NONCE_LENGTH = 20;

    static final Logger logger = Logger.getLogger(TimeStamper.class.getName());

    public static byte[] stampSignatures(InputStream signedDocumentStream, String serverURLStr) throws XMLSignatureException
    {
        if (signedDocumentStream == null || serverURLStr == null)
            throw new InvalidParameterException("Null argument!");
        try
        {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss");
            byte[] nonceBuffer = new byte[DEFAULT_NONCE_LENGTH];
            Map<BigInteger, TimeStampRequest> requests = new HashMap<BigInteger, TimeStampRequest>();
            Map<BigInteger, Element> signatures = new HashMap<BigInteger, Element>();
            Map<BigInteger, byte[]> digests = new HashMap<BigInteger, byte[]>();
            BigInteger nonce;

            SecureRandom random = new SecureRandom();
            random.setSeed(dateFormat.format(new Date()).getBytes());

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(signedDocumentStream);
            NodeList signatureList = document.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
            int listLength = signatureList.getLength();
            if (listLength == 0)
                throw new XMLSignatureException("The document is not signed");
            for (int i = 0; i < listLength; i++)
            {
                random.nextBytes(nonceBuffer);
                nonce = new BigInteger(nonceBuffer);
                signatures.put(nonce, (Element) signatureList.item(i));
            }

            Iterator<BigInteger> iter = signatures.keySet().iterator();
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            MessageDigest messageDigest = MessageDigest.getInstance(DEFAULT_DIGEST);
            TimeStampRequestGenerator requestGenerator = new TimeStampRequestGenerator();
            byte[] digest;

            XMLSignatureFactory sigFactory = XMLSignatureFactory.getInstance("DOM");
            XMLSignature sig;
            while (iter.hasNext())
            {
                nonce = iter.next();
                sig = sigFactory.unmarshalXMLSignature(new DOMStructure(signatures.get(nonce)));
                messageDigest.reset();
                digest = messageDigest.digest(sig.getSignatureValue().getValue());
                digests.put(nonce, digest);
                requests.put(nonce, requestGenerator.generate(TSPAlgorithms.SHA1, digest, nonce));
            }

            // TODO : rewrite this loop to be more efficient
            iter = requests.keySet().iterator();
            byte[] stamp;
            Element signature;
            while (iter.hasNext())
            {
                nonce = iter.next();
                stamp = queryStamp(requests.get(nonce), serverURLStr);
                if (stamp == null)
                    throw new XMLSignatureException("Stamp generation failed!");
                TimeStampResponse tsResponse = new TimeStampResponse(stamp);
                if (validateStamp(nonce, requests.get(nonce), tsResponse) == false)
                    throw new XMLSignatureException("Invalid time stamp!");

                signature = signatures.get(nonce);
                Element objectElement = getChildElement(signature, "ds:Object");
                if (objectElement == null)
                    throw new XMLSignatureException("Invalid signature!");

                Element qualifyingPropElement = getChildElement(objectElement, "xad:QualifyingProperties");
                if (qualifyingPropElement == null)
                    throw new XMLSignatureException("Invalid signature!");
                Element unsignedPropElement = getChildElement(qualifyingPropElement, "xad:UnsignedProperties");
                if (unsignedPropElement == null)
                {
                    unsignedPropElement = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "UnsignedProperties");
                    qualifyingPropElement.appendChild(unsignedPropElement);
                }
                Element unsignedSignPropElement = getChildElement(unsignedPropElement, "xad:UnsignedSignatureProperties");
                if (unsignedSignPropElement == null)
                {
                    unsignedSignPropElement = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "UnsignedSignatureProperties");
                    unsignedPropElement.appendChild(unsignedSignPropElement);
                }

                Element sigTsElement = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "SignatureTimeStamp");
                unsignedSignPropElement.appendChild(sigTsElement);

                Element hashDataInfoElmt = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "HashDataInfo");
                hashDataInfoElmt.setAttribute("uri", "#" + signature.getAttribute("Id"));
                sigTsElement.appendChild(hashDataInfoElmt);

                Element transforms = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "Transforms");
                hashDataInfoElmt.appendChild(transforms);

                Element transformsElmt = document.createElement("ds:Transform");
                transformsElmt.setAttribute("Algorithm", "http://www.w3.org/2000/09/xmldsig#rsa-sha1");
                transforms.appendChild(transformsElmt);
                Element stampElmt = XADESSignUtil.createXadesElement(document, XADESSignUtil.xadesNS, "EncapsulatedTimeStamp");

                stampElmt.setAttribute("Id", signature.getAttribute("Id") + "_TS");
                Text stString = document.createTextNode(new String(Base64.encode(stamp), "UTF8"));
                stampElmt.appendChild(stString);
                sigTsElement.appendChild(stampElmt);
            }


            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            transformer.transform(new DOMSource(document), new StreamResult(byteArrayOutputStream));
            return byteArrayOutputStream.toByteArray();

        } catch (MarshalException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (TSPValidationException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (CertStoreException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (CertificateExpiredException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (CertificateNotYetValidException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (TSPException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (TransformerException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);

        } catch (NoSuchAlgorithmException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);  //should never happen
        } catch (SAXException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (IOException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (ParserConfigurationException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (OperatorCreationException ex) {
            logger.log(Level.SEVERE, null, ex);
            throw  new XMLSignatureException(ex.getMessage(), ex);
        }
    }

    static Element getChildElement(Element element, String childTagName)
    {
        NodeList childs = element.getChildNodes();
        Element childElement = null;
        for (int i = 0; i < childs.getLength(); i++)
        {
            Node child = childs.item(i);
            if (child instanceof Element)
            {
                if (!((Element) child).getTagName().equals(childTagName))
                    continue;
                else
                {
                    childElement = (Element) child;
                    break;
                }
            }
        }
        return childElement;
    }

    /**
     * query a timestamp for a single request.
     */
    static byte[] queryStamp(TimeStampRequest request, String serverURLStr)
    {

        try
        {
            URL url = new URL(serverURLStr);
            URLConnection connection = url.openConnection();
            connection.setRequestProperty("Content-Type", "application/timestamp-query");

            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);

            DataOutputStream outStream = new DataOutputStream(connection.getOutputStream());
            outStream.write(request.getEncoded()); //outStream.write(Base64.encode(request.getEncoded()));
            outStream.flush();
            outStream.close();

            DataInputStream inStream = new DataInputStream(connection.getInputStream());
            while (inStream.available() == 0)
            {
                try
                {
                    Thread.sleep(300);
                } catch (InterruptedException ex)
                {
                    logger.log(Level.SEVERE, null, ex);
                    return null;
                }
            }
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[2048];
            int readen;
            do
            {
                readen = inStream.read(buffer);
                if (readen > 0)
                    baos.write(buffer, 0, readen);
            }
            while (readen != -1);
            return baos.toByteArray();

        } catch (IOException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            return null;
        }
    }

    static boolean validateStamp(BigInteger nonce, TimeStampRequest req, TimeStampResponse response) throws TSPValidationException, CertStoreException, CertificateExpiredException, CertificateNotYetValidException, OperatorCreationException
    {
        try
        {
            if (response.getStatus() != PKIStatus.GRANTED && response.getStatus() != PKIStatus.GRANTED_WITH_MODS)
                return false;
            if (!nonce.equals(response.getTimeStampToken().getTimeStampInfo().getNonce()))
                return false;
            response.validate(req);

            if (!validate(response.getTimeStampToken().toCMSSignedData().getSignerInfos(),
                    response.getTimeStampToken().getCertificates(), response))
                    // response.getTimeStampToken().getCertificatesAndCRLs("Collection", "BC"), response))
                return false;
            return true;
        } catch (TSPException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new TSPValidationException(ex.getMessage());
        } catch (NoSuchAlgorithmException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new TSPValidationException(ex.getMessage());
        } catch (NoSuchProviderException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new TSPValidationException(ex.getMessage());
        } catch (CMSException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new TSPValidationException(ex.getMessage());
        }
    }

//    static boolean validate(SignerInformationStore signers, CertStore certStore, TimeStampResponse response)
//            throws CertStoreException, NoSuchAlgorithmException, NoSuchProviderException, CMSException, TSPException,
//            TSPValidationException, CertificateExpiredException, CertificateNotYetValidException
    static boolean validate(SignerInformationStore signers, Store store, TimeStampResponse response)
            throws CertStoreException, NoSuchAlgorithmException, NoSuchProviderException, CMSException, TSPException,
            TSPValidationException, CertificateExpiredException, CertificateNotYetValidException, OperatorCreationException
    {
        Iterator iter = signers.getSigners().iterator(); // there may be more than one signers
        SignerInformation signer;
        // X509Certificate certificate;
        Date today = new Date();
        TimeStampToken tsToken = response.getTimeStampToken();
        while (iter.hasNext())
        {
            signer = (SignerInformation) iter.next();
            
            X509CertificateHolder cert = (X509CertificateHolder) store.getMatches(signer.getSID()).iterator().next();
            SignerInformationVerifier verifier = new BcRSASignerInfoVerifierBuilder(
                    new DefaultCMSSignatureAlgorithmNameGenerator(),
                    new DefaultSignatureAlgorithmIdentifierFinder(),
                    new DefaultDigestAlgorithmIdentifierFinder(),
                    new BcDigestCalculatorProvider()).build(cert);

            if (cert.getNotBefore().before(today)
                    && cert.getNotAfter().after(today)) {
                tsToken.validate(verifier);
            } else {
                return false;
            }

//            Collection certColl = certStore.getCertificates(signer.getSID());
//
//            java.util.Iterator certIter = certColl.iterator();
//            while (certIter.hasNext())
//            {
//                certificate = (X509Certificate) certIter.next();
//
//                // TODO: if needed, perform here further validations about the TS authority's certificate
//
//                if (certificate.getNotBefore().before(today)
//                        && today.before(certificate.getNotAfter()))
//                    tsToken.validate(certificate, "BC");
//                else
//                    return false;
//            }
        }
        return true;
    }

}



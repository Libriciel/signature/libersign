/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.parapheur.applets.splittedsign;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.crypto.dsig.XMLSignatureException;

/**
 *
 * @author a.sarr
 */
public class Main1 {
   static  String path = "C:/Documents and Settings/a.sarr/Mes documents/Etudes/adullact/CryptoTests/";
    public static void main(String[] args)
    {
       FileInputStream is = null;
        try
        {
            is = new FileInputStream(path + "exemple.xml");
            KeyStore ks = KeyStore.getInstance("JKS");
            ks.load(new FileInputStream(path + "mykeystore.jks"), "mystorepass".toCharArray());
            KeyStore.PrivateKeyEntry pkEntry = (KeyStore.PrivateKeyEntry) ks.getEntry("mykey",
                    new KeyStore.PasswordProtection("mystorepass".toCharArray()));
            PrivateKey privateKey = pkEntry.getPrivateKey();
            InputStream inStream = new FileInputStream(path + "mycert.crt");
            CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
            X509Certificate certificate = (X509Certificate) certFactory.generateCertificate(inStream);
            byte[] signedDoc = XADESCosigner.communeSign(certificate, privateKey, is, "annotation publique.");
            is.close();
            FileOutputStream fostream = new FileOutputStream(path+"cosign.xml");            
            fostream.write(signedDoc, 0, signedDoc.length);
            fostream.close();

        } catch (XMLSignatureException ex)
        {
            Logger.getLogger(Main1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnrecoverableEntryException ex)
        {
            Logger.getLogger(Main1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex)
        {
            Logger.getLogger(Main1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex)
        {
            Logger.getLogger(Main1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CertificateException ex)
        {
            Logger.getLogger(Main1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (KeyStoreException ex)
        {
            Logger.getLogger(Main1.class.getName()).log(Level.SEVERE, null, ex);
        } finally
        {
            try
            {
                is.close();
            } catch (IOException ex)
            {
                Logger.getLogger(Main1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}

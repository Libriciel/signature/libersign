/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.parapheur.applets.splittedsign;

import org.bouncycastle.cms.CMSException;
import sun.security.x509.X500Name;
import sun.security.x509.X509CertInfo;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by lhameury on 19/05/16.
 */
public class PKCS1SignUtil {

    static final Logger logger = Logger.getLogger(PKCS1SignUtil.class.getName());
    private static final String OS_NAME = System.getProperty("os.name");

    static byte[] ebicsC14N(byte[] data) {
        byte[] groomedData = new byte[data.length];
        int bytesCopied = 0;

        for (byte datum : data) {
            switch (datum) {
                case (char) 0x0a:
                case (char) 0x0d:
                case (char) 0x1a:
                    break;
                default:
                    groomedData[bytesCopied++] = datum;
            }
        }

        byte[] packedData = new byte[bytesCopied];

        System.arraycopy(groomedData, 0, packedData, 0, bytesCopied);

        return packedData;
    }

    private static byte[] sign(X509Certificate[] certChain, PrivateKey privateKey, byte[] document, boolean toDownload) throws NoSuchAlgorithmException, InvalidKeyException, IOException, SignatureException, CertificateException, CMSException {

        Principal issuerName = certChain[0].getIssuerDN();
        if (!(issuerName instanceof X500Name)) {
            X509CertInfo tbsCert = new X509CertInfo(certChain[0].getTBSCertificate());
            issuerName = (Principal) tbsCert.get("issuer.dname");
            System.out.println("Certificat fourni par: " + issuerName);
        }
        /**
         * Set up the basic stuff
         * LEGACY PROCESS
         */

        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());


        String provider = "SunRsaSign";

        if (privateKey instanceof java.security.interfaces.RSAPrivateKey) {
            provider = "BC";
        } else if (OS_NAME.startsWith("Windows")) {
            provider = "SunMSCAPI";
        }

        byte[] signature;
        try {
            Signature sig = Signature.getInstance("SHA256withRSA", provider);
            sig.initSign(privateKey);

            if(toDownload) {
                String documentPath = new String(document);

                URL downloadLink;
                if(documentPath.startsWith("https")) {
                    TrustManager[] trustAllCerts = new TrustManager[]{
                            new X509TrustManager() {
                                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                    return null;
                                }

                                public void checkClientTrusted(
                                        java.security.cert.X509Certificate[] certs, String authType) {
                                }

                                public void checkServerTrusted(
                                        java.security.cert.X509Certificate[] certs, String authType) {
                                }
                            }
                    };
                    // Install the all-trusting trust manager
                    try {
                        SSLContext sc = SSLContext.getInstance("SSL");
                        sc.init(null, trustAllCerts, new java.security.SecureRandom());
                        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                    } catch (Exception ignored) {
                    }

                    downloadLink = new URL(null, documentPath, new sun.net.www.protocol.https.Handler());
                } else {
                    downloadLink = new URL(null, documentPath, new sun.net.www.protocol.http.Handler());
                }

                // Open the connection to the URL
                int BUFSIZE = 8192;

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                InputStream is = null;
                try {
                    is = downloadLink.openStream();
                    byte[] byteChunk = new byte[BUFSIZE];
                    int n;

                    while ( (n = is.read(byteChunk)) > 0 ) {
                        baos.write(byteChunk, 0, n);
                    }
                }
                catch (IOException e) {
                    e.printStackTrace ();
                    // Perform any other exception handling that's appropriate.
                }
                finally {
                    if (is != null) { is.close(); }
                }

                sig.update(PKCS1SignUtil.ebicsC14N(baos.toByteArray()));
            } else {
                sig.update(PKCS1SignUtil.ebicsC14N(document));
            }

            signature = sig.sign();
        } catch(Exception e) {
            throw new SignatureException("Exception ... " + e.getLocalizedMessage());
        }
        dump("Signature EBICS", signature);

        return signature;
    }

    static private void dump(String title, byte[] bytes) {
        System.out.printf("%s : ", title);
        for (byte b : bytes) {
            System.out.printf("%02x ", b & 0xff);
        }
        System.out.println();
    }

    /*
     * Genere la signature pkcs#1 à partir du document
     * @param cert certificat du signataire
     * @param privKey clef privée de signature
     * @param document valeur du document
     * @return signature RSA
     */
    public static byte[] sign(X509Certificate cert, PrivateKey privKey, byte[] document, boolean toDownload) throws CMSException,
            InvalidKeyException {
        try {
            java.security.cert.X509Certificate[] certChain = {cert};
            return sign(certChain, privKey, document, toDownload);
        } catch (NoSuchAlgorithmException ex) {
            logger.log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            throw new CMSException(ex.getMessage(), ex);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            throw new CMSException(ex.getMessage(), ex);
        } catch (SignatureException ex) {
            logger.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
            ex.printStackTrace();
            throw new CMSException(ex.getMessage(), ex);
        } catch (CertificateException ex) {
            logger.log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            throw new CMSException(ex.getMessage(), ex);
        }
    }
}

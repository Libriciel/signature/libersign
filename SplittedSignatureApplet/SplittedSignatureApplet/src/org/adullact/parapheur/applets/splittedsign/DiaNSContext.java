/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.parapheur.applets.splittedsign;

import java.util.Iterator;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.namespace.NamespaceContext;

/**
 *
 * @author a.sarr
 */
public class DiaNSContext  implements NamespaceContext{
                @Override
                public String getNamespaceURI(String prefix)
                {
                    if ("xad".equals(prefix))
                        return XADESSignUtil.xadesNS122;
                    if ("ds".equals(prefix))
                        return XMLSignature.XMLNS;
                    if ("dia".equals(prefix))
                        return XADESCosigner.DIA_NS;
                    return null;
                }

                @Override
                public Iterator getPrefixes(String val)
                {
                    throw new UnsupportedOperationException();
                }

                @Override
                public String getPrefix(String uri)
                {
                    if (XADESSignUtil.xadesNS122.equals(uri))
                        return "xad";
                    if ("ds".equals(uri))
                        if (XMLSignature.XMLNS.equals(uri))
                            return "ds";
                    if (XADESCosigner.DIA_NS.equals(uri))
                        return "dia";
                    return null;
                }

}

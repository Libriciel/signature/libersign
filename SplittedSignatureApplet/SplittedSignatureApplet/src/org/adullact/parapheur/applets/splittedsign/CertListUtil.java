/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.parapheur.applets.splittedsign;

import java.io.IOException;
import java.lang.reflect.Field;
import java.security.*;
import java.security.cert.*;
import java.security.cert.Certificate;
import java.util.*;

/**
 * Classe de listing des certificats
 * Created by lhameury on 27/01/16.
 */
public class CertListUtil {

    private KeyStore ks = null;
    // Ajout pour Macintosheries PKCS11
    private KeyStore macTokenks = null;
    private Enumeration<String> macTokenaliases = null;
    protected PrivateKey signatureKey_;
    protected PublicKey verificationKey_;

    public CertListUtil() {
        try {
            Configurator.reset();
            ks = Configurator.getInstance().getKeyStore();
            if (Configurator.isMacOsX()) {
                // Try to detect Token device
                macTokenks = Configurator.getInstance().getMacTokenKeyStore();
            }
        } catch(KeyStoreException ex) {
            System.out.println("## WARN!! getKeyStore-ops: " + ex.getLocalizedMessage());
        }
    }

    public PrivateKey getKey(X509Certificate cert) {
        try {
            if (Configurator.isMacOsX()) {
                if (macTokenks != null && cert instanceof iaik.x509.X509Certificate) {
                    // Jetons materiels receuperes depuis IAIK
                    String certAlias = macTokenks.getCertificateAlias(cert);
                    System.out.println("\tsigning ALIAS: [" + ks.containsAlias(certAlias) + "]");
                    System.out.println("\tsigning ALIAS: [" + certAlias + "]");
                    return (PrivateKey) macTokenks.getKey(certAlias, Configurator.getInstance().getPassword(false));
                } else {
                    // Jetons logiciels recuperes depuis Le porte cle MACOS --> Non utilise normalement
                    String certAlias = ks.getCertificateAlias(cert);
                    System.out.println("\tsigning ALIAS: [" + ks.containsAlias(certAlias) + "]");
                    System.out.println("\tsigning ALIAS: [" + certAlias + "]");
                    if (certAlias.endsWith(" 1")) {
                        return (PrivateKey) ks.getKey(certAlias.substring(0, certAlias.length()-2), Configurator.getInstance().getPassword(false));
                    } else {
                        return (PrivateKey) ks.getKey(certAlias, Configurator.getInstance().getPassword(false));
                    }
                }
            } else {
                Certificate loadedCert;
                int i = 0;
                do {
                    // Java 8u101 - double vérification et gestion des doublons dans le keystore
                    String certAlias = ks.getCertificateAlias(cert);
                    if(i > 0) {
                        certAlias = certAlias + " (" + i + ")";
                    }
                    loadedCert = ks.getCertificate(certAlias);
                    if (loadedCert != null && loadedCert.hashCode() == cert.hashCode()) {
                        return (PrivateKey) ks.getKey(certAlias, Configurator.getInstance().getPassword(false));
                    }
                    i++;
                } while(loadedCert != null);
                throw new KeyStoreException("Certificat " + ks.getCertificateAlias(cert) + "introuvable");
            }
        } catch (KeyStoreException ex) {
            System.out.println(ex.getLocalizedMessage());
            return null;
        } catch (UnrecoverableKeyException ex) {
            System.out.println(ex.getLocalizedMessage());
            return null;
        } catch (NoSuchAlgorithmException ex) {
            System.out.println(ex.getLocalizedMessage());
            return null;
        }
    }

    /**
     * Sometimes Microsoft CryptoAPI KeyStore can have aliases duplicates.
     * Actually: MSCAPI provider does not create unique aliase names
     *  http://forums.sun.com/thread.jspa?threadID=5400459
     *  http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6672015
     *
     * @param keyStore
     */
    private static void _fixAliases(KeyStore keyStore) {
        Field field;
        KeyStoreSpi keyStoreVeritable;
        try {
            field = keyStore.getClass().getDeclaredField("keyStoreSpi");
            field.setAccessible(true);
            keyStoreVeritable = (KeyStoreSpi)field.get(keyStore);

            // System.out.println("Class: "+keyStoreVeritable.getClass().getName());
            if("sun.security.mscapi.KeyStore$MY".equals(keyStoreVeritable.getClass().getName())) {
                Collection entries;
                String alias, hashCode;
                X509Certificate[] certificates;

                field = keyStoreVeritable.getClass().getEnclosingClass().getDeclaredField("entries");
                field.setAccessible(true);
                Object entriesObj = field.get(keyStoreVeritable);
                if(entriesObj instanceof HashMap) {
                    return;
                }
                entries = (Collection) entriesObj;

                for(Object entry : entries) {
                    field = entry.getClass().getDeclaredField("certChain");
                    field.setAccessible(true);
                    certificates = (X509Certificate[])field.get(entry);

                    hashCode = Integer.toString(certificates[0].hashCode());

                    field = entry.getClass().getDeclaredField("alias");
                    field.setAccessible(true);
                    alias = (String)field.get(entry);

                    if(!alias.equals(hashCode)) {
                        field.set(entry, alias.concat(" - ").concat(hashCode));
                    }
                }
            }
        } catch(Exception exception) {
            System.err.println(exception);
            System.out.println(exception.getLocalizedMessage());
        }
    }

    /**
     * One vital Certificate extension is the "Basic Constraints" extension.
     * It tells NSS whether the cert is a CA cert, or not, and affects every
     * other aspect of how the cert is interpreted by NSS.  The OID for this
     * extension is { 2 5 29 19 }, encoded in hex as 0x55, 0x1d, 0x13.
     * If the extension is present and has the value TRUE, then this cert is
     * taken to be a CA cert.  Otherwise it is not
     *
     * @param alias
     * @param xcert
     * @return TRUE if xcert is not a CA cert, FALSE otherwise
     */
    private boolean certIsNotCA(String alias, X509Certificate xcert) throws IOException {
        /*
         * Portage BC 1.47 et +
         *
         * on souhaite que ce ne soit pas une AC.
         * Donc le retour DOIT etre '-1'
         */
        return (xcert.getBasicConstraints() == -1);

        //        byte[] basicConstraints = xcert.getExtensionValue("2.5.29.19");
        //        if (basicConstraints == null) {
        //            // System.out.print("INFO: Alias [" + alias + "] sans BasicConstraints, ");
        //            return true;
        //        } else {
        //            byte[] bValue = ((ASN1OctetString) ASN1Object.fromByteArray(basicConstraints)).getOctets();
        //            BasicConstraints bc = new BasicConstraints((ASN1Sequence)ASN1Sequence.fromByteArray(bValue));
        //            if (bc.isCA()) {
        //                return false;
        //            } else {
        //                return true;
        //            }
        //        }
    }

    /**
     * Tous les certificats ne sont pas eligibles à la signature electronique.
     * On se repose sur l'extension 'keyUsage' du certificat X509. Ce champ
     * renseigne sur l'utilisation qui doit être faite de la clé.
     * Voir : http://www.ietf.org/rfc/rfc3280.txt
     *
     * The key usage extension defines the purpose (e.g., encipherment,
     * signature, certificate signing) of the key contained in the
     * certificate.  The usage restriction might be employed when a key that
     * could be used for more than one operation is to be restricted.  For
     * example, when an RSA key should be used only to verify signatures on
     * objects other than public key certificates and CRLs, the
     * digitalSignature and/or nonRepudiation bits would be asserted.
     * Likewise, when an RSA key should be used only for key management, the
     * keyEncipherment bit would be asserted.
     *
     * This extension MUST appear in certificates that contain public keys
     * that are used to validate digital signatures on other public key
     * certificates or CRLs.  When this extension appears, it SHOULD be
     * marked critical.
     *
     * @param alias
     * @param xcert
     * @return true si le certificat est OK pour signer, false sinon.
     */
    private boolean certIsGoodForSignature(String alias, X509Certificate xcert) {
        boolean res = false;
        try {
            /**
             * Recup des extensions critiques
             */
            Set<String> critExts = xcert.getCriticalExtensionOIDs();
            if (xcert.hasUnsupportedCriticalExtension()) {
                System.out.print("# (info: alias  [" + alias + "] has unsupported critical extension) ");
            }
            if (critExts == null || critExts.isEmpty()) {
                System.out.print("# (info: no critical ext.) ");
                /**
                 * pas de keyUsage en extension critique... le champ Key usage
                 * est ici a titre indicatif.
                 * "if keyUsage extension is absent, the cert is assumed to be
                 *  valid for all key usages (Netscape NSS)."
                 * Si present: Verification a-minima du flag digitalSignature
                 */
                if (certIsNotCA(alias, xcert)) {
                    if (xcert.getKeyUsage() == null) {
                        System.out.print(" (info: no keyUsage) ");
                        res = true;
                    } else if (xcert.getKeyUsage()[1]) { // (1): nonRepudiation
                        res = true;
                        /**
                         * KeyUsage extension, (OID = 2.5.29.15). The key usage extension
                         * defines the purpose (e.g., encipherment, signature, certificate
                         * signing) of the key contained in the certificate. The ASN.1
                         * definition for this is:
                         *  KeyUsage ::= BIT STRING {
                         *      digitalSignature        (0),
                         *      nonRepudiation          (1),
                         *      keyEncipherment         (2),
                         *      dataEncipherment        (3),
                         *      keyAgreement            (4),
                         *      keyCertSign             (5),
                         *      cRLSign                 (6),
                         *      encipherOnly            (7),
                         *      decipherOnly            (8) }
                         */
                    } else {
                        HashMap<String, String> issuerInfo = CertificateInfosExtractor.makeIssuerInfos(xcert);
                        if(issuerInfo !=null && issuerInfo.containsKey("CN") &&
                                issuerInfo.get("CN").contains("CSF - Classe III - Sign et Crypt") &&
                                xcert.getKeyUsage()[0]) {
                            /* Les certificats CLASSE 3 de CHAMBERSIGN se sont gourrés
                             * ils ont activé le bit 'digital signature'. FAIL !
                             */
                            res = true;
                            System.out.print("WARN cert CSF Classe III, on accepte quand meme. ");
                        } else {
                            System.out.println("Alias [" + alias + "]: keyUsage présent, non critique, mais 'nonRepudiation' absent...");
                        }
                    }
                } else {
                    System.out.println("Alias [" + alias + "] est une AC.");
                }
            } else if (critExts.contains("2.5.29.15")) {
                /**
                 * keyUsage est une extension critique, on s'appuie dessus: la
                 * cle ne doit etre utilisee que dans le but specifie, et toute
                 * autre utilisation serait contrevenir a la politique de l'AC.
                 */
                if (xcert.getKeyUsage() == null) {
                    System.out.println("# Alias  [" + alias + "]: keyUsage critique mais absent??? NON RETENU !");
                    java.util.List<String> extUsages = xcert.getExtendedKeyUsage();
                    if (extUsages != null) {
                        /**
                         * OID = 2.5.29.37    extendedKeyUsage meaning.
                         * -----              -------
                         * serverAuth         SSL/TLS Web Server Authentication.
                         * clientAuth         SSL/TLS Web Client Authentication.
                         * codeSigning        Code signing.
                         * emailProtection    E-mail Protection (S/MIME).
                         * timeStamping       Trusted Timestamping
                         * msCodeInd          Microsoft Individual Code Signing (authenticode)
                         * msCodeCom          Microsoft Commercial Code Signing (authenticode)
                         * msCTLSign          Microsoft Trust List Signing
                         * msSGC              Microsoft Server Gated Crypto
                         * msEFS              Microsoft Encrypted File System
                         * nsSGC              Netscape Server Gated Crypto
                         */
                        for (String s :extUsages) {
                            System.out.println("\textKeyUsage: " + s);
                        }
                    } else {
                        System.out.println("\t(pour info: pas de ExtendedKeyUsage non plus.)");
                    }

                    /**
                     * KeyUsage extension, (OID = 2.5.29.15). The key usage extension
                     * defines the purpose (e.g., encipherment, signature, certificate
                     * signing) of the key contained in the certificate. The ASN.1
                     * definition for this is:
                     *  KeyUsage ::= BIT STRING {
                     *      digitalSignature        (0),
                     *      nonRepudiation          (1),
                     *      keyEncipherment         (2),
                     *      dataEncipherment        (3),
                     *      keyAgreement            (4),
                     *      keyCertSign             (5),
                     *      cRLSign                 (6),
                     *      encipherOnly            (7),
                     *      decipherOnly            (8) }
                     */
                } else if (xcert.getKeyUsage()[1]) { // (1): nonRepudiation
                    // check if cert is CA: we do not want CA aliases
                    if (certIsNotCA(alias, xcert)) {
                        res = true;
                    } else {
                        System.out.println("# Alias [" + alias + "] est une AC.");
                    }
                } else if (CertificateInfosExtractor.makeIssuerInfos(xcert).get("CN").contains("CSF - Classe III - Sign et Crypt")
                           && xcert.getKeyUsage()[0]) {
                    /* Les certificats CLASSE 3 de CHAMBERSIGN se sont gourrés
                     * ils ont activé le bit 'digital signature'. FAIL !
                     */
                    res = true;
                    System.out.print("# cert CSF Classe III, on accepte quand meme. ");
                } else {
                    System.out.println("# Alias sans droit de signature: [" + alias + "], serial= " + xcert.getSerialNumber().toString(16));
                }

            } else {
                /**
                 * pas de keyUsage en extension critique... le champ Key usage
                 * est ici a titre indicatif.
                 * "if keyUsage extension is absent, the cert is assumed to be
                 *  valid for all key usages (Netscape NSS)."
                 * Si present: Verification a-minima du flag digitalSignature
                 */
/*                if (xcert.getKeyUsage() == null && certIsNotCA(alias, xcert)) {
                    System.out.println("Alias retenu: [" + alias + "], mais SANS extension keyUsage");
                    res = true;
                } else if (xcert.getKeyUsage()[0] && certIsNotCA(alias, xcert)) {
                    res = true;
                } else {
                    System.out.println("Alias [" + alias + "]: keyUsage présent, non critique, mais digitalSignature absent...");
                }*/
                if (certIsNotCA(alias, xcert)) {
                    if (xcert.getKeyUsage() == null) {
                        System.out.print("# (info: no keyUsage) ");
                        res = true;
                    } else if (xcert.getKeyUsage()[1]) { // (1): nonRepudiation
                        res = true;
                    } else {
                        System.out.println("# Alias [" + alias + "]: keyUsage présent, non critique, mais champ nonRepudiation absent...");
                    }
                } else {
                    System.out.println("# Alias [" + alias + "] est une AC.");
                }
            }
        } catch (CertificateParsingException ex) {
            // do nothing
            System.out.println("incapable de lire le certificat.");
        } catch (java.io.IOException io) {
            System.out.println("ça a chié dans le ventilo: " + io.getLocalizedMessage());
        }
        return res;
    }

    public List<Certificate> getAvailableCertificates() {
        List<Certificate> certList = new ArrayList<Certificate>();
        Enumeration<String> aliases = null;
        // Les certificats chez Apple, c'est super bizarre
        boolean weAreOnMac = Configurator.isMacOsX();

        try {
            // Les certificats chez Microsoft, c'est un peu bizarre.
            if (Configurator.isWindows()) {
                _fixAliases(ks);
            }
            aliases = ks.aliases();

            // DEBUG: sur OSX c'est pire.
            //            if (weAreOnMac) {
            //                while (aliases.hasMoreElements()) {
            //                    String toto = aliases.nextElement().toString();
            //                    System.out.println("Debug OSX: Liste des aliases: " + toto);
            //                }
            //                aliases = ks.aliases();
            //            }
        } catch (KeyStoreException ex) {
            System.out.println("## Sync-op: " + ex.getLocalizedMessage());
        }

        System.out.println("########################### init aliases");
        while (aliases.hasMoreElements()) {
            String alias = aliases.nextElement();
            if (alias == null) {
                /* should never ever happen */
                System.out.println("##  WTF??? Null alias in keystore??");
            } else {
                Certificate cert;
                try {
                    if (weAreOnMac) {

                        // Taken from http://lists.apple.com/archives/apple-cdsa/2008/Feb/msg00016.html
                        // At the moment, suppose the CN fragment of my Subject DN is
                        //  'firstname lastname', the alias name for my private key is
                        // going to be 'firstname lastname', and the one for the
                        // corresponding cert is going to be 'firstname lastname 1'.
                        // If there's no key, the alias name seems to be only the CN
                        // fragment (so a number is added only if there already is a
                        // key). In practice, this makes it very difficult to have a
                        // consistent configuration mechanism from a Java application,
                        // since it always depends on what other certificates may be
                        // available in the keychains.
                        // key:"alias", cert:"alias 1"

                        // check on OSX if cert has a priv-key available?
                        System.out.print("#OSX: [" + alias + "], ");
                        java.security.Key key = ks.getKey(alias, null);
                        if (key instanceof PrivateKey) {
                            System.out.println("Found PrivateKey key, ");

                        } else if (ks.isKeyEntry(alias)) {
                            System.out.print(" alias 'isKeyEntry' (not 'PrivateKey') ");

                            Certificate[] certChain = ks.getCertificateChain(alias);
                            cert = (X509Certificate) certChain[0];
                            boolean[] keyUsage = ((X509Certificate)cert).getKeyUsage();

                            if ((keyUsage == null) || keyUsage[0] || keyUsage[1]) {

                                // Analyse du certificat correspondant
                                X509Certificate xcert = (X509Certificate) cert;
                                if (certIsGoodForSignature(alias, xcert)) {
                                    // We show ONLY date-valid certificates
                                    xcert.checkValidity();
                                    System.out.println(" ALIAS RETENU: [" + alias + "] , serial= " + xcert.getSerialNumber().toString(16));
                                    certList.add(cert);
                                }

                            } else {
                                System.out.println("The  alias is not eligible to digital sig.");
                            }

                        } else if (ks.isCertificateEntry(alias)) {
                            System.out.println(" alias 'isCertificateEntry', forget it !!!");
                        } else {
                            System.out.println("The alias is not a key... AT ALL");
                        }


                        //                        if (alias.endsWith(" 1")) {
                        //                            String supposedKeyAlias = alias.substring(0, alias.length()-2);
                        //                            if (ks.containsAlias(supposedKeyAlias)
                        //                                    && ks.isKeyEntry(supposedKeyAlias)) {
                        //                                // Let's keep it... ??  FIXME: non c'est une cle privee
                        //                            } else {
                        //                                throw new KeyStoreException("#  OSX: doublon d'alias, mais pas de clef privee pour " + alias);
                        //                            }
                        //                        } else {
                        //                            // throw new KeyStoreException("OSX: pas de clef privee pour " + alias);
                        //                            if (ks.isCertificateEntry(alias)) {
                        //                                // SI token: alors pas d'alias qui se termine avec " 1".
                        //                                System.out.print("#OSX: Lonely certificate: " + alias + "  ");
                        //                                // DONC: on le garde. ???
                        //                            } else if (ks.isKeyEntry(alias)) {
                        //                                throw new KeyStoreException("#  OSX: " + alias + " reference une clef privee ??!!");
                        //                            }
                        //                        }



                    } else {
                        /***************************************************
                         * HERE IS MICROSOFT CLASSIC Certificate's pickup
                         ***************************************************/
                        //   if (ks.isCertificateEntry(alias)) {
                        cert = ks.getCertificate(alias);
                        if (cert instanceof java.security.cert.X509Certificate) {
                            X509Certificate xcert = (X509Certificate) cert;

                            if (certIsGoodForSignature(alias, xcert)) {
                                // We show ONLY date-valid certificates
                                xcert.checkValidity();
                                System.out.println("ALIAS RETENU: [" + alias + "] , serial= " + xcert.getSerialNumber().toString(16));
                                certList.add(cert);
                            }
                        }
                    }
                    //  } else {
                    //      System.out.println("alias '" + alias + "' is not a certificate.");
                    //  }
                } catch (KeyStoreException ex) {
                    // do nothing
                    System.out.println("Probleme de Keystore: " + ex.getLocalizedMessage());
                } catch (CertificateExpiredException e1) {
                    System.out.println("Certificat expiré détecté dans Keystore");
                } catch (CertificateNotYetValidException e2) {
                    System.out.println("Certificat pas encore valide détecté dans Keystore");
                } catch (NoSuchAlgorithmException ex) {
                    System.out.println("Algorithme cryptographique indisponible: " + ex.getLocalizedMessage());
                } catch (UnrecoverableKeyException ex) {
                    System.out.println("Recuperation de cle impossible: " + ex.getLocalizedMessage());
                }
            }
        }

        /***********************************************
         * Go Ahead with OSX Tokens         (l'experience montre que ce code n'est jamais execute, cause macTokenks==null )
         ***********************************************/
        try {
            if (weAreOnMac) {

                if (macTokenks != null) {

                    Enumeration<String> mcTokAliases = macTokenks.aliases();
                    while (mcTokAliases.hasMoreElements()) {
                        String alias = mcTokAliases.nextElement();
                        java.security.Key key = macTokenks.getKey(alias, null);
                        if (key instanceof PrivateKey) {
                            Certificate[] certChain = macTokenks.getCertificateChain(alias);
                            X509Certificate cert = (X509Certificate) certChain[0];
                            boolean[] keyUsage = cert.getKeyUsage();
                            if ((keyUsage == null) || keyUsage[0] || keyUsage[1]) { // check for digital signature or non-repudiation, but also accept if none set

                                System.out.println("The alias of the token keystore is: " + alias);
                                System.out.println("The token sign-key is:         " + key );
                                System.out.println("The token sign-certificate is: " + cert.toString());

                                certList.add(cert);
                                //signatureKey_ = (RSAPrivateKey) key;
                                //signatureKey_ = (PrivateKey) key;
                                //verificationKey_ = cert.getPublicKey();
                                break;
                            }



                        }
                    }
                    /*if (signatureKey_ == null) {
                        System.out.println("ERROR Found no signature key. Ensure that a valid card is inserted.");
                        // System.exit(0);
                    } */

                } else {
                    System.out.println("The macTokenks is null, meaning no HSM detected. STOP.");
                }

            }
        } catch (KeyStoreException ex) {
            // do nothing
            System.out.println("Probleme de Keystore: " + ex.getLocalizedMessage());
        } catch (NoSuchAlgorithmException ex) {
            System.out.println("Probleme d'algorithme indisponible: " + ex.getLocalizedMessage());
        } catch (UnrecoverableKeyException ex) {
            System.out.println("Probleme de clef inaccessible: " + ex.getLocalizedMessage());
        } catch (Exception ex) {
            System.out.println("Probleme de clef inaccessible: " + ex.getLocalizedMessage());
            ex.printStackTrace();

        }
        return certList;
    }
}

/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.parapheur.applets.splittedsign;

import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cms.*;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.cms.bc.BcRSASignerInfoVerifierBuilder;
import org.bouncycastle.operator.DefaultDigestAlgorithmIdentifierFinder;
import org.bouncycastle.operator.DefaultSignatureAlgorithmIdentifierFinder;
import org.bouncycastle.operator.bc.BcDigestCalculatorProvider;
import org.bouncycastle.util.Store;

/**
 * Verification de signature PKCS#7
 * 
 * @author a.sarr - Netheos
 * @author Stephane Vast - Adullact Projet
 */
public class PKCS7VerUtil {

    public PKCS7VerUtil() {
    }

    
    public static boolean validatePkcs7Signature(CMSSignedData signedData) throws Exception {
        Store certs = signedData.getCertificates();
        SignerInformationStore signers = signedData.getSignerInfos();
        Iterator it = signers.getSigners().iterator();
        if (it.hasNext()) {
            SignerInformation signer = (SignerInformation) it.next();
            X509CertificateHolder cert = (X509CertificateHolder) certs.getMatches(signer.getSID()).iterator().next();
            
            /* TODO :
             coder validateCertificate(certificate);
             les certificats considérés comme valides dépendront de l'aplication,
             cette méthode est donc à developper en conformité avec l'application*/
            if (!validateCertificate(cert)) {
                return false;
            }
            
            SignerInformationVerifier verifier = new BcRSASignerInfoVerifierBuilder(
                    new DefaultCMSSignatureAlgorithmNameGenerator(),
                    new DefaultSignatureAlgorithmIdentifierFinder(),
                    new DefaultDigestAlgorithmIdentifierFinder(),
                    new BcDigestCalculatorProvider()).build(cert);
            return signer.verify(verifier);
        }
        return false;
    }

    
    /**
     * Vérifie la signature contenue dans p7 signature
     * 
     *  NOTE : ce code ne procède pas à une validation du certificat avant de statuer.
     * 
     * @param p7signature signature au format p7
     * @param digestValue haché de la donnée à vérifier
     * @return true si ok, false sinon.
     * @throws java.security.SignatureException
     */
    public static boolean validatePkcs7Signature(byte[] p7signature, byte[] digestValue) throws SignatureException {
        try {
            CMSSignedData cmsSignature = new CMSSignedData(new CMSProcessableByteArray(digestValue), p7signature);
            
            // Porting effort to BC 1.47 onwards
            return validatePkcs7Signature(cmsSignature);
            
//            
//            // CertStore certStore = cmsSignature.getCertificatesAndCRLs("Collection", "BC");
//            CertStore certStore = new JcaCertStoreBuilder().setProvider("BC").addCertificates(cmsSignature.getCertificates()).build();
//            
//            SignerInformationStore signers = cmsSignature.getSignerInfos();
//            // Collection signersCollect = signers.getSigners();
//            Iterator it = signers.getSigners().iterator();
//            if (!it.hasNext()) {
//                throw new SignatureException("Invalid Signature!!!");
//            }
//            SignerInformation signer = (SignerInformation) it.next();
//            
//            // Porting effort to BC 1.47 onwards
//            //Collection certCollection = certStore.getCertificates(signer.getSID());
//            X509CertSelector signerConstraints = new JcaX509CertSelectorConverter().getCertSelector(signer.getSID());
//            signerConstraints.setKeyUsage(JcaUtils.getKeyUsage(KeyUsage.digitalSignature));
//            
//            PKIXCertPathBuilderResult result = JcaUils.   ;   // voir cwguide-070313.pdf page 29
//                    
//             // BcUtils
//
//
//            /* TODO :
//             coder validateCertificate(certificate);
//             les certificats considérés comme valides dépendront de l'aplication,
//             cette méthode est donc à developper en conformité avec l'application*/
//            X509Certificate cert = (X509Certificate) certCollection.iterator().next();
//            if (!validateCertificate(cert)) {
//                return false;
//            }
//            return signer.verify(cert, "BC");
            
        } catch (GeneralSecurityException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new SignatureException(ex.getMessage(), ex);
//        } catch (CertificateExpiredException ex) {
//            Logger.getLogger("global").log(Level.SEVERE, null, ex);
//            throw new SignatureException(ex.getMessage(), ex);
//        } catch (CertificateNotYetValidException ex) {
//            Logger.getLogger("global").log(Level.SEVERE, null, ex);
//            throw new SignatureException(ex.getMessage(), ex);
//        } catch (CertStoreException ex) {
//            Logger.getLogger("global").log(Level.SEVERE, null, ex);
//            throw new SignatureException(ex.getMessage(), ex);
//        } catch (NoSuchAlgorithmException ex) {
//            Logger.getLogger("global").log(Level.SEVERE, null, ex);
//            throw new SignatureException(ex.getMessage(), ex);
//        } catch (NoSuchProviderException ex) {
//            Logger.getLogger("global").log(Level.SEVERE, null, ex);
//            throw new SignatureException(ex.getMessage(), ex);
        } catch (CMSException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new SignatureException(ex.getMessage(), ex);
        } catch (Exception ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new SignatureException(ex.getMessage(), ex);
        }
    }

    /**
     * valide le certificat du signataire
     * @param cert certif à valider
     * @return true si ok, false sinon
     */
    public static boolean validateCertificate(X509Certificate cert) {
        /* TODO : implémenter ceci suivant votre système de validation, ,pki, ...
         */
        return true;
    }

    /**
     * valide le certificat du signataire
     * @param cert certif à valider
     * @return true si ok, false sinon
     */
    public static boolean validateCertificate(X509CertificateHolder cert) {
        /* TODO : implémenter ceci suivant votre système de validation, ,pki, ...
         */
        return true;
    }

    /**
     * valide la signature p7
     * NOTE : la fonction de hachage est supposée être sha1
     * 
     * @param documentInputStream flux d'entrée des données
     * @param p7signature signature à valider.
     * @return true si la signature est valide, false sinon
     * @throws SignatureException
     */
    public static boolean validatePkcs7Signature(byte[] p7signature, InputStream documentInputStream) throws SignatureException {
        try {

            MessageDigest messageDigest = MessageDigest.getInstance("sha1");
            DigestInputStream digestStream = new DigestInputStream(documentInputStream, messageDigest);
            byte[] buffer = new byte[1024];
            while (digestStream.read(buffer)!=-1) {
                // ce n'est pas une erreur...
                ;
            } 
            byte[] digestValue = messageDigest.digest();
            return validatePkcs7Signature(p7signature, digestValue);
        } catch (SignatureException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new SignatureException(ex.getMessage(), ex);
        } catch (IOException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new SignatureException(ex.getMessage(), ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new SignatureException(ex.getMessage(), ex);
        }
    }



    /*
     * Renvoie le certificat du signataire
     */
    public static X509Certificate getSignerCertificate(byte[] p7signature) throws SignatureException {
        try {
            org.bouncycastle.cms.CMSSignedData signature = new org.bouncycastle.cms.CMSSignedData(p7signature);
            org.bouncycastle.cms.SignerInformation signer = (org.bouncycastle.cms.SignerInformation) signature.getSignerInfos().getSigners().iterator().next();
            org.bouncycastle.util.Store certStore = signature.getCertificates();
            
            // java.security.cert.CertStore cs = signature.getCertificatesAndCRLs("Collection", "BC");
            X509CertificateHolder cert = (X509CertificateHolder) certStore.getMatches(signer.getSID()).iterator().next();
            JcaX509CertificateConverter converter = new JcaX509CertificateConverter();
            return converter.setProvider("BC").getCertificate(cert);
            
//            java.security.cert.CertStore cs = JcaCertStoreBuilder().;
//            org.bouncycastle.cert.jcajce.JcaCertStoreBuilder jcaCertStoreBuilder = new  org.bouncycastle.cert.jcajce.JcaCertStoreBuilder();
//            jcaCertStoreBuilder.setProvider("BC");
//            SignerInformation signer = (SignerInformation) it.next();
//            java.util.Iterator iter = cs.getCertificates(signer.getSID()).iterator();
//            return (java.security.cert.X509Certificate) iter.next();
        } catch (CertificateException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new SignatureException(ex.getMessage(), ex);
//        } catch (CertStoreException ex) {
//            Logger.getLogger("global").log(Level.SEVERE, null, ex);
//            throw new SignatureException(ex.getMessage(), ex);
//        } catch (NoSuchAlgorithmException ex) {
//            Logger.getLogger("global").log(Level.SEVERE, null, ex);
//            throw new SignatureException(ex.getMessage(), ex);
//        } catch (NoSuchProviderException ex) {
//            Logger.getLogger("global").log(Level.SEVERE, null, ex);
//            throw new SignatureException(ex.getMessage(), ex);
        } catch (CMSException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new SignatureException(ex.getMessage(), ex);
        }
    }

    public static int indexOf(byte[] mainArray, byte[] searchSequence) {
        return indexOf(mainArray, searchSequence, 0);
    }

    public static int indexOf(byte[] mainArray, byte[] searchSequence, int fromIndex) {
        byte v1[] = mainArray;
        byte v2[] = searchSequence;
        int max = mainArray.length;

        if (fromIndex >= max) {
            if (mainArray.length == 0 && fromIndex == 0 && searchSequence.length == 0) {
                return 0;
            }
            return -1;  // from index too large
        }

        if (fromIndex < 0) {
            fromIndex = 0;
        }
        if (searchSequence.length == 0) {
            return fromIndex;
        }

        byte first = v2[0];
        int i = fromIndex;

        startSearchForFirstChar:
        while (true) {
            /* Look for first character. */
            while (i < max && v1[i] != first) {
                i++;
            }

            if (i >= max) // didn't find the sequence: return -1
            {
                return -1;
            }

            /* Found first character, now look at the rest of v2 */
            int j = i + 1;
            int end = j + searchSequence.length - 1;
            int k = 1;
            while (j < end) {
                if (v1[j++] != v2[k++]) {
                    i++;
                    /* Look for str's first char again. */
                    continue startSearchForFirstChar;
                }
            }
            return i;    // Found whole string!!!
        }
    }


    public static int indexOf(byte[] mainArray, byte searchByte, int fromIndex) {
        int len = mainArray.length;

        if (fromIndex < 0) {
            fromIndex = 0;
        } else if (fromIndex >= len) {
            return -1;
        }

        for (int i = fromIndex; i < len; i++) {
            if (mainArray[i] == searchByte) {
                return i;
            }
        }

        return -1;                     // did not find anything...
    }

    public static byte[] decode(String s) {
        return decode(s.toCharArray());
    }

    public static byte[] decode(char[] in) {
        int iLen = in.length;
        if (iLen % 4 != 0) {
            throw new IllegalArgumentException("Length of Base64 encoded input string is not a multiple of 4.");
        }
        while (iLen > 0 && in[iLen - 1] == '=') {
            iLen--;
        }
        int oLen = (iLen * 3) / 4;
        byte[] out = new byte[oLen];
        int ip = 0;
        int op = 0;
        while (ip < iLen) {
            int i0 = in[ip++];
            int i1 = in[ip++];
            int i2 = ip < iLen ? in[ip++] : 'A';
            int i3 = ip < iLen ? in[ip++] : 'A';
            if (i0 > 127 || i1 > 127 || i2 > 127 || i3 > 127) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            int b0 = map2[i0];
            int b1 = map2[i1];
            int b2 = map2[i2];
            int b3 = map2[i3];
            if (b0 < 0 || b1 < 0 || b2 < 0 || b3 < 0) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            int o0 = (b0 << 2) | (b1 >>> 4);
            int o1 = ((b1 & 0xf) << 4) | (b2 >>> 2);
            int o2 = ((b2 & 3) << 6) | b3;
            out[op++] = (byte) o0;
            if (op < oLen) {
                out[op++] = (byte) o1;
            }
            if (op < oLen) {
                out[op++] = (byte) o2;
            }
        }
        return out;
    }


    public static byte[] pem2der(byte[] pem, byte[] header, byte[] footer) throws IOException {
        int start, end;
        start = indexOf(pem, header);
        end = indexOf(pem, footer);
        if (start == -1 || end == -1) {
            return null;
        }  //  no headers!
        start = indexOf(pem, (byte) '\n', start) + 1;

        // skip past any more text, by avoiding all lines less than 64 characters long...
        int next;
        while ((next = indexOf(pem, (byte) '\n', start)) < start + 60) {
            if (next == -1) // really shouldn't ever happen...
            {
                break;
            }
            start = next + 1;
        }

        if (start == -1) // something wrong - no end of line after '-----BEGIN...'
        {
            return null;
        }
        int len = end - start;
        byte[] data = new byte[len];

        System.arraycopy(pem, start, data, 0, len); // remove the PEM fluff from tbe base 64 data, stick in 'data'
        //System.out.println(data.toString());
        return Base64.decode(data);
    }

    private static final char[] map1 = new char[64];

    static {
        int i = 0;
        for (char c = 'A'; c <= 'Z'; c++) {
            map1[i++] = c;
        }
        for (char c = 'a'; c <= 'z'; c++) {
            map1[i++] = c;
        }
        for (char c = '0'; c <= '9'; c++) {
            map1[i++] = c;
        }
        map1[i++] = '+';
        map1[i++] = '/';
    }

    private static final byte[] map2 = new byte[128];

    static {
        for (int i = 0; i < map2.length; i++) {
            map2[i] = -1;
        }
        for (int i = 0; i < 64; i++) {
            map2[map1[i]] = (byte) i;
        }
    }

}

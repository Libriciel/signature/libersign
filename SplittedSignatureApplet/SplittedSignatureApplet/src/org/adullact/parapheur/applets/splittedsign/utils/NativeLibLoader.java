/** 
 * This file is part of LIBERSIGN.
 * 
 * Copyright (c) 2008-2014, ADULLACT-Projet
 *  Initiated by ADULLACT-Projet S.A.
 *  Developped by ADULLACT-Projet S.A.
 *
 *  contact@adullact-projet.coop
 * 
 * LIBERSIGN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LIBERSIGN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with LIBERSIGN.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.adullact.parapheur.applets.splittedsign.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * From project j4ops (code.google.com) GPLv3
 * 
 * @author fzanutto 
 * @author svast
 */
public class NativeLibLoader {
    private static final Logger logger = Logger.getLogger("NativeLibLoader");
    public static final int OS_UNSUPPORTED = -1;
    public static final int OS_LINUX = 1;
    public static final int OS_WINDOWS = 2;
    public static final int OS_WINDOWS_CE = 3;
    public static final int OS_MAC_OS_X = 4;
    private static HashSet lstLibraryLoaded = new HashSet ();
       
    public static int getOS() {
        int os;// = 0;
        String sysName = System.getProperty("os.name");
        if (sysName == null) {
            logger.log(Level.SEVERE, "Native Library not available on unknown platform");
            os = OS_UNSUPPORTED;
        } else {
            sysName = sysName.toLowerCase();
            if (sysName.contains("windows")) {
                if (sysName.contains("ce")) {
                    os = OS_WINDOWS_CE;
                } else {
                    os = OS_WINDOWS;
                }
            } else if (sysName.contains("mac os x")) {
                os = OS_MAC_OS_X;
            } else if (sysName.contains("linux")) {
                os = OS_LINUX;
            } else {
                logger.log(Level.SEVERE, "Native Library not available on platform {0}", sysName);
                os = OS_UNSUPPORTED;
            }
        }
        return os;
    }

    private static boolean copy2File(InputStream is, File fd) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(fd);
            byte b[] = new byte[1000];
            int len;
            while ((len = is.read(b)) >= 0) {
                fos.write(b, 0, len);
            }
            return true;
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error on create temp file " + fd.getAbsolutePath(), e);
            return false;
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException ignore) {
                    fos = null;
                }
            }
        }
    }

    public static void setLibraryPath(String path) throws Exception {
        System.setProperty("java.library.path", path);

        //set sys_paths to null
        final Field sysPathsField = ClassLoader.class.getDeclaredField("sys_paths");
        sysPathsField.setAccessible(true);
        sysPathsField.set(null, null);
    }

    public static void addLibraryPath(String pathToAdd) throws Exception {
        final Field usrPathsField = ClassLoader.class.getDeclaredField("usr_paths");
        usrPathsField.setAccessible(true);

        //get array of paths
        final String[] paths = (String[]) usrPathsField.get(null);

        //check if the path to add is already present
        for (String path : paths) {
            if (path.equals(pathToAdd)) {
                return;
            }
        }

        //add the new path
        final String[] newPaths = Arrays.copyOf(paths, paths.length + 1);
        newPaths[newPaths.length - 1] = pathToAdd;
        usrPathsField.set(null, newPaths);
    }

    public static File extractLib(String path, final String name) throws Exception {
        InputStream in = null;
        String fileName = name;
        File fileOut = null;

        try {
            String sysName = System.getProperty("os.name");
            String sysArch = System.getProperty("os.arch");
            if (sysArch != null) {
                sysArch = sysArch.toLowerCase();
            } else {
                sysArch = "";
            }

            switch (getOS()) {
                case OS_WINDOWS_CE:
                    fileName += "_ce.dll";
                    break;
                case OS_WINDOWS:
                    if ((sysArch.contains("amd64")) || (sysArch.contains("x86_64"))) {
                        fileName += "_x64";
                    }
                    fileName += ".dll";
                    break;
                case OS_MAC_OS_X:
                    fileName += ".jnilib";
                    break;
                case OS_LINUX:
                    if ((sysArch.contains("i386")) || (sysArch.length() == 0)) {
                        // regular Intel
                    } else if ((sysArch.contains("amd64")) || (sysArch.contains("x86_64"))) {
                        fileName += "_x64";
                    } else if ((sysArch.contains("x86"))) {
                        // regular Intel under IBM J9
                    } else {
                        // Any other system
                        fileName += "_" + sysArch;
                    }
                    fileName += ".so";
                    break;

                case OS_UNSUPPORTED:
                    throw new Exception("Native Library " + name + " not available on [" + sysName + "] platform");

                default:
                    throw new Exception("Native Library " + name + " not available on platform " + sysName);
            }

            in = NativeLibLoader.class.getResourceAsStream(fileName);
            if (in == null) {
                throw new Exception("Resource " + fileName + " not found");
            }

            fileOut = new File(System.getProperty("java.io.tmpdir") + System.getProperty("file.separator") + path + fileName);
            if (fileOut.exists()) {
                fileOut.delete();
            }

            logger.log(Level.INFO, "write lib into: {0}", fileOut.getAbsolutePath());
            copy2File(in, fileOut);

        } catch (Exception e) {
            logger.log(Level.SEVERE, e.toString(), e);
            throw new Exception("Error on loading library " + name, e);
        } finally {
            try {
                if (in != null) {
                    in.close();
                    in = null;
                }
            } catch (Exception ex) {
            }
        }

        return fileOut;
    }

    public static void loadLib(String path, final String name) throws Exception {

        try {
            if (lstLibraryLoaded.contains(name)) {
                return;
            }

            File fileOut = extractLib(path, name);
            System.load(fileOut.toString());

            logger.log(Level.INFO, "Library {0} loaded successful", fileOut.getAbsolutePath());

            lstLibraryLoaded.add(name);
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.toString(), e);
            throw new Exception("Error on loading library " + name, e);
        }

    }

}

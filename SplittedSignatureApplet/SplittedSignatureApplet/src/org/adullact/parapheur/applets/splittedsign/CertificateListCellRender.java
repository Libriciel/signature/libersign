/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.parapheur.applets.splittedsign;

import java.awt.Component;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.HashMap;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * Customized renderer for the list of certificates to be chosen
 * 
 * @author Xavier Facelina (Nethoes), Stephane Vast (Adullact Projet)
 */
public class CertificateListCellRender 
        extends JLabel implements ListCellRenderer {
    
    /**
     *
     */
    private static final long serialVersionUID = 6550125935944557463L;

    /**
     * Creates a new instance of CertificateListCellRender
     */
    private CertificateListCellRender() {
        // do nothing
    }

    public static CertificateListCellRender getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new CertificateListCellRender();
        }
        return uniqueInstance;
    }

    public static void reset() {
        uniqueInstance = null;
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, 
            int index, boolean isSelected, boolean cellHasFocus) {
        JLabel label = new JLabel();
        setText(makeCertificateText(value));
        setOpaque(true);
        setBackground(isSelected
                ? list.getSelectionBackground()
                : list.getBackground());
        setForeground(isSelected
                ? list.getSelectionForeground()
                : list.getForeground());
        return this;
    }

    /**
     * Formatage des éléments de la liste de certificats aptes à la signature:
     * Depuis Subject, on prend: <B>CN</B> <I>EMAILADDRESS</I>, T, O,  (si présents)
     *    (expire le "getNotAfter")
     * Depuis Issuer, on prend :   , émis par CN
     * 
     * @param certObject le certificat à analyser
     * @return le code HTML pour la ListCellRenderer
     */
    private static String makeCertificateText(Object certObject) {
        X509Certificate cert = (X509Certificate) certObject;
        HashMap subjectInfos = CertificateInfosExtractor.makeSubjectInfos(cert);
        HashMap issuerInfos = CertificateInfosExtractor.makeIssuerInfos(cert);
        String text = "<html>&nbsp;<b>" + (String) subjectInfos.get("CN") + "</b>";
        if (subjectInfos.get("EMAILADDRESS") != null) {
            String emailString = (String) subjectInfos.get("EMAILADDRESS");
            if (!emailString.trim().isEmpty()) {
                text += " <i>&lt;" + emailString.trim() + "&gt;</i>";
            }
        }
        if (subjectInfos.get("T") != null) {
            String titleString = (String) subjectInfos.get("T");
            if (!titleString.trim().isEmpty()) {
                text += ", " + titleString.trim();
                if (subjectInfos.get("O") != null) {
                    String orgString = (String) subjectInfos.get("O");
                    if (!orgString.trim().isEmpty()) {
                        text += " " + orgString.trim();
                    }
                }
            }
        }
        Calendar cal = Calendar.getInstance(); cal.setTime(cert.getNotAfter());
        text += "&nbsp;&nbsp;(expire le "+cal.get(Calendar.DAY_OF_MONTH)+"/"+(cal.get(Calendar.MONTH)+1)+"/"+cal.get(Calendar.YEAR)+")";
        // text += ",&nbsp;&nbsp;<span style=\"color:#808080\">émis par " + (String)issuerInfos.get("CN") + "</span>";// + "</font>";
        text += ",&nbsp;&nbsp;émis par " + (String)issuerInfos.get("CN");// + "</font>";
        text += "</html>";
        return text;
    }

    private static CertificateListCellRender uniqueInstance = null;
}

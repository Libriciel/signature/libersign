/** 
 * This file is part of LIBERSIGN.
 * 
 * Copyright (c) 2008-2015, ADULLACT-Projet
 *  Initiated by ADULLACT-Projet S.A.
 *  Developped by ADULLACT-Projet S.A.
 *
 *  contact@adullact-projet.coop
 * 
 * LIBERSIGN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LIBERSIGN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with LIBERSIGN.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.adullact.parapheur.applets.splittedsign.providers;

import iaik.pkcs.pkcs11.*;
import iaik.pkcs.pkcs11.objects.PrivateKey;
import iaik.pkcs.pkcs11.objects.RSAPrivateKey;
import iaik.pkcs.pkcs11.provider.IAIKPkcs11;
import static iaik.pkcs.pkcs11.wrapper.PKCS11Constants.*;
import java.io.File;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.adullact.parapheur.applets.splittedsign.utils.HexString;
import org.adullact.parapheur.applets.splittedsign.utils.IaikUtil;
import org.adullact.parapheur.applets.splittedsign.utils.NativeLibLoader;


/**
 *From project j4ops (code.google.com) GPLv3
 * 
 * @author fzanutto 
 * @author svast
 */
public class IaikPkcs11 extends Pkcs11Provider {
    private static final Logger logger = Logger.getLogger("IaikPkcs11");
    private KeyIDAndX509Cert selectKeyIDAndCertificate;    

    private Mechanism mechanismSignAlgId = null;
    private Module pkcs11Module = null;
    private Session session = null;
    private boolean flagInit = false;
    private static boolean isModuleInitialized = false;
   
        static {        
        try {            
            int os = NativeLibLoader.getOS();
            String library;
            if (os == NativeLibLoader.OS_WINDOWS ||
                os == NativeLibLoader.OS_WINDOWS_CE) {
                library = "/pkcs11wrapper";
            }
            else {                
                library = "/libpkcs11wrapper";
            }
            File file = NativeLibLoader.extractLib("", library);
            if (file != null) {
                String absolutePath = file.getAbsolutePath();
                String filePath = absolutePath.substring(0,absolutePath.lastIndexOf(File.separator));                                
                NativeLibLoader.addLibraryPath(filePath);
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
        }        
   
    public IaikPkcs11(String tokensConfig) {
        super(tokensConfig);        
    }  
   
    @Override
    public void init (String digestAlgName, String encryptionAlgName, SignProviderHandler handlerProvider, String securityProvider) throws Exception {  
        long algId = CKM_SHA256_RSA_PKCS;
       
        // init token
        super.init(digestAlgName, encryptionAlgName, handlerProvider, securityProvider);
       
        // get algorithm
        if ("RSA".equals(encryptionAlgName)) {
            if ("SHA1".equals(digestAlgName)) {
                algId = CKM_SHA1_RSA_PKCS;
            }
            else if ("SHA256".equals(digestAlgName)) {
                algId = CKM_SHA256_RSA_PKCS;
            }
            else if ("SHA384".equals(digestAlgName)) {
                algId = CKM_SHA384_RSA_PKCS;
            }    
            else if ("SHA512".equals(digestAlgName)) {
                algId = CKM_SHA512_RSA_PKCS;
            }        
            else {
                algId = CKM_RSA_PKCS;
            }
        }
        else if ("DSA".equals(encryptionAlgName)) {
            if ("SHA1".equals(digestAlgName)) {
                algId = CKM_DSA_SHA1;
            }                    
        }
        logger.info(String.format("algId %d", algId));
        mechanismSignAlgId = Mechanism.get(algId);
        
        org.adullact.parapheur.applets.splittedsign.token.TokenInformation ti = getTokenInfo();
        String driverPath=ti.getDriver();
        // Dans EL Capitan, les installer ne peuvent plus ecrire dans /usr/lib,
        // par consequent les drivers se trouvent maintenant dans /usr/local/lib
        File driverFile = new File(driverPath);
        if (!driverFile.exists()){
            System.out.println("1- driverPath=" + driverPath);
            Map<String,String> searchs = new HashMap<String, String>();
            searchs.put("/usr/lib", "/usr/local/lib");
            searchs.put("/usr/lib/pkcs11", "/usr/local/lib");
            for (Map.Entry<String,String> search : searchs.entrySet()) {
                String newDriverPath=driverPath.replace(search.getKey(), search.getValue());
                System.out.println("2- recherche driverPath=" + newDriverPath);
                if (new File(newDriverPath).exists()){
                    System.out.println("3- Trouve new driverPath=" + newDriverPath);
                    driverPath = newDriverPath;
                    ti.setDriver(driverPath);
                    break;
                }
            }
        }
        Properties properties = new Properties();
        System.out.println("4-driverPath=" + driverPath);
        properties.put("PKCS11_NATIVE_MODULE", driverPath);
//        properties.put("PKCS11_NATIVE_MODULE", getTokenInfo().getDriver());

        // get module
        if (!isModuleInitialized) {
            pkcs11Module = IAIKPkcs11.getModule(properties);
        }
        // select token
        Token token; // = null;
        if (getTokenInfo().getSlotID() >= 0) {
            token = IaikUtil.selectToken(pkcs11Module, getTokenInfo().getSlotID());
        } else {
            token = IaikUtil.selectToken(pkcs11Module);
        }
        if (token == null) {
            throw new Exception("No token to proceed.");
        }
        
        properties.put("SLOT_ID", Long.toString(token.getSlot().getSlotID()));
        IAIKPkcs11 provider = new IAIKPkcs11(properties);
        Security.addProvider(provider);
        
   //     logger.info (String.format("Token ID %s Info %s", token.getTokenID(), token.getTokenInfo()));

        // select mechanism
        List supportedMechanisms = Arrays.asList(token.getMechanismList());
        if (!supportedMechanisms.contains(mechanismSignAlgId)) {
            throw new Exception(String.format("This token does not support raw %s signing!", mechanismSignAlgId));
        } else {
            MechanismInfo rsaMechanismInfo = token.getMechanismInfo(mechanismSignAlgId);
            if (!rsaMechanismInfo.isSign()) {
                throw new Exception(String.format("This token does not support %s signing according to PKCS!", mechanismSignAlgId));
            }
        }
        // provider is initialized
        flagInit = true;
        
        //ADD stv
        //session.closeSession();
        //session = null;
    }

    // stv added
    private Token p_token;
    public Token getToken() {
        return p_token;
    }
    public KeyIDAndX509Cert getKeyIDAndCertificate() {
        return selectKeyIDAndCertificate;
    }
   
    @Override
    public void destroy () throws Exception {
        if (session != null) {
            session.closeSession();
            session = null;
        }
        if (pkcs11Module != null) {
            pkcs11Module.finalize(null);        
        }        
        flagInit = false;
        isModuleInitialized = false;
    }    
   
    @Override
    public byte [] sign(byte[] toEncrypt) throws Exception {
       
        // check if init provider
        if (!flagInit) {
            throw new Exception ("Provider not initialized");
        }        
        logger.log(Level.INFO, "KeyID:{0}", HexString.hexify(selectKeyIDAndCertificate.getKeyID()));        
       
        // get private key
        RSAPrivateKey keyTemplate = new RSAPrivateKey();
        keyTemplate.getSign().setBooleanValue(Boolean.TRUE);        
        PrivateKey selectedSignatureKey = IaikUtil.getPrivateKey(session, keyTemplate, selectKeyIDAndCertificate.getKeyID());
        if (selectedSignatureKey == null) {
            throw new Exception ("No key retrieve");
        }
       
        // initialize for signing
        session.signInit(mechanismSignAlgId, selectedSignatureKey);        

        logger.info(String.format("data to sign(length:%d):%s", toEncrypt.length, HexString.hexify(toEncrypt)));

        // sign the data to be signed        
        byte [] signature = session.sign(toEncrypt);
       
        logger.info(String.format("data signed(length:%d):%s", signature.length, HexString.hexify(signature)));    
       
        return signature;
    }
   
    @Override
    public X509Certificate getX509Certificate () {
        assert null != selectKeyIDAndCertificate;  
        return selectKeyIDAndCertificate.getX509Cert();
    }
   
    @Override    
    public String getCertLabel () {
        assert null != selectKeyIDAndCertificate;
        return selectKeyIDAndCertificate.getCertLabel();        
    }      
   
    @Override
    public String toString () {
        return "IaikPKCS11";
    }
}

/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.parapheur.applets.splittedsign;

import javax.swing.*;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

/**
 * Cette classe est un modèle de List destiné à afficher les certificats de
 * l'utilisateur.
 * Elle étend DefaultListModel en en faisant un SINGLETON. Par ailleurs, elle
 * comprend une méthode sync() qui lui permet de mettre à jour le model en
 * fonction des certificats présents dans le Keystore. Ainsi la liste peut être
 * mise à jour.
 *
 * @author Xavier FACELINA - Netheos
 * @author Stephane VAST - Adullact Projet
 */
public class UserCertificatesListModel extends DefaultListModel {

    CertListUtil certList;

    /** Constructeur privé [SINGLETON] */
    private UserCertificatesListModel() {
        certList = new CertListUtil();
        for(Certificate cer: certList.getAvailableCertificates()) {
            this.addElement(cer);
        }
    }

    public PrivateKey getKey(X509Certificate cert) {
        return certList.getKey(cert);
    }

    public static UserCertificatesListModel getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new UserCertificatesListModel();
        }
        return uniqueInstance;
    }

    public static void reset() {
        uniqueInstance = null;
    }


    private static UserCertificatesListModel uniqueInstance = null;
}

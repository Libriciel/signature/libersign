/** 
 * This file is part of LIBERSIGN.
 * 
 * Copyright (c) 2008-2014, ADULLACT-Projet
 *  Initiated by ADULLACT-Projet S.A.
 *  Developped by ADULLACT-Projet S.A.
 *
 *  contact@adullact-projet.coop
 * 
 * LIBERSIGN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LIBERSIGN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with LIBERSIGN.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.adullact.parapheur.applets.splittedsign.providers;

import java.util.List;
import org.adullact.parapheur.applets.splittedsign.token.TokenInformation;
import org.adullact.parapheur.applets.splittedsign.token.TokenReader;

/**
 * From project j4ops (code.google.com) GPLv3
 * 
 * @author fzanutto 
 * @author svast
 */
public abstract class Pkcs11Provider implements SignProvider {
    private TokenInformation tokenInfo;
    private String tokensConfig;

    public Pkcs11Provider(String tokensConfig) {
        this.tokensConfig = tokensConfig;
    }

    public void setTokensConfig(String tokensConfig) {
        this.tokensConfig = tokensConfig;
    }

    @Override
    public void init(String digestAlgName, String encryptionAlgName, SignProviderHandler handlerProvider, String securityProvider) throws Exception {
        List<TokenInformation> lstTokens = TokenReader.listTokens(tokensConfig);
        if (lstTokens.isEmpty()) {
            throw new Exception("No token found in da usb-slot");
        }
        tokenInfo = handlerProvider.selectToken(lstTokens);
        if (tokenInfo == null) {
            throw new Exception("Token not selected");
        }
    }

    public TokenInformation getTokenInfo() {
        return tokenInfo;
    }

}

/** 
 * This file is part of LIBERSIGN.
 * 
 * Copyright (c) 2008-2014, ADULLACT-Projet
 *  Initiated by ADULLACT-Projet S.A.
 *  Developped by ADULLACT-Projet S.A.
 *
 *  contact@adullact-projet.coop
 * 
 * LIBERSIGN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LIBERSIGN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with LIBERSIGN.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.adullact.parapheur.applets.splittedsign.token;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.smartcardio.ATR;
import javax.smartcardio.Card;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.TerminalFactory;
import org.adullact.parapheur.applets.splittedsign.utils.HexString;

/**
 * From project j4ops (code.google.com) GPLv3
 * 
 * @author fzanutto 
 * @author svast
 */
public class TokenReader {
    private static final Logger logger = Logger.getLogger("TokenReader");

    public static List<TokenInformation> listTokens(String file) throws CardException {//throws Exception {
        System.out.println("listTokens(" + file + ")");
        
        ArrayList<TokenInformation> lstTokenInfos = new ArrayList<TokenInformation>();
        List<CardTerminal> lstTerminals = null;
        Card card; //= null;

        TerminalFactory factory = TerminalFactory.getDefault();
        lstTerminals = factory.terminals().list();
        System.out.println("## listTokens(" + file + "): Found "+ lstTerminals.size() +" Terminal" + ((lstTerminals.size()>1) ? "(s)." : "."));

        if (lstTerminals.isEmpty()) {
            throw new CardException("No terminal detected");
        } else {
            for (int index = 0; index < lstTerminals.size(); index++) {
                CardTerminal terminal = lstTerminals.get(index);
                logger.info(String.format("Terminal:\"%s\" check card", terminal.getName()));

                if (true || terminal.isCardPresent()) {
                    try {
                        card = terminal.connect("*");
                        System.out.println("## Card: " + card);

                        logger.info(String.format("SlotId: %d Terminal: %s ATR: %s", index, terminal.getName(),
                                HexString.hexify(card.getATR().getBytes())));

                        TokenInformation tokenInfo = new TokenInformation();
                        tokenInfo.setAtr(HexString.hexify(card.getATR().getBytes()));
                        tokenInfo.setSlotID(index);
                        tokenInfo.setTerminalName(terminal.getName());

                        // recognize card
                        System.out.println("  ## do recognize with " + file + ", " + tokenInfo.getTerminalName());
                        if (TokenRecognize.recognize(file, tokenInfo) != null) {
                            lstTokenInfos.add(tokenInfo);
                        }

                        card.disconnect(true);
                    } catch (Exception ex) {
                        Logger.getLogger(TokenReader.class.getName()).log(Level.SEVERE, null, ex);
                        throw new CardException(ex.getMessage());
                    }
                } else {
                    logger.info("CardFactory says no Card Present... :-(, try to play with ATR ?");
                    card = terminal.connect("T=0");
                    ATR atr = card.getATR();
                    
                    
                    card.disconnect(false);
                }
            }

        }
        return lstTokenInfos;
    }

    public static void main(String[] args) throws Exception {
        listTokens("tokens.xml");
    }

}

/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.parapheur.applets.splittedsign;

/**
 * Gestion de Digest
 * Created by lhameury on 27/01/16.
 */
public class Digest {
    
    String digests;
    String formatSignature;
    String p7s;
    String pesIds;
    String pespolicyid;
    String pespolicydesc;
    String pespolicyhash;
    String pesspuri;
    String pescity;
    String pespostalcode;
    String pescountryname;
    String pesclaimedrole;
    String encodingMap;


    public Digest(String digests, String formatSignature, String p7s, String pesIds, String pespolicyid,
                  String pespolicydesc, String pespolicyhash, String pesspuri, String pescity, String pespostalcode,
                  String pescountryname, String pesclaimedrole, String encodingMap) {
        this.digests = digests;
        this.formatSignature = formatSignature;
        this.p7s = p7s;
        this.pesIds = pesIds;
        this.pespolicyid = pespolicyid;
        this.pespolicydesc = pespolicydesc;
        this.pespolicyhash = pespolicyhash;
        this.pesspuri = pesspuri;
        this.pescity = pescity;
        this.pespostalcode = pespostalcode;
        this.pescountryname = pescountryname;
        this.pesclaimedrole = pesclaimedrole;
        this.encodingMap = encodingMap;
    }
}

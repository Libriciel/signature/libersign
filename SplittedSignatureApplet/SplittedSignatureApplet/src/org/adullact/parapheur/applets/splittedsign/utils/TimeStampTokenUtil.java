/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.adullact.parapheur.applets.splittedsign.utils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.cert.CertPathValidatorException;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.PKIXCertPathChecker;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.ByteArrayRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaCertStoreBuilder;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.SignerId;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.tsp.*;
import org.bouncycastle.util.Store;
import org.bouncycastle.util.encoders.Base64;

/**
 *
 * @author svast
 */
public class TimeStampTokenUtil {
    private static final Logger logger = Logger.getLogger("TimeStampTokenUtil");

    public static X509Certificate validateTimeStampToken (TimeStampToken timeStampToken, Set<X509Certificate> trustedCerts, final String securityProvider) throws Exception {

        SignerId signerId = timeStampToken.getSID();
        BigInteger certSerialNumber = signerId.getSerialNumber();
        //CertStore cs = timeStampToken.getCertificatesAndCRLs("Collection", securityProvider);
        Store store = timeStampToken.getCertificates();
        
        //CertStore cs = new JcaCertStore(); //timeStampToken.getCertificates();
        JcaCertStoreBuilder certStoreBuilder = new JcaCertStoreBuilder();
        CertStore cs = certStoreBuilder.addCertificates(store).build();
        Collection certs = cs.getCertificates(null);

        Iterator<X509Certificate> iter = certs.iterator();
        X509Certificate x509Certificate = null;
        while (iter.hasNext()) {
            X509Certificate x509Cert = iter.next();
            if (certSerialNumber != null) {
                if (x509Cert.getSerialNumber().equals(certSerialNumber)) {
                    logger.log(Level.INFO, "using certificate with serial: {0}", x509Cert.getSerialNumber());
                    x509Certificate = x509Cert;
                }
            } else {
                if (x509Certificate == null) {
                    x509Certificate = x509Cert;
                }
            }
            logger.log(Level.INFO, "Certificate subject dn {0}", x509Cert.getSubjectDN());
            logger.log(Level.INFO, "Certificate serial {0}", x509Cert.getSerialNumber());
        }
        if (x509Certificate == null) {
            throw new Exception("certificate not found");
        }

        logger.log(Level.INFO, "validateCertificate:{0}", x509Certificate.getSubjectDN());
        X509CertificateHolder x509CertificateHolder = new X509CertificateHolder(x509Certificate.getEncoded());
        TSPUtil.validateCertificate(x509CertificateHolder);
        
        logger.log(Level.INFO, "checkValidity:{0}", x509Certificate.getSubjectDN());
        x509Certificate.checkValidity();
        
        // checking for ExtendedKeyUsage only for TSA ceretificates
        PKIXCertPathChecker pkixCertPathChecker = new PKIXCertPathChecker() {

            @Override
            public void init(boolean forward) throws CertPathValidatorException {
            }

            @Override
            public boolean isForwardCheckingSupported() {
                return true;
            }

            @Override
            public Set<String> getSupportedExtensions() {
                return Collections.EMPTY_SET;
            }

            @Override
            public void check(Certificate cert, Collection<String> unresolvedCritExts) throws CertPathValidatorException {
                try {
                    X509Certificate x509Cert = X509Util.toX509Certificate(cert.getEncoded(), securityProvider);
                    if (x509Cert.getExtendedKeyUsage() != null) {
                        List<String> lstExtendedKeyUsage = x509Cert.getExtendedKeyUsage();
                        if (lstExtendedKeyUsage.size() == 1 && lstExtendedKeyUsage.contains(KeyPurposeId.id_kp_timeStamping.getId())) {
                            if( unresolvedCritExts.contains (Extension.extendedKeyUsage.getId())){ 
                                unresolvedCritExts.remove(Extension.extendedKeyUsage.getId()); 
                            } 
                        }
                    }
                }
                catch (Exception ex) {
                    logger.warning(ex.toString());
                    throw new CertPathValidatorException (ex.toString(), ex);
                }
            }        
        };       
        
        logger.log(Level.INFO, "validateChain:{0}", x509Certificate.getSubjectDN());
        X509Util.validateChain (x509Certificate, trustedCerts, pkixCertPathChecker, securityProvider);

        return x509Certificate;  
    }
    
    
    
    public static TimeStampToken getTimeStampToken(URL url, String user, String password, 
                                                   byte[] fingerPrint, ASN1ObjectIdentifier digestAlgOID, BigInteger nonce, String securityProvider) throws 
                                                   IOException, TSPException, NoSuchAlgorithmException, 
                                                   NoSuchProviderException, CMSException, CertStoreException,
                                                   CertificateExpiredException, CertificateNotYetValidException {

        logger.info(String.format("getTimeStampToken (%s, %s, %s, %s, %s, %d, %s)",
                url.toString(), user, password, 
                HexString.hexify(fingerPrint), digestAlgOID, nonce, securityProvider));
        logger.info(String.format("hash:%s len:%d", HexString.hexify(fingerPrint), fingerPrint.length));
        
        PostMethod method = new PostMethod (url.toString());
        TimeStampRequestGenerator reqGen = new TimeStampRequestGenerator();
        
        //request TSA to return certificate
        reqGen.setCertReq(true);

        //make a TSP request this is a dummy sha1 hash (20 zero bytes) and nonce=100
        TimeStampRequest request = reqGen.generate(digestAlgOID, fingerPrint, nonce);
        byte[] encReq = request.getEncoded();

        method.setRequestEntity (new ByteArrayRequestEntity(encReq));
        method.setRequestHeader("Content-type", "application/timestamp-query");
        if ((user != null && !"".equals(user)) &&
            (password != null && !"".equals(password))) {
            String userPassword = user + ":" + password;
            String basicAuth = "Basic " + new String(Base64.encode(userPassword.getBytes()));
            method.setRequestHeader("Authorization", basicAuth);            
            logger.log(Level.INFO, "add basic authorization:{0}", basicAuth);
        }

        HttpClient httpClient = new HttpClient();
        httpClient.executeMethod(method);
        InputStream in = method.getResponseBodyAsStream();

        //read TSP response
        TimeStampResponse resp = new TimeStampResponse(in);     
        if (resp.getStatus() != 0) {            
            throw new TSPException (String.format("response error status %d - %s", resp.getStatus(), resp.getStatusString()));
        }
        resp.validate(request);                   
        logger.info("TimestampResponse validated");

        TimeStampToken timeStampToken = resp.getTimeStampToken();

        logger.log(Level.INFO, "TimeStampToken: {0}", HexString.hexify(timeStampToken.getEncoded()));

        return timeStampToken;
    }
    
    public static void main (String []args) throws Exception {
        URL url = new URL("http://tsa.swisssign.net");
        
        //URL url = new URL("http://timestamping.edelweb.fr/service/tsp");
        //URL url = new URL("http://tss.pki.gva.es:8318/tsa");
        //byte []hash = HexString.parseHexString("A6E4E9F5BBF46B694736A105C972D203E21FDEA9");//new byte [20];
        byte []hash = HexString.parseHexString("A6E4E9F5BBF46B694736A105C972D203E21FDEA926ABED9DF7A7AA5C5AE68FAA");
        
        //DOMConfigurator.configure("log4j.xml");        
        Security.addProvider(new BouncyCastleProvider());        
        
        System.out.println("hash " + hash.length + " bytes");         
        
        //----- request timestamp ------------
        TimeStampToken tst = TimeStampTokenUtil.getTimeStampToken(url, null, null, hash, TSPAlgorithms.SHA256, BigInteger.valueOf(0), "BC");

        if (tst == null) {
            System.out.println("NO TST");
            return;
        }
        byte[] tsrdata = tst.getEncoded();
        
        FileOutputStream fos = new FileOutputStream ("prova.txt");
        fos.write (HexString.hexify(tsrdata).getBytes());
        fos.flush();
        fos.close();

        System.out.println("Got tsr " + tsrdata.length + " bytes");
    }
}

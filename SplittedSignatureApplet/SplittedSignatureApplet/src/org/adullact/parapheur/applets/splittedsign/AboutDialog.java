/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.parapheur.applets.splittedsign;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.URISyntaxException;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

/**
 * Fenetre de Notice legale
 *
 * @author Stephane Vast - ADULLACT Projet
 */
public class AboutDialog  extends JDialog implements HyperlinkListener {
    private Main parent;

    public AboutDialog(Main parent) {
        super(new Frame(), "Notice Légale de " + Main.productString);
        this.parent = parent;
    }

    void display() {
        int width = 420;
        int height = 400;
        setSize(width, height);
        /**
         * Centrer la fenetre sur l'ecran
         */
        setLocationRelativeTo(null);
        setBackground(Color.white);

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                performCloseAction();
            }
        });

        JPanel p = new JPanel();
        p.setLayout(new BorderLayout());
        p.setBackground(Color.white);

        // Header
        JLabel lbl = new JLabel(new ImageIcon(getClass().getResource("/logo-libersign-400px.png")));
        // Border b1 = new BevelBorder(BevelBorder.LOWERED);
        Border b2 = new EmptyBorder(0, 0, 0, 0);
        lbl.setBorder(b2);   //(new CompoundBorder(b1, b2));
        //pnl.add(lbl);
        //getContentPane().add(pnl, BorderLayout.WEST);

        // Texte HTML
        JEditorPane aboutText = createHtmlEditorPane();
        aboutText.setBorder(new EmptyBorder(5, 10, 10, 10));
        aboutText.setBackground(Color.white);
        aboutText.setText(
                "<p style=\"font-family:verdana, sans-serif; font-size:14; text-align: center\">Version " + Main.versionString + "</p>"
                + "<p style=\"font-family:verdana, sans-serif; font-size:12\">" + Main.copyrightMsg + "<br/>" + Main.licenceString + "</p>"
                + "<p style=\"font-family:verdana, sans-serif; font-size:10; text-align: right\">v" + Main.versionString
                + ", " + Main.buildString + "</p>");
        aboutText.addHyperlinkListener(this);
        aboutText.setEditable(false);

        // Bouton Fermer
        JPanel s = new JPanel();
        s.setLayout(new FlowLayout());
        JButton closeButton = new JButton("Fermer");
        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                performCloseAction();
            }
        });
        s.add(closeButton);

        p.add(lbl, BorderLayout.NORTH);
        //p.add(headText, BorderLayout.NORTH);
        p.add(aboutText, BorderLayout.CENTER);
        p.add(s, BorderLayout.SOUTH);

        this.pack();
        this.setContentPane(p);

        this.setAlwaysOnTop(true);
        this.setEnabled(true);
        this.setSize(width, height);
        this.setVisible(true);
    }

    private JEditorPane createHtmlEditorPane() {
        return new JEditorPane("text/html", "");
    }

    private void performCloseAction() {
        setVisible(false);
    }

    @Override
    public void hyperlinkUpdate(final HyperlinkEvent e) {
        Desktop desktop;
        if (e.getEventType() != HyperlinkEvent.EventType.ACTIVATED) {
            return;
        }
        desktop = Desktop.getDesktop();
        if (desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(e.getURL().toURI());
            } catch (IOException e1) {
                JOptionPane.showMessageDialog(this,
                        "About.UserDefaultBrowserNotFound",
                        "Erreur", JOptionPane.ERROR_MESSAGE);
            } catch (URISyntaxException e1) {
                JOptionPane.showMessageDialog(this,
                        "About.InvalidUrl" + ": " + e.getURL(),
                        "Erreur", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}

/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact.parapheur.applets.splittedsign;

//import iaik.asn1.structures.AlgorithmID;
import iaik.pkcs.pkcs11.Mechanism;
import iaik.pkcs.pkcs11.MechanismInfo;
import iaik.pkcs.pkcs11.Module;
import iaik.pkcs.pkcs11.Session;
import iaik.pkcs.pkcs11.Slot;
import iaik.pkcs.pkcs11.Token;
import iaik.pkcs.pkcs11.TokenException;
import iaik.pkcs.pkcs11.objects.Key;
import iaik.pkcs.pkcs11.objects.Object;
import iaik.pkcs.pkcs11.objects.PrivateKey;
import iaik.pkcs.pkcs11.objects.RSAPrivateKey;
import iaik.pkcs.pkcs11.objects.X509PublicKeyCertificate;
import iaik.pkcs.pkcs11.wrapper.PKCS11Constants;
import iaik.utils.CryptoUtils;
import java.io.IOException;
import java.math.BigInteger;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
//import java.util.Hashtable;
import java.util.List;
//import java.util.Vector;

/**
 * Creates a signature on a token. 
 *  This class helps using PKCS11 Wrapper in case of signing on OSX which does 
 *  not allow raw sign (sign by encrypting hash with private key).
 * 
 * Made from exemple from IAIK PKCS11 Wrapper
 * 
 * @author Raphael Bellec
 * @author Stephane Vast - Adullact Projet
 */
public class OSXPkcs11Signer {

    
    private final boolean     rwSession           ;
    private final char    []  pinCode             ;
    private Module      pkcs11Module        = null;
    private Token       token               = null;
 // private Long        selectedTokenID     = null;
    private Session     session             = null;
    private Key         selectedSigningKey  = null;
    private String      pkcs11moduleName    = null;
    
    /**
     * A OSXPkcs11Signer must be created with the Pkcs11 module path.
     * Pin code on token will stay the same during all this object life.
     *
     * @param pkcs11moduleNameParam The PKCS#11 module to use.
     * @param pinCodeParam Token pin code.
     */
    public OSXPkcs11Signer(String pkcs11moduleNameParam, char [] pinCodeParam ){
        pkcs11moduleName    = pkcs11moduleNameParam;
        pinCode             = pinCodeParam; 
        rwSession           = Token.SessionReadWriteBehavior.RW_SESSION;
    }
    
    /**
     * Raw sign a digest with the private key associated to certificate on the
     * Token. NOTE: currently use the first available key allowed to sign.
     *
     * @param certificat Signer certificate, used to find the matching private
     * key.
     * @param digestValue Digest value of the file to sign.
     * @return The digested file signature.
     * @exception Exception TODO: need to be more precise about exception.
     * @preconditions (pkcs11Module <> null)
     * @postconditions
     */
    public byte []  signHashWithPKCS11(X509Certificate certificat, byte[] digestValue) throws Exception{
        byte    []  signedDigest;
        
        initializePkcs11 ();
        validateMechanism();
        openSession(pinCode);
        
        selectedSigningKey = selectMatchingSigningPrivateKey( certificat );
        signedDigest       = signWithSelectedKey(digestValue);
        
        // Debug
        System.out.println("The signature value is: " + new BigInteger(1, signedDigest).toString(16));
        
        return signedDigest;
    }


    private void    initializePkcs11() throws TokenException, IOException {
        
        pkcs11Module = Module.getInstance(pkcs11moduleName);
        pkcs11Module . initialize(null);

        // --------------------------------------------------------------------------------------------   selectToken                

        Slot []  slotsWithToken   = pkcs11Module.getSlotList(Module.SlotRequirement.TOKEN_PRESENT);
        Token[]  tokens           = new Token[slotsWithToken.length];        

        for (int i = 0; i < slotsWithToken.length; i++) {
                tokens[i] = slotsWithToken[i].getToken();
        }
    

        if (tokens.length == 0) {
                System.out.println("There is no slot with a present token.");
        } else if (tokens.length == 1) {
                System.out.println("Taking token with ID: " + tokens[0].getTokenID());
                // selectedTokenID = tokens[0].getTokenID();
                token = tokens[0];
        } else {
                    System.out.println("Only one token allowed on this version");
        }
        // -------------------------------------------------------------------------------------------- / selectToken /               

    }
    
    private void    validateMechanism() throws Exception {
        // first check out what attributes of the keys we may set
        Mechanism[]     mechanisms          = token.getMechanismList();
        HashMap       supportedMechanisms = new HashMap(mechanisms.length);
        MechanismInfo   signatureMechanismInfo;

        for (int i = 0; i < mechanisms.length; i++) {
            supportedMechanisms.put(mechanisms[i], mechanisms[i]);
        }
        if (supportedMechanisms.containsKey(Mechanism.get(PKCS11Constants.CKM_RSA_PKCS))) {
            signatureMechanismInfo = token.getMechanismInfo(Mechanism.get(PKCS11Constants.CKM_RSA_PKCS));
        } else {
            signatureMechanismInfo = null;
            System.out.println("The token does not support mechanism RSA_PKCS. Going to exit.");
            throw new Exception("The token does not support mechanism RSA_PKCS.");
        }

        if ((signatureMechanismInfo == null) || !signatureMechanismInfo.isSign()) {
            System.out.println("The token does not support signing with mechanism RSA_PKCS. Going to exit.");
            throw new Exception("The token does not support signing with mechanism RSA_PKCS.");
        }
    }
    
    
    private void    openSession(char [] pinCode) throws TokenException {
        
        session = token.openSession(Token.SessionType.SERIAL_SESSION, rwSession, null, null);
        session . login(Session.UserType.USER, pinCode);
    }
    
    private Key     selectFirstSigningPrivateKey() throws TokenException{
        RSAPrivateKey       templateSignatureKey = new RSAPrivateKey();
        templateSignatureKey.getSign().setBooleanValue(Boolean.TRUE);

        // KeyAndCertificate selectedSignatureKeyAndCertificate;

        // Key keyTemplate,

        System.out.println("OSXPkcs11: searching for keys");

        ArrayList keyList = new ArrayList(4);

        // --- recherche des clefs capables de signer
        session.findObjectsInit(templateSignatureKey);
        Object[] matchingKeys;

        while ((matchingKeys = session.findObjects(1)).length > 0) {
                keyList.add(matchingKeys[0]);
        }
        session.findObjectsFinal();
        // --- Fin de recherche des clefs capable de signer.

        // try to find the corresponding certificates for the signature keys
        // Note : le certificat sera déjà disponible en amont.
        /*  Hashtable   keyToCertificateTable = new Hashtable(4);
        Enumeration keyListEnumeration    = keyList.elements();*/
        
        return (Key) keyList.get(0);
        
    }
    
    
    
    
    private byte [] signWithSelectedKey(byte [] digestValue) throws TokenException{
        
        //be sure that your token can process the specified mechanism TODO: refactor, see validateMechanism
        Mechanism signatureMechanism = Mechanism.get(PKCS11Constants.CKM_RSA_PKCS);
        
        // initialize for signing
        session.signInit(signatureMechanism, selectedSigningKey);

        byte[] signatureValue = session.sign(digestValue);
        return signatureValue;
        
    }
    
    
    
    
    public Key     selectMatchingSigningPrivateKey(X509Certificate certificat) throws TokenException, CertificateException{

        BigInteger referenceSerialNumber = certificat.getSerialNumber();
        // 1- Look for keys with signing capability
        RSAPrivateKey        templateSignatureKey = new RSAPrivateKey();
        templateSignatureKey.getSign().setBooleanValue(Boolean.TRUE);

        // KeyAndCertificate selectedSignatureKeyAndCertificate;

        // Key keyTemplate,


        List<PrivateKey>                privateKeyList;
        List<X509PublicKeyCertificate>  matchingCertificateList;
        privateKeyList                  = new ArrayList<PrivateKey>();
        matchingCertificateList         = new ArrayList<X509PublicKeyCertificate>();

        // ------------- recherche des clefs capables de signer
        session.findObjectsInit(templateSignatureKey);
        Object[] matchingKeys;
        while (( matchingKeys = session.findObjects(1)).length > 0) {
            privateKeyList.add( (PrivateKey) matchingKeys[0]);    
        }
        session.findObjectsFinal();
        // ------------- Fin de recherche des clefs capables de signer.

        // For each key, try to find a corresponding certificate. If the certificate has the same serial than the X509Certificate in parameter
        // we found the right key. 

        
        // Pour chaque clef privée sur le Token, for all private keys on token
        for (PrivateKey signatureKey : privateKeyList){
            matchingCertificateList.clear(); // Remise à 0 des cerificats matchant la clef actuelle.
            byte []     keyID                            = signatureKey.getId().getByteArrayValue();
            
            // Création de la requete.
            X509PublicKeyCertificate certificateTemplate = new X509PublicKeyCertificate();
            
            // On examine tous les certificats avec la même keyID (sauf AEP HSM).
            // Look for certificate with the same keyID as the key (except in AEP HSM).
            if(!session.getModule().getInfo().getManufacturerID().contains("AEP")){   //AEP HSM can't find certificate IDs with findObjects
              certificateTemplate.getId().setByteArrayValue(keyID);
            }

            // Recherche de tous les certificats matchant la clef.
            session.findObjectsInit(certificateTemplate);                       // OPEN certificate search.
            Object[] correspondingCertificates;
            while (( correspondingCertificates = session.findObjects(1)).length > 0) {
                X509PublicKeyCertificate examinedCert = (X509PublicKeyCertificate) correspondingCertificates[0];
                if (session.getModule().getInfo().getManufacturerID().contains("AEP")) { 
                    if (CryptoUtils.equalsBlock(examinedCert.getId().getByteArrayValue(), keyID)) {
                        matchingCertificateList.add(examinedCert);
                    }
                } else {
                    matchingCertificateList.add(examinedCert);    
                }
            }
            session.findObjectsFinal();
            // matchingCertificateList contient tous les certifs matchant la clef.
            
            
            // On vérifie tous les certificats, le premier matchant le certif actuel interromp la fonction.
            BigInteger testedSerialNumber;
            for (X509PublicKeyCertificate testedCertificate : matchingCertificateList){
                X509Certificate cert    = new iaik.x509.X509Certificate(testedCertificate.getValue().getByteArrayValue());
                testedSerialNumber      = cert.getSerialNumber();
                // testedSerialNumber = new BigInteger (testedCertificate.getSerialNumber().getByteArrayValue());
                if (referenceSerialNumber.equals(testedSerialNumber)){
                    return signatureKey;
                }
            }
        }
        return null; // Exception ?  Aucune clef trouvée.
                
    }
    
}

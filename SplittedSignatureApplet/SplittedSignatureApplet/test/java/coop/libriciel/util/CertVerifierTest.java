/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.util;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.X509ExtendedKeyManager;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * CertVerifier Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>oct. 9, 2020</pre>
 */
public class CertVerifierTest {

    static KeyStore adullactRoots;

    @BeforeClass
    public static void globalInitialisation() throws Exception {
        InputStream keystoreIs = new FileInputStream("keystores/adullact.jks");
        adullactRoots = KeyStore.getInstance("JKS");
        adullactRoots.load(keystoreIs, "certificate-adullact".toCharArray());
        CertVerifier.crlListContent = new byte[]{};
    }

    @After
    public void after() throws Exception {
    }

    /**
     * Method: loadAuthorizedCertificated()
     */
    @Test
    public void loadAuthorizedCertificated_test() throws Exception {
//TODO: Test goes here...
    }

    /**
     * Method: getVerifiedWith(X509Certificate cert)
     */
    @Test
    public void getVerifiedWith_test() throws Exception {
//TODO: Test goes here...
    }

    /**
     * Method: verifyWith(X509Certificate cert, KeyStore keystore)
     */
    @Test
    public void verifyWith_test() throws Exception {
        X509Certificate certificate = loadCertInP12AtPath("certificates/boris.lucas-revocable@libriciel.fr.p12", "revocable");

        boolean res = CertVerifier.verifyWith(certificate, adullactRoots);
        assert (res);
    }


    @Test()
    public void checkCrlWithKeystore_test_success() throws Exception {
        X509Certificate validCertificate = loadCertInP12AtPath("certificates/testcert_blucas@libriciel.fr.p12", "testcert");

        boolean res = CertVerifier.checkCrlWithKeystore(validCertificate, adullactRoots, new ArrayList<String>());
        assertTrue(res);
    }

    @Test(expected = CertificateException.class)
    public void checkCrlWithKeystore_test_revoked() throws Exception {
        X509Certificate revokedCertificate = loadCertInP12AtPath("certificates/boris.lucas-revocable@libriciel.fr.p12", "revocable");

        CertVerifier.checkCrlWithKeystore(revokedCertificate, adullactRoots, new ArrayList<String>());
    }

    X509Certificate loadCertInP12AtPath(String path, String password) throws Exception {
        InputStream inStream = new FileInputStream(path);

        KeyStore ks = KeyStore.getInstance("PKCS12");
        ks.load(inStream, password.toCharArray());
        System.out.println("Keystore size : " + ks.size());
        String alias = ks.aliases().nextElement();
        System.out.println("loading cert for alias : " + alias);
        X509Certificate certificate = (X509Certificate) ks.getCertificate(alias);
        return certificate;
    }

}

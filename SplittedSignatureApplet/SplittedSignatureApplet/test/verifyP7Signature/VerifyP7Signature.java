/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 * Initiated by ADULLACT-Projet S.A.
 * Developed by Netheos & Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package verifyP7Signature;

import java.io.FileNotFoundException;
import org.bouncycastle.cms.*;
import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.security.Security;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.bc.BcRSASignerInfoVerifierBuilder;
import org.bouncycastle.operator.DefaultDigestAlgorithmIdentifierFinder;
import org.bouncycastle.operator.DefaultSignatureAlgorithmIdentifierFinder;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.bc.BcDigestCalculatorProvider;
import org.bouncycastle.util.Store;

/**
 * Base sample code to verify CMS signature
 *
 * @author Stephane Vast - Adullact Projet
 * @author a.sarr - Netheos
 */
public class VerifyP7Signature {

    public VerifyP7Signature() {
    }

    static boolean verify(byte[] data, byte[] p7signature) {
        try {
            org.bouncycastle.cms.CMSSignedData signed = new org.bouncycastle.cms.CMSSignedData
                      (new org.bouncycastle.cms.CMSProcessableByteArray(data), p7signature);
            Store certs = signed.getCertificates();
            SignerInformationStore signers = signed.getSignerInfos();
            Iterator it = signers.getSigners().iterator();
            while (it.hasNext()) {
                SignerInformation signer = (SignerInformation) it.next();
                X509CertificateHolder cert = (X509CertificateHolder) certs.getMatches(signer.getSID()).iterator().next();
                SignerInformationVerifier verifier = new BcRSASignerInfoVerifierBuilder(
                        new DefaultCMSSignatureAlgorithmNameGenerator(),
                        new DefaultSignatureAlgorithmIdentifierFinder(),
                        new DefaultDigestAlgorithmIdentifierFinder(),
                        new BcDigestCalculatorProvider()).build(cert);
                if (signer.verify(verifier)) {
                    continue;
                } else {
                    return false;
                }
            }
            return true;

//            java.security.cert.CertStore certs = signed.getCertificatesAndCRLs("Collection", "BC");
//            org.bouncycastle.cms.SignerInformationStore signers = signed.getSignerInfos();
//            java.util.Collection coll = signers.getSigners();
//            java.util.Iterator it = coll.iterator();
//
//            while (it.hasNext()) {
//                org.bouncycastle.cms.SignerInformation signer = (org.bouncycastle.cms.SignerInformation) it.next();
//                java.util.Collection certCollection = certs.getCertificates(signer.getSID());
//
//                java.util.Iterator certIt = certCollection.iterator();
//                java.security.cert.X509Certificate cert = (java.security.cert.X509Certificate) certIt.next();
//
//                if (signer.verify(cert.getPublicKey(), "BC")) {
//                    continue;
//                } else {
//                    return false;
//                }
//            }
//            return true;
//        } catch (CertStoreException ex) {
//            Logger.getLogger("global").log(Level.SEVERE, null, ex);
//            return false;
//        } catch (NoSuchAlgorithmException ex) {
//            Logger.getLogger("global").log(Level.SEVERE, null, ex);
//            return false;
//        } catch (NoSuchProviderException ex) {
//            Logger.getLogger("global").log(Level.SEVERE, null, ex);
//            return false;
        } catch (CMSException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            return false;
        } catch (OperatorCreationException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        java.io.File file = new java.io.File("signature.p7s");
        byte[] buffer = new byte[(int) file.length()];
        java.io.DataInputStream in = new java.io.DataInputStream(new java.io.FileInputStream(file));
        in.readFully(buffer);
        in.close();
        java.io.File datafile = new java.io.File("Dell-inspiron1525NB-ubuntu.pdf");
        byte[] databuffer = new byte[(int) datafile.length()];
        java.io.DataInputStream datain = new java.io.DataInputStream(new java.io.FileInputStream(datafile));
        datain.readFully(databuffer);
        datain.close();
        System.out.println("RESULTAT VERIF :" + verify(databuffer, buffer));
    }
}

/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.adullact;

import java.util.Date;

/**
 *
 * @author a.sarr
 */
public class StampValidationResult
{
    // stamped or not signature id

    private String signatureId;

    //indicate weither or not the validated signature is stamped
    private boolean stamped;

    // the stamp date, if any
    private Date stampDate;

    // stamp validity if any.
    private boolean stampValidity;

    StampValidationResult(String aSigId, boolean aStamped, Date aStDate, boolean aValidity)
    {
        signatureId = aSigId;
        stamped = aStamped;
        stampDate = aStDate;
        stampValidity = aValidity;
    }

    public String getSignatureId()
    {
        return signatureId;
    }

    void setSignatureId(String signatureId)
    {
        this.signatureId = signatureId;
    }

    public Date getStampDate()
    {
        return stampDate;
    }

    void setStampDate(Date stampDate)
    {
        this.stampDate = stampDate;
    }

    public boolean getStampValidity()
    {
        return stampValidity;
    }

    void setStampValidity(boolean stampValidity)
    {
        this.stampValidity = stampValidity;
    }

    public boolean isStamped()
    {
        return stamped;
    }

    void setStamped(boolean stamped)
    {
        this.stamped = stamped;
    }

    @Override
    public String toString()
    {
        return "Signature Id :" + signatureId + ", stamped :" + stamped + ", stamp date : "
                + stampDate.toString() + ", stamp validity: " + stampValidity + "\n";
    }

}

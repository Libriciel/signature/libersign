/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * PKCS7VerUtil.java
 *
 * Created on 23 sept. 2008, 15:00:07
 *
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.adullact;

import java.io.File;
import org.bouncycastle.cms.*;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertStoreException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.security.Security;
import java.security.cert.CertStore;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Iterator;
import org.bouncycastle.cms.CMSException;

/**
 *
 * @author a.sarr
 */
public class PKCS7VerUtil {

    static Logger logger = Logger.getLogger(PKCS7VerUtil.class.getName());

    public PKCS7VerUtil() {
    }

    /**
     * Validate a p7 contained following the "whole or nothing rule".
     * @param data the data upon whihc the signature i sgoing to be verified
     * @param p7signature p7 signatures container
     * @return true i fall signatures are valid, false otherwise.
     */
    public static boolean verify(byte[] data, byte[] p7signature) {
        int[] ret = verifyAndCount(data, p7signature);
        return (ret[0] == ret[1]);
    }

    /**
     * Verify the signatures and return the number of valid signatures
     * @param data to upon which the signatures are supposed generated
     * @param p7signature pkcs7 signatures container
     * @return an array of two integers, the first integer indicates the number of valid signatures
     *         and the second, the number of signatures (valid and invalid ones).
     *         null if the signature  format is invalid
     */
    public static int[] verifyAndCount(byte[] data, byte[] p7signature) {
        return verifyAndCount(new CMSProcessableByteArray(data), p7signature);
    }

    public static int[] verifyAndCount(File file, byte[] p7signature) {
        return verifyAndCount(new CMSProcessableFile(file), p7signature);
    }

    public static boolean verify(File file, byte[] p7signature) {
        int[] ret = verifyAndCount(file, p7signature);
        return (ret[0] == ret[1]);
    }

    private static int[] verifyAndCount(CMSProcessable data, byte[] p7signature) {
        int[] cnt = {0, 0};
        try {
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
            org.bouncycastle.cms.CMSSignedData signed = new CMSSignedData(data, p7signature);

            CertStore certs = signed.getCertificatesAndCRLs("Collection", "BC");
            SignerInformationStore signers = signed.getSignerInfos();
            cnt[1] = signers.size();
            Collection coll = signers.getSigners();
            Iterator it = coll.iterator();
            SignerInformation signer;
            X509Certificate cert;

            while (it.hasNext()) {
                signer = (SignerInformation) it.next();
                Collection certColl = certs.getCertificates(signer.getSID());

                java.util.Iterator certIt = certColl.iterator();
                while (certIt.hasNext()) {
                    cert = (X509Certificate) certIt.next();
                    try {
                        // TODO : validate certificate
                        if (signer.verify(cert.getPublicKey(), "BC")) {
                            logger.info("==validadtion sucess");
                            ++(cnt[0]);
                        }
                    } catch (CMSException ex) {
                        logger.log(Level.SEVERE, null, ex);
                        continue;
                    }
                }
            }
            return cnt;
        } catch (CMSException ex) {
            logger.log(Level.SEVERE, null, ex);
            return null;
        } catch (CertStoreException ex) {
            logger.log(Level.SEVERE, null, ex);
            return null;
        } catch (NoSuchAlgorithmException ex) {
            logger.log(Level.SEVERE, null, ex);
            return null;
        } catch (NoSuchProviderException ex) {
            logger.log(Level.SEVERE, null, ex);
            return null;
        }
    }

}

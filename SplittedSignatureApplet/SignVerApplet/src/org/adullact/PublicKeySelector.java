/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * PublicKeySelector.java
 *
 * Created on 10 oct. 2008, 11:57:59
 *
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.adullact;

//import org.adullact.parapheur.applets.splittedsign.*;
import javax.xml.crypto.KeySelectorException;

/**
 * @author a.sarr
 */
class PublicKeySelector extends javax.xml.crypto.KeySelector {

    public PublicKeySelector() {
    }

    public javax.xml.crypto.KeySelectorResult select(javax.xml.crypto.dsig.keyinfo.KeyInfo keyInfo, javax.xml.crypto.KeySelector.Purpose purpose, javax.xml.crypto.AlgorithmMethod algorithMethod, javax.xml.crypto.XMLCryptoContext cryptoContext) throws KeySelectorException {
        if (keyInfo == null) {
            throw new javax.xml.crypto.KeySelectorException("The keyInfo argument is null !!!");
        }
        javax.xml.crypto.dsig.SignatureMethod signatureMethod = (javax.xml.crypto.dsig.SignatureMethod) algorithMethod;
        java.security.PublicKey publicKey = null;
        java.util.List keyInfoContent = keyInfo.getContent();
        java.util.List x509DataContent = null;
        javax.xml.crypto.XMLStructure xmlStructure = null;
        java.security.cert.X509Certificate certificate = null;
        for (int count = 0; count < keyInfoContent.size(); count++) {
            xmlStructure = (javax.xml.crypto.XMLStructure) keyInfoContent.get(count);
            if (xmlStructure instanceof javax.xml.crypto.dsig.keyinfo.X509Data) {
                x509DataContent = ((javax.xml.crypto.dsig.keyinfo.X509Data) xmlStructure).getContent();
                for (int ncount = 0; ncount < x509DataContent.size(); ncount++) {
                    if (x509DataContent.get(ncount) instanceof java.security.cert.X509Certificate) {
                        certificate = (java.security.cert.X509Certificate) x509DataContent.get(ncount);
                        publicKey = certificate.getPublicKey();
                        //rsa-sha1, c'est le seul algo supporté conformément à la specification de l'applet
                        if (publicKey.getAlgorithm().equalsIgnoreCase("RSA") && signatureMethod.getAlgorithm().equalsIgnoreCase(javax.xml.crypto.dsig.SignatureMethod.RSA_SHA1)) {
                            return new PublicKeySelectorResult(publicKey);
                        }
                    }
                }
            }
        }
        throw new javax.xml.crypto.KeySelectorException("The keyInfo argument is not consistent !!!");
    }
}

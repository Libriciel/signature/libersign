/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * XADESVerUtil.java
 *
 * Created on 10 oct. 2008, 14:48:58
 *
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.adullact;

import java.io.IOException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.parsers.ParserConfigurationException;
import org.jdom.JDOMException;
import org.xml.sax.SAXException;

/**
 *
 * @author a.sarr
 */
public class XADESVerUtil {

    public XADESVerUtil() {
    }

    /**
     * XML type signatures signing time getter
     * @param signature the detached signature which signing time is required
     * @return a Date indicating the signing time
     * @throws XMLSignatureException
     */
    public static java.util.Date getSigningTime(byte[] signature) throws XMLSignatureException {
        try {
            javax.xml.parsers.DocumentBuilderFactory documentBuilderFactory = javax.xml.parsers.DocumentBuilderFactory.newInstance();
            documentBuilderFactory.setNamespaceAware(true);
            org.w3c.dom.Document signatureDocument = documentBuilderFactory.newDocumentBuilder().parse(new java.io.ByteArrayInputStream(signature));
            org.w3c.dom.NodeList nodeList = signatureDocument.getElementsByTagName("SigningTime");
            if (nodeList.getLength() == 0) {
                throw new javax.xml.crypto.dsig.XMLSignatureException("The signature argument is invalid !!!");
            }
            return (new java.text.SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss")).parse(nodeList.item(0).getFirstChild().getNodeValue());
        } catch (ParseException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (SAXException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (IOException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        }
    }

    public static java.util.Date getUtcSigningTime(byte[] signature) throws XMLSignatureException {
        try {
            javax.xml.parsers.DocumentBuilderFactory documentBuilderFactory = javax.xml.parsers.DocumentBuilderFactory.newInstance();
            documentBuilderFactory.setNamespaceAware(true);
            org.w3c.dom.Document signatureDocument = documentBuilderFactory.newDocumentBuilder().parse(new java.io.ByteArrayInputStream(signature));
            org.w3c.dom.NodeList nodeList = signatureDocument.getElementsByTagNameNS("http://uri.etsi.org/01903/v1.1.1#", "SigningTime");
            if (nodeList.getLength() == 0) {
                throw new javax.xml.crypto.dsig.XMLSignatureException("The signature argument is invalid !!!");
            }
            return (new java.text.SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ssz")).parse(nodeList.item(0).getFirstChild().getNodeValue().replaceAll("Z", "UTC"));
        } catch (ParseException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (SAXException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (IOException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        }
    }

    /**
     * detache dsignature signing certificate getter
     * @param signature the signature from which the signing certificate si extractacted
     * @return the signing certificate
     * @throws javax.xml.crypto.dsig.XMLSignatureException
     */
    public static java.security.cert.X509Certificate getSigningCertificate(byte[] signature) throws javax.xml.crypto.dsig.XMLSignatureException {
        try {

            javax.xml.parsers.DocumentBuilderFactory documentBuilderFactory = javax.xml.parsers.DocumentBuilderFactory.newInstance();
            documentBuilderFactory.setNamespaceAware(true);
            org.w3c.dom.Document signatureDocument = documentBuilderFactory.newDocumentBuilder().parse(new java.io.ByteArrayInputStream(signature));
            org.w3c.dom.NodeList nodeList = signatureDocument.getElementsByTagNameNS(javax.xml.crypto.dsig.XMLSignature.XMLNS, "Signature");
            if (nodeList.getLength() == 0) {
                throw new javax.xml.crypto.dsig.XMLSignatureException("signing cert: The signature argument is invalid !!!");
            }
            javax.xml.crypto.dsig.XMLSignatureFactory signatureFactory = javax.xml.crypto.dsig.XMLSignatureFactory.getInstance("DOM");
            javax.xml.crypto.dsig.dom.DOMValidateContext validateContext = new javax.xml.crypto.dsig.dom.DOMValidateContext(new org.adullact.PublicKeySelector(), nodeList.item(0));
            javax.xml.crypto.dsig.XMLSignature xmlSignature = signatureFactory.unmarshalXMLSignature(validateContext);
            javax.xml.crypto.dsig.keyinfo.KeyInfo keyInfo = xmlSignature.getKeyInfo();
            javax.xml.crypto.dsig.keyinfo.X509Data x509Data = (javax.xml.crypto.dsig.keyinfo.X509Data) keyInfo.getContent().get(0);
            return (java.security.cert.X509Certificate) (x509Data.getContent().get(0));
        } catch (MarshalException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (XMLSignatureException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (SAXException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (IOException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        }
    }

    /**
     * verify xml type detached signatures
     * @param data the signed dtat
     * @param signature the detached xml type signature
     * @return true of false (valid or invalid)
     * @throws javax.xml.crypto.dsig.XMLSignatureException
     */
    public static boolean verify(byte[] data, byte[] signature) throws javax.xml.crypto.dsig.XMLSignatureException {
        try {
            javax.xml.parsers.DocumentBuilderFactory documentBuilderFactory = javax.xml.parsers.DocumentBuilderFactory.newInstance();
            documentBuilderFactory.setNamespaceAware(true);
            org.w3c.dom.Document signatureDocument = documentBuilderFactory.newDocumentBuilder().parse(new java.io.ByteArrayInputStream(signature));
            org.w3c.dom.NodeList nodeList = signatureDocument.getElementsByTagNameNS(javax.xml.crypto.dsig.XMLSignature.XMLNS, "Signature");
            if (nodeList.getLength() == 0) {
                throw new javax.xml.crypto.dsig.XMLSignatureException("verify: The signature argument is invalid !!!");
            }
            javax.xml.crypto.dsig.XMLSignatureFactory signatureFactory = javax.xml.crypto.dsig.XMLSignatureFactory.getInstance("DOM");
            javax.xml.crypto.dsig.dom.DOMValidateContext validateContext = new javax.xml.crypto.dsig.dom.DOMValidateContext(new PublicKeySelector(), nodeList.item(0));
            ContextSpecificURIDereferencer uriDereferencer = new ContextSpecificURIDereferencer(signatureFactory.getURIDereferencer());
            //toujours appeller cette methode avant de passer l'uridereferencer.
            uriDereferencer.setData(data);
            validateContext.setURIDereferencer(uriDereferencer);
            javax.xml.crypto.dsig.XMLSignature xmlSignature = signatureFactory.unmarshalXMLSignature(validateContext);
            return xmlSignature.validate(validateContext);
        } catch (MarshalException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (SAXException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        } catch (IOException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
            throw new javax.xml.crypto.dsig.XMLSignatureException(ex.getMessage(), ex);
        }
    }

    /**
     * the same method as above
     * @param dataFile the data file
     * @param signature the detached xml signaturre
     * @return valid or invalid
     * @throws XMLSignatureException
     */
    public static boolean verify(java.io.File dataFile, byte[] signature) throws XMLSignatureException {
        try {
            byte[] dataBuffer = new byte[(int) dataFile.length()];
            java.io.DataInputStream dataInputStream = new java.io.DataInputStream(new java.io.FileInputStream(dataFile));
            dataInputStream.readFully(dataBuffer);
            dataInputStream.close();
            return verify(dataBuffer, signature);
        } catch (IOException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static void printSignature(byte[] signature, java.io.StringWriter stringWriter) {
        try {
            org.jdom.Document signatureDocument = new org.jdom.input.SAXBuilder().build(new java.io.ByteArrayInputStream(signature));
            org.jdom.output.XMLOutputter outputter = new org.jdom.output.XMLOutputter(org.jdom.output.Format.getPrettyFormat());
            outputter.output(signatureDocument, stringWriter);
        } catch (JDOMException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
        }
    }


}

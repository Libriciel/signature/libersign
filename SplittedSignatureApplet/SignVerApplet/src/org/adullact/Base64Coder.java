/*
 * A Base64 Encoder/Decoder.
 *
 * <p>
 * This class is used to encode and decode data in Base64 format as described in RFC 1521.
 *
 * <p>
 * This is "Open Source" software and released under the <a href="http://www.gnu.org/licenses/lgpl.html">GNU/LGPL</a> license.<br>
 * It is provided "as is" without warranty of any kind.<br>
 * Copyright 2003: Christian d'Heureuse, Inventec Informatik AG, Switzerland.<br>
 * Home page: <a href="http://www.source-code.biz">www.source-code.biz</a><br>
 *
 * <p>
 * Version history:<br>
 * 2003-07-22 Christian d'Heureuse (chdh): Module created.<br>
 * 2005-08-11 chdh: Lincense changed from GPL to LGPL.<br>
 * 2006-11-21 chdh:<br>
 *  &nbsp; Method encode(String) renamed to encodeString(String).<br>
 *  &nbsp; Method decode(String) renamed to decodeString(String).<br>
 *  &nbsp; New method encode(byte[],int) added.<br>
 *  &nbsp; New method decode(String) added.<br>
 */
package org.adullact;

public class Base64Coder {

// Mapping table from 6-bit nibbles to Base64 characters.
    private static char[] map1 = new char[64];

    static {
        int i = 0;
        for (char c = 'A'; c <= 'Z'; c++) {
            map1[i++] = c;
        }
        for (char c = 'a'; c <= 'z'; c++) {
            map1[i++] = c;
        }
        for (char c = '0'; c <= '9'; c++) {
            map1[i++] = c;
        }
        map1[i++] = '+';
        map1[i++] = '/';
    }

// Mapping table from Base64 characters to 6-bit nibbles.
    private static byte[] map2 = new byte[128];

    static {
        for (int i = 0; i < map2.length; i++) {
            map2[i] = -1;
        }
        for (int i = 0; i < 64; i++) {
            map2[map1[i]] = (byte) i;
        }
    }

    /**
     * Encodes a string into Base64 format.
     * No blanks or line breaks are inserted.
     * @param s  a String to be encoded.
     * @return   A String with the Base64 encoded data.
     */
    public static String encodeString(String s) {
        return new String(encode(s.getBytes()));
    }

    /**
     * Encodes a byte array into Base64 format.
     * No blanks or line breaks are inserted.
     * @param in  an array containing the data bytes to be encoded.
     * @return    A character array with the Base64 encoded data.
     */
    public static char[] encode(byte[] in) {
        return encode(in, in.length);
    }

    /**
     * Encodes a byte array into Base64 format.
     * No blanks or line breaks are inserted.
     * @param in   an array containing the data bytes to be encoded.
     * @param iLen number of bytes to process in <code>in</code>.
     * @return     A character array with the Base64 encoded data.
     */
    public static char[] encode(byte[] in, int iLen) {
        int oDataLen = (iLen * 4 + 2) / 3;       // output length without padding
        int oLen = ((iLen + 2) / 3) * 4;         // output length including padding
        char[] out = new char[oLen];
        int ip = 0;
        int op = 0;
        while (ip < iLen) {
            int i0 = in[ip++] & 0xff;
            int i1 = ip < iLen ? in[ip++] & 0xff : 0;
            int i2 = ip < iLen ? in[ip++] & 0xff : 0;
            int o0 = i0 >>> 2;
            int o1 = ((i0 & 3) << 4) | (i1 >>> 4);
            int o2 = ((i1 & 0xf) << 2) | (i2 >>> 6);
            int o3 = i2 & 0x3F;
            out[op++] = map1[o0];
            out[op++] = map1[o1];
            out[op] = op < oDataLen ? map1[o2] : '=';
            op++;
            out[op] = op < oDataLen ? map1[o3] : '=';
            op++;
        }
        return out;
    }

    /**
     * Decodes a string from Base64 format.
     * @param s  a Base64 String to be decoded.
     * @return   A String containing the decoded data.
     * @throws   IllegalArgumentException if the input is not valid Base64 encoded data.
     */
    public static String decodeString(String s) {
        return new String(decode(s));
    }

    /**
     * Decodes a byte array from Base64 format.
     * @param s  a Base64 String to be decoded.
     * @return   An array containing the decoded data bytes.
     * @throws   IllegalArgumentException if the input is not valid Base64 encoded data.
     */
    public static byte[] decode(String s) {
        return decode(s.toCharArray());
    }

    /**
     * Decodes a byte array from Base64 format.
     * No blanks or line breaks are allowed within the Base64 encoded data.
     * @param in  a character array containing the Base64 encoded data.
     * @return    An array containing the decoded data bytes.
     * @throws    IllegalArgumentException if the input is not valid Base64 encoded data.
     */
    public static byte[] decode(char[] in) {
        int iLen = in.length;
        if (iLen % 4 != 0) {
            throw new IllegalArgumentException("Length of Base64 encoded input string is not a multiple of 4.");
        }
        while (iLen > 0 && in[iLen - 1] == '=') {
            iLen--;
        }
        int oLen = (iLen * 3) / 4;
        byte[] out = new byte[oLen];
        int ip = 0;
        int op = 0;
        while (ip < iLen) {
            int i0 = in[ip++];
            int i1 = in[ip++];
            int i2 = ip < iLen ? in[ip++] : 'A';
            int i3 = ip < iLen ? in[ip++] : 'A';
            if (i0 > 127 || i1 > 127 || i2 > 127 || i3 > 127) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            int b0 = map2[i0];
            int b1 = map2[i1];
            int b2 = map2[i2];
            int b3 = map2[i3];
            if (b0 < 0 || b1 < 0 || b2 < 0 || b3 < 0) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            int o0 = (b0 << 2) | (b1 >>> 4);
            int o1 = ((b1 & 0xf) << 4) | (b2 >>> 2);
            int o2 = ((b2 & 3) << 6) | b3;
            out[op++] = (byte) o0;
            if (op < oLen) {
                out[op++] = (byte) o1;
            }
            if (op < oLen) {
                out[op++] = (byte) o2;
            }
        }
        return out;
    }

// Dummy constructor.
    private Base64Coder() {
    }
    private static final byte[] encodingTable = {(byte) 'A', (byte) 'B',
        (byte) 'C', (byte) 'D', (byte) 'E', (byte) 'F', (byte) 'G',
        (byte) 'H', (byte) 'I', (byte) 'J', (byte) 'K', (byte) 'L',
        (byte) 'M', (byte) 'N', (byte) 'O', (byte) 'P', (byte) 'Q',
        (byte) 'R', (byte) 'S', (byte) 'T', (byte) 'U', (byte) 'V',
        (byte) 'W', (byte) 'X', (byte) 'Y', (byte) 'Z', (byte) 'a',
        (byte) 'b', (byte) 'c', (byte) 'd', (byte) 'e', (byte) 'f',
        (byte) 'g', (byte) 'h', (byte) 'i', (byte) 'j', (byte) 'k',
        (byte) 'l', (byte) 'm', (byte) 'n', (byte) 'o', (byte) 'p',
        (byte) 'q', (byte) 'r', (byte) 's', (byte) 't', (byte) 'u',
        (byte) 'v', (byte) 'w', (byte) 'x', (byte) 'y', (byte) 'z',
        (byte) '0', (byte) '1', (byte) '2', (byte) '3', (byte) '4',
        (byte) '5', (byte) '6', (byte) '7', (byte) '8', (byte) '9',
        (byte) '+', (byte) '/'
    };

    public static byte[] der2pem(byte[] data) {
        byte[] bytes;
        String pemType = "PKCS7";
        int modulus = data.length % 3;
        int add = 0;
        if (pemType != null) {
            if (pemType.length() > 0) {
                add = data.length / 45;
                add += 17; /* ----BEGIN ...-----\n */
                add += pemType.length();
                add += 16; /* \n----END ...-----\n */
                add += pemType.length();
            }
        }
        if (modulus == 0) {
            bytes = new byte[(4 * data.length / 3) + add];
        } else {
            bytes = new byte[(4 * ((data.length / 3) + 1)) + add];
        }

        int dataLength = (data.length - modulus);
        int j, a1, a2, a3;
        j = 0;
        if (add != 0) {
            byte[] btmp;
            String stmp;
            stmp = "-----BEGIN " + pemType + "-----";
            btmp = stmp.getBytes();
            for (j = 0; j < btmp.length; j++) {
                bytes[j] = btmp[j];
            }
        }
        for (int i = 0; i < dataLength; i += 3, j += 4) {
            a1 = data[i] & 0xff;
            a2 = data[i + 1] & 0xff;
            a3 = data[i + 2] & 0xff;

            if (add != 0 && (i % 45) == 0) {
                bytes[j++] = 0x0a;
            }
            bytes[j] = encodingTable[(a1 >>> 2) & 0x3f];
            bytes[j + 1] = encodingTable[((a1 << 4) | (a2 >>> 4)) & 0x3f];
            bytes[j + 2] = encodingTable[((a2 << 2) | (a3 >>> 6)) & 0x3f];
            bytes[j + 3] = encodingTable[a3 & 0x3f];
        }

        /*
         * process the tail end.
         */
        int b1, b2, b3;
        int d1, d2;

        switch (modulus) {
            case 0: /* nothing left to do */
                break;
            case 1:
                d1 = data[data.length - 1] & 0xff;
                b1 = (d1 >>> 2) & 0x3f;
                b2 = (d1 << 4) & 0x3f;

                bytes[j++] = encodingTable[b1];
                bytes[j++] = encodingTable[b2];
                bytes[j++] = (byte) '=';
                bytes[j++] = (byte) '=';
                break;
            case 2:
                d1 = data[data.length - 2] & 0xff;
                d2 = data[data.length - 1] & 0xff;

                b1 = (d1 >>> 2) & 0x3f;
                b2 = ((d1 << 4) | (d2 >>> 4)) & 0x3f;
                b3 = (d2 << 2) & 0x3f;

                bytes[j++] = encodingTable[b1];
                bytes[j++] = encodingTable[b2];
                bytes[j++] = encodingTable[b3];
                bytes[j++] = (byte) '=';
                break;
        }

        if (add != 0) {
            byte[] btmp;
            String stmp;
            stmp = "\n-----END " + pemType + "-----\n";
            btmp = stmp.getBytes();
            for (a1 = 0; a1 < btmp.length; a1++, j++) {
                bytes[j] = btmp[a1];
            }
        }

        return bytes;
    }

    /**
     *  Standard header/footer for the base 64 encoded info block of a pem file.
     */
    public static final byte[] PEM_BEGIN = (new String("-----BEGIN")) .getBytes();
    public static final byte[] PEM_END = (new String("-----END")) .getBytes();

    public static boolean isPEM(byte[] test) {

        if (indexOf(test, PEM_BEGIN) == -1) {
            return false;
        }   // no PEM start string

        if (indexOf(test, PEM_END) == -1) {
            return false;
        }   // no PEM end string

        return true;  // has PEM begin and end tags - probably a PEM!
    }

    /**
     *    Returns the position that a searchByte first appears in a byte array.
     *
     *    @param mainArray the byte array to search within
     *    @param searchByte the byte to look for
     */
    public static int indexOf(byte[] mainArray, byte searchByte) {
        return indexOf(mainArray, searchByte, 0);
    }

    /**
     *    Returns the first position, greater than a given index,
     *    that a searchByte first appears at within an array.
     *
     *    @param mainArray the byte array to search within
     *    @param searchByte the byte to look for
     */
    public static int indexOf(byte[] mainArray, byte searchByte, int fromIndex) {
        int len = mainArray.length;

        if (fromIndex < 0) {
            fromIndex = 0;
        } else if (fromIndex >= len) {
            return -1;
        }

        for (int i = fromIndex; i < len; i++) {
            if (mainArray[i] == searchByte) {
                return i;
            }
        }

        return -1;                     // did not find anything...
    }

    /**
     *    Tries to match a byte sequence within a larger byte array.
     *
     *    @param mainArray the base array to search within.
     *    @param searchSequence the short sequence to find the position of
     *           within the main array.
     *
     *    @return the index of the searchSequence within the main Array,
     *            or -1 if not found.
     */
    public static int indexOf(byte[] mainArray, byte[] searchSequence) {
        return indexOf(mainArray, searchSequence, 0);
    }

    /**
     *    Tries to match a byte sequence within a larger byte array.
     *
     *    @param mainArray the base array to search within.
     *    @param searchSequence the short sequence to find the position of
     *           within the main array.
     *    @param fromIndex the position to start searching from.
     *
     *    @return the index of the searchSequence within the main Array,
     *            or -1 if not found.
     */
    public static int indexOf(byte[] mainArray, byte[] searchSequence, int fromIndex) {
        byte v1[] = mainArray;
        byte v2[] = searchSequence;
        int max = mainArray.length;

        if (fromIndex >= max) {
            if (mainArray.length == 0 && fromIndex == 0 && searchSequence.length == 0) {
                return 0;
            }
            return -1;  // from index too large
        }

        if (fromIndex < 0) {
            fromIndex = 0;
        }
        if (searchSequence.length == 0) {
            return fromIndex;
        }

        byte first = v2[0];
        int i = fromIndex;

        startSearchForFirstChar:
        while (true) {
            /* Look for first character. */
            while (i < max && v1[i] != first) {
                i++;
            }

            if (i >= max) // didn't find the sequence: return -1
            {
                return -1;
            }

            /* Found first character, now look at the rest of v2 */
            int j = i + 1;
            int end = j + searchSequence.length - 1;
            int k = 1;
            while (j < end) {
                if (v1[j++] != v2[k++]) {
                    i++;
                    /* Look for str's first char again. */
                    continue startSearchForFirstChar;
                }
            }
            return i;    // Found whole string!!!
        }
    }

    public static byte[] pem2der(byte[] pem, byte[] header, byte[] footer) {
        int start, end;
        start = indexOf(pem, header);
        end = indexOf(pem, footer);
        if (start == -1 || end == -1) {
            return null;
        }  //  no headers!
        start = indexOf(pem, (byte) '\n', start) + 1;
        System.out.println("start/end  = "+ start +" / "+ end );

        // skip past any more text, by avoiding all lines less than 64 characters long...
//        int next;
//        while ((next = indexOf(pem, (byte) '\n', start)) < start + 64) {
//            if (next == -1)
//            {
//                break;
//            }
//            start = next + 1;
//        }
//        if (start == -1)
//            return null;

        int len = end - start;
        System.out.println("start/end/len = "+ start +" / "+ end +" / "+ len);
        byte[] data = new byte[len];

        System.arraycopy(pem, start, data, 0, len); // remove the PEM fluff from tbe base 64 data, stick in 'data'
        System.out.println(new String(data));
        String vlstr = null;
        try {
            vlstr = new String(data, "US-ASCII");
        } catch (Exception e){
            vlstr = new String(data);
        }
        StringBuffer vlbuffer  = new StringBuffer(vlstr);
        while (vlbuffer.lastIndexOf("\n") != -1) // < vlbuffer.length())
            vlbuffer.deleteCharAt(vlbuffer.indexOf("\n"));
        return decode(vlbuffer.toString());//CBBase64.decode(data);               // return the raw binary data
    }
} // end class Base64Coder


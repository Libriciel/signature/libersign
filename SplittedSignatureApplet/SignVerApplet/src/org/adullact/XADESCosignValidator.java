/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.adullact;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author a.sarr
 */
public class XADESCosignValidator
{

    static Logger logger = Logger.getLogger(XADESCosignValidator.class.getName());

    /**
     * multiSignature validator
     * @param signedDocIs signed coument inputstream
     * @return an int array indicating the number fo signatures and the number valid signatures
     * @throws XMLSignatureException
     */
    public static int[] validate(InputStream signedDocIs) throws XMLSignatureException
    {
        int[] cnt =
        {
            0, 0
        };
        try
        {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            documentBuilderFactory.setNamespaceAware(true);
            Document signedDocument = documentBuilderFactory.newDocumentBuilder().parse(signedDocIs);
            NodeList nodeList = signedDocument.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
            int sigCount = nodeList.getLength();
            if (sigCount == 0)
                throw new XMLSignatureException("The document is not signed");
            // see the DOMValidateContext documentation (jdk 1.6)
            DOMValidateContext[] contextes = new DOMValidateContext[sigCount];
            cnt[1] = sigCount;
            for (int i = 0; i < contextes.length; i++)
                contextes[i] = new DOMValidateContext(new PublicKeySelector(), nodeList.item(i));            

            XMLSignatureFactory factory = XMLSignatureFactory.getInstance("DOM");
            XMLSignature signature = null;
            for (int i = 0; i < sigCount; i++)
            {
                try
                {
                    signature = factory.unmarshalXMLSignature(contextes[i]);
                    if (signature.validate(contextes[i]) == true)
                        cnt[0]++;
                } catch (MarshalException ex)
                {
                    logger.log(Level.SEVERE, null, ex);
                }
            }
            return cnt;
        } catch (SAXException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (IOException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (ParserConfigurationException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        }
    }

}

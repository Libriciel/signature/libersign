/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * X500PrincipalUtil.java
 *
 * Created on 17 oct. 2008, 15:25:46
 *
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.adullact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.jce.X509Principal;
import sun.security.pkcs.PKCS9Attribute;


public class X500PrincipalUtil {

    private static final String ATTR_TERMINATOR = "=";


    public static final Map<String, String> COMMONS_ADDITIONAL_KEYWORDS = new HashMap<String, String>();
    static {
        COMMONS_ADDITIONAL_KEYWORDS.put("E", PKCS9Attribute.EMAIL_ADDRESS_OID.toString());
    }

    public static int LEASTSIGNIFICANT = 0;
    public static int MOSTSIGNIFICANT = 1;
    public static final String attrCN = "CN";
    public static final String attrOU = "OU";
    public static final String attrO = "O";
    public static final String attrC = "C";
    public static final String attrL = "L";
    public static final String attrST = "ST";
    public static final String attrSTREET = "STREET";
    public static final String attrE = "E";
    public static final String attrEMAIL = "EMAIL";
    public static final String attrEMAILADDRESS = "EMAILADDRESS";
    public static final String attrEMAIL_ADDRESS_OID = PKCS9Attribute.EMAIL_ADDRESS_OID.toString();
    public static final String attrUID = "UID";
    List<List<String>> rdnNameArray = new ArrayList<List<String>>();

    public X500PrincipalUtil(X500Principal principal) {
        parseDN(principal.getName(X500Principal.RFC2253));
    }

    public static final X500Principal buildX500Principal(final String sCommonName, final String sEmail, final String sOrganisationUnit, final String sOrganisation, final String sLocality, final String sState, final String sCountryCode) {

        Hashtable<DERObjectIdentifier, String> attrs = new Hashtable<DERObjectIdentifier, String>();
        Vector<DERObjectIdentifier> vOrder = new Vector<DERObjectIdentifier>();

        if (sCountryCode != null) {
            attrs.put(X509Principal.C, sCountryCode);
            vOrder.add(0, X509Principal.C);
        }
        if (sState != null) {
            attrs.put(X509Principal.ST, sState);
            vOrder.add(0, X509Principal.ST);
        }
        if (sLocality != null) {
            attrs.put(X509Principal.L, sLocality);
            vOrder.add(0, X509Principal.L);
        }
        if (sOrganisation != null) {
            attrs.put(X509Principal.O, sOrganisation);
            vOrder.add(0, X509Principal.O);
        }
        if (sOrganisationUnit != null) {
            attrs.put(X509Principal.OU, sOrganisationUnit);
            vOrder.add(0, X509Principal.OU);
        }
        if (sEmail != null) {
            attrs.put(X509Principal.E, sEmail);
            vOrder.add(0, X509Principal.E);
        }
        if (sCommonName != null) {
            attrs.put(X509Principal.CN, sCommonName);
            vOrder.add(0, X509Principal.CN);
        }
        return new X500Principal(new X509Principal(vOrder, attrs).getName(), COMMONS_ADDITIONAL_KEYWORDS);
    }


    public void setPrincipal(X500Principal principal) {
        parseDN(principal.getName(X500Principal.RFC2253));
    }



    public String getCN() {
        return findPart(attrCN);
    }

    public String getOU() {
        return findPart(attrOU);
    }

    public String getO() {
        return findPart(attrO);
    }

    public String getC() {
        return findPart(attrC);
    }

    public String getL() {
        return findPart(attrL);
    }

    public String getST() {
        return findPart(attrST);
    }

    public String getSTREET() {
        return findPart(attrSTREET);
    }

    public static boolean isValidEmailAddress(final String email) {
        if (email == null || email.length() < 6) {
            return false;
        }
        return email.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*\\.[_A-Za-z0-9-]+$");
    }




    public String getEMAILDDRESS() {
        String found = findPart(attrE);
        if (found != null) {
            return found;
        }
        found = findPart(attrEMAIL);
        if (found != null) {
            return found;
        }
        found = findPart(attrEMAILADDRESS);
        if (found != null) {
            return found;
        }
        found = findPart(attrEMAIL_ADDRESS_OID);
        if (found != null && X500PrincipalUtil.isValidEmailAddress(found)) {
            return found;
        }

        if (found != null && found.startsWith("#")) {
            String email = "";
            for (int i = 1; i < found.length(); i = i + 2) {
                String hex = found.substring(i, i + 2);
                email += ((char) Integer.parseInt(hex, 16));
            }
            email = email.trim();
            if (X500PrincipalUtil.isValidEmailAddress(email)) {
                return email;
            }
        }
        return null;
    }


    public String getUID() {
        return findPart(attrUID);
    }


    private void parseDN(String dn) throws IllegalArgumentException {
        int startIndex = 0;
        char c = '\0';
        List<String> nameValues = new ArrayList<String>();
        rdnNameArray.clear();
        while (startIndex < dn.length()) {
            int endIndex;
            for (endIndex = startIndex; endIndex < dn.length(); endIndex++) {
                c = dn.charAt(endIndex);
                if (c == ',' || c == '+') {
                    break;
                }
                if (c == '\\') {
                    endIndex++;
                }
            }
            if (endIndex > dn.length()) {
                throw new IllegalArgumentException("unterminated escape " + dn); //$NON-NLS-1$
            }
            nameValues.add(dn.substring(startIndex, endIndex));

            if (c != '+') {
                rdnNameArray.add(nameValues);
                if (endIndex != dn.length()) {
                    nameValues = new ArrayList<String>();
                } else {
                    nameValues = null;
                }
            }
            startIndex = endIndex + 1;
        }
        if (nameValues != null) {
            throw new IllegalArgumentException("improperly terminated DN " + dn); //$NON-NLS-1$
        }
    }



    public List<String> getAllValues(String attributeID) {
        List<String> retList = new ArrayList<String>();
        String searchPart = attributeID + ATTR_TERMINATOR;

        for (Iterator<List<String>> iter = rdnNameArray.iterator(); iter.hasNext();) {
            List<String> nameList = iter.next();
            String namePart = nameList.get(0);
            if (namePart.startsWith(searchPart)) {
                retList.add(namePart.toString().substring(searchPart.length()));
            }
        }
        return retList;
    }

    private String findPart(String attributeID) {
        return findSignificantPart(attributeID, MOSTSIGNIFICANT);
    }


    private String findSignificantPart(String attributeID, int significance) {
        String retNamePart = null;
        String searchPart = attributeID + ATTR_TERMINATOR;

        for (Iterator iter = rdnNameArray.iterator(); iter.hasNext();) {
            ArrayList nameList = (ArrayList) iter.next();
            String namePart = (String) nameList.get(0);

            if (namePart.startsWith(searchPart)) {
                retNamePart = namePart.toString().substring(searchPart.length());
                if (significance == MOSTSIGNIFICANT) {
                    break;
                }
            }
        }

        return retNamePart;
    }

    public String getPrintableString() {

        String toReturn = null;
        String tmp = this.getCN();
        if (tmp != null) {
            toReturn = "<b>par  </b>" + tmp + ", ";
        }
        tmp = getEMAILDDRESS();
        if (tmp != null) {
            toReturn += "<b> email  </b>" + tmp + ", ";
        }
        tmp = getO();
        if (tmp != null) {
            toReturn += "<b> de l'organisation </b>" + tmp;
        }
        tmp = getOU();
        if (tmp != null) {
            toReturn += " " + tmp;
        }
        toReturn += ", ";
        return toReturn;
    }
}
/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * ContextSpecificURIDereferencer.java
 *
 * Created on 10 oct. 2008, 16:22:05
 *
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.adullact;

/** 
 * @author a.sarr
 */
public class ContextSpecificURIDereferencer implements javax.xml.crypto.URIDereferencer {

    private javax.xml.crypto.URIDereferencer defaultDereferencer;
    private byte[] data;

    public ContextSpecificURIDereferencer(javax.xml.crypto.URIDereferencer dereferencer) {
        this.defaultDereferencer = dereferencer;
    }

    public void setData(byte[] adata) {
        this.data = adata;
    }

    public javax.xml.crypto.Data dereference(javax.xml.crypto.URIReference uriReference, javax.xml.crypto.XMLCryptoContext context) throws javax.xml.crypto.URIReferenceException {
        if (null == uriReference.getURI()) {
            return new javax.xml.crypto.OctetStreamData(new java.io.ByteArrayInputStream(data));
        }
        return this.defaultDereferencer.dereference(uriReference, context);
    }
}

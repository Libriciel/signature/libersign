/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.adullact;


import java.security.cert.X509Certificate;
import java.util.HashMap;

/**
 * Cette classe extrait les information textuelles et personnelles
 * contenues dans des champs de certificat X509 en en fournissant un 
 * accès interessant pour la programmation (HashMap par exemple).
 * @author Xavier FACELINA <x.facelina@nethos.net>
 */
public class CertificateInfosExtractor {

    /** Creates a new instance of CertificateInfosExtractor */
    public CertificateInfosExtractor() {
    }

    /**
     * fourni une représentation clé,valeur de la ligne SUBJECT obtenue
     * par cert.getSubjectX500Principal().toString();
     * @param cert le certificate concerné
     * @return le hash de valeurs
     */
    public static HashMap<String, String> makeSubjectInfos(X509Certificate cert) {
        String subject = cert.getSubjectX500Principal().toString();
        String[] subjectArray = subject.split(", ");
        HashMap subjectHashMap = new HashMap();
        for (String kvPair : subjectArray) {
            String[] kv = kvPair.split("=");
            subjectHashMap.put(kv[0], kv[1]);
        }
        return subjectHashMap;
    }

    public static HashMap<String, String> makeIssuerInfos(X509Certificate cert) {

        String issuer = cert.getIssuerX500Principal().toString();
        String[] issuerArray = issuer.split(", ");
        HashMap issuerHashMap = new HashMap();
        for (String kvPair : issuerArray) {
            String[] kv = kvPair.split("=");
            issuerHashMap.put(kv[0], kv[1]);
        }
        return issuerHashMap;
    }

}

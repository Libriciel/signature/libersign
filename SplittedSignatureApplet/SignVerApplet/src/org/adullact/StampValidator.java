/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.adullact;

import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dom.DOMStructure;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.tsp.TSPValidationException;
import org.bouncycastle.tsp.TimeStampResponse;
import org.bouncycastle.tsp.TimeStampToken;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;
import org.bouncycastle.util.encoders.Base64;

/**
 *
 * @author a.sarr
 */
public class StampValidator
{

    static Logger logger = Logger.getLogger(StampValidator.class.getName());

    static final String DEFAULT_DIGEST = "SHA1";

    public static List<StampValidationResult> validate(InputStream stampedDocStream) throws XMLSignatureException, NoSuchAlgorithmException
    {
        try
        {
            List<StampValidationResult> results = new ArrayList<StampValidationResult>();
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            documentBuilderFactory.setNamespaceAware(true);
            Document signedDocument = documentBuilderFactory.newDocumentBuilder().parse(stampedDocStream);
            XMLSignatureFactory sigFactory = XMLSignatureFactory.getInstance("DOM");
            XMLSignature sig = null;
            NodeList signaturesNodeList = signedDocument.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
            NodeList encapsulatedStamps = signedDocument.getElementsByTagName("xad:EncapsulatedTimeStamp");
            int sigNumber = signaturesNodeList.getLength();
            int tsNumber = encapsulatedStamps.getLength();
            MessageDigest messageDigest = MessageDigest.getInstance(DEFAULT_DIGEST);
            byte[] digest = null;
            Element tsElement = null;
            String text = null;
            Text textNode = null;
            TimeStampResponse tsResponse = null;

            next_signature:
            for (int i = 0; i < sigNumber; i++)
            {
                sig = sigFactory.unmarshalXMLSignature(new DOMStructure(signaturesNodeList.item(i)));
                messageDigest.reset();
                digest = messageDigest.digest(sig.getSignatureValue().getValue());
                for (int j = 0; j < tsNumber; j++)
                {
                    tsElement = (Element) encapsulatedStamps.item(j);
                    if (tsElement.getAttribute("Id").equals(sig.getId() + "_TS"))
                    {
                        textNode = (Text) tsElement.getFirstChild();
                        text = textNode.getData().trim();
                        tsResponse = new TimeStampResponse(Base64.decode(text.getBytes("UTF8")));

                        if (!Arrays.equals(digest, tsResponse.getTimeStampToken().getTimeStampInfo().getMessageImprintDigest()))
                        {
                            results.add(new StampValidationResult(sig.getId(), true, tsResponse.getTimeStampToken().getTimeStampInfo().getGenTime(), false));
                            continue next_signature;
                        }
                        if (!validate(tsResponse.getTimeStampToken().toCMSSignedData().getSignerInfos(),
                                tsResponse.getTimeStampToken().getCertificatesAndCRLs("Collection", "BC"), tsResponse))
                        {
                            results.add(new StampValidationResult(sig.getId(), true, tsResponse.getTimeStampToken().getTimeStampInfo().getGenTime(), false));
                            continue next_signature;
                        }
                        results.add(new StampValidationResult(sig.getId(), true, tsResponse.getTimeStampToken().getTimeStampInfo().getGenTime(), true));
                        continue next_signature;
                    }
                }
            }

            return results;
        } catch (CertStoreException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (NoSuchProviderException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (CMSException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (TSPValidationException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (CertificateExpiredException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (CertificateNotYetValidException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (TSPException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (MarshalException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (SAXException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (IOException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        } catch (ParserConfigurationException ex)
        {
            logger.log(Level.SEVERE, null, ex);
            throw new XMLSignatureException(ex.getMessage(), ex);
        }
    }

    static boolean validate(SignerInformationStore signers, CertStore certStore, TimeStampResponse response) throws CertStoreException, NoSuchAlgorithmException, NoSuchProviderException, CMSException, TSPException, TSPValidationException, CertificateExpiredException, CertificateNotYetValidException
    {
        Iterator iter = signers.getSigners().iterator(); // there may be more than one signers
        SignerInformation signer;
        X509Certificate certificate;
        Date today = new Date();
        TimeStampToken tsToken = response.getTimeStampToken();
        while (iter.hasNext())
        {
            signer = (SignerInformation) iter.next();
            Collection certColl = certStore.getCertificates(signer.getSID());

            java.util.Iterator certIter = certColl.iterator();
            while (certIter.hasNext())
            {
                certificate = (X509Certificate) certIter.next();

                if (!validateCertificate(certificate))
                    return false;

                if (certificate.getNotBefore().before(today)
                        && today.before(certificate.getNotAfter()))
                    tsToken.validate(certificate, "BC");
                else
                    return false;
            }
        }
        return true;
    }

    static boolean validateCertificate(X509Certificate tsAuthorityCertificate)
    {
        // TODO:  further validations about the TS authority's certificate
        return true;
    }

}

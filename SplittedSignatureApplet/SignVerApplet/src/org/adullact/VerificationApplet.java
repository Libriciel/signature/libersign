/*
 * LiberSign
 * Copyright (C) 2008-2022 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * VerificationApplet.java
 *
 * Created on 24 septembre 2008, 10:54
 */
package org.adullact;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.KeyStore;
import java.security.Principal;
import java.security.cert.CertPath;
import java.security.cert.CertPathBuilder;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertStore;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.PKIXBuilderParameters;
import java.security.cert.PKIXCertPathBuilderResult;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.xml.crypto.dsig.XMLSignatureException;

/**
 *
 * @author  a.sarr
 */
public class VerificationApplet extends javax.swing.JApplet {

    private String format;
    private byte[] signature;
    private final String validSignatureText = "<html><font color=green><b><center>La signature porte bien sur ce document</center></b></font></html>";
    private final String invalidSignatureText = "<html><font color=red><b><center>La signature ne porte pas sur ce document</center></b></font></html>";
    private final String getSignatureErrorText = "<html><font color=red></center>Erreur sur la récupération de la signature</center></font></html>";
    private String signatureLabelText = "<html><body><b>Signature effectuée le </b>";
    String CAsKeystorePWD = "???";
    boolean certValidity = false;
    X509Certificate validatedCertificate;
    Date signatureDate;

    @Override
    public void stop() {
        System.exit(0);
    }

    /** Initializes the applet VerificationApplet */
    @Override
    public void init() {
        {
            java.io.InputStream istream = null;
            String vlStr = null;
            X500PrincipalUtil subjectX500Util = null;
            X500PrincipalUtil issuerX500Util = null;
            try {
                javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
                javax.swing.SwingUtilities.updateComponentTreeUI(this);
                initComponents();
                String paramSignatureURL = getParameter("url_get_signature");

                format = getParameter("format");
                System.out.println("URL, format: " + paramSignatureURL + ", " + format);
                if (paramSignatureURL != null) {
                    java.net.URL signUrl = new java.net.URL(paramSignatureURL);
                    istream = signUrl.openStream();
                    java.io.ByteArrayOutputStream bastream = new java.io.ByteArrayOutputStream();
                    byte[] buffer = new byte[1024];
                    int readen = 0;
                    if (istream != null) {
                        readen = istream.read(buffer);
                        while (readen != -1) {
                            bastream.write(buffer, 0, readen);
                            readen = istream.read(buffer);
                        }
                        istream.close();
                    } else {
                        System.out.println("istream is NULL!!");
                    }
                    //this.signature = bastream.toByteArray();
                    this.signature = bastream.toByteArray();
                    vlStr = new String(this.signature);
                    System.out.println("signature url_get_signature (" + this.signature.length + "/" + vlStr.length() + "):\n" + vlStr);
                }
                if (this.signature != null) {
                    if ("CMS".equalsIgnoreCase(format)) {
                        byte[] cms = Base64Coder.decode(vlStr);
                        System.out.println("Signature décodée base64:\n" + new String(cms));
                        this.signature = Base64Coder.pem2der(cms, Base64Coder.PEM_BEGIN, Base64Coder.PEM_END);
                        sun.security.pkcs.PKCS7 p7signature = new sun.security.pkcs.PKCS7(this.signature);

                        //recup certfis...
                        java.security.cert.X509Certificate[] certificates = p7signature.getCertificates();
                        // signer Info
                        sun.security.pkcs.SignerInfo[] signerInfos = p7signature.getSignerInfos();
                        // heure de signature
                        sun.security.pkcs.PKCS9Attribute signingTimeAttribute = signerInfos[0].getAuthenticatedAttributes().getAttribute(sun.security.pkcs.PKCS9Attribute.SIGNING_TIME_OID);
                        //if(signingTimeAttribute==null)
                        signatureDate = (java.util.Date) signingTimeAttribute.getValue();
                        java.text.DateFormat dateFormat = java.text.DateFormat.getDateTimeInstance();
                        String formatedDate = dateFormat.format(signatureDate);
                        signatureLabelText += " " + formatedDate + "<br>";
                        subjectX500Util = new X500PrincipalUtil(certificates[0].getSubjectX500Principal());
                        issuerX500Util = new X500PrincipalUtil(certificates[0].getIssuerX500Principal());
                        signatureLabelText += subjectX500Util.getPrintableString() + " ";
                        signatureLabelText += "<br><b> certificat émis par </b>" + issuerX500Util.getCN() + "</html>";
                        this.signatureEditor.setText(signatureLabelText);

                        validatedCertificate = certificates[0];

                    } else if ("XADES".equalsIgnoreCase(format)) {
                        this.signature = Base64Coder.decode(vlStr);
                        System.out.println("Signature décodée base64:\n" + new String(signature));
                        java.security.cert.X509Certificate signerCertificate = XADESVerUtil.getSigningCertificate(this.signature);
                        signatureDate = XADESVerUtil.getSigningTime(signature);
                        java.text.DateFormat dateFormat = java.text.DateFormat.getDateTimeInstance();
                        String formatedDate = dateFormat.format(signatureDate);
                        signatureLabelText += " " + formatedDate + "<br>";
                        subjectX500Util = new X500PrincipalUtil(signerCertificate.getSubjectX500Principal());
                        issuerX500Util = new X500PrincipalUtil(signerCertificate.getIssuerX500Principal());
                        signatureLabelText += subjectX500Util.getPrintableString() + " ";
                        signatureLabelText += "<br><b> certificat émis par </b>" + issuerX500Util.getCN() + "</html>";
                        this.signatureEditor.setText(signatureLabelText);

                        validatedCertificate = signerCertificate;

                    } else if ("XADES-env".equalsIgnoreCase(format)) {
                        //System.out.println("Signature :\n" + new String(signature));
                        java.security.cert.X509Certificate signerCertificate = XADESVerUtil.getSigningCertificate(this.signature);
                        // System.out.println("Cert :\n" + signerCertificate.toString());
                        signatureDate = XADESVerUtil.getUtcSigningTime(signature);
//                        java.text.DateFormat dateFormat = java.text.DateFormat.getDateTimeInstance();
                        SimpleDateFormat dateformatter = new SimpleDateFormat("dd/MM/yyyy à HH:mm");
                        String formatedDate = "n/a"; // dateFormat.format(signatureDate);
                        if (signatureDate != null) {
                            formatedDate = dateformatter.format(signatureDate);
                            System.out.println("formatedDate : " + formatedDate);
                        }
                        signatureLabelText += " " + formatedDate + "<br>";
                        subjectX500Util = new X500PrincipalUtil(signerCertificate.getSubjectX500Principal());
                        issuerX500Util = new X500PrincipalUtil(signerCertificate.getIssuerX500Principal());
                        signatureLabelText += subjectX500Util.getPrintableString() + " ";
                        System.out.println("signatureLabelText1 : " + signatureLabelText);
                        signatureLabelText += "<br><b> certificat émis par </b>" + issuerX500Util.getCN() + "</body></html>";
                        System.out.println("signatureLabelText2 : " + signatureLabelText);
                        this.signatureEditor.setText(signatureLabelText);
                        
                        validatedCertificate = signerCertificate;
                        
                        //this.signatureEditor.setText("<html><body><font color=red><b>Format de signature '" + format + "' insupportable</b></font></body></html>");
                    } else {
                        this.signatureEditor.setText("<html><font color=red><b>Format de signature '" + format + "' inconnu</b></font></html>");
                        java.util.logging.Logger.getLogger("org.adullact").severe("unsupportedSignatureFormat");
                    }
                } else {
                    this.signatureEditor.setText(this.getSignatureErrorText);
                }
                this.chooseButton.addActionListener(new java.awt.event.ActionListener() {

                    public void actionPerformed(java.awt.event.ActionEvent event) {
                        javax.swing.JFileChooser chooser = new javax.swing.JFileChooser();
                        chooser.setFileSelectionMode(javax.swing.JFileChooser.FILES_ONLY);
                        chooser.setMultiSelectionEnabled(false);
                        chooser.setVisible(true);
                        javax.swing.filechooser.FileSystemView view = javax.swing.filechooser.FileSystemView.getFileSystemView();
                        chooser.setFileSystemView(view);

                        // un bug de java déja identifiée
                        // voir  http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6684952
                        int val = chooser.showDialog(null, "Choisir");
                        if (val == javax.swing.JFileChooser.APPROVE_OPTION) {
                            VerificationApplet.this.dataTextField.setText(chooser.getSelectedFile().getPath());

                            if (VerificationApplet.this.signature != null) {
                                boolean signatureValidity = false;
                                boolean certificateValidity = false;
                                String invalidCertificateText = "Le certificat ne peut pas être vérifié";
                                if ("CMS".equalsIgnoreCase(format)) {
                                    signatureValidity = PKCS7VerUtil.verify(chooser.getSelectedFile(), signature);
                                } else {
                                    try {
                                        signatureValidity = org.adullact.XADESVerUtil.verify(chooser.getSelectedFile(), signature);
                                    } catch (XMLSignatureException ex) {
                                        Logger.getLogger("global").log(Level.SEVERE, null, ex);
                                    }
                                }
                                List<String> invalidsCRL = new ArrayList<String>();
                                while (true) {
                                    try {
                                        validateCertificate(validatedCertificate, false, invalidsCRL);
                                        certificateValidity = true;
                                        break;
                                    } catch (CertificateSelfSignedException e) {
                                        certificateValidity = false;
                                        invalidCertificateText = "Le certificat est auto-signé";
                                        break;
                                    } catch (CertificateExpiredException e) {
                                        certificateValidity = false;
                                        invalidCertificateText = "Le certificat est expiré";
                                        break;
                                    } catch (CertificateNotYetValidException e) {
                                        certificateValidity = false;
                                        invalidCertificateText = "Le certificat n'est pas encore valide";
                                        break;
                                    } catch (CertificateRevokedException e) {
                                        certificateValidity = false;
                                        invalidCertificateText = "Le certificat a été révoqué";
                                        break;
                                    } catch (CertPathBuilderException e) {
                                        certificateValidity = false;
                                        invalidCertificateText = "L'autorité de certification est inconnue";
                                        break;
                                    } catch (CRLNotFoundException e) {
                                        final AtomicReference<Integer> result = new AtomicReference<Integer>();
                                        final CRLNotFoundException crlNotFoundException = e;

                                        SwingUtilities.invokeAndWait(new Runnable() {
                                            public void run() {
                                                JDialog frame = new JDialog();
                                                frame.setVisible(false);
                                                frame.setAlwaysOnTop(true);
                                                frame.toFront();
                                                result.set(JOptionPane.showConfirmDialog(frame,
                                                                                         "ATTENTION: La CRL '" + crlNotFoundException.getCrlLocation() +
                                                                                         "' ne peut être contactée. \nSouhaitez-vous quand même continuer ?",
                                                                                         "CRL inconnue",
                                                                                         JOptionPane.YES_NO_OPTION));
                                            }
                                        });
                                        int result = result.get();
                                        if (result == JOptionPane.OK_OPTION) {
                                            invalidsCRL.add(e.getCrlLocation());
                                            continue;
                                        } else {
                                            certificateValidity = false;
                                            invalidCertificateText = "La CRL " + e.getCrlLocation() + " ne peut être contactée";
                                            break;
                                        }
                                    } catch (Exception e) {
                                        certificateValidity = false;
                                        break;
                                    }
                                }
                                if (signatureValidity == true) {
                                    if (certificateValidity == true) {
                                        VerificationApplet.this.validValLabel.setText(validSignatureText);
                                        VerificationApplet.this.validValLabel.setForeground(Color.GREEN);
                                    } else {
                                        VerificationApplet.this.validValLabel.setText(invalidCertificateText);
                                        VerificationApplet.this.validValLabel.setForeground(Color.ORANGE);
                                    }
                                } else {
                                    VerificationApplet.this.validValLabel.setText(invalidSignatureText);
                                    VerificationApplet.this.validValLabel.setForeground(Color.RED);
                                }
                            }
                        }
                        VerificationApplet.this.validValLabel.setVisible(true);
                        VerificationApplet.this.repaint();
                        VerificationApplet.this.validate();
                    }
                });
                validate();
            } catch (XMLSignatureException ex) {
                Logger.getLogger("global").log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger("global").log(Level.SEVERE, null, ex);
            } catch (java.lang.ClassNotFoundException ex) {
                java.util.logging.Logger.getLogger("global").log(java.util.logging.Level.SEVERE, null, ex);
            } catch (java.lang.InstantiationException ex) {
                java.util.logging.Logger.getLogger("global").log(java.util.logging.Level.SEVERE, null, ex);
            } catch (java.lang.IllegalAccessException ex) {
                java.util.logging.Logger.getLogger("global").log(java.util.logging.Level.SEVERE, null, ex);
            } catch (javax.swing.UnsupportedLookAndFeelException ex) {
                java.util.logging.Logger.getLogger("global").log(java.util.logging.Level.SEVERE, null, ex);
            }
        }
    }

    boolean validateCertificate(final X509Certificate certificate, boolean acceptSelfSigned, List<String> invalidsCRL) throws Exception {
        if (invalidsCRL == null) {
            invalidsCRL = new ArrayList<String>();
        }

        boolean selfSigned = false;

        certificate.checkValidity(signatureDate);

        Principal subject = certificate.getSubjectDN();
        Principal issuer = certificate.getIssuerDN();
        if (issuer.equals(subject)) {
            if (!acceptSelfSigned) {
                throw new CertificateSelfSignedException("Self signed certificates are refused");
            }
            selfSigned = true;
        }

        InputStream kst_pwd_is = VerificationApplet.class.getResourceAsStream("/ac-truststore.password");
        ByteArrayOutputStream ksPasswordWriter = new ByteArrayOutputStream();
        int readed;
        byte[] data = new byte[256];
        while ((readed = kst_pwd_is.read(data)) != -1) {
            ksPasswordWriter.write(data, 0, readed);
        }
        String ksPassword = new String(ksPasswordWriter.toByteArray());

        /* inputstrean containing the jks of trusted CAs*/
        InputStream kst_is = VerificationApplet.class.getResourceAsStream("/ac-truststore.jks");
        KeyStore keystore = KeyStore.getInstance("JKS");
        // TODO : set the ksPassword to the right value
        keystore.load(kst_is, ksPassword.toCharArray());
        /* Input stream containing the  CRLs' URL (on per line)*/
        InputStream crlConfIs = VerificationApplet.class.getResourceAsStream("/crl-list.conf");
        String nLine = null; // a line in the conf file
        URL url = null;
        InputStream crlIstream = null;
        X509CRL crl = null;
        BufferedReader bfReader = null;
        ArrayList certAndCrls = new ArrayList();
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        bfReader = new BufferedReader(new java.io.InputStreamReader(crlConfIs));
        while ((nLine = bfReader.readLine()) != null) {
            try {
                url = new URL(nLine);
                crlIstream = url.openStream();
                crl = (X509CRL) cf.generateCRL(crlIstream);
                certAndCrls.add(crl);
                if (crl.isRevoked(certificate)) {
                    throw new CertificateRevokedException("Certificate is revoked");
                }
                crlIstream.close();
            } catch (IOException e) {
                if (!invalidsCRL.contains(nLine)) {
                    throw new CRLNotFoundException(nLine, nLine);
                }
            }
        }
        bfReader.close();

        if (!selfSigned) {
            certAndCrls.add(certificate);

            CertStore certStore = CertStore.getInstance("Collection", new CollectionCertStoreParameters(certAndCrls), "SUN");
            java.security.cert.X509CertSelector certSelector = new java.security.cert.X509CertSelector();
            certSelector.setCertificate(certificate);

            PKIXBuilderParameters builderParams = new PKIXBuilderParameters(keystore, certSelector);
            // certpath building parameters (CAs, cert and CRLs)
            builderParams.addCertStore(certStore);
            builderParams.setRevocationEnabled(false);

            CertPathBuilder builder = CertPathBuilder.getInstance("PKIX", "SUN");
            // building a valid certpath
            PKIXCertPathBuilderResult result = (PKIXCertPathBuilderResult) builder.build(builderParams);
            //if the certpath is needed, if not delete the following line
            CertPath certPath = result.getCertPath();
        }

        return true;
    }

    /** This method is called from within the init() method to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        signValLabel = new javax.swing.JLabel();
        signatureLabel = new javax.swing.JLabel();
        validityLabel = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        dataTextField = new javax.swing.JTextField();
        dataLabel = new javax.swing.JLabel();
        chooseButton = new javax.swing.JButton();
        validValLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        signatureEditor = new javax.swing.JEditorPane();
        signatureEditor.setEditorKit(new javax.swing.text.html.HTMLEditorKit());

        signValLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        signatureLabel.setText("<html><b><center>Signature :</center></b></html>");
        signatureLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        signatureLabel.setName("signatureLabel"); // NOI18N

        validityLabel.setText("<html><b><center>Validité : </center></b></html>");
        validityLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        validityLabel.setName("validityLabel"); // NOI18N

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        dataTextField.setName("fileNameTextField"); // NOI18N

        dataLabel.setFont(new java.awt.Font("Verdana", 1, 15)); // NOI18N
        dataLabel.setText("<html><b><center>Fichier à vérifier :</center></b></html>");
        dataLabel.setName("fileLabel"); // NOI18N

        chooseButton.setText("choisir");

        validValLabel.setFont(new java.awt.Font("DejaVu Sans", 0, 18)); // NOI18N
        validValLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        validValLabel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jScrollPane1.setViewportView(signatureEditor);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(validValLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 596, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(dataLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dataTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 371, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(chooseButton)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dataLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chooseButton)
                    .addComponent(dataTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(validValLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

private void dataTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dataTextFieldActionPerformed
    // TODO add your handling code here:
}//GEN-LAST:event_dataTextFieldActionPerformed
    // Variables declaration - do not modify
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton chooseButton;
    private javax.swing.JLabel dataLabel;
    private javax.swing.JTextField dataTextField;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel signValLabel;
    private javax.swing.JEditorPane signatureEditor;
    private javax.swing.JLabel signatureLabel;
    private javax.swing.JLabel validValLabel;
    private javax.swing.JLabel validityLabel;
    // End of variables declaration//GEN-END:variables
}
